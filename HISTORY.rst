=======
History
=======

1.1.0 (2018-11-07)
------------------

Apps: this release adds hooks to the application bootstrap phase, which is a
small but important change.


1.0.0 (2018-06-12)
------------------

This is a big milestone for me. The library migrated to python 3. I've decided
to move along and postpone the node-related code to future releases. Core and
base things work, apps, including the transport ones, do as well.

Some package-level imports added for convenience.


0.2.8-2 (2016-05-12)
--------------------

* apps:
    - base: extend_modules class method added
    - runners: thread pool was not cleaning thead instance on normal stop

* ci:
    - handlers.smp: _validate method cleans internal message to prevent _handle
      from sending doubled error response
    - api: BaseAPI now checks for sys_name on init to prevent registration with
      it being None

* tests:
    - base: file methods added
    - apps.client: updated
    - e2e.client: updated


0.2.8-1 (2016-04-13)
--------------------

* apps:
    - handlers.signals: fixed ci reference and ``_base_validate`` method
    - base: ``_base_setup_post`` method added
    - cli: ci reference fixed
    - client: setup and cycle methods moved to those of the base

* ci:
    - api: autonomous mode support (init without mount)
    - modules: get_handler shortcut added


0.2.8 (2016-04-12)
------------------

* library general: parts of core.defaults related to messages, apps, configs
  etc moved to corresponding modules

* configs: source is now only loaded if there are required keys

* pools:
    - ``__contains__``, ``__getitem__`` and ``index`` methods now check name
      and id besides usual check

* timers: support for Paranoid Pirate strategy for both client and worker side
  instant connections and simple timers

* ci.api:
    - moved to module base
    - timers used to maintain instant connection (PPP reconnect strategy)

* plugins: base revised

* apps.base:
    - base-level setup, clean and flush separated from API methods
    - run condition unified for all sub-classes
    - infrastructure support for mounting modules added
    - bases separated from threading module to support multiprocessing
      interface as well

* apps.broker:
    - base now uses base-level methods
    - control interface and logger and transactions api moved to modules

* apps.worker:
    - base now uses base-level methods
    - control interface and logger api moved to modules

* apps.client:
    - base now uses base-level methods
    - CI and smp router moved to modules

* nodes:
    - api module separated for transactions and logger
    - rest node migrated to new API paradigm

* tests:
    - shortcut names added
    - transport test base added
    - mock node class for remote connection testing added


0.2.7-1 (2016-03-23)
--------------------

* apps.handlers.signals: help handler now prints groups as well


0.2.7 (2016-03-20)
------------------

* adapters:
    - base revised
    - file base added

* files: containers and modules removed, adapters added

* messages: all message modules now support 'errors' method instead of
  'internal.errors'

* plugins: base revised, filters in progress

* apps: worker and generic apps self.messages->self.core.messages in
  logger_fe_handle

* tests.core: core test now initializes and cleans logger automatically


0.2.6 (2016-03-14)
------------------

* all: modules now have shortcuts, e.g. GenericCoreConfigModule can be
  imported as BaseConfig

* all: class naming changed:
  - BaseUnboundModule -> BaseModule ("unbound" is inappropriate for message bus
    and core connection)
  - GenericUnboundModule -> GenericModule
  - GenericSystemModule -> GenericInfraModule
  - GenericInfrastructureUnit -> GenericInfraUnit
  - AbstractMessageModule -> MessageModule ("abstract" is inappropriate for
    message bus and core connection)

* docs: complete (almost) structure for sphynx autodoc

* core.units: resource and plugin flags for all units


0.2.5-2 (2016-02-26)
--------------------

* configs.loaders: comments in json files supported

* apps.base: DEBUG settings switches logger level do DEBUG

* nodes.rest.handlers: Content-Type only set to JSON for JSON response


0.2.5-1 (2016-02-17)
--------------------

* apps.handlers.smp: reponse code representation fixed

* protocols.smp.ci.handlers: fixed error message doubling, handle method now
  (bus_mirror parameter for error feedback methods), handle method support for
  super-class try-except paradigm

* routers.handlers: handlers now support aliases

* nodes.logger:
    - some handlers didn't return True on extra validation
    - main app now imports ALL_HANDLERS

* nodes.rest.handlers: Content-Type only set to JSON for JSON response

0.2.5 (2016-02-16)
------------------

* ci.containers: __repr_name__ method added
* ci.handlers: __repr_name__ method added

* protocols.smp.api: override kwargs changed: _transaction_id

* routers:handlers: try-except and exc_info as error message

* apps.base: preparing to move to SMPTransportBase
* apps.broker: socket polling method updated, signal override kwargs: _client_id
* apps.client: some messages changed to trace level, signal override kwargs
  changed: _transaction_id
* apps.generic: some messages changed to trace level
* apps.worker:
    - logger_api now uses timeouts and request retries
    - sleep for socket polling interval for unbound worker

* nodes.transactions:
    - transaction delete handler
    - apps now import ALL_HANDLERS from ci module
    - http method and name fix
    - delete unit method and handler: single id support for deletion


0.2.4 (2016-02-12)
------------------

* apps.client:
    - EndpointClientApp: FINAL message is generated on request timeout
    - socket polling error due to wrong logic (REPLY_TIMEOUT * 1000)

* apps.runners: ThreadPool now prints a debug message on starting a thread

* nodes.rest:
    - tornado ioloop stop on shutdown
    - migrating to thread pool paradigm
    - run cycle fix
    - shutdown now triggers tornado IOLoop stop
    - handlers now get api dynamically from a pool object
    - instance_by_name method fix
    - enabling handler logging
    - initialize uses handler_name to initialize logger
    - logging ep client instance added
    - debug message on transaction enqueued


0.2.3-1 (2016-02-11)
--------------------

* apps.transactions: service thread and cleaner unit added


v0.2.3 (2016-02-10)
-------------------

* core.datatypes: DotDict and Journal shortcuts added

* configs.base: extend_defaults and extend_required_keys class methods added

* ci.interfaces: fixed errors when obtaining handler from mapping
* ci.parsers: fixed args method not using shlex splitting

* console: templates added

* pools.containers: added functionality needed for ThreadPool runner

* protocols.smp.defaults: added gateway timeout and service unavailable codes

* apps.core.modules.logger:
    - now uses color definitions from core.defaults
    - * added trace message level
* apps.handlers.signals: help signal handler added (almost working)
* apps: CLI app added, ThreadPool runner added
* apps.runners.ThreadPool: threads can now have different classes
* apps.worker: added UnboundWorkerApp for running background service threads

* pools.containers.WorkerInfo: name removed, init_args and init_kwargs added,
  ``class_`` added to support ThreadPool management

* nodes.logger:
    - added cleaner thread
    - extra_messages_count referenced before assignment
    - default service thread interval changed to 120 seconds
    - cleaner unit message level changed to info


0.2.2-9 (2015-12-17)
--------------------

* nodes.rest.handlers:
    - OPTIONS method added
    - options CORS headers fixed
* nodes.transactions.ci: http_method added


0.2.2-8 (2015-12-15)
--------------------

* nodes.rest.handlers:
    - CORS header moved to prepare
    - kwargs moved to prepare, renamed to query_kwargs, DELETE method added


0.2.2-7 (2015-12-10)
--------------------

* apps.client: control interface info message


0.2.2-6 (2015-12-09)
--------------------

* pools.containers: completed->finished, +unfinished

* apps.client:
    - cli arguments didn't trigger signal parser initialization
    - signal parser late init issue
    - cycle condition error due to changes in pools
    - one shot cycle condition fix


0.2.2-5 (2015-12-07)
--------------------

* core.units: all units now have flush method cleaning their internal message bus

* nodes.rest: working version
* nodes.rest.apps:
    - config missed source setting
    - shutdown and clean mechanisms


0.2.2-4 (2015-12-04)
--------------------

* core.datatypes.DotDict fixed to work with non-string keys

* pools.containers: now support job statuses

* ci.interfaces now uses Queue module

* nodes.logger.ci: fixed double FINAL message
* nodes.transactions.ci: fixed double FINAL message
* nodes.rest: wokring


0.2.2-3 (2015-11-16)
--------------------

* messages.mixins: minor fixes

* ci.handlers: get_first_bus_error method added

* apps.broker: minor fixes
* apps.generic: minor fixes
* apps.worker: minor fixes

* nodes: logger and transactions migrated to get_first_bus_error


0.2.2-2 (2015-11-15)
--------------------

* ci.handlers:
    - handling for None values
    - kwargs validation: None value handling

* nodes.broker:
    - basic transaction pool connection
    - minor tab-related fixes
    - logger and trx error logging fix
* nodes.transactions.units: flush method added
* nodes.transactions.ci: read handler name fixed


0.2.2-1 (2015-11-14)
--------------------

* apps.base: flush for CI changed
* apps.broker: now purges inactive workers
* apps.broker: recovery mechanism fix
* apps.pools: WorkerPool updated, some methods moved to WorkerInfo

* nodes.logger.units: fixed wrong deletion order


0.2.2-1 (2015-11-10)
--------------------

* ci.handlers: validation: repr_string fix

* smp.ci.handlers: client response on error
* smp.ci.handlers: self._ci_fe returned None fixed
* smp.ci.handlers: operation_uuid -> transaction_id

* nodes.broker.generic: minor changes
* nodes.logger.apps: minor changes
* nodes.logger.units: flush method added


0.2.2 (2015-11-07)
------------------

* operation_uuid -> transaction_id

* core.repr: support for (abstract) class and instance methods
* core.containers: refactored, transport and json methods separated
* core.containers.fields: "low level" transport repr validation
* core.runtime: changed reset key behaviour

* routers.units: extracted common base for interfaces and content handlers
  (Router, QueueRouter, FlatMappingMixin, GroupedMappingMixin)
* routers.handlers: BaseHandler, BaseFlatMappingHandler,
  BaseGroupMappingHandler correspondingly

* ci.handlers: automated parameter validation and collection,now inherits from
  GroupedMappingMixin and QueueRouter

* apps.broker: renamed from master
* apps.client: basic support for command line options and autonomous mode,
  transaction pool handling separated
* apps.pools: TransactionPool: renamed from OperationPool

* nodes.broker.generic: added
* nodes.logger: first working version
* nodes.transactions: first working version


v0.2.1 (2015-04-14)
-------------------

* apps.master: worker recovery on heartbeat for non-present in worker pool
* apps.master: message levels for worker sign-in/sign-out debug->info
* concerned tests fixed


v0.2.0 (2015-04-11)
-------------------

* apps.core.modules.logger: color stream handler added
* apps.core.units: ThreadCore renamed to Core
* apps.core.handlers: standard content handlers added
* apps.base: setup_hook, reset, logging using logging.config, some docstrings
* apps.client: added
* apps.pools: operations pool for client tracking
* apps.master: LC/RESET, minor bugfixes
* apps.worker: error response on signal missing
* ci.handlers: now use signal_validate return
* ci.parsers: added
* egg-info: added


0.1.9-3 (2015-03-28)
--------------------

* some restructuring, clean-up
* adding egg info and uncommitted modules
* apps.master: minor changes
* ci.handlers: minor changes


0.1.9-2 (2015-03-28)
--------------------

* some restructuring, clean-up
* apps.master: minor changes
* ci.handlers: minor changes


0.1.9-1 (2015-01-26)
--------------------

* messages: debug and critical levels added as well as corresponding functions


0.1.9 (2015-01-26)
------------------

* core.units: behaviour for mounting shared units fixed
* ci: control interface added
* configs: revised
