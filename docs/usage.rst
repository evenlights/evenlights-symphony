=====
Usage
=====

Let's imagine we have a Django + DRF backend and create a basic console API
client app for it.


Define an application and a a configuration for it:

.. code-block:: python
   :caption: app.py

    # -*- coding: utf-8 -*-
    import os

    from symphony.configs import json_file_loader
    from symphony.apps import CLIAppConfig, HelpSignalHandler
    from symphony.apps.cli import CLIApp

    from components import APIClient, AccountService
    from commands import Upload

    NAME = os.path.basename(__file__).split('.')[0]


    class Config(CLIAppConfig):
        loader = json_file_loader
        source = '{}/.config/your-project/{}.json'.format(
            os.path.expanduser('~'), NAME
        )
        required_keys = CLIAppConfig.extend_required_keys([
            'API_URL', 'API_TOKEN',
        ])
        defaults = CLIAppConfig.extend_defaults({
            'ID': NAME,
            'TEMPLATE_DIR': 'templates',

            'LOGGING': {
                'version': 1,

                'formatters': {
                    'color': {
                        '()': 'symphony.apps.core.logger.ColorSTDOUTFormatter',
                        'format': '%(levelname)-8s: %(message)s',
                    },
                },

                'handlers': {
                    'stdout_handler': {
                        'class': 'logging.StreamHandler',
                        'formatter': 'color'
                    },
                },

                'loggers': {
                    NAME: {
                        'handlers': ['stdout_handler'],
                        'level': 'INFO'
                    },
                },
            },
        })


        class App(CLIApp):
            """
            """
            class Infrastructure:
                components = CLIApp.extend_components([
                    APIClient, AccountService
                ])

            name = NAME
            config = Config
            signal_handlers = [HelpSignalHandler, Upload]

            def bootstrap(self):
                setattr(
                    self.config, 'ROOT_DIR', os.path.dirname(os.path.abspath(__file__))
                )
                super(App, self).bootstrap()


        if __name__ == '__main__':
            App().run()



Define a ``coreapi``-compatible API client:

.. code-block:: python
   :caption: components/api.py

    from symphony.api import (
        GenericClient, GenericCRUDEndpoint, single_object_response
    )


    class Users(GenericCRUDEndpoint):
        BASE_KEYS = ('users', 'users')


    class UserActions(GenericCRUDEndpoint):
        BASE_KEYS = ('users',)

        @single_object_response
        def current_user(self):
            return self.action(['current-user', 'read'])


    class ArchiveStorages(GenericCRUDEndpoint):
        BASE_KEYS = ('archives', 'storages')


    class ArchiveFiles(GenericCRUDEndpoint):
        BASE_KEYS = ('archives', 'files')


    class ArchivePath(GenericCRUDEndpoint):
        BASE_KEYS = ('archives', 'path')


    class APIClient(GenericClient):

        def __init__(self, **kwargs):
            super().__init__(**kwargs)

            self.users = Users(self)
            self.user_actions = UserActions(self, pagination=self.pagination)
            self.archive_storages = ArchiveStorages(self, pagination=self.pagination)
            self.archive_files = ArchiveFiles(self, pagination=self.pagination)
            self.archive_path = ArchivePath(self, pagination=self.pagination)


Define a service that will use your API to fetch data:

.. code-block:: python
   :caption: components/archives.py

    from symphony.core.infra import GenericComponent

    from components import APIClient


    class ArchiveService(GenericComponent):
        name = 'archives'
        shared = True

        def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self._storages = []

        def activate(self):
            super().activate()
            api = self.core.shared.get('api')
            if api.messages.errors:
                return

            self.messages.info('activating archive service')
            self._storages = api.archive_storages.list()

        def get_storage(self, storage_name):
            storage = list(
                filter(lambda s: s['name'] == storage_name, self._storages)
            )

            if len(storage):
                return storage[0]

            raise LookupError(storage_name)


Define a command handler:

.. code-block:: python
   :caption: commands/upload.py

    class Upload(SignalHandler):
        name = 'upload'
        args = [StringField, StringField]
        kwargs = {
        }

        def prepare(self):
            super().prepare()
            archives_: ArchiveService = self.core.shared.get('archives')

            if self.messages.errors:
                return

            self.runtime['source_path'] = os.path.abspath(self.runtime['args'][0])
            if not os.path.exists(self.runtime['source_path']):
                self.messages.error(
                    'source path does not exist: {source_path}'.format(
                        **self.runtime
                    )
                )
                return

            self.runtime['target_path'] = self.runtime['args'][1]

            do_cool_stuff()

        def handle(self):
            api_: APIClient = self.core.shared.get('api')
            self.messages.info(
                'uploading {source_path} to {target_path}'.format(**self.runtime)
            )
            do_cool_stuff()

            self.info_response('done')
