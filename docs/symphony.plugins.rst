symphony.plugins package
========================

Submodules
----------

symphony.plugins.base module
----------------------------

.. automodule:: symphony.plugins.base
    :members:
    :undoc-members:
    :show-inheritance:

symphony.plugins.chainers module
--------------------------------

.. automodule:: symphony.plugins.chainers
    :members:
    :undoc-members:
    :show-inheritance:

symphony.plugins.filters module
-------------------------------

.. automodule:: symphony.plugins.filters
    :members:
    :undoc-members:
    :show-inheritance:

symphony.plugins.presets module
-------------------------------

.. automodule:: symphony.plugins.presets
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: symphony.plugins
    :members:
    :undoc-members:
    :show-inheritance:
