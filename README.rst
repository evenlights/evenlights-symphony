.. highlight:: shell

========
Symphony
========

Symphony is an Open Source project written in Python with ZeroMQ as transport
layer. The idea behind the project is to create a framework for distributed
systems with no central broker. It is largely inspired by Pieter Hintjens'
brilliant ideas about how a distributed infrastructure should look and behave
like. Though, Symphony can be easily used for building general-purpose apps,
such as command line tools -- all within the same application design structure.

The project is currently a one man show in alpha stage. The core is already
relatively stable, though, there is a lot of work to be done yet and even more
things to improve.

Free software: BSD license


Local development
-----------------

The recommended way is to have a virtualenv. Note that the project ``Makefile``
is be default set to use the virtualenv. You can disable this behaviour by
setting the ``ENV`` variable in the ``Makefile`` to something like
``ENV := NOENV=true``. Note that project conventions suggest that you keep
your venv inside the VCS root.

Clone the repo:

.. code-block:: console

    $ git clone git@bitbucket.org:evenlights/evenlights-symphony.git


Create the environment:

.. code-block:: console
    $ cd evenlights-symphony
    $ virtualenv -p python3 venv


Install development packages:

.. code-block:: console

    $ source ./venv/bin/activate
    $ pip install -r requirements_dev.txt

The ``release`` make target requires that you have the aws-cli_ package
installed and configured.


Release flow
------------

* create a release branch, desirably using a gitflow plugin

* bump the version:

.. code-block::: console

  $ bumpversion {major/minor/patch}

* update ``HISTORY.rst`` describing changes that this release introduces

* commit, publish the branch

* create a pull request

* run the ``release`` make target

* check things once again, at this point you still can upload the release
  without version bumping

* finish the release using gitflow, push branches


Features
--------

* classes for client, broker and worker apps as well as general purpose apps

* control interfaces which can assume signals from abstract sources, be it
  command line user input or socket, and pass them to corresponding handlers

* ZMQ-to-HTTP bridge with a generalized scheme for HTTP RPC calls

* clients, brokers and workers implement reliability patterns including two-way
  heartbeats, reconnects and smart remote


Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
.. _aws-cli: https://docs.aws.amazon.com/cli/latest/userguide/installing.html
