import pytz
from pytz import utc

from datetime import datetime


class TimezoneTool:
    def __init__(self, timezone=pytz.utc):
        self.timezone = timezone

    @classmethod
    def from_string(cls, string):
        tz = pytz.timezone(string)
        return cls(timezone=tz)

    def naive_to_local(self, naive_date: datetime):
        return self.timezone.localize(naive_date)

    def naive_to_utc(self, date: datetime):
        """
        When a client makes a request without a timezone they mean their
        organization timezone. So we have to add this information to the
        datetime object.
        :param date: datetime object without a timezone
        :return: UTC datetime object
        """
        if date:
            if not date.tzinfo:
                local_date = self.naive_to_local(date)
                return local_date.astimezone(utc)
            else:
                return date.astimezone(utc)

    def utc_to_local(self, utc_date: datetime):
        return utc_date.astimezone(self.timezone)

    def local_to_utc(self, local_date: datetime):
        return local_date.astimezone(utc)

    def trancate_to_date(self, date):
        pass
