# -*- coding: utf-8 -*-
import simplejson as json

from symphony.core.exceptions import ImproperlyConfigured
from symphony.core.containers import GenericObjectContainer, GenericListContainer
from symphony.core.modules import GenericInfraModule


class PluginChainElement(GenericObjectContainer):

    class DataModel:
        structure = {
            'name': None,
            'presets': None,
            'custom': None,
        }

    def __init__(self, name=None, presets=None, custom=None, **kwargs):
        self.name = name
        self.presets = presets
        self.custom = custom
        super(PluginChainElement, self).__init__(**kwargs)


class PluginChain(GenericListContainer):

    class DataModel:
        root_key = 'plugin_chain'
        member_class = PluginChainElement


class BasePluginChainer(GenericInfraModule):
    """ Что нужно:
    валидация: перед выполнением цепочки должен убеждаться, что плагины зарегистрированны в
    системе (core.shared.plugins)

    выполнение собственно пакетной обработки
    """
    sys_name = 'plugin_chainer'

    def __init__(self, **kwargs):
        super(BasePluginChainer, self).__init__(**kwargs)
        self._chain = None

    def _load_chain_validate(self, json_source):
        """ проверяем, можно ли восстановить цепочку, зарегистрированы ли плагины
        """
        try:
            test_chain = PluginChain.from_json(json_source)

        except json.JSONDecodeError as e:
            self._mount_point.messages.error('invalid json format: %s' % str(e))
            return
        except ImproperlyConfigured as e:
            self._mount_point.messages.error('invalid plugin chain format: %s' % str(e))
            return
        except ValueError as e:
            self._mount_point.messages.error('invalid plugin chain format: %s' % str(e))
            return
        except KeyError as e:
            self._mount_point.messages.error('invalid plugin chain format: %s' % str(e))
            return

        for plugin in test_chain:
            if not self.infra.core.shared.plugins.by_name(plugin.name):
                self._mount_point.messages.error('plugin unavailable: %s' % plugin.name)

    def load_chain(self, json_source):
        self._load_chain_validate(json_source)
        self._chain = PluginChain.from_json(json_source)

