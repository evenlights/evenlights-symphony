# -*- coding: utf-8 -*-
from symphony.core.containers import BaseObjectContainer, BaseListContainer
from symphony.core.modules import GenericInfraModule


class BasePresetContainer(BaseObjectContainer):

    class DataModel:
        structure = {
            'name': None,
            'data': None
        }

    def __init__(self, name=None, data=None):
        self.name = name
        self.data = data
        super(BasePresetContainer, self).__init__()


class BasePresetManager(BaseListContainer):

    class DataModel:
        root_key = 'presets'
        member_class = BasePresetContainer

    def __repr__(self):
        return '[%s]' % ','.join(['"%s"' % p.name for p in self[:]])

    def by_name(self, name):
        for preset_nr, preset in enumerate(self):
            if preset.name == name:
                return self[preset_nr]

    def load_from_json(self, json):
        presets = BasePresetManager.from_json(json)
        self.extend(presets[:])


class PresetManagerModule(GenericInfraModule, BasePresetManager):
    sys_name = 'presets'


Presets = PresetManagerModule
