# -*- coding: utf-8 -*-
from symphony.plugins.base import BasePlugin
from symphony.plugins.presets import (
    BasePresetContainer as PresetContainer,
    BasePresetManager as PresetManager
)

MULTIBAND_RUNTIME_DEFAULTS = {
    'custom': '',
    'presets': ['default', ],
    'target_ranges': [],
    'chain': PresetManager(),
}

ONEPASS_RUNTIME_DEFAULTS = {
    'custom': '',
    'presets': ['default', ],
    'chain': PresetManager(),
}


class BaseOnePassFilter(BasePlugin):
    """ Общая база для фильтров, выполняющих обработку, исходя из какого-то одного критерия
    (т.е., не требующая суммирования целевых диапазонов).
    """
    def __init__(self, *args, **kwargs):
        super(BaseOnePassFilter, self).__init__(ONEPASS_RUNTIME_DEFAULTS, *args, **kwargs)

    def prepare_extra(self):
        self.target = self.source


class BaseMultiBandFilter(BasePlugin):
    """ База многополосного ильтра диапазонов собирает целевые диапазоны в установку рантайма.
    Затем база проверяет все значения потока на вхождение в целевые диапазоны и передает
    функции :func:`on_condition_hit` результат порверки и само значение. Дальнейшай обработка
    и реакция на условие зависит от конкретного фильтра.
    """

    def __init__(self, *args, **kwargs):
        super(BaseMultiBandFilter, self).__init__(MULTIBAND_RUNTIME_DEFAULTS, *args, **kwargs)

    def prepare_extra(self):
        self.target = self.source
        self.runtime.reset('target_ranges')

        # добавляем к целевым диапазонам все диапазоны пресетов
        for preset in self.runtime.chain[:]:
            self.runtime.target_ranges += preset.data

    def process(self):
        """ При обработке исходных данных фильтр суммирует все необходимые диапазоны,
        включая переданные в параметре ``params.custom``, и поочередно проверяет каждый
        символ на вхождение в один из них. Результаты передаются функции
        :func:`on_condition_hit`.
        """

        for nr, value in enumerate(self.target):
            factor = False

            # ищем хотя бы одно вхождение в целевые диапазоны
            for rng in self.runtime.target_ranges:
                if self.get_factor(value, rng):
                    factor = True

            # обработка результата
            self.handle(factor, value)

        return self.target
