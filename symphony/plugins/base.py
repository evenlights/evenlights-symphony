# -*- coding: utf-8 -*-
from abc import abstractmethod

from symphony.core.units import GenericInfraUnit
from symphony.core.runtime import RuntimeStorage
from symphony.messages.components import Messages
from symphony.plugins.presets import Presets
from symphony.plugins.presets import BasePresetManager as PresetManager


class BasePlugin(GenericInfraUnit):
    """ Плагин это :ref:`блок <core.units>` обработки данных при помощи :ref:`пресетов
    <plugins.presets>` -- заданных параметров обработки.

    Общая логика работы плагина такова:

    1. Настройка: плагин загружает пресеты в и выполняет прочие подготовительные
       действия, например, суммирует целевые диапазоны.

       Плагин обычно имеет ряд "фабричных" пресетов, кторые он заргужает при
       инициализации. Помимо этого, плагину может быть передан список дополнительных
       настроек. И те и другие могут участвовать в формировании ``цепочки обработки``

    2. Валидация: плагин проверяет наличие всех запрошенных пресетов в цепочке обработки,
       а также выполняет дополнительную валидацию

    3. Обработка: исходя их текущих пресетов для каждого блока информации потока плагин
       определяет, соответствует ли он условиям или нет. Этот факт, а также сами данные,
       плагин передает методу обработки данных :func:`_handle`:.

       В зависимости от модифицации плагин может полностью удалять данные, не
       соответствующие определенным критериям, зменять (band-stop) или, наоборот,
       оставлять без зименений (band-pass).

    Поле ``source`` перед запуском содержать ссылку на исходные данные, поле
    ``target`` -- ссылку результат. ``Ссылкой`` может быть как просто переменная,
    так и файловый обработчик.

    Обработка данных может осуществляться поточно либо охватывать входные данные
    полностью. """

    class Infrastructure:
        modules = [Messages, Presets]

    def __init__(self, runtime_defaults=None, *args, **kwargs):
        super(BasePlugin, self).__init__(*args, **kwargs)

        # зугружаем пресеты, если плагин их предоставляет
        if self.load_default_presets():
            self.presets.load_from_json(self.load_default_presets())

        self.infra.core.shared.plugins.register(self)

        # runtime
        self.source = None
        self.target = None

        if runtime_defaults:
            self.runtime = RuntimeStorage(runtime_defaults)
        else:
            self.runtime = RuntimeStorage()

    # Пресеты

    def presets_add_default(self):
        """ Метод загружает в цепочку обработки ``runtime.chain`` мета-пресет
        ``default``. """

        if self.presets.by_name('default'):
            self.messages.debug('loading default meta-preset')

            for name in self.presets.by_name('default').data:
                self.runtime.chain.append(self.presets.by_name(name))

    def presets_add_custom(self):
        """ Метод загружает в цепочку обработки ``runtime.chain`` кастомные пресеты,
        если они заданы. """

        if self.runtime.custom:
            custom_presets = PresetManager.from_json(self.runtime.custom)
            self.messages.debug('loading custom presets: %s' % repr(custom_presets))

            for preset in custom_presets[:]:
                self.runtime.chain.append(preset)

    # API

    def prepare(self):
        """ Если передан список пресетов, метод загружает их в цепочку обработки. Если
        пресеты не заданы, фильтр использует настройки из мета-пресета ``default``.

        Кастомные пресеты, если таковые присутствуют, добавляются в цепочку обработки
        в любом случае.
        """
        self.messages.debug('preparing')
        self.runtime.reset('chain')

        # список пресетов задан в явном виде
        if self.runtime.presets:
            self.messages.debug('using runtime preset list: %s' % self.runtime.presets)

            for name in self.runtime.presets:

                if self.presets.by_name(name):

                    if name == 'default':
                        self.presets_add_default()
                    else:
                        self.runtime.chain.append(self.presets.by_name(name))

            if self.runtime.custom:
                self.presets_add_custom()

        # список пресетов не задан, настройки по умолчанию
        else:
            self.presets_add_default()

            if self.runtime.custom:
                self.presets_add_custom()

        self.prepare_extra()

    def validate(self):
        self.messages.debug('validating')

        for name in self.runtime.presets:
            # не мета-пресет
            if name != 'default':
                if not self.runtime.chain.by_name(name):
                    self.messages.error(
                        'preset "%s" unavailable in runtime chain: %s' %
                        (name, repr(self.runtime.chain))
                    )
                    return

        self.validate_extra()

    def run(self):
        """ Логика процедуры выглядит следующим образом: производится валидация,
        описанная в :func:`validate_extra`, если ошибок не возникло, производится
        установка начальных параметров, описанная в :func:`api_initial_conditions`
        и затем обработка, описанная в :func:`process`
        """
        self.messages.debug('running')
        self.prepare()

        if not self.messages.errors:
            self.validate()

        if not self.messages.errors:
            return self.process()

    def flush(self):
        super(BasePlugin, self).flush()
        self.runtime.flush()
        self.flush_extra()
        self.source = None
        self.target = None

    # Абстрактные методы

    def load_default_presets(self):
        """ Метод подразумевает загрузку "фабричных" пресетов из внешнего источника
        (файла, переменной) и должен возвращать JSON-представление пресетов
        <plugins.presets>. """
        self.messages.debug('loading default presets')

    def prepare_extra(self):
        """ Установка начальных условий, в качестве примера, может включать суммирование
        целевых диапазонов. """
        pass

    def validate_extra(self):
        """ Здесь может быть описана кастомная валидация. Метод вызывается после всех
        остальных валидаций плагина. """
        pass

    @abstractmethod
    def process(self):
        """ Метод описывает логику обработки значений и устанавливает связь между
        результатами выполнения методов :func:`get_factor` и :func:`_handle`.

        Например, если речь идет об обработке занчений, выходящих за пределы заданных
        диапазонов, то достаточно вхождения значения хотя бы в один из них, чтобы
        значение подверглось обработке. Таким образом, условие :func:`get_factor`
        должно быть проверено многократно -- для каждого из диапазонов.

        В зависимости от модификации плагин может обрабатывать данные поточно, либо иметь
        доступ ко всему исходному массиву.
        """
        self.messages.debug('processing')

    @abstractmethod
    def get_factor(self, *args, **kwargs):
        """ Метод устанавливает соответствие между текущими настройками плагина и
        обрабатываемым значением. Это соответствие может выражаться по-разному: как
        ``True/False`` в значении "обрабатывать/не обрабатывать" или как целочисленный
        или дробный коэффициент в значении "степени обработки".
        """

    @abstractmethod
    def handle(self, factor, value):
        """ В зависимости от модифицации плагин может полностью удалять данные, не
        соответствующие определенным критериям, зменять (band-stop) или, наоборот,
        оставлять без зименений (band-pass).

        Функция описывает обработку переданного значения, когда плагин решает, что такая
        обработка необходимо. В качестве входных параметров обычно передаются значения, которые
        необходимо преобразовать.
        """

    def flush_extra(self):
        """ """
        pass
