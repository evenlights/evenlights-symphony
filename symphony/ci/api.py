# -*- coding: utf-8 -*-
import uuid

from symphony.core.exceptions import ImproperlyConfigured
from symphony.core.infra.generic import InfraComponent
from symphony.messages.components import Messages
from symphony.ci.containers import GenericSignal
from symphony.timers.containers import GenericTimer, PPPTimer
from symphony.protocols.smp.adapters import SMPSocketAdapter

from symphony.protocols.smp.defaults import (
    SMP_CONTENT_SIGNAL, SMP_TYPE_FINAL, SMP_TYPE_PARTIAL
)


class BaseAPI(InfraComponent):

    @staticmethod
    def transaction_random_id():
        return uuid.uuid1().__str__()

    @staticmethod
    def transaction_repr(func_name, kwargs):
        trx_id = kwargs['_transaction_id'] if '_transaction_id' in kwargs else 'auto_id'
        return '%s <%s>' % (func_name, trx_id)

    def transaction_get(self, kwargs):
        return kwargs.pop('_transaction_id') if '_transaction_id' in kwargs  \
            else self.transaction_random_id()

    # Methods
    def sync(self, *args, **kwargs):
        """ Метод синхронного запроса """
        raise NotImplementedError

    def async(self, *args, **kwargs):
        """ Метод асинхронного запроса """
        raise NotImplementedError

    def single(self, *args, **kwargs):
        """ Метод, предполагающий ответ из одного сообщения. """
        raise NotImplementedError

    def sync_single(self, *args, **kwargs):
        """ Метод, предполагающий ответ из одного сообщения. """
        raise NotImplementedError

    def async_single(self, *args, **kwargs):
        """ Метод, предполагающий ответ из одного сообщения. """
        raise NotImplementedError

    def stream(self, *args, **kwargs):
        """ Метод, предполагающий поточный ответ. """
        raise NotImplementedError

    def sync_stream(self, *args, **kwargs):
        """ Метод, предполагающий поточный ответ. """
        raise NotImplementedError

    def async_stream(self, *args, **kwargs):
        """ Метод, предполагающий поточный ответ. """
        raise NotImplementedError


class RemoteAPI(BaseAPI):
    """ Удаленный API-интерфейс служит для вызова функций удаленного узла и
    обработки ошибок. По большей части используется для управления *постоянными*
    соединениями, такими как узлы логгера, транзакций итп.

    Поскольку узлы системы могут оказываться недоступными на неопределенное время,
    API после заданного числа неудачных попыток будет деактивирован и возобновит
    попытки через некоторое время. При каждой следющей неудачно попытке интервал
    повтрного подключения увеличивается до заданного предела. При следующем удачном
    запросе функционирование модуля будет восстановлено. Поле ``is_active``
    отображает текущее состояние интерфейса.

    :param name: имя узла служит для загрузки параметров конфигурации и названия
        компонентов API, обязательный параметр.
    :param sys_name: используется в качестве метки монтирования, обязательный параметр

    Модуль предназначен для монтирования к объекту приложения и требует, чтобы у него
    присутствовали поля ``sys_id``, ``context`` и ``poller``, ``socket_poll`` и
    ``_socket_poll`` -- использутся для обеспечения транспорта сообщений..

    Также, удаленный API использует следующие значения :ref:`конфигурации ядра
    <apps.core.modules.config>`:

    :param SOCKET_POLLING_INTERVAL: частота опроса сокетов, обязательный параметр
    :param <NAME>_SOCKET_CONF: путь к файлу конфигурации сокета, обязательный параметр
    :param <NAME>_REPLY_TIMEOUT: таймаут ответа в секундах, значение по умолчанию 1
    :param <NAME>_REQUEST_RETRIES: количество попыток до деактивации API, значение
        по умолчанию 5
    :param <NAME>__RECONNECT: максимальный интервал повторной активации API в секундах,
        значение по умолчанию 32
    """
    def __init__(self, socket=None, poller=None, *args, **kwargs):
        super(RemoteAPI, self).__init__(*args, **kwargs)

        class _ReconnectTimer(GenericTimer):

            def on_timeout(self):
                self.mount_point.is_active = True

        class _PPPTimer(PPPTimer):

            def on_zero_liveness(self):
                """ При очередном достижении нулевой живучести таймер ждет заданный
                интервал и повтрно подключает сокеты интерфейса управления и жизенного
                цикла. """
                self.mount_point.messages.warning(
                    'remote %s api inaccessible, reconnect in %d seconds' %
                    (self.mount_point.name, self.retry_in))
                self.mount_point.is_active = False

        self._PPPTimer = _PPPTimer
        self.is_active = True

        # transport
        self.socket = socket
        self.poller = poller
        self._socket_poll = dict()

        # timers
        self.ppp_timer = _PPPTimer(mount_point=self)
        self.reconnect_timer = _ReconnectTimer(mount_point=self)

    def config_get(self, key):
        config_ = self.infra.core.config
        try:
            return getattr(config_, '%s_%s' % (self.name.upper(), key.upper()))
        except AttributeError:
            return None
        except KeyError:
            return None

    # Module
    def mount(self, mount_point):
        super(RemoteAPI, self).mount(mount_point=mount_point)

        if not self.config_get('SOCKET_CONF'):
            setattr(self._mount_point, self.sys_name, None)
            self._mount_point = None
            self._mount_state = False
            return

        self.messages.debug('initializing remote %s api' % self.name)
        core_ = self.infra.core
        mp_ = self._mount_point

        self.messages.debug('initializing transport')
        from zmq import Poller
        self.poller = Poller()
        self.socket = SMPSocketAdapter(
            sys_name='%s_socket' % self.name,
            core=core_,
            config_source=self.config_get('SOCKET_CONF'),
            context=mp_.context,
            poller=self.poller,
            identity=mp_.sys_id,
        )
        self.socket.attach()

        # todo: https://bitbucket.org/evenlights/symphony/issues/5/ciapi-reply_timeout-request_retries
        # todo: вынести в defaults
        self.messages.debug('initializing PPP timer')
        self.ppp_timer = self._PPPTimer(
            mount_point=self,
            seconds=self.config_get('REPLY_TIMEOUT') or 1,
            liveness=self.config_get('REQUEST_RETRIES') or 5,
            retry_init=1,
            retry_max=self.config_get('RECONNECT') or 32,
        )

    def umount(self):
        self.messages.debug('unmounting remote %s api' % self.name)
        self.socket.detach()
        super(RemoteAPI, self).umount()

    # API
    def _send(self, func_name, *args, **kwargs):
        transaction_repr = self.transaction_repr(func_name, kwargs)
        transaction_id = self.transaction_get(kwargs)
        group, name = self._get_name_group(func_name)

        self.messages.debug('remote call: %s' % transaction_repr)
        self.socket.client_request(
            transaction_id=transaction_id,
            content_type=SMP_CONTENT_SIGNAL,
            content=GenericSignal(
                group=group, name=name, args=args, kwargs=kwargs
            ).repr.json
        )

    def sync(self, func_name, *args, **kwargs):
        partial_msg = []
        final_msg = None

        # API будет пропускать все запросы, если находится в неактивном состоянии,
        # а интервал повторного подключения еще не истек
        if not self.is_active:
            self.reconnect_timer.refresh()
            if not self.is_active:
                return partial_msg, final_msg

        if '_transaction_id' not in kwargs:
            kwargs['_transaction_id'] = self.transaction_random_id()

        transaction_repr = self.transaction_repr(func_name, kwargs)

        self.ppp_timer.reset_counter()
        self._send(func_name, *args, **kwargs)

        self.messages.debug('waiting for reply for: %s' % transaction_repr)

        while not final_msg and self.is_active:
            self.poll_sockets()

            if self.poll_in():
                msg = self.socket.client_recv()

                if msg.message_type == SMP_TYPE_PARTIAL:
                    partial_msg.append(msg)
                else:
                    final_msg = msg

            # refresh
            if self.ppp_timer.is_timed_out:
                self.messages.warning('transaction timed out, resending: %s' % transaction_repr)
                self.socket.reattach()
                self._send(func_name, *args, **kwargs)
                partial_msg = []
                final_msg = None
                self.ppp_timer.timeout_handle()

        if final_msg:
            # в случае успешной транзакции ppp_timer сбрасывается, и API возвращается
            # в состояние нормальной работы
            self.ppp_timer.reset()

        else:
            self.messages.error('remote call failed due to gateway timeout: %s' % transaction_repr)
            self.reconnect_timer.set_delta(seconds=self.ppp_timer.retry_in)
            self.reconnect_timer.reset()

        return partial_msg, final_msg

    def sync_single(self, func_name, *args, **kwargs):
        partial, final = self.sync(func_name, *args, **kwargs)
        return final

    def sync_stream(self, func_name, *args, **kwargs):
        return self.sync(func_name, *args, **kwargs)

    def single(self, func_name, *args, **kwargs):
        return self.sync_single(func_name, *args, **kwargs)

    def stream(self, func_name, *args, **kwargs):
        return self.sync_stream(func_name, *args, **kwargs)

    def __call__(self, func_name, *args, **kwargs):
        return self.sync(func_name, *args, **kwargs)

    # Run Cycle
    def handle(self):
        self.reconnect_timer.refresh()
        if self.poll_in():
            self.messages.trace('handling %s api socket' % self.name)

            # сообщение протокола SMP/Client
            msg = self.socket.client_recv()

            # ошибка протокола
            if self.socket.messages.internal.errors:
                for msg in self.socket.messages.internal.errors:
                    self.messages.error(msg)
                return

            # выводим только ошибки
            if msg.code > 299:
                self.core.messages.error('%s' % msg.content)

    # Transport
    def poll_in(self):
        return self.socket._socket in self._socket_poll

    def poll_sockets(self):
        self._socket_poll = dict(
            self.poller.poll(self.infra.core.config.SOCKET_POLLING_INTERVAL)
        )


class LocalThreadAPI(BaseAPI):
    sys_name = 'api'
    name = 'local-thread'

    def __init__(self, *args, **kwargs):
        super(LocalThreadAPI, self).__init__(*args, **kwargs)
        from tornado.concurrent import Future
        self.future_class = Future
        self.queue = None

    # Module
    def mount(self, mount_point):
        super(LocalThreadAPI, self).mount(mount_point=mount_point)
        mp_ = self._mount_point

        self.messages.debug('initializing %s api' % self.name)
        self.queue = mp_.ci.interface.queue

    # API
    def _send(self, func_name, *args, **kwargs):
        transaction_repr = self.transaction_repr(func_name, kwargs)
        group, name = self._get_name_group(func_name)

        # обработка аргументов _transaction_id и _future выполнеяет endpoint-клиент
        self.messages.debug('enqueueing transaction: %s' % transaction_repr)
        self.queue.put(GenericSignal(group=group, name=name, args=args, kwargs=kwargs))

    def async(self, func_name, *args, **kwargs):
        if '_transaction_id' not in kwargs:
            kwargs['_transaction_id'] = self.transaction_random_id()

        future = self.future_class()
        self._send(func_name, _future=future, *args, **kwargs)
        return future

    def __call__(self, func_name, *args, **kwargs):
        return self.async(func_name, *args, **kwargs)
