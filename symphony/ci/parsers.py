# -*- coding: utf-8 -*-
import shlex

from symphony.core.infra.generic import GenericComponent
from symphony.ci.containers import GenericSignal


class StringSignalParser(GenericComponent):
    """
    Строковой парсер
    """
    name = 'signal_parser'
    convert_values = True
    mapping = None
    shared = True

    def __init__(self, **kwargs):
        convert_values = kwargs.pop('convert_values', None)
        if convert_values is not None:
            self.convert_values = convert_values

        super(StringSignalParser, self).__init__(**kwargs)

    def init(self, core=None, config=None, registry=None, mount_point=None,
             mapping=None, **kwargs):
        super(StringSignalParser, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )

        if mapping:
            self.mapping = mapping

        return self

    def check(self):
        super(StringSignalParser, self).check()
        if not self.mapping:
            self._raise(
                'trying to initialize with empty mapping',
                exception_class=ValueError
            )

    @staticmethod
    def split(string):
        return [s.strip() for s in shlex.split(string)]

    def pop_name(self, tokens):

        # пробуем найти значение группа__имя
        try:
            v = self.mapping['%s__%s' % (tokens[0], tokens[1])]
            return '%s__%s' % (tokens.pop(0), tokens.pop(0))
        except KeyError:
            pass
        except IndexError:
            pass

        # пробуем найти просто имя
        try:
            v = self.mapping[tokens[0]]
            return tokens.pop(0)
        except KeyError:
            self.messages.error('no matching handler found')

    def convert(self, value):
        """

        :param value:
        :return:
        """
        if self.convert_values:
            # boolean
            if value.lower() in ['false', 'no']:
                return False

            elif value.lower() in ['true', 'yes']:
                return True

            # numeric
            else:
                try:
                    return int(value)
                except ValueError:
                    try:
                        return float(value)
                    except ValueError:
                        pass

        return value

    def validate_arg_tokens(self, tokens):
        """

        :param tokens:
        :return:
        """
        arg_nrs = [nr for nr, arg in enumerate(tokens) if '=' not in arg]
        kwarg_nrs = [nr for nr, arg in enumerate(tokens) if '=' in arg]

        # последовательность аргументов
        if arg_nrs and kwarg_nrs:
            if arg_nrs[-1] > kwarg_nrs[0]:
                self.messages.error(
                    'positional arguments should always '
                    'precede keyword arguments'
                )
                return False

        return True

    def get_args(self, tokens):
        """ """
        return [self.convert(arg) for arg in tokens if '=' not in arg] or None

    def get_kwargs(self, tokens):
        """ """
        kwargs = {}
        for token in tokens:
            if '=' in token:
                name, value = token.split('=')
                kwargs[name] = self.convert(value)
        return kwargs or None

    # Interface
    def get_signal(self, signal_string):
        tokens = self.split(signal_string)

        # имя
        name = self.pop_name(tokens)
        if not name:
            return

        if not self.validate_arg_tokens(tokens):
            return

        args = self.get_args(tokens)
        kwargs = self.get_kwargs(tokens)

        return GenericSignal(name=name, args=args, kwargs=kwargs)
