# -*- coding: utf-8 -*-
from symphony.routers.handlers import BaseHandler


class SignalHandler(BaseHandler):
    """ Общий обработчик ИУ получает управление от :ref:`интерфейса управления
    <ci.interfaces>` и реализует логику валидации полученного сигнала.

    В ходе валлидации сигнала всегда необходимо установить, выполнены ли
    следующие условия:

    * переданы все позиционные аргументы (обязательные параметры)

    * все ключевые аргемнты имеют допустимые имена

    * для всех аргументов необходимо установить, имеют правильный они верный
      тип данных, формат, и если указаны диапазоны значений, то входят ли
      переданные значения в них

    Помимо этого, порцедура валидации должна оставлять простор для кастомной
    валидации, выходящей за рамки этой логики, например, когда обязвтельные
    аргументы отсутствуют, но должен быт указан тот ИЛИ иной ключевой аргумент.

    В ходе валидации может быть установлена часть значений runtime, необходимых
    для процедуры обработки, что позволит не получать некоторые значения второй
    раз.
    """
    short_description = None
    full_description = None
    args = None
    kwargs = None

    def __init__(self, **kwargs):
        super(SignalHandler, self).__init__(**kwargs)
        self.runtime.update_defaults({
            'object': None,
            'args': [],
            'kwargs': {}
        })

    def prepare(self):
        """
        В начале обработки устанавливаются значения runtime, которые не были получены в ходе
        валидации.
        :return:
        """
        self.messages.debug('validating arguments')
        if not self.validate_args():
            return

        if not self.validate_kwargs():
            return

        self.messages.debug('collecting arguments')
        self.collect_args()
        self.collect_kwargs()

    def validate_value(self, field, value):
        # тип данных
        if hasattr(field, 'datatype'):
            datatype = field.datatype
            if not field.validate_datatype(value):
                try:
                    datatype_name = datatype.__name__
                except AttributeError:
                    datatype_name = datatype.__class__.__name__

                self.error_response(
                    'value error: %s is not a valid representation of the data type: %s' %
                    (value, datatype_name))
                return False

        # формат
        if hasattr(field, 'formatter'):
            formatter = field.formatter
            if not field.validate_format(value):
                self.error_response(
                    'value error: %s doesn\'t match the format specified: %s' %
                    (value, str(formatter)))
                return False

        # диапазон
        if hasattr(field, 'range'):
            rng = field.range
            if not field.validate_range(value):
                self.error_response(
                    'value error: %s doesn\'t match the range specified: %s' %
                    (value, str(rng)))
                return False

        return True

    def validate_args(self):
        """
        валидатору передается транспортное представление ИЛИ значение
        """
        handler_args = self.args or []
        signal_args = self.runtime['object'].args or []

        # аргемнты переданы, когда не должны быть
        if not handler_args and signal_args:
            self.error_response('%s takes no arguments' % self.name)
            return False

        # передано неверное количество
        if len(handler_args) != len(signal_args):
            self.error_response(
                '%s takes exactly %d argument(s) (%d given)' %
                (self.name, len(handler_args), len(signal_args)))
            return False

        for index, field in enumerate(handler_args):
            # используем методы объекта, если класс инициализирован,
            # иначе методы класса
            if not self.validate_value(field, signal_args[index]):
                return False

        return True

    def validate_kwargs(self):
        handler_kwargs = self.kwargs or {}
        signal_kwargs = self.runtime['object'].kwargs or {}

        for name, value in list(signal_kwargs.items()):

            if name not in handler_kwargs:
                self.error_response(
                    'type error: %s got an unexpected keyword argument: %s' % (self.name, name)
                )
                return False

            if not self.validate_value(value, signal_kwargs[name]):
                return False

        return True

    def collect_args(self):
        """ """
        handler_args = self.args or []
        signal_args = self.runtime['object'].args or []

        for index, field in enumerate(handler_args):
            value = signal_args[index]

            if field and value:
                value = field.from_transport(value)

            self.runtime['args'].append(value)

    def collect_kwargs(self):
        """ """
        signal_kwargs = self.runtime['object'].kwargs or {}

        for name, value in list(signal_kwargs.items()):
            field = self.kwargs[name]

            if field and value:
                value = field.from_transport(value)

            self.runtime['kwargs'][name] = value


class RESTEndpointMixin(object):
    http_method = 'GET'

    @classmethod
    def as_url(cls, node_name=None):
        """
        Для методов ``GET`` и ``DELETE`` подразумевается, что как позиционные аргументы, так и ключевые
        не слишком велики и могут быть переданы как regex-группы или параметры строки запроса.

        Поэтому для таких обработчиков формируется URL примерно следующего вида::

            /arg1/arg2/arg3/../?key1=value1&key2-value2&...

        Логика метода ``POST`` и ``PUT`` несколько сложнее: подразумевается, что позиционные аргументы
        могут быть как достаточно большими (либо иметь неподходящий тип данных), и тогда они должны
        передаваться в теле запроса, так и не слишком большими, и могут тогда передаваться как
        regex-группы, а ключевые аргументы всегда не слишком велики и могут быть пепреданы как параметры
        строки запроса::

            /arg1/arg2/?key1=value1&key2-value2&...

            "[arg2, arg4, ...]"

        :param node_name:
        :return:
        """
        addr_sections = []

        # [logger]
        if node_name:
            addr_sections.append(node_name)

        # [logger, message, list]
        addr_sections.extend(cls.name.split('__'))
        handler_args = cls.args or []

        if cls.http_method and (cls.http_method.upper() in ['GET', 'DELETE']):
            for field in handler_args:
                # [logger, sender, read, sender_id]
                addr_sections.append(field.as_regex_group())

        if cls.http_method and cls.http_method.upper() in ['POST', 'PUT']:
            for field in handler_args:
                # [logger, sender, write, sender_id]
                try:
                    addr_sections.append(field.as_regex_group())
                except AttributeError:
                    pass

        return r'/%s/' % '/'.join(addr_sections)
