# -*- coding: utf-8 -*-

"""Top-level package for Symphony."""

__author__ = """George Makarovsky"""
__email__ = 'development@evenlights.com'
__version__ = '1.1.1'
