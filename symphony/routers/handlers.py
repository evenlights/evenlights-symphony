# -*- coding: utf-8 -*-
from symphony.core.infra.generic import GenericComponent


class BaseHandler(GenericComponent):
    """ Обработчик получает управление от маршрутизатора. """
    aliases = None

    def __init__(self, aliases=None, **kwargs):
        super(BaseHandler, self).__init__(**kwargs)
        self.mapping = None

        if aliases:
            self.aliases = aliases
        self.aliases = self.aliases or []

    # Component
    def init(self, core=None, config=None, registry=None, mount_point=None,
             mapping=None, **kwargs):
        super(BaseHandler, self).init(core=core, config=config,
                                      registry=registry,
                                      mount_point=mount_point, **kwargs)
        self.mapping = mapping

        # дополнительная регистрация в мэппинге роутера
        self.register(self.mapping, registry_name=self.get_mapping_key(),
                      primary=False)

        # дополнительная регистрация алиасов
        for alias in self.aliases:
            self.register(self.mapping, registry_name=alias, primary=False)

        return self

    def shutdown(self):
        self.unregister(registry=self.mapping,
                        registry_name=self.get_mapping_key(), primary=False)

        for alias in self.aliases:
            self.unregister(registry=self.mapping, registry_name=alias, primary=False)

        super(BaseHandler, self).shutdown()

    # Handler
    def prepare(self):
        pass

    def handle(self):
        pass

    def flush(self):
        super(BaseHandler, self).flush()
        self.runtime.flush()

    # Responses
    def response(self, content, code='info'):
        getattr(self.messages, code)(content)

    def info_response(self, msg):
        self.response(msg, code='info')

    def warning_response(self, msg):
        self.response(msg, code='warning')

    def error_response(self, msg):
        self.response(msg, code='error')

    def get_mapping_key(self):
        return getattr(self, getattr(self, 'mapping_key', None) or 'name')
