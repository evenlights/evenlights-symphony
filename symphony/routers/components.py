# -*- coding: utf-8 -*-
import traceback
from collections import deque

from symphony.core.infra.generic import GenericComponent


class Router(GenericComponent):
    """ Маршрутизатор служит единой точкой входа, когда возникает необходимость передачи управления
    различным обработчикам, основываясь на формальных признаках обрабатываемого объекта.

    Чаще всего такая необходимость возникает при обработке различных типов данных, поступающих через
    один канал, таких как запись в базу данных разнородных объектов или получение сообщений различного
    типа в рамках одного суб-протокола.

    В этом случае, во-первых, необходимо уметь различать эти типы, а во-вторых, располагать механизмом
    для сопоставления типа объекта и обработчика.

    Таким образом, роутер связан с двумя основными понятиями:

    * мэппинг, устанавливающий соответствие между объектом и обработчиком

    * и обработчик, которому передается объект, если соответствие установлено
    """
    mapping_key = 'name'
    handler_source = None
    handlers = None

    def __init__(self, handlers=None, handler_source=None, mapping_key=None,
                 **kwargs):
        super(Router, self).__init__(**kwargs)
        self.mapping = {}

        if handlers:
            self.handlers = handlers

        if handler_source:
            self.handler_source = handler_source

        if mapping_key:
            self.mapping_key = mapping_key

    # Component

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        self.component.init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.infra.init(
            core=self.core, config=self.config, registry=self.infra.components,
            mount_point=self, **kwargs
        )

        # try to pick up handler source from the mount point
        if not self.handlers and self.handler_source:
            try:
                self.handlers = getattr(self.mount_point, self.handler_source)
            except AttributeError:
                pass

        if self.handlers:
            self.messages.debug('initializing handlers')
            self.infra.init(
                core=self.core,
                config=self.config,
                registry=self.infra.components,
                mount_point=self,
                mapping=self.mapping,

                source_feed=self.handlers,
                )
            self.messages.debug(
                'resulting mapping: %s'
                % ', '.join(map(str, list(self.mapping.keys())))
            )
        else:
            self.messages.warning('initialized without handlers')

        return self

    def handler_call(self, handler, obj):
        """

        :param handler:
        :param obj:
        :return:
        """
        self.messages.debug('handler call to: %s' % handler)

        handler.runtime['object'] = obj

        try:
            actual_prepare = handler._prepare
        except AttributeError:
            actual_prepare = handler.prepare

        try:
            actual_handle = handler._handle
        except AttributeError:
            actual_handle = handler.handle

        try:
            handler.messages.debug('preparing')
            actual_prepare()

            if handler.messages.errors:
                return

            handler.messages.debug('control obtained')
            return actual_handle()

        except:
            handler.error_response(msg=traceback.format_exc())

    def get_handler(self, handler_id):
        try:
            return self.mapping[handler_id]

        except KeyError:
            self.messages.error('handler not found: %s' % handler_id)
            return

    def handle(self, obj):
        """
        Метод передает данные соответствующему обработчику, при условии, что
        таковой найден.
        """
        handler_id = self.get_handler_id(obj)
        self.messages.debug('routing id: %s' % handler_id)

        handler = self.get_handler(handler_id)

        if handler:
            return self.handler_call(handler, obj)

    # Router
    def get_handler_id(self, obj):
        """ Идентификатор *обработчика* получается путем обращения к соответствующему полю *объекта*.
        Например, если сигнал имеет имя ``db__write``, то будет вызван обработчик, зарегистрированный
        в мэппинге под именем ``db__write``, а для сообщения протокола ``SMP`` с типом контента
        ``RAW (1)`` будет вызвано обработчик с идентификатором ``1``.
        :param obj:
        :return:
        """
        try:
            return getattr(obj, self.mapping_key)

        except AttributeError:
            self.messages.error(
                'error getting id field %s for the object, available are: %s' %
                (self.mapping_key, ', '.join([f for f in dir(obj) if not f.startswith('__')]))
            )
            return
        except KeyError:
            self.messages.error(
                'error getting id field %s for the object, available are: %s' %
                (self.mapping_key, ', '.join(map(str, list(obj.keys()))))
            )
            return

    def has_handler(self, obj):
        return self.get_handler_id(obj) in self.mapping


class QueueRouter(Router):
    """ Диспетечр с поддержой очереди очреди. """

    def __init__(self, *args, **kwargs):
        super(QueueRouter, self).__init__(*args, **kwargs)
        self.queue = deque()

    def queue_clear(self):
        self.queue.clear()

    def queue_empty(self):
        return not len(self.queue)

    def queue_get(self):
        self.messages.debug('getting from queue')
        return self.queue.popleft()

    def queue_put(self, item):
        self.messages.debug('appending to queue: %s' % item.name)
        return self.queue.append(item)

    def queue_size(self):
        return len(self.queue)

    def task_done(self):
        pass

    def handle_next(self):
        if self.queue_size():
            entry = self.queue_get()
            resp = self.handle(entry)
            self.task_done()
            return resp
