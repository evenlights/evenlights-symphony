# -*- coding: utf-8 -*-
import os
import time
import datetime
import hashlib

from symphony.adapters.base import BaseAdapter
from symphony.core.exceptions import ImproperlyConfigured

WRITE_MODES = ['a', 'a+', 'aU', 'w', 'w+', 'wU', 'ra', 'rw']


class BaseFileAdapter(BaseAdapter):
    handler_class = None

    def __init__(self,  handler_class=None, **kwargs):
        super(BaseFileAdapter, self).__init__(**kwargs)
        if handler_class:
            self.handler_class = handler_class
        self.handler = None

    # Context Manager
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.handler:
            self.close()

    # Component
    def init(self, core=None, config=None, registry=None, mount_point=None,
             context=None, poller=None, socket_registry=None, **kwargs):
        super(BaseAdapter, self).init(
            core=core, config=config, registry=registry, mount_point=mount_point, **kwargs
        )
        self.messages.debug('init')

        if not self.handler_class:
            self._raise('cannot initialize without a handler class specified',
                        exception_class=ImproperlyConfigured)

        return self

    def connect(self, target=None):
        """ Помимо валидации :ref:`базы адаптеров <adapters.base>` метод
        проверяет, является ли путь файлом.
        """
        super(BaseFileAdapter, self).connect(target=target)

        if not os.path.isfile(self.target):
            self.messages.error('target is not a file: %s' % self.target)
            return

        self.is_connected = True

    def disconnect(self):
        """ """
        if self.handler:
            self.close()

        super(BaseFileAdapter, self).disconnect()

        self.is_connected = False

    # File Operations
    def open(self, target=None, lock_timeout=10, **kwargs):

        if not self.is_connected:
            self._raise('trying to open with adapter disconnected', exception_class=RuntimeError)
            return self

        if self.handler:
            self._raise('file already opened', exception_class=RuntimeError)
            return self

        if target:
            self.target = target

        if not self.target:
            self._raise('cannot open file, target unspecified', exception_class=RuntimeError)
            return self

        # при открытии для записи ждем, пока файл освободится
        if 'mode' in kwargs:
            if kwargs['mode'] in WRITE_MODES:
                self.lock_wait(timeout=lock_timeout)
                if self.messages.internal.errors:
                    return self
                self.lock_set()

        self.handler = self.handler_class(self.target, **kwargs)

        # поскольку метод может вызываться из with-statement,
        # всегда необходимо возвращать контекст-менеджер для вызова __exit__
        return self

    def close(self):
        self.messages.debug('closing file: %s' % self.target)

        if not self.is_connected:
            self._raise('trying to close with adapter disconnected', exception_class=RuntimeError)
            return

        if not self.handler_class:
            self._raise('trying to close with file not opened', exception_class=RuntimeError)
            return

        self.handler.close()
        self.handler = None

        # снимаем замок
        if self.lock_exists:
            self.lock_release()

    # CRUD
    def delete(self):
        """ Метод удаляет файл и отвязывает адаптер от его пути. """

        if not self.is_connected:
            self._raise('trying to delete with detached storage', exception_class=RuntimeError)
            return

        try:
            os.remove(self.target)
        except IOError:
            self.messages.error('I/O error when deleting file: %s' % self.target)
            return

        self.handler = None
        self.is_connected = None

    # Utilities
    @property
    def abspath(self):
        """ Метод возвращает абсолютный путь хранилища. """
        return os.path.abspath(self.target)

    def makedirs(self, abspath):
        if not os.path.exists(os.path.dirname(abspath)):
            self.messages.debug('directory not present, creating: %s' % os.path.dirname(abspath))
            os.makedirs(os.path.dirname(abspath))

    def get_basename(self, target=None):
        target_ = self.target if self.target else target

        if not target_:
            self._raise('cannot get basename, target unspecified', exception_class=RuntimeError)
            return

        try:
            return os.path.basename(os.path.abspath(target_))
        except IOError:
            self.messages.error('I/O error when getting basename: %s' % target_)
            return

    def get_dirname(self, target=None):
        target_ = self.target if self.target else target

        if not target_:
            self._raise('cannot get dirname, target unspecified', exception_class=RuntimeError)
            return

        try:
            return os.path.dirname(os.path.abspath(target_))
        except IOError:
            self.messages.error('I/O error when getting dirname: %s' % target_)
            return

    def get_checksum(self, target=None):
        target_ = self.target if self.target else target

        if not target_:
            self._raise('cannot get dirname, target unspecified', exception_class=RuntimeError)
            return

        try:
            with open(target_, 'rb') as infile:
                return hashlib.sha256(infile.read()).hexdigest()
        except IOError:
            self.messages.error('I/O error when getting checksum: %s' % target_)
            return

    # Lock File
    @property
    def lock_abspath(self):
        return '%s.lock' % self.target

    @property
    def lock_exists(self):
        return os.path.exists(self.lock_abspath)

    def lock_set(self):
        self.messages.debug('setting lock')
        open(self.lock_abspath, 'w').close()

    def lock_release(self):
        self.messages.debug('releasing lock')
        os.remove(self.lock_abspath)

    def lock_wait(self, timeout=10, check_interval=0.2):
        if self.lock_exists:
            self.messages.debug('waiting for lock, TIMEOUT %d' % timeout)
            wait_init = datetime.datetime.now()
            wait_delta = datetime.timedelta(seconds=timeout)

            while self.lock_exists:
                if datetime.datetime.now() > wait_init + wait_delta:
                    self.messages.error('lock wait TIMEOUT exceeded')
                    return
                time.sleep(check_interval)
