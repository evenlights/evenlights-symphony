# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod

from symphony.core.infra.generic import GenericComponent


class BaseAdapter(GenericComponent, metaclass=ABCMeta):
    """ Адаптеры используются как слой абстракции для работы с различными ресурсами
    (файлами, базами данных, сокетами, REST-интерфейсами) при в терминах операций, аналогичных
    файловым, таких как чтение, запись, создание, удаление.

    База адаптера вводит такое понятие, как ``привязка``, или ассоциация адаптера с
    определенной ``целью`` -- ресурсом, с которым выполняются операции.

    В то время, как цель адаптера, например адрес сокета, обычно (хотя не обязательно)
    остается постояной, состояние привязки является преходящим и, при различных
    обстоятельствах, может меняться.

    .. note::

        Методы, предоставляемые базой являются абстрактными и должны быть
        конкретизированы в классах-потомках.
    """
    target = None

    def __init__(self, target=None, **kwargs):
        super(BaseAdapter, self).__init__(**kwargs)
        self.is_connected = False
        if target:
            self.target = target

    # Target

    @abstractmethod
    def connect(self, target=None):
        """ Базовый метод для привязки адаптера к цели (ресурсу, файлу, сокету) проверяет

        * не выполнена ли уже привязка, поскольку во многих случаях такая перепривязка крайне
          нежелательна

        * и задана ли цель при инициализации или непосредственном вызове метода

        :param target: если указан, адаптер будет привязан к данной цели, если нет, будет
        использовано поле ``target``; если обе переменные пусты, адаптер породит исключение
        """
        self.messages.debug('connect')

        if self.is_connected:
            self._raise('adapter already connected', exception_class=RuntimeError)

        if target:
            self.target = target

        if not self.target:
            self._raise('adapter target unspecified', exception_class=RuntimeError)

        self.messages.debug('attaching to target: %s' % repr(self.target))

    @abstractmethod
    def disconnect(self):
        """ Базовый метод отвязки от цели проверяет, осуществлена ли привязка. """
        self.messages.debug('disconnect')

        if not self.is_connected:
            self._raise('adapter not connected', exception_class=RuntimeError)

        self.messages.debug('detaching from target: %s' % self.target)
        self.target = None

    # Adapter

    @abstractmethod
    def read(self, *args, **kwargs):
        """ Базовый метод чтения проверяет, осуществлена ли привязка. """

        if not self.is_connected:
            self._raise('trying to read with adapter disconnected', exception_class=RuntimeError)

        self.messages.trace('reading from target')

    @abstractmethod
    def write(self, *args, **kwargs):
        """ Абстрактный метод записи проверяет, осуществлена ли привязка. """

        if not self.is_connected:
            self._raise('trying to write with adapter disconnected', exception_class=RuntimeError)

        self.messages.trace('writing to target')
