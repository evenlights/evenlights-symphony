# -*- coding: utf-8 -*-
import time
import uuid

import zmq
from zmq.sugar.constants import (
    bytes_sockopt_names as STRING_OPTIONS, int_sockopt_names as INT_OPTIONS
)

from symphony.configs.utils import resolve_value
from symphony.adapters.base import BaseAdapter

from symphony.core.defaults import DEFAULT_CODEC

BASE_PARAMS = ['target', 'type', 'method']
CUSTOM_OPTIONS = ['RECONNECT_DELAY']
VALID_OPTIONS = STRING_OPTIONS + INT_OPTIONS + CUSTOM_OPTIONS
ZMQ_DEFAULT_CODEC = 'ISO-8859-2'


class ZMQSocketAdapter(BaseAdapter):
    """
    2/ZTC

    Философия инициализации:
    Умолчания, конфигурация, параметры

        "type": "SUB",
        "option": {
            "hwm": 1000,
            "swap": 25000000
        },

    Списки:

        "frontend": {
        "option": {
            "subscribe": [ "10001", "10002" ]
        },
        "bind": [ "tcp://eth0:5555", "inproc://device" ]
    """
    type = None
    method = None
    options = None

    def __init__(self, type=None, method=None, reconnect_delay=None,
                 identity=None, hwm=None, **kwargs):
        super(ZMQSocketAdapter, self).__init__(**kwargs)
        self.runtime.update_defaults({
            'defaults': {
                'reconnect_delay': 0.2,
                'identity': str(uuid.uuid4()),
            },
            'init': {
                'target': self.target,
                'type': type,
                'method': method,
                'options': {
                    'sndhwm': hwm,
                    'rcvhwm': hwm,
                    'identity': identity,
                    'reconnect_delay': reconnect_delay,
                },
            },
            'config': {
                'target': None,
                'type': None,
                'method': None,
                'options': {},
            },
        })
        if self.options is None:
            self.options = {}

    # Component

    def init(self, core=None, config=None, registry=None, mount_point=None,
             context=None, poller=None, socket_registry=None, **kwargs):
        self.config_load(config)
        self.config_resolve()

        super(ZMQSocketAdapter, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.messages.debug('init')
        self.context = context or zmq.Context()
        self.poller = poller
        self.socket = None

        if socket_registry is not None:
            self.socket_registry = socket_registry
            self.register(socket_registry, primary=False)
        return self

    def activate(self):
        super(ZMQSocketAdapter, self).activate()
        self.connect()

    def deactivate(self):
        self.disconnect()
        super(ZMQSocketAdapter, self).deactivate()

    # Target

    def connect(self, target=None):
        super(ZMQSocketAdapter, self).connect(target=target)

        self.messages.debug('params: type=%s, method=%s' % (self.type, self.method))
        self.socket = self.context.socket(self.type)

        self.apply_options()

        if self.method == 'bind':
            connect_method = self.socket.bind
        else:
            connect_method = self.socket.connect

        targets = [self.target] if not isinstance(self.target, list) else self.target

        for target in targets:
            connect_method(target)

        if self.poller:
            self.messages.debug('poller registeration')
            self.poller.register(self.socket, zmq.POLLIN)

        self.is_connected = True

    def disconnect(self):
        super(ZMQSocketAdapter, self).disconnect()

        if self.poller:
            self.messages.debug('poller un-registeration')
            self.poller.unregister(self.socket)

        self.socket.close(linger=0)

        while not self.socket.closed:
            time.sleep(0.01)

        self.socket = None
        self.is_connected = False

    def reconnect(self, delay=None):
        delay = delay or self.options['reconnect_delay']

        target = self.target
        if self.is_connected:
            self.disconnect()

        self.messages.info('reconnecting in %0.2f seconds' % delay)
        time.sleep(delay)

        self.connect(target)

    def shutdown(self):
        self.messages.debug('shutdown')
        if self.is_connected:
            self.disconnect()

        try:
            self.unregister(self.socket_registry, primary=False)
            self.socket_registry = None
        except AttributeError:
            pass

        super(ZMQSocketAdapter, self).shutdown()

    # Adapter
    def read(self):
        super(ZMQSocketAdapter, self).read()
        try:
            return self.decode_multipart(self.socket.recv_multipart())
        except zmq.ZMQError as e:
            self.messages.error('cannot read from socket: %s' % e)

    def write(self, mp_msg, *args, **kwargs):
        super(ZMQSocketAdapter, self).write(self, mp_msg, *args, **kwargs)
        try:
            self.socket.send_multipart(self.encode_multipart(mp_msg))
        except zmq.ZMQError as e:
            self.messages.error('cannot write to socket: %s' % e)

    recv_multipart = read
    send_multipart = write

    # Config
    def config_load(self, config):
        if config:

            # тип
            if 'type' in config:
                try:
                    self.runtime['config']['type'] = getattr(zmq, config['type'])

                except AttributeError:
                    self._raise(
                        'cannot resolve socket type: %s' % config['type'], exception_class=ValueError)

            # метод и адрес
            if 'bind' in config and 'connect' in config:
                self._raise(
                    'a socket should either bind or connect, not both', exception_class=ValueError)

            if 'bind' in config:
                self.runtime['config']['method'] = 'bind'
                self.runtime['config']['target'] = config['bind']

            elif 'connect' in config:
                self.runtime['config']['method'] = 'connect'
                self.runtime['config']['target'] = config['connect']

            # опции
            if 'option' in config:
                for name in list(config['option'].keys()):
                    if name.upper() not in VALID_OPTIONS:
                        self._raise(
                            'unrecognized socket option: %s' % name, exception_class=ValueError)
                self.runtime['config']['options'] = config['option']

    def config_resolve(self):
        # адрес, тип, метод
        for key in BASE_PARAMS:
            try:
                value = resolve_value(
                    config=self.runtime['config'][key],
                    init=self.runtime['init'][key],
                    raise_on_none=True
                )
                setattr(self, key, value)
            except ValueError as e:
                self._raise('required key "%s": %s' % (key, e))

        # используем все опции со значениями, отличными от None
        option_keys = list(
            set(
                key for key, value in list(self.runtime['defaults'].items())
                if value is not None
            ) |
            set(
                key for key, value
                in list(self.runtime['config']['options'].items())
                if value is not None
            ) |
            set(
                key for key, value
                in list(self.runtime['init']['options'].items())
                if value is not None
            )
        )

        for key in option_keys:
            value = resolve_value(
                default=self.runtime['defaults'].get(key, None),
                config=self.runtime['config']['options'].get(key, None),
                init=self.runtime['init']['options'].get(key, None),
            )
            if value:
                self.options[key] = value

    def apply_options(self):
        for name, value in list(self.options.items()):
            try:
                self.messages.debug('applying option: %s=%s' % (name, value))
            except AttributeError:
                pass

            if name.upper() not in CUSTOM_OPTIONS:

                if isinstance(value, (str, int, float)):
                    target_values = [
                        value.encode(DEFAULT_CODEC) if isinstance(value, str)
                        else value
                    ]

                elif isinstance(value, (list, set, tuple)):
                    target_values = list(
                        map(
                            lambda v: v.encode(DEFAULT_CODEC)
                            if isinstance(v, str) else v,
                            value
                        )
                    )
                else:
                    raise RuntimeError(
                        'Trying to pass unsupported option value: {}'.format(
                            value
                        )
                    )

                for v in target_values:
                    self.socket.setsockopt(getattr(zmq, name.upper()), v)

    # Messages
    @staticmethod
    def encode_multipart(msg):
        """ Метод кодирует все фреймы при помощи кодека ``DEFAULT_CODEC (utf-8)``. Не-строковые фреймы
        предварительно конвуртируются в юникод.
        """
        for nr, frame in enumerate(msg):
            try:
                msg[nr] = frame.encode(DEFAULT_CODEC)
            except AttributeError:
                msg[nr] = str(frame).encode(DEFAULT_CODEC)
        return msg

    @staticmethod
    def decode_multipart(msg):
        """ Метод пытается декодировать строкове фреймы при помощи кодека ``DEFAULT_CODEC (utf-8). Если
        такое декодирование не удается, производится попытка декодировать при помощи кодека ZMQ
        по-умолчанию ``ZMQ_DEFAULT_CODEC (ISO-8859-2)``.
        """
        for nr, frame in enumerate(msg):
            try:
                msg[nr] = frame.decode(DEFAULT_CODEC)
            except UnicodeDecodeError:
                msg[nr] = frame.decode(ZMQ_DEFAULT_CODEC)
        return msg

    @property
    def identity(self):
        return self.socket.identity.decode()
