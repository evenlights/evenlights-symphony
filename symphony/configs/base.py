# -*- coding: utf-8 -*-
from symphony.core.exceptions import ValidationError
from symphony.messages.compat import MessageCompatibilityMixin


class ConfigBase(MessageCompatibilityMixin, object):
    """
    При инициализациии объекту могут быть переданы опциональные параметры,
    которые заменят значения свойств класса. Объект может наследоваться
    наряду с другими классами.

    :param source: абстрактный идентификатор источника, который будет передан
      функции (имя модуля, файла, либо словарь)
    :param loader: процедура загрузчика (необходимо учитывать, что процедура
      вызывается как метод объекта)
    :param required_keys: обязательные ключи
    """
    source = None
    loader = None
    defaults = None
    required_keys = None

    def __init__(self, source=None, loader=None, defaults=None, required_keys=None, *args, **kwargs):
        super(ConfigBase, self).__init__(*args, **kwargs)

        if source:
            self.source = source

        if loader:
            self.loader = loader

        if required_keys:
            self.required_keys = required_keys

        if defaults:
            self.defaults = defaults

        self.DEBUG = False

    def __contains__(self, item):
        return hasattr(self, item)

    def __getitem__(self, item):
        try:
            return getattr(self, item)
        except AttributeError:
            raise KeyError(item)

    def __setitem__(self, key, value):
        setattr(self, key, value)

    @classmethod
    def extend_defaults(cls, new_dict):
        if cls.defaults:
            return {
                k: v for d in (cls.defaults, new_dict)
                for k, v in list(d.items())
            }
        else:
            return new_dict

    @classmethod
    def extend_required_keys(cls, new_list):
        if cls.required_keys:
            return cls.required_keys + new_list
        else:
            return new_list

    def load(self):
        """
        """
        try:
            self.messages.debug('loading source: %s' % self.source)
        except AttributeError:
            pass

        # обязательные ключи указаны, а источник пуст
        if self.required_keys and self.source is None:
            self._message_or_raise(
                'source is empty with required keys specified',
                exception_class=ValidationError
            )

        # параметры по умолчанию
        if self.defaults:
            for key in self.defaults:
                self[key] = self.defaults[key]

        # источник
        if self.source:
            source_dict = self.loader(self.source)

            # загрузка обязательных ключей
            if self.required_keys:
                for key in self.required_keys:
                    if key in source_dict:
                        self[key] = source_dict.pop(key)
                    else:
                        self._message_or_raise(
                            'required parameter not found: %s' % key,
                            exception_class=KeyError
                        )

            # загрузка остальных ключей
            for key in list(source_dict.keys()):
                self[key] = source_dict.pop(key)
