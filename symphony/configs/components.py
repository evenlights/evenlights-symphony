# -*- coding: utf-8 -*-
from symphony.core.infra.generic import GenericComponent
from symphony.configs.base import ConfigBase
from symphony.configs.loaders import dict_loader, module_loader, json_file_loader


class BaseConfig(ConfigBase, GenericComponent):
    name = 'config'


class JSONFileConfig(BaseConfig):
    loader = json_file_loader


class DictConfig(BaseConfig):
    loader = dict_loader


class ModuleConfig(BaseConfig):
    source = 'settings'
    loader = module_loader

