# -*- coding: utf-8 -*-
import copy
import configparser

import importlib
import simplejson

from symphony.packages.jsoncomment import JsonComment
from symphony.core.datatypes import DotDict

from symphony.configs.defaults import (
    CFGFILE_MAIN_SECTION, MODULE_EXCLUDED_KEYS
)

json = JsonComment(simplejson)


def dict_loader(self, source):
    """ Словарь передается без обработки. """
    return DotDict(copy.deepcopy(source))


def module_loader(self, source):
    """ Метод загружает все ключи из модуля, кроме перечисленных в
    ``EXCLUDED_KEYS``.

    :param source: имя модуля для загрузки в формате ``mod1.mod2.mod3.mod4``
    """
    config_module = importlib.import_module(source)
    module_keys = [k for k in dir(config_module) if k not in MODULE_EXCLUDED_KEYS]

    target_dict = {}
    for key in module_keys:
        target_dict[key] = getattr(config_module, key)
    return DotDict(target_dict)


def json_file_loader(self, source):
    """ Метод загрузки словаря из JSON-файла.

    :param source: путь к файлу
    """
    with open(source) as infile:
        return DotDict(json.loads(infile.read()))


def json_string_loader(self, source):
    """ Метод загрузки словаря из JSON-файла.

    :param source: путь к файлу
    """
    return DotDict(json.loads(source))


def cfg_file_loader(self, source):
    """ Поскольку в cfg-файлах любое значение должно относиться к секции,
    загрузка производится из секции ``[main]``.
    """
    config_parser = configparser.SafeConfigParser()
    config_parser.read(source)

    target_dict = {}
    for entry in config_parser.items(CFGFILE_MAIN_SECTION):
        try:
            value = int(entry[1])
        except ValueError:
            value = entry[1]

        target_dict[entry[0].upper()] = value
    return DotDict(target_dict)

