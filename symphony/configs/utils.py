# -*- coding: utf-8 -*-


def resolve_value(default=None, config=None, init=None, raise_on_none=False):
    value = None

    if default:
        value = default

    if config:
        value = config

    if init:
        value = init

    if not value and raise_on_none:
        raise ValueError(
            'value could not be resolved: default=%s, config=%s, init=%s' % (default, config, init)
        )

    return value
