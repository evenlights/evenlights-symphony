# -*- coding: utf-8 -*-

ENV_PROJECT_ENV = 'PROJECT_ENV'
ENV_SETTINGS_MODULE = 'SYMPHONY_SETTINGS_MODULE'
CFGFILE_MAIN_SECTION = 'main'
MODULE_EXCLUDED_KEYS = ['__builtins__', '__doc__', '__file__', '__name__', '__package__', 'os', 'sys']
SUPPORTED_LOADER_TYPES = ['function', 'instancemethod']
