# -*- coding: utf-8 -*-
import copy


class RuntimeStorage(dict):
    """
    Хранилище времени исполнения представляет собой словарь. В отличие от
    обычного словаря, хранилище RTS может помнить и восстанавливать
    "настройки по умолчанию", то есть, свое начальное состояние.
    """
    def __init__(self, defaults=None, *args, **kwargs):
        """
        :param defaults: RTS-хранилище может принимать список значений по
            умолчанию в качестве аргумента инициализации. Эти значения будут
            схоранены и восстановлены при сбросе (:func:`flush`) объекта.
        """
        super(RuntimeStorage, self).__init__(*args, **kwargs)
        self['__defaults__'] = copy.deepcopy(defaults) if defaults else {}
        self.update(self['__defaults__'])

    def in_defaults(self, key):
        """ Метод возвращает ``True``, если ключ имеется в значениях по
        умолчанию, либо ``False``, если ключ отсутствует в занчениях по
        умолчанию или значения по умолчанию отсуствуют.
        """
        if self['__defaults__']:
            if key in self['__defaults__']:
                return True
        return False

    def update_defaults(self, other):
        self['__defaults__'].update(copy.deepcopy(other))
        for key in other:
            self.reset(key)

    def reset(self, *keys):
        """ При сбросе в качестве ключа устанавливается копия
        соответствующего значения из словаря ``__defaults__``. """
        for key in keys:
            self[key] = copy.copy(self['__defaults__'][key])

    def flush(self):
        """ Метод производит сброс ключей по логике, описанной в :func:`reset_key`.
        """
        self_keys = [k for k in list(self.keys()) if k != '__defaults__']

        for key in self_keys:
            if self.in_defaults(key):
                self.reset(key)
            else:
                self.pop(key)
