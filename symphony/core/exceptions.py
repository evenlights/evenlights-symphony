# -*- coding: utf-8 -*-


class ImproperlyConfigured(Exception):
    """ Объект неверно сконфигурирован. """
    pass


class IncorrectDataType(Exception):
    """ (Получен) неверный тип данных. """
    pass


class ValidationError(Exception):
    """ Ошибка валидации """
    pass
