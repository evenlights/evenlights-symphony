# -*- coding: utf-8 -*-
import copy

from symphony.messages.compat import MessageCompatibilityMixin
from symphony.core.runtime import RuntimeStorage
from symphony.core.repr import ComponentReprMixin

from symphony.core.infra.defaults import INFRA_DECLARATION_CLASS, INFRA_COMPONENT_FEED


class ComponentManager(object):
    def __init__(self, target):
        self.state = {
            'plug_successful': False,
            'config_successful': False,
            'registration_successful': False,
            'mount_successful': False,
            'init_successful': False,

            'activation_successful': False,

            'setup_successful': False,

            'check_successful': False,

            'cleanup_successful': False,

            'deactivation_successful': False,

            'unmount_successful': False,
            'unregistration_successful': False,
            'unplug_successful': False,
            'shutdown_successful': False,

            'mount_name': None,
            'registry_name': None,
        }
        self.target = target

    # Init Phase

    def plug(self, core):
        """
        :param core:
        :return:
        """
        if self.state['plug_successful']:
            self.target._raise(
                'already plugged: %s' % self.target.name,
                exception_class=RuntimeError
            )

        self.target.core = core

        # обрабатываем только атрибуты
        if self.target.shared and isinstance(self.target.shared, bool):
            try:
                core.shared.register(self.target)
            except AttributeError:
                pass

        self.state['plug_successful'] = True

    def config(self, config):
        """
        :return:
        """
        if self.state['config_successful']:
            self.target._raise('already configured',
                               exception_class=RuntimeError)

        self.target.config = config
        self.state['config_successful'] = True

    def register(self, registry, registry_name=None, primary=True):
        """
        :param registry:
        :param registry_name:
        :param primary:
        :return:
        """
        if registry_name is None:
            registry_name = self.target.name

        if not isinstance(registry, dict):
            self.target._raise(
                'registry should be a dict subclass, got: %s' %
                registry.__class__.__name__, exception_class=RuntimeError)

        if registry_name in registry:
            self.target._raise(
                'registry name already in use: %s' % registry_name,
                exception_class=RuntimeError
            )

        registry[registry_name] = self.target

        if primary:
            if self.state['registration_successful']:
                self.target._raise('already registered',
                                   exception_class=RuntimeError)

            self.target.registry = registry
            self.state['registry_name'] = registry_name
            self.state['registration_successful'] = True

    def mount(self, mount_point, mount_name=None, primary=True):
        """
        :param mount_point:
        :param mount_name:
        :param primary:
        :return:
        """
        if not mount_name:
            mount_name = self.target.name

        if getattr(mount_point, mount_name, None):
            self.target._raise(
                'mount name already in use: %s on %s' %
                (mount_name, mount_point),
                exception_class=RuntimeError
            )

        setattr(mount_point, mount_name, self.target)

        if primary:
            if self.state['mount_successful']:
                self.target._raise('already mounted',
                                   exception_class=RuntimeError)

            self.target.mount_point = mount_point
            self.state['mount_name'] = mount_name
            self.state['mount_successful'] = True

    def accept_kwargs(self, **kwargs):
        for name, value in list(kwargs.items()):
            setattr(self.target, name, value)

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        """ Типичный процесс включает установку местоположения ядра, как некой
        общей точки инфраструктуры, регистрация в каком-либо реесте и создание
        точки монтирования на указанном объекте.

        Хотя процесс инициализации не накладывает особых ограничений в отношении
        порядка, количества и содержания шагов, все же можно выделить некоторые
        общие рекомендации:

        * Присоединение: в большинстве случаев вам будут нужны ядро и его каналы
          сообщений -- центральная шина и логгер. Однако, инфраструктура вполне
          может работать и без них.

        * Конфигурация: в отношении конфигурации существует только метод
          :meth:`.config_successful`, устаналивающий успешность фазы
          конфигурации, поскольку невозможно с уверенностью сказать, где и как
          именно в процессе инициализации будут использованы ее параметры.
          Однако следует иметь в виду, что регистрация и монтирование будут
          выполнены, только если конфигурация успешно завершена. Фактически, это
          сигнал для нфраструктуры о том, что компонент решил принимать участие
          в процессе. В противном случае, объект будет просто собран коллктором
          мусора.

        * Регистрация: в дальнейшем все групповые действия, такие как активация,
          обработка, сброс и завершение, выполняются только в отношении
          компонентов, которые внесли себя в реестр. Если компонент должен
          получать управление от инфраструктуры, регистрация обязательна.

        * Монтирование: не является обязательным шагом и служит скорее для
          удобства.

        :param core:
        :param config:
        :param registry:
        :param mount_point:
        :param kwargs:
        :return:
        """
        if self.state['init_successful']:
            self.target._raise('already initialized',
                               exception_class=RuntimeError)

        if core:
            self.plug(core)

        self.config(config)

        if registry is not None and self.state['config_successful']:
            self.register(registry)
        else:
            self.target._message_or_warning(
                'component has not been configured or no registry provided, '
                'skipping registration'
            )

        if mount_point and self.state['config_successful']:
            self.mount(mount_point)
        else:
            self.target._message_or_warning(
                'component has not been configured or no mount point provided, '
                'skipping mounting'
            )

        self.accept_kwargs(**kwargs)

        self.state['init_successful'] = True

    # Activation Phase

    def activate(self):
        """
        :return:
        """
        if self.state['activation_successful']:
            self.target._raise('already activated',
                               exception_class=RuntimeError)

        self.state['activation_successful'] = True

    # Setup Phase

    def setup(self):
        """
        :return:
        """
        if self.state['setup_successful']:
            self.target._raise('already set up', exception_class=RuntimeError)

        self.state['setup_successful'] = True

    # Check Phase

    def check(self):
        """
        :return:
        """
        if self.state['check_successful']:
            self.target._raise('already checked', exception_class=RuntimeError)

        self.state['check_successful'] = True

    # Cleanup Phase

    def cleanup(self):
        """
        :return:
        """
        if self.state['cleanup_successful']:
            self.target._raise('already cleaned up',
                               exception_class=RuntimeError)

        self.state['cleanup_successful'] = True

    # Shutdown Phase

    def deactivate(self):
        """
        :return:
        """
        if not self.state['activation_successful']:
            self.target._raise('not activated', exception_class=RuntimeError)

        if self.state['deactivation_successful']:
            self.target._raise('already deactivated',
                               exception_class=RuntimeError)

        self.state['deactivation_successful'] = True

    def unmount(self, mount_point=None, mount_name=None, primary=True):
        """
        :param mount_point:
        :param mount_name:
        :param primary:
        :return:
        """
        if not mount_point:
            mount_point = self.target.mount_point

        if primary:
            if not mount_name:
                mount_name = self.state['mount_name']

            if not self.state['mount_successful']:
                self.target._raise('not mounted', exception_class=RuntimeError)

            if self.state['unmount_successful']:
                self.target._raise('already unmounted',
                                   exception_class=RuntimeError)
        else:
            if not mount_name:
                mount_name = self.target.name

        if not hasattr(mount_point, mount_name):
            self.target._raise('mount name missing',
                               exception_class=RuntimeError)

        delattr(mount_point, mount_name)

        if primary:
            self.target.mount_point = None
            self.state['mount_name'] = None
            self.state['unmount_successful'] = True

    def unregister(self, registry=None, registry_name=None, primary=True):
        """
        :param registry:
        :param registry_name:
        :param primary:
        :return:
        """
        if registry is None:
            registry = self.target.registry

        if primary:
            if registry_name is None:
                registry_name = self.state['registry_name']

            if not self.state['registration_successful']:
                self.target._raise('not registered',
                                   exception_class=RuntimeError)

            if self.state['unregistration_successful']:
                self.target._raise('already unregistered',
                                   exception_class=RuntimeError)
        else:
            if registry_name is None:
                registry_name = self.target.name

        if registry_name not in registry:
            self.target._raise('registry name missing',
                               exception_class=RuntimeError)

        del registry[registry_name]

        if primary:
            self.target.registry = None
            self.state['registry_name'] = None
            self.state['unregistration_successful'] = True

    def unplug(self):
        """
        :return:
        """
        if not self.state['plug_successful']:
            self.target._raise('not plugged', exception_class=RuntimeError)

        if self.state['unplug_successful']:
            self.target._raise('already unplugged',
                               exception_class=RuntimeError)

        if self.target.shared:
            try:
                self.target.core.shared.unregister(self.target)
            except KeyError:
                pass

        self.target.core = None
        self.state['unplug_successful'] = True

    def shutdown(self):
        """
        :return:
        """
        if not self.state['init_successful']:
            self.target._raise('not initialized', exception_class=RuntimeError)

        if self.state['mount_successful']:
            self.unmount()

        if self.state['registration_successful']:
            self.unregister()

        if self.state['plug_successful']:
            self.unplug()

        self.target.runtime.flush()

        self.state['shutdown_successful'] = True


class BaseComponent(MessageCompatibilityMixin, ComponentReprMixin, object):
    """ :class:`BaseComponent` представляет основной и максимально абстрактный
    строительный блок инфраструктуры и определяет философию жизненного цикла ее
    функциональных единиц. Согласно этой философии, жизненный цикл
    инраструктурных единиц условно делится на 5 фаз:

    * Фаза инициализации: в самом широком смысле инициализация включает в себя 4
      основных момента: присоединение к существующей инфраструктуре, анализ и
      применение существующей конфигурации, регистрацию в каком-либо реестре и
      монтирование. В общем смысле, инициализация означает создание и настройку
      и должна обеспечить применение всех необходимых настроек до начала работы.

      На этом этапе компонент, основываясь на существующей конфигурации должен
      решить, участвовать в процессе или нет. При положительном решении
      компонент регистрирует себя в каком-либо реестре и (опционально) выбирает
      точку монтирования.

      ..note::

          Конфигурация не вынесена в отдельный метод, поскольку невозможно с
          уверенностью сказать, где именно и как она бедт использована.

    * Фаза активации: после того, как компонент был присоединен к инфраструктуре
      и полностью настроен, он может начать взаимодействие с окружающим миром,
      например, установить соединение с базой данных, удаленным узлом либо итд.
      Другими словами, подготовить все, что необходимо для непосредственной
      работы. Активация -- не обязательный шаг и требуется в ограниченных
      случаях.

    * Фаза обработки: описывает основную продуктивную деятельность компонента в
      виде последовательных процедур обработки и очистки.

    * Фаза деактивации: служит для отметы активации. Также не является
      обязательным шагом.

    * Фаза завершения: сотоит в последовательной отмене шагов инициализации:
      деактивации, отмонтирования, отмены регистрации и отсоединения от
      инфраструктуры.
    """
    name = None
    shared = False

    def __init__(self, name=None, shared=False, defaults=None):
        self.component = ComponentManager(self)
        self.runtime = RuntimeStorage(defaults if defaults else {})

        if name:
            self.name = name

        if shared:
            self.shared = shared

        self.core = None

        if not getattr(self, 'config', None):
            self.config = None

        self.registry = None
        self.mount_point = None

        if not self.name:
            self._raise('component cannot initialize without a name')

    # Init Phase

    def plug(self, core):
        """
        :param core:
        :return:
        """
        self.component.plug(core)

    def register(self, registry, registry_name=None, primary=True):
        """
        :param registry:
        :param registry_name:
        :param primary:
        :return:
        """
        self.component.register(registry, registry_name=registry_name,
                                primary=primary)

    def mount(self, mount_point, mount_name=None, primary=True):
        """
        :param mount_point:
        :param mount_name:
        :param primary:
        :return:
        """
        self.component.mount(mount_point, mount_name=mount_name,
                             primary=primary)

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        """ Типичный процесс включает установку местоположения ядра, как некой
        общей точки инфраструктуры, регистрация в каком-либо реесте и создание
        точки монтирования на указанном объекте.

        Хотя процесс инициализации не накладывает особых ограничений в отношении
        порядка, количества и содержания шагов, все же можно выделить некоторые
        общие рекомендации:

        * Присоединение: в большинстве случаев вам будут нужны ядро и его каналы
          сообщений -- центральная шина и логгер. Однако, инфраструктура вполне
          может работать и без них.

        * Конфигурация: в отношении конфигурации существует только метод
          :meth:`.config_successful`, устаналивающий успешность фазы
          конфигурации, поскольку невозможно с уверенностью сказать, где и как
          именно в процессе инициализации будут использованы ее параметры.
          Однако следует иметь в виду, что регистрация и монтирование будут
          выполнены, только если конфигурация успешно завершена. Фактически, это
          сигнал для нфраструктуры о том, что компонент решил принимать участие
          в процессе. В противном случае, объект будет просто собран коллктором
          мусора.

        * Регистрация: в дальнейшем все групповые действия, такие как активация,
          обработка, сброс и завершение, выполняются только в отношении
          компонентов, которые внесли себя в реестр. Если компонент должен
          получать управление от инфраструктуры, регистрация обязательна.

        * Монтирование: не является обязательным шагом и служит скорее для
          удобства.

        :param core:
        :param config:
        :param registry:
        :param mount_point:
        :param kwargs:
        :return:
        """
        self.component.init(core=core, config=config, registry=registry,
                            mount_point=mount_point, **kwargs)
        return self

    # Activation Phase

    def activate(self):
        """
        :return:
        """
        self.component.activate()

    # Setup Phase

    def setup(self):
        """
        :return:
        """
        self.component.setup()

    # Setup Phase

    def check(self):
        """
        :return:
        """
        self.component.check()

    # Handling Phase

    def handle(self, *args, **kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        pass

    def flush(self):
        """
        :return:
        """
        pass

    # Cleanup Phase

    def cleanup(self):
        """
        :return:
        """
        self.component.cleanup()

    # Shutdown Phase

    def deactivate(self):
        """
        :return:
        """
        self.component.deactivate()

    def unmount(self, mount_point=None, mount_name=None, primary=True):
        """
        :param mount_point:
        :param mount_name:
        :param primary:
        :return:
        """
        self.component.unmount(mount_point=mount_point, mount_name=mount_name,
                               primary=primary)

    def unregister(self, registry=None, registry_name=None, primary=True):
        """
        :param registry:
        :param registry_name:
        :param primary:
        :return:
        """
        self.component.unregister(
            registry=registry, registry_name=registry_name, primary=primary)

    def unplug(self):
        """

        :return:
        """
        self.component.unplug()

    def shutdown(self):
        """
        :return:
        """
        self.component.shutdown()


class InfraManager(object):
    """

    """
    def __init__(self, target):
        self.registry = {
            'components': {},
            'message_channels': [],
        }
        self.target = target

    @property
    def infra_class(self):
        try:
            return getattr(self.target.__class__, INFRA_DECLARATION_CLASS)
        except AttributeError:
            pass

    @property
    def infra_class_components(self):
        try:
            return getattr(self.infra_class, INFRA_COMPONENT_FEED)
        except AttributeError:
            return []

    @property
    def components(self):
        return self.registry['components']

    def infra_call(self, method_name, *args, **kwargs):
        # метод unregister изменяет поле self.components, поэтому используется
        # копия
        components = copy.copy(self.components)

        for component_name in components:
            instance = self.components[component_name]
            getattr(instance, method_name)(*args, **kwargs)

    def get_instance(self, component):
        """ Метод для получения конечного объекта для включения в
        инфраструктуру.

        :param component: класс или объект компонента
        :return: инициализированный класс, либо, если объект уже
          инициализирован, его копия, если объект не поддается коипрованию,
          метод возвращает сам объект
        """
        try:
            return component()
        except TypeError as e:
            # re-raise, если какая-либо другая ошибка
            if 'object is not callable' not in str(e):
                raise
            try:
                return copy.deepcopy(component)
            except TypeError:
                return component

    def join(self, component, core=None, config=None, registry=None,
             mount_point=None, **kwargs):
        """ Метод присоединения компонента к инфраструктуре.
        :param component:
        :param core:
        :param config:
        :param registry:
        :param mount_point:
        :param kwargs:
        :return:
        """
        if not core:
            core = self.target.core
        if not config:
            config = self.target.config
        if registry is None:
            registry = self.components
        if not mount_point:
            mount_point = self.target

        instance = self.get_instance(component)
        instance.init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs)

        return instance

    def init(self, source_feed=None, mapping_key=None, **kwargs):
        if not source_feed:
            source_feed = self.infra_class_components

        for component in source_feed:
            instance = self.get_instance(component)
            instance.init(**kwargs)

    def activate(self):
        self.infra_call('activate')

    def setup(self):
        self.infra_call('setup')

    def check(self):
        self.infra_call('check')

    def handle(self, *args, **kwargs):
        self.infra_call('handle', *args, **kwargs)

    def cleanup(self):
        self.infra_call('cleanup')

    def flush(self):
        self.infra_call('flush')

    def deactivate(self):
        self.infra_call('deactivate')

    def shutdown(self):
        self.infra_call('shutdown')


class InfraComponent(BaseComponent):
    """ Инфраструктуру можно описать как частный случай компонента. Основное ее
    отличие состоит в том, что она может включать другие компоненты и имеет
    средства для их регистрации и централизованного управления ими как одним
    целым.

    Общее соглашение в отношени компонента и его ифнраструктуры состоит в том,
    что сначала все действия фазы инициализации, активации и обработки
    выполняются сначала для самого компонента, а затем для инфраструктуры.

    Шаги же фазы деактивации и завершения выполняются в обратном порядке:
    сначала для инфраструктуры, а затем для компонента.

    Различные компоненты могут следовать этому соглашению с различной степенью
    строгости в зависимости от модификации и назначения.
    """
    def __init__(self, **kwargs):
        super(InfraComponent, self).__init__(**kwargs)
        self.infra = InfraManager(self)

    @classmethod
    def extend_components(cls, new):
        infra_class = cls.infra_class()
        if not infra_class:
            cls._raise(
                'only existing infrastructure components can be extended',
                exception_class=ValueError
            )
        try:
            existing = infra_class.components[:]
            return existing + [c for c in new if c not in existing]
        except AttributeError:
            return new

    @classmethod
    def infra_class(cls, target_class=None):

        if not target_class:
            target_class = cls

        return getattr(target_class, INFRA_DECLARATION_CLASS, None)

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(InfraComponent, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.infra.init(
            core=self.core, config=self.config, registry=self.infra.components,
            mount_point=self
        )

        return self

    def activate(self):
        super(InfraComponent, self).activate()
        self.infra.activate()

    def setup(self):
        super(InfraComponent, self).setup()
        self.infra.setup()

    def check(self):
        super(InfraComponent, self).check()
        self.infra.check()

    def handle(self, *args, **kwargs):
        super(InfraComponent, self).handle(*args, **kwargs)
        self.infra.handle(*args, **kwargs)

    def flush(self):
        super(InfraComponent, self).flush()
        self.infra.flush()

    def cleanup(self):
        self.infra.cleanup()
        super(InfraComponent, self).cleanup()

    def deactivate(self):
        self.infra.deactivate()
        super(InfraComponent, self).deactivate()

    def shutdown(self):
        self.infra.shutdown()
        super(InfraComponent, self).shutdown()
