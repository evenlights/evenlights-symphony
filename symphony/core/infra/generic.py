# -*- coding: utf-8 -*-
from symphony.core.infra.base import InfraComponent
from symphony.messages.components import Messages


class GenericComponent(InfraComponent):

    class Infrastructure:

        components = [Messages]
