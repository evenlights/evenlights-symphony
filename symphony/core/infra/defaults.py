# -*- coding: utf-8 -*-

INFRA_DECLARATION_CLASS = 'Infrastructure'
INFRA_MANAGER_NAME = 'infra'
COMPONENT_MANAGER_NAME = 'infra'
INFRA_COMPONENT_FEED = 'components'
