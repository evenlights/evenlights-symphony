# -*- coding: utf-8 -*-
import hashlib
import operator
from uuid import uuid4
from hashlib import sha512

from kitchen.text.converters import to_unicode

from symphony.core.defaults import KEY_LENGTH


class BaseGenerator(object):
    """
    """
    def __call__(self):
        pass

    def __repr__(self):
        return self.__call__()

    def __str__(self):
        return self.__call__()


class GenericKeyGenerator(BaseGenerator):
    """
    """
    def __repr__(self):
        return 'GenericTokenGenerator(%s)' % str(self.__call__())

    def __str__(self):
        return str(self.__call__())

    def __call__(self):
        return sha512(uuid4().hex.encode()).hexdigest()[0:KEY_LENGTH]


class GenericTokenGenerator(BaseGenerator):
    """
    """
    def __repr__(self):
        return 'GenericTokenGenerator(%s)' % self.__call__()

    def __str__(self):
        return str(self.__call__())

    def __init__(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs

    def __call__(self):
        key = ' '.join([args_to_str(self._args), args_to_str(self._kwargs)])
        return hashlib.md5(key.encode()).hexdigest()

    def check_token(self, token):
        return token == self.__call__()


def args_to_str(args, j=':'):
    """
    """
    if isinstance(args, tuple):
        return j.join(map(to_unicode, args))
    elif isinstance(args, dict):
        return ' '.join([
            j.join([to_unicode(key), to_unicode(value)])
            for key, value in sorted(args.items(), key=operator.itemgetter(0))
        ])
