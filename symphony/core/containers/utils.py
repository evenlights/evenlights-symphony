# -*- coding: utf-8 -*-
import simplejson as json


def container_validate(container_class, container_source):
    """
    :param container_class: класс контейнера для валидации
    :param container_source: представление контейнера
    :return:
    """

    try:
        container_class.from_json(container_source)

    # ошибки JSON-декодера
    except json.JSONDecodeError:
        # self.messages.error('container cannot be decoded')
        return False

    except TypeError:
        # self.messages.error('container cannot be decoded')
        return False

    #
    except KeyError:
        # self.messages.error('key error in container')
        return False

    return True
