# -*- coding: utf-8 -*-
from symphony.core.exceptions import ImproperlyConfigured
from symphony.core.containers.base import BaseContainer

from symphony.core.containers.defaults import (
    CONTAINERS_DATA_MODEL_DECLARATION,
    CONTAINERS_ROOT_KEY_FIELD,
    CONTAINERS_STRUCTURE_FIELD,
)


class ObjectContainer(BaseContainer):
    """ Контейнер типа :class:`object` описывается следующим образом::

        class Container(ObjectContainer):

            class DataModel:
                root_key = 'sample'
                structure = {
                    'field_1': None,
                    'field_2': SampleClass,
                }

            def __init__(self):
                self.field = u'данные'
                self.field_2 = SampleClass(field_a='a1', field_b='b1')

    :param root_key: опциональный параметр, если он задан, представление контейнера будет включать
        корневой ключ::

            {'sample': {
                'field_1': u'данные',
                'field_2': {'field_b': 'b1', 'field_a': 'a1'},
                }
            }

    :param structure: обязательный параметр, описывает поля объекта, участвующие в валидации/
        представлении/восстановлении. В качестве значения может быть задан класс, тогда его
        возможности будут исползованы при восставновлени и представлении::

            structure = {
                'sample_field_1': None,         # данные будут использованы в исходном виде
                'sample_field_2': SampleClass,  # класс используется для представления и восстановления
            }
    """

    def __init__(self, root_key=None, structure=None, **kwargs):
        super(ObjectContainer, self).__init__()

        self._root_key = root_key if root_key else self.class_root_key()
        self._structure = structure if structure else self.class_structure()

        self.root_key = self.instance_root_key
        self.structure = self.instance_structure

        self.validate_source(kwargs)

        for name, value in list(kwargs.items()):
            self.__setattr__(name, value)

    # DataModel
    @classmethod
    def data_model_class(cls):
        return getattr(cls, CONTAINERS_DATA_MODEL_DECLARATION, None)

    @classmethod
    def class_root_key(cls):
        data_model_class = cls.data_model_class()
        if data_model_class:
            return getattr(data_model_class, CONTAINERS_ROOT_KEY_FIELD, None)

    def instance_root_key(self):
        return self._root_key

    @classmethod
    def class_structure(cls):
        data_model_class = cls.data_model_class()
        if data_model_class:
            return getattr(data_model_class, CONTAINERS_STRUCTURE_FIELD, None)

    def instance_structure(self):
        return self._structure

    # Validation
    @classmethod
    def raise_if_no_structure(cls, structure):
        if not structure:
            raise ImproperlyConfigured(
                '%s: ObjectContainer subclasses require structure to be defined' %
                cls.__class__.__name__)

    @classmethod
    def raise_if_source_field_not_in_structure(cls, source, structure):
        for field_name, repr_class in list(source.items()):
            if field_name not in structure:
                raise AttributeError(
                    '%s: field declared in the structure is missing in the target class: %s' %
                    (cls.__class__.__name__, field_name))

    def instance_validate_source(self, content):
        """ Метод *экземпляра класса* используется при валидации при инициализации объекта. При этом
        в качестве источника ему передается словарь ключевых аргументов. На данном этапе допускается
        передача не всех аргемнтов и важно только, чтобы все переданные аргументы были в составе
        объявленной структуры. """

        structure = self.structure()
        self.raise_if_no_structure(structure)
        self.raise_if_source_field_not_in_structure(content, structure)

    @classmethod
    def class_validate_source(cls, content):
        """ Метод *класса* используется для начальной валидации транспортного представления
        конструкторами и проверяет только наличие корневого ключа в исходном словаре. Дальнейшая
        валидация осуществляется методом *экземпляра класса*.
        """
        structure = cls.structure()
        cls.raise_if_no_structure(structure)

        root_key = cls.root_key()
        if root_key and (root_key not in content):
            raise KeyError(
                '%s: root key declared is missing in the source: %s' %
                (cls.__class__.__name__, root_key))

    @classmethod
    def class_to_transport(cls, content):
        """ Для всех полей, указанных в структуре метод транспортного представления либо использует
        соответствующий класс и его методы, либо, если такой класс не указан, представляет их в
        неизменном виде.
        """
        d = {}
        structure = cls.structure()
        root_key = cls.root_key()

        for field_name, repr_class in list(structure.items()):
            content_value = getattr(content, field_name)

            # если у объекта есть модуль repr, используем его
            try:
                value_repr = content_value.repr.transport
            # если нет, передаем в неизменном виде
            # todo: валидация по типам?
            except AttributeError:
                value_repr = content_value
            except KeyError:
                value_repr = content_value

            d.update({field_name: value_repr})

        if root_key:
            d = {root_key: d}

        return d

    @classmethod
    def class_from_transport(cls, content):
        """ Все поля объекта восстаналиваются по общей логике: либо используется соответствующий
        класс и его методы, либо, если такой класс не указан, данные представляются в неизменном
        виде.
        """
        cls.class_validate_source(content)

        target_kwargs = {}
        source_kwargs = content
        structure = cls.structure()
        root_key = cls.root_key()

        if root_key:
            source_kwargs = source_kwargs[root_key]

        for field_name, repr_class in list(structure.items()):

            if repr_class:
                try:
                    value_instance = repr_class.from_transport(source_kwargs[field_name])
                except AttributeError:
                    value_instance = repr_class(source_kwargs[field_name])

            else:
                value_instance = source_kwargs[field_name]

            target_kwargs.update({field_name: value_instance})

        return cls(**target_kwargs)

    # Interface
    @classmethod
    def root_key(cls):
        return cls.class_root_key()

    @classmethod
    def structure(cls):
        return cls.class_structure()

    def update(self, other):
        """ Метод обновления. """
        structure = self.structure()

        for field_name, repr_class in list(structure.items()):

            if hasattr(other, field_name):

                if repr_class:
                    try:
                        setattr(self, field_name,
                                repr_class.from_transport(getattr(other, field_name).repr.transport))
                    except AttributeError:
                        setattr(self, field_name,
                                repr_class(getattr(other, field_name)))

                # иначе просто присваиваем значение
                else:
                    setattr(self, field_name, getattr(other, field_name))
