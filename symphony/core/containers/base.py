# -*- coding: utf-8 -*-
import simplejson as json

from symphony.core.repr import ContainerReprMixin


class BaseContainer(ContainerReprMixin, object):
    """ """
    def __init__(self, *args, **kwargs):
        super(BaseContainer, self).__init__(*args, **kwargs)

        self.validate_source = self.instance_validate_source

        self.from_transport = self.instance_from_transport
        self.to_transport = self.instance_to_transport

        self.from_json = self.instance_from_json
        self.to_json = self.instance_to_json

    # Validation
    @classmethod
    def class_validate_source(cls, content):
        raise True

    def instance_validate_source(self, content):
        raise self.class_validate_source(content)

    # Transport
    @classmethod
    def class_to_transport(cls, content):
        """ Абстрактный метод представления данных в транспортном виде. """
        return content

    @classmethod
    def class_from_transport(cls, content):
        """ Абстрактный метод восстановления данных из транспортного
        представления. """
        return content

    def instance_to_transport(self, content=None):
        return self.class_to_transport(content if content else self)

    def instance_from_transport(self, content):
        return self.class_from_transport(content)

    # JSON
    @classmethod
    def class_to_json(cls, content):
        """ Абстрактный метод представления данных в виде JSON-строки. """
        return json.dumps(cls.class_to_transport(content))

    @classmethod
    def class_from_json(cls, content):
        """ Абстрактный метод восстановления данных из JSON-строки. """
        return cls.class_from_transport(json.loads(content))

    def instance_to_json(self, content=None):
        return self.class_to_json(content if content else self)

    def instance_from_json(self, content):
        return self.class_from_json(content)

    # Interface
    @classmethod
    def validate_source(cls, content):
        return cls.class_validate_source(content)

    @classmethod
    def from_transport(cls, content):
        return cls.class_from_transport(content)

    @classmethod
    def to_transport(cls, content):
        return cls.class_to_transport(content)

    @classmethod
    def from_json(cls, content):
        return cls.class_from_json(content)

    @classmethod
    def to_json(cls, content):
        return cls.class_to_json(content)
