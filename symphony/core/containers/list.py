# -*- coding: utf-8 -*-
from symphony.core.containers.base import BaseContainer

from symphony.core.containers.defaults import (
    CONTAINERS_DATA_MODEL_DECLARATION,
    CONTAINERS_ROOT_KEY_FIELD,
    CONTAINERS_MEMBER_CLASS_FIELD
)


class ListContainer(BaseContainer, list):
    """ Контейнер типа class:`list` описывается следующим образом::

        class SampleListContainer(BaseListContainer):

            class DataModel:
                root_key = 'sample'
                member_class = SampleClass

    :param root_key: опциональный параметр, если он задан, представление контейнера будет
        включать корневой ключ::

            {'sample': [{'field_b': 'b1', 'field_a': 'a1'}, 'field_b': 'b2', 'field_a': 'a2'}, }

    :param member_class: опциональный параметр, если задан, для представления и восстановления будут
        использользоваться методы указанного класса.
    """
    def __init__(self, source=None, member_class=None, root_key=None):
        super(ListContainer, self).__init__()

        self._root_key = root_key if root_key else self.class_root_key()
        self._member_class = member_class if member_class else self.class_member_class()

        self.root_key = self.instance_root_key
        self.member_class = self.instance_member_class

        if source:
            self.validate_source(source)
            self.extend(source)

    # DataModel
    @classmethod
    def data_model_class(cls):
        return getattr(cls, CONTAINERS_DATA_MODEL_DECLARATION, None)

    @classmethod
    def class_root_key(cls):
        data_model_class = cls.data_model_class()
        if data_model_class:
            return getattr(data_model_class, CONTAINERS_ROOT_KEY_FIELD, None)

    def instance_root_key(self):
        return self._root_key

    @classmethod
    def class_member_class(cls):
        data_model_class = cls.data_model_class()
        if data_model_class:
            return getattr(data_model_class, CONTAINERS_MEMBER_CLASS_FIELD, None)

    def instance_member_class(self):
        return self._member_class

    # Validation
    def instance_validate_source(self, content):
        """ Метод *экземпляра класса* используется при валидации при инициализации объекта. Метод
        проверяет только являются ли исходные данные списком. """
        if not isinstance(content, list):
            raise TypeError(
                '%s: source type list expected, got %s' %
                (self.__class__.__name__, type(content).__name__))

    @classmethod
    def class_validate_source(cls, content):
        """ Метод *класса* используется для начальной валидации транспортного представления
        конструкторами и проверяет соответствие исходных данных формальным критериям собственной
        модели. Дальнейшая валидация осуществляется методом *экземпляра класса*.
        """
        root_key = cls.root_key()

        # корневой ключ объявлен, но исходник - СЛОВАРЬ
        if not root_key and isinstance(content, dict):
            raise TypeError(
                '%s: no root key declared but source is a dict. '
                'are you passing a representation with root key?' %
                cls.__class__.__name__)

        # корневой ключ объявлен, но исходник - НЕ словарь
        if root_key and not isinstance(content, dict):
            raise TypeError(
                '%s: root key is declared but source is not a dict. '
                'are you passing a representation without root key?' %
                cls.__class__.__name__)

        # корневой ключ объявлен, но отсутствует в исходнике
        if root_key and (root_key not in content):
            raise KeyError(
                '%s: root key declared not found in the source: %s' %
                (cls.__class__.__name__, root_key))

    @classmethod
    def class_to_transport(cls, content):
        """ Для всех элементов списка метод транспортного представления либо использует
        соответствующий класс и его методы, либо, если такой класс не указан, представляет их в
        неизменном виде.
        """
        root_key = cls.root_key()
        l = []

        for element in content[:]:
            try:
                value_repr = element.repr.transport
            except AttributeError:
                value_repr = element

            l.append(value_repr)

        if root_key:
            return {root_key: l[:]}
        else:
            return l[:]

    @classmethod
    def class_from_transport(cls, content):
        """ Все элементы списка восстаналиваются по общей логике: либо используется соответствующий
        класс и его методы, либо, если такой класс не указан, данные представляются в неизменном
        виде.
        """
        cls.validate_source(content)

        root_key = cls.root_key()
        member_class = cls.member_class()

        target = []
        source = content

        if source:
            if root_key:
                source = source[root_key]

            for number, element in enumerate(source):

                if source[number] is not None:

                    # если есть более общий метод восстановления из траноспортного
                    # представления, используем его
                    if member_class:
                        try:
                            value_instance = member_class.from_transport(source[number])
                        except AttributeError:
                            value_instance = member_class(source[number])

                    # если класса нет, передаем в неизменном виде
                    else:
                        value_instance = source[number]

                    target.append(value_instance)

        return cls(target)

    # Interface
    @classmethod
    def root_key(cls):
        return cls.class_root_key()

    @classmethod
    def member_class(cls):
        return cls.class_member_class()
