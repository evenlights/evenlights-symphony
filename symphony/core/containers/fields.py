# -*- coding: utf-8 -*-
import datetime
import simplejson as json

from symphony.core.containers.base import BaseContainer

from symphony.core.containers.defaults import FIELDS_DATETIME_GENERIC_FORMATTER, FIELDS_DATETIME_SIMPLE_FORMATTER


class BaseField(BaseContainer):
    """ Класс поля отличается от класса контейнера прежде всего тем,
    что оперирует только с транспортным представлением, не рассматривая
    сериализацию (поскольку переход от сериализации к транспортному
    представлению происходит на самом верхнем уровне).

    Также, в отличие от класса контейнера, поле является хранилищем единичного
    значения.

    Как и контейнер, поле обладает методами для представления своего значения,
    его восстановления и валидации.

    :param formatter: используется для представления в транспортном виде и
        восстановлении из него
    :param range: диапазон допустимых значений
    :param datatype: тип ожидаемых данных
    """
    sys_name = None
    default = None
    datatype = None
    formatter = None
    range = None

    def __init__(self, value=None, default=None, datatype=None, range=None, formatter=None):
        super(BaseField, self).__init__()

        if datatype:
            self.datatype = datatype

        if range:
            self.range = range

        if default:
            self.default = default

        if formatter:
            self.formatter = formatter

        self._value = value

        self.validate_datatype = self.instance_validate_datatype
        self.validate_range = self.instance_validate_range
        self.validate_format = self.instance_validate_format

    # Validation
    @classmethod
    def class_validate_source(cls, source):

        # значение не входит в диапазон, возвращаем False
        if cls.datatype:
            if not cls.class_validate_datatype(source):
                return False

        # формат не совпадает, возвращаем False
        if cls.formatter:
            cls.class_validate_format(source)
            return False

        # значение не входит в диапазон, возвращаем False
        if cls.range:
            if not cls.class_validate_range(source):
                return False

        return True

    @classmethod
    def class_validate_datatype(cls, content):
        """ Валидация типа данных имеет смысл в том случае, если этот тип
        данных может быть восстановлен сериализатором, как например,
        целочисленные занчения. """
        return True

    @classmethod
    def class_validate_range(cls, content):
        """" Валидация диапазона. """
        return True

    @classmethod
    def class_validate_format(cls, content):
        """ Валидация формата относится к таким случаям, как восстановление
        объекта даты из строки или валидация при помощи регулярных выражений
        или дргих механизмов с аналогичной логикой. """
        return True

    # методы объекта по умолчанию возвращают методы класса
    def instance_validate_source(self, source):
        return self.class_validate_source(source)

    def instance_validate_datatype(self, content):
        return self.class_validate_datatype(content)

    def instance_validate_range(self, content):
        return self.class_validate_range(content)

    def instance_validate_format(self, content):
        return self.class_validate_format(content)

    # Interface
    @classmethod
    def validate_datatype(cls, content):
        return cls.class_validate_datatype(content)

    @classmethod
    def validate_range(cls, content):
        return cls.class_validate_range(content)

    @classmethod
    def validate_format(cls, content):
        return cls.class_validate_format(content)


class GenericDateTimeField(BaseField):
    """ Общее поле даты и времени с точностью до микросекунд::

        2015-10-30 13:22:39.469000
    """
    formatter = FIELDS_DATETIME_GENERIC_FORMATTER
    datatype = datetime.datetime

    @classmethod
    def class_validate_format(cls, content):
        try:
            cls.datatype.strptime(content, cls.formatter)
            return True
        except ValueError:
            return False

    @classmethod
    def class_from_transport(cls, content):
        return cls.datatype.strptime(content, cls.formatter)

    @classmethod
    def class_to_transport(cls, content):
        return content.strftime(cls.formatter)

    # в методах объекта используем переданные во время инициализации
    # параметрами вместо свойств класса
    def instance_validate_format(self, content):
        try:
            self.datatype.strptime(content, self.formatter)
            return True
        except ValueError:
            return False

    def instance_from_transport(self, content):
        return self.datatype.strptime(content, self.formatter)

    def instance_to_transport(self, content=None):
        c = content if content else self._value
        return c.strftime(self.formatter)


class SimpleDateTimeField(GenericDateTimeField):
    """ Упрощенное поле даты и времени с точностью до секунд::

        2015-10-30 13:22:39
    """

    formatter = FIELDS_DATETIME_SIMPLE_FORMATTER


class IntegerField(BaseField):
    datatype = int

    @classmethod
    def class_validate_datatype(cls, content):
        try:
            int(content)
            return True
        except ValueError:
            return False

    @classmethod
    def class_from_transport(cls, content):
        return int(content)

    @staticmethod
    def as_regex_group():
        return r'(\d+)'


class FloatField(BaseField):
    datatype = float

    @classmethod
    def class_validate_datatype(cls, content):
        try:
            float(content)
            return True
        except ValueError:
            return False

    @classmethod
    def class_from_transport(cls, content):
        return float(content)

    @staticmethod
    def as_regex_group():
        return r'(\d+\.\d+)'


class BooleanField(BaseField):
    datatype = bool
    range = [1, 0, True, False]

    @classmethod
    def class_validate_range(cls, content):
        return content in cls.range

    @classmethod
    def class_from_transport(cls, content):
        return cls.datatype(content)


class StringField(BaseField):
    datatype = str

    @classmethod
    def class_validate_datatype(cls, content):
        if not isinstance(content, str):
            return False
        try:
            cls.datatype(content)
            return True
        except UnicodeDecodeError:
            return False

    @staticmethod
    def as_regex_group():
        return r'([a-zA-Z0-9._-]+)'


class JSONStringField(BaseField):
    datatype = json

    @classmethod
    def class_validate_datatype(cls, content):
        if not isinstance(content, str):
            return False

        try:
            cls.datatype.loads(content)
            return True
        except cls.datatype.JSONDecodeError:
            return False

    @classmethod
    def class_from_transport(cls, content):
        return cls.datatype.loads(content)

    @classmethod
    def class_to_transport(cls, content):
        try:
            return content.repr.json
        except AttributeError:
            return cls.datatype.dumps(content)


class GenericListField(BaseField):
    """ Общий список, допускающий элементы произвольного типа. """
    datatype = list

    @classmethod
    def class_validate_datatype(cls, content):
        return isinstance(content, cls.datatype)


class GenericDictField(BaseField):
    """ Общий список, допускающий элементы произвольного типа. """
    datatype = dict

    @classmethod
    def class_validate_datatype(cls, content):
        return isinstance(content, cls.datatype)
