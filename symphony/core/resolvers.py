# -*- coding: utf-8 -*-
import re


def resolve_name(name):
    """ Function resolves field names with comparison into normal field names.

    Resolution rules are:

    - ``__lt``, ``__lte``, ``__gt``, ``__gte`` and will be removed, e.g: ``field__gte -> field``
    - ``-`` mark will be removed, e.g.: ``-field -> field``
    - ``__`` sign will be resolved into a ``.`` getter, e.g.: ``level1__level2 -> level1.level2``
    """
    ns = name.split('__')
    if ns[len(ns) - 1] in ['lt', 'lte', 'gt', 'gte']:
        del ns[len(ns) - 1]
    if ns[0][0] == '-':
        ns[0] = re.sub(r'-', '', ns[0])
    return '.'.join(ns)


def resolve_comparison(key, value):
    """ :returns: :func:`lambda` function or ``True``

    Function is used to resolve (as it reads) comparison postfixes ``__lt``,
    ``__lte``, ``__gt``, ``__gte`` into corresponding functions. If no postfix
    is given, comparison will always return ``True``
    """
    ns = key.split('__')
    last = ns[len(ns) - 1]

    if last == 'lt':
        return lambda v: v < value
    elif last == 'lte':
        return lambda v: v <= value
    elif last == 'gt':
        return lambda v: v > value
    elif last == 'gte':
        return lambda v: v >= value
    else:
        return lambda v: v == value
