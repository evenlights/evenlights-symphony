# -*- coding: utf-8 -*-

KEY_LENGTH = 32

COLOR_HEADER = 'blue'

DEFAULT_CODEC = 'utf-8'

SYS_NAME_PROPERTY = 'sys_name'

INFRA_DECLARATION_CLASS = 'Infrastructure'
INFRA_NAME = 'app'
INFRA_COMPONENT_FEED = 'components'
CORE_FIELD = 'core'

REPR_NAME = 'repr'
REPR_TRANSPORT = 'transport'
