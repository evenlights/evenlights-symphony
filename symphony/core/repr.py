# -*- coding: utf-8 -*-
from kitchen.text.converters import to_unicode, to_str


class ComponentReprMixin(object):

    def __repr_name__(self):
        self_repr = [self.name]

        if self.mount_point:
            self_repr.insert(0, str(self.mount_point))

        return '.'.join(self_repr)

    def __str__(self):
        return to_unicode(self.__repr_name__())

    def __unicode__(self):
        return to_unicode(self.__repr_name__())

    def __repr__(self):
        return to_unicode('<%s: %s>' % (self.__class__.__name__, self.name))


class ContainerReprManager(object):

    def __init__(self, target):
        self.target = target

    def get_repr(self, name):
        return getattr(self.target, 'to_%s' % name)()

    @property
    def transport(self):
        return self.get_repr('transport')

    @property
    def json(self):
        return self.get_repr('json')


class ContainerReprMixin(object):

    def __str__(self):
        return to_unicode(self.to_transport())

    def __unicode__(self):
        return to_unicode(self.to_transport())

    def __repr__(self):
        return to_unicode('<%s: %s>' % (self.__class__.__name__, self.to_transport()))

    def __init__(self, *args, **kwargs):
        super(ComponentReprMixin, *args, **kwargs)
        self.repr = ContainerReprManager(self)


def dict_repr(dct):
    return ', '.join('%s=%s' % (k, dct[k]) for k in dct)


def list_repr(lst):
    return ', '.join(lst)
