# -*- coding: utf-8 -*-
from symphony.core.exceptions import ImproperlyConfigured
from symphony.routers.handlers import BaseHandler
from symphony.ci.handlers import SignalHandler

from symphony.protocols.smp.defaults import (
    SMP_CONTENT_NONE, SMP_CONTENT_STRING,
    SMP_CODE_OK, SMP_CODE_CREATED, SMP_CODE_ACCEPTED, SMP_CODE_NO_CONTENT,

    SMP_CODE_BAD_REQUEST, SMP_CODE_FORBIDDEN, SMP_CODE_NOT_FOUND, SMP_CODE_CONFLICT,
    SMP_CODE_UNSUPPORTED_MEDIA_TYPE,

    SMP_CODE_ERROR, SMP_CODE_SERVICE_UNAVAILABLE, SMP_CODE_GATEWAY_TIMEOUT,
    SMP_CODES_HR, SMP_INFO_CODES, SMP_ERROR_CODES
)

WORKER_CI_SOCKET_NAMES = ['ci_sock', 'ci_socket']
BROKER_CI_SOCKET_NAMES = ['ci_fe', 'ci_frontend']


class SMPSignalHandler(SignalHandler):
    """ Стандартный обработчик ИУ SMP-протокола несколько расширяет арсенал стандартного обработчика
    ИУ с учетом специфики протокола.

    **Обработка ошибок**

    Поскольку обработка ошибок непосредственно связана с ответами, ``SMP``-обработчик несколько
    автоматизирует процесс подготовки (:meth:`prepare`) и обработки (:meth:`handle`), возвращая ответы
    с *первой* ошибки во внутренней шине сообщений обработчика и кодами ``[400] BAD REQUEST`` и ``[500]
    ERROR``, соответственно. Благодаря этому соответствующие методы могут породить сообщение об ошибке и
    вернуть управление, не заботясь о ``FINAL``-сообщении. Такое поведение может быть изменено путем
    очистки внутренней шины, например, если требуется вернуть персонализированный ответ.

    **Ответы**

    Результатом обработки может быть код, код и строка, контейнер, либо поток. Универсальная процедура,
    в которой единый объект ответа будет включать все сообщения и коды, которые необходимо вернуть,
    теоретически возможна, однако на данный момент не входит в стандартный комплект.

    *200+: информационные ответы*

    Инфомационные ответы используют интерфейс метода :meth:`info_response`, несколько упрощая его
    в виде фиксированных кодов. Тип контента по умолчанию равен ``SMP_CONTENT_STRING`` с расчетом на
    то, что большая часть таких сообщений содержит лишь строковой ответ.

    *400+: ответы ошибок*

    Ответы ошибок аналогичны информационным ответам с той разницей, что используют интерфейс метода
    :meth:`info_response`. Тип контента по умолчанию также равен ``SMP_CONTENT_STRING``.
    """

    # Handler
    def _prepare(self):
        """ В рамках протокола ``SMP`` важно вернуть клиенту сообщение, если в ходе подготовки
        возникли ошибки. По умолчанию метод :meth:`prepare` при наличии ошибок возвращает ответ
        с кодом ``[400] BAD REQUEST``.
        """
        self.prepare()
        if self.messages.errors:
            err_msg = self.messages.internal.first_error_content
            self.bad_request_response(msg=err_msg)

    def _handle(self):
        """ Обработчики протокола [SMP] исползуют метод-обертку для автоматической отправки
        соответствующего сообщения при возниконевении ошибки во внутренней шине компонента.
        Благодаря этому основной метод :meth:`handle` может просто породить сообщение об ошибке
        и вернуть управление, не заботясь об оповещении получателя.

        По умолчанию используется код ответа ``[500] ERROR``.

        Для стандартных обработчиков протокола [SMP] действуте соглашение о том, что обработка
        прерывается при при первой ошибке, хотя ничто не мешает создать другой тип обработчика,
        который, например, будет информировать пользователя обо всех ошибках, возникших в ходе
        валидации (такая логика может быть актуальной при ресурсоемких операциях).

        Обработчик может выполнять действия самостоятельно, либо делегировать всю обработку или ее
        часть другому блоку приложения либо удаленному узлу: при этом для автоматического ответа
        возникшие ошибки должны должны приводить к появлению соответствующих сообщений (дублирующих
        сообщения об ошибках других компонентов либо произвольной формы) в шине самого обработчика.

        Если необходима отправка персонализированных сообщений, метод :meth:`handle` должен очищать
        внутреннюю шину, чтобы избежать повторной отправки ``FINAL`-сообщений.
        """
        res = self.handle()

        if self.messages.errors:
            err_msg = self.messages.internal.first_error_content
            self.error_response(msg=err_msg)

        return res

    # Responses
    @staticmethod
    def get_response_message(content=None, code=None, truncate=80):
        """
        :param content:
        :param code:
        :param truncate:
        :return:
        """
        msg = ''
        if code:
            msg += '[%d] %s' % (code, SMP_CODES_HR[code])

        if content:
            msg += ': %s' % (content[:truncate] + '...' if len(content) > truncate else content)
        return msg

    def get_code_level(self, code=None):
        """
        :param code:
        :return:
        """
        if code:
            if code in SMP_INFO_CODES:
                return 'info'

            elif code in SMP_ERROR_CODES:
                return 'error'
            else:
                self._raise('unable to determine code level: %s' % code, exception_class=KeyError)

    def partial_response(self, content=None, code=None, content_type=SMP_CONTENT_NONE):
        """
        :param content:
        :param code:
        :param content_type:
        :return:
        """
        raise NotImplemented

    def final_response(self, content=None, code=None, content_type=SMP_CONTENT_NONE):
        raise NotImplemented

    def response(self, content=None, code=None, content_type=SMP_CONTENT_NONE, final=True):
        """
        :param content:
        :param code:
        :param content_type:
        :param final:
        :return:
        """
        # message bus
        level = self.get_code_level(code)
        message = self.get_response_message(content=content, code=code)
        self.messages.emit(level, message)

        # protocol
        if final:
            self.final_response(content=content, code=code, content_type=content_type)
        else:
            self.partial_response(content=content, code=code, content_type=content_type)

    def info_response(self, msg=None, code=SMP_CODE_OK, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param code:
        :param content_type:
        :param final:
        :return:
        """
        self.response(content=msg, code=code, content_type=SMP_CONTENT_STRING, final=final)

    def warning_response(self, msg=None, code=None, content_type=SMP_CONTENT_STRING, final=False):
        """
        Предупреждения могут быть направлены клиенту при помощи метода :meth:`partial_response` и
        контейнера :class:`GenericMessage`.
        :param msg:
        :return:
        """

        self.response(content=msg, code=300, content_type=SMP_CONTENT_STRING, final=final)

    def error_response(self, msg=None, code=SMP_CODE_ERROR, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param code:
        :param content_type:
        :param final:
        :return:
        """
        self.response(content=msg, code=code, content_type=SMP_CONTENT_STRING, final=final)

    # 200: успешные ответы
    # В случае успешного ответа сложно предсказать, будет ли ответ
    # окончательным, а также его тип, поэтому эти тра параметра необходимо
    # указать в явном виде.
    def ok_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.info_response(msg=msg, code=SMP_CODE_OK, content_type=content_type, final=final)

    def created_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.info_response(msg=msg, code=SMP_CODE_CREATED, content_type=content_type, final=final)

    def accepted_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.info_response(msg=msg, code=SMP_CODE_ACCEPTED, content_type=content_type, final=final)

    def no_content_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.info_response(msg=msg, code=SMP_CODE_NO_CONTENT, content_type=content_type, final=final)

    # 400-500: ответы ошибок
    # Поскольку выполнение прерыается при первой ошибке, используется сигнал
    # FINAL. Содержание ошибки передается в виду строки.
    def bad_request_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.error_response(msg=msg, code=SMP_CODE_BAD_REQUEST, content_type=content_type, final=final)

    def forbidden_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.error_response(msg=msg, code=SMP_CODE_FORBIDDEN, content_type=content_type, final=final)

    def not_found_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.error_response(msg=msg, code=SMP_CODE_NOT_FOUND, content_type=content_type, final=final)

    def conflict_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.error_response(msg=msg, code=SMP_CODE_CONFLICT, content_type=content_type, final=final)

    def unsupported_media_type_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.error_response(
            msg=msg, code=SMP_CODE_UNSUPPORTED_MEDIA_TYPE, content_type=content_type, final=final)

    def service_unavailable_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.error_response(
            msg=msg, code=SMP_CODE_SERVICE_UNAVAILABLE, content_type=content_type, final=final)

    def gateway_timeout_response(self, msg=None, content_type=SMP_CONTENT_STRING, final=True):
        """
        :param msg:
        :param content_type:
        :param final:
        :return:
        """
        self.error_response(
            msg=msg, code=SMP_CODE_GATEWAY_TIMEOUT, content_type=content_type, final=final)


class SMPWorkerHandler(SMPSignalHandler):
    """ """
    def get_ci_socket(self):
        app = self.core.shared.get('app')
        socket_registry = app.runtime['transport']['sockets']
        for name in WORKER_CI_SOCKET_NAMES:
            if name in socket_registry:
                return socket_registry[name]

        self._raise('no control interface socket registered', exception_class=RuntimeError)

    # Responses
    def partial_response(self, content=None, code=None, content_type=SMP_CONTENT_NONE):
        ci_socket = self.get_ci_socket()
        ci_socket.worker_partial(
            content_type=content_type,
            transaction_id=self.runtime.object.transaction_id,
            code=code,
            client_id=self.runtime.object.address,
            content=content
        )

    def final_response(self, content=None, code=None, content_type=SMP_CONTENT_NONE):
        ci_socket = self.get_ci_socket()
        ci_socket.worker_final(
            content_type=content_type,
            transaction_id=self.runtime.object.transaction_id,
            code=code,
            client_id=self.runtime.object.address,
            content=content
        )


class SMPBrokerHandler(SMPSignalHandler):
    """ """
    def get_ci_socket(self):
        app = self.core.shared.get('app')
        socket_registry = app.runtime['transport']['sockets']
        for name in BROKER_CI_SOCKET_NAMES:
            if name in socket_registry:
                return socket_registry[name]

    # Responses
    def partial_response(self, content=None, code=None, content_type=SMP_CONTENT_NONE):
        ci_fe = self.get_ci_socket()
        ci_fe.client_partial(
            address=self.runtime['object'].address,
            content_type=content_type,
            transaction_id=self.runtime['object'].transaction_id,
            code=code,
            content=content
        )

    def final_response(self, content=None, code=None, content_type=SMP_CONTENT_NONE):
        ci_fe = self.get_ci_socket()
        ci_fe.client_final(
            address=self.runtime['object'].address,
            content_type=content_type,
            transaction_id=self.runtime['object'].transaction_id,
            code=code,
            content=content
        )


class SMPMessageHandler(BaseHandler):
    """ """
    mapping_key = 'content_type'
    content_type = None

    def __init__(self, content_type=None, **kwargs):
        super(SMPMessageHandler, self).__init__(**kwargs)

        if content_type is not None:
            self.content_type = content_type

        if self.content_type is None:
            self._raise(
                'SMP content handler cannot initialize without content_type',
                exception_class=ImproperlyConfigured)


WorkerSignalHandler = SMPWorkerHandler
BrokerSignalHandler = SMPBrokerHandler
