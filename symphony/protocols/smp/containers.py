# -*- coding: utf-8 -*-
from kitchen.text.converters import to_unicode, to_str

from symphony.core.repr import ContainerReprManager
from symphony.ci.containers import GenericSignal

from symphony.protocols.smp.defaults import (
    SMP_SUB_CLIENT, SMP_SUB_WORKER, SMP_SUB_LIFECYCLE,
    SMP_TYPE_REQUEST, SMP_TYPE_PARTIAL, SMP_TYPE_FINAL, SMP_TYPE_LC_HEARTBEAT,
    SMP_CONTENT_SIGNAL,
    SMP_SUB_HR, SMP_TYPES_HR
)


class SMPMessageReprMixin(object):
    def __init__(self, *args, **kwargs):
        super(SMPMessageReprMixin, *args, **kwargs)
        self.repr = ContainerReprManager(self)

    def __repr__(self):
        return to_unicode(
            '<SMP/%s: %s>' % (SMP_SUB_HR[self.sub], SMP_TYPES_HR[self.message_type])
        )


class SMPClientMessage(SMPMessageReprMixin, object):
    """ Обмен сообщениями субпротокола ``SMP/Client`` происходит между клиентом и брокером или сервером.

    В большинстве случаев сообщения ``SMP/Client`` имеют тип ``REQUEST``. Однако в случае STREAM-запросов
    могут использоваться и другие типы, такие как ``PARTIAL/FINAL``. В этом лучае для идентификации потока
    используется идентификатор транзакции первичного сообщения ``REQUEST``.

    Сообщение ``REQUEST``::

        Frame 0: "SMPC01" (шесть байт, идентификатор субпротокола SMP/Client v0.1)
        Frame 1: message type (один байт, REQUEST)
        Frame 2: transaction id (строка, идентификатор операции)
        Frame 3: content type (один байт, SIGNAL/STREAM)
        Frame 4: content (бинарный формат)

    Сообщения ``PARTIAL/FINAL``::

        Frame 0: "SMPC01" (шесть байт, идентификатор субпротокола SMP/Client v0.1)
        Frame 1: message type (один байт, PARTIAL/FINAL)
        Frame 2: transaction id (строка, идентификатор операции)
        Frame 3: content type (один байт, NONE, STRING, MESSAGE, SIGNAL, FILE, STREAM)
        Frame 4: code (два байт,а код ответа)
        Frame 5: content (бинарный формат)

    .. py:attribute:: address

        Используется брокером, получающего сообщение от клиента.
    """
    sub = SMP_SUB_CLIENT

    def __init__(self, address=None, message_type=SMP_TYPE_REQUEST, transaction_id=None,
                 content_type=SMP_CONTENT_SIGNAL, code=None, content=None):
        super(SMPClientMessage, self).__init__()
        self.address = address

        self.message_type = message_type

        self.transaction_id = transaction_id
        self.content_type = content_type

        self.code = code
        self.content = content

    def to_transport(self):
        transport_message = []

        # address if present
        if self.address:
            transport_message += [self.address]

        # Common
        transport_message += [self.sub, self.message_type, self.transaction_id or '', self.content_type]

        # для типов PARTIAL и FINAL код  должен присутствовать в любом случае
        if self.message_type in [SMP_TYPE_PARTIAL, SMP_TYPE_FINAL]:
            transport_message += [self.code or '']

        # контент должен присутствовать в любом случае
        transport_message += [self.content or '']

        return transport_message


class SMPLifeCycleMessage(SMPMessageReprMixin, object):
    """ Обмен сообщениями субпротокола ``SMP/LifeCycle`` происходит между воркерами и брокером.

    Субпротокол ``SMP/LifeCycle`` используется для контроля состояния жизненного цикла приложений:
    инициализации, завершения, а также двустороннего  сердебиения между воркерами и брокером,
    благодаря чему последний имеет возможность контролировать состояние воркеров, а воркеры -- соединение
    с брокером.

    Сообщение ``INIT`` передается от воркера к брокеру и означает готовность к работе. После этого начинается
    процесс двустороннего сердцебиения::

        Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
        Frame 1: message type (один байт, INIT)

    Сообщения ``HEARTBEAT``::

        Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
        Frame 1: message type (один байт, HEARTBEAT)

    Сообщения ``RESET`` используются брокером для прерывания текущей задачи брокера::

        Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
        Frame 1: message type (один байт, RESET)

    Сообщение ``SHUTDOWN`` брокер отправляет воркерам. Получив его, воркеры отвечают сигналом ``SHUTDOWN``,
    подтверждая свое завершение::

        Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
        Frame 1: message type (один байт, SHUTDOWN)
    """
    sub = SMP_SUB_LIFECYCLE

    def __init__(self, address=None, message_type=SMP_TYPE_LC_HEARTBEAT):
        super(SMPLifeCycleMessage, self).__init__()
        self.address = address
        self.message_type = message_type

    def to_transport(self):
        transport_message = []

        # address if present
        if self.address:
            transport_message += [self.address]

        # Common
        transport_message += [self.sub, self.message_type]

        return transport_message


class SMPWorkerMessage(SMPMessageReprMixin, object):
    """ Субпротокол ``SMP/Worker`` используется, в основном, для содержательной коммуникации (передачи
    сигналов и ответов) между брокером и воркерами. Здесь также используются все три типа сигналов:
    ``REQUEST``, ``PARTIAL`` и ``FINAL``.

    В случае обычного запроса в рамках одного сообщения брокер получает ноль или более ``PARTIAL``-сигналов
    и один ``FINAL``-сигнал.

    При поточном запросе брокер отправляет запрос с типом контента ``STREAM``, после чего воркер должен
    быть готов принять ноль или более ``PARTIAL``-сигналов и один ``FINAL``-сигнал. Ответ также содержит
    ноль или более ``PARTIAL``-сигналов и один ``FINAL``-сигнал.

    Сообщение ``REQUEST``::

        Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
        Frame 1: message type (один байт, REQUEST)
        Frame 2: transaction id (строка, идентификатор операции)
        Frame 3: content type (один байт, SIGNAL/STREAM)
        Frame 4: client id (строка, идентификатор клиента)
        Frame 5: content (бинарный формат)

    Сообщения ``PARTIAL/FINAL``::

        Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
        Frame 1: message type (один байт, PARTIAL/FINAL)
        Frame 2: transaction id (строка, идентификатор операции)
        Frame 3: content type (один байт, NONE, STRING, MESSAGE, SIGNAL, FILE)
        Frame 4: code (два байт,а код ответа)
        Frame 5: client id (строка, идентификатор клиента)
        Frame 6: content (бинарный формат)
    """
    sub = SMP_SUB_WORKER

    def __init__(self, address=None, client_id=None, message_type=SMP_TYPE_FINAL, transaction_id=None,
                 content_type=None, code=None, content=None):
        super(SMPWorkerMessage, self).__init__()

        self.address = address
        self.client_id = client_id

        self.message_type = message_type
        self.transaction_id = transaction_id
        self.content_type = content_type

        self.code = code
        self.content = content

    def to_transport(self):
        transport_message = []

        # address if present
        if self.address:
            transport_message += [self.address]

        # Common
        transport_message += [self.sub, self.message_type, self.transaction_id or '', self.content_type]

        # для типов PARTIAL и FINAL код  должен присутствовать в любом случае
        if self.message_type in [SMP_TYPE_PARTIAL, SMP_TYPE_FINAL]:
            transport_message += [self.code or '']

        transport_message += [self.client_id or '']

        # контент должен присутствовать в любом случае
        transport_message += [self.content or '']

        return transport_message


class SMPSignal(GenericSignal):
    """ """

    def __init__(self, address=None, transaction_id=None, *args, **kwargs):
        super(SMPSignal, self).__init__(*args, **kwargs)
        self.address = address
        self.transaction_id = transaction_id
