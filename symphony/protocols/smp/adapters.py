# -*- coding: utf-8 -*-
from symphony.adapters.sockets import ZMQSocketAdapter
from symphony.protocols.smp.parsers import SMPParserBase
from symphony.protocols.smp.containers import SMPClientMessage, SMPLifeCycleMessage, SMPWorkerMessage

from symphony.protocols.smp.defaults import (
    SMP_TYPE_REQUEST, SMP_TYPE_PARTIAL, SMP_TYPE_FINAL,
    SMP_TYPE_LC_INIT, SMP_TYPE_LC_HEARTBEAT, SMP_TYPE_LC_SHUTDOWN, SMP_TYPE_LC_RESET,
)


class SMPSocketAdapter(SMPParserBase, ZMQSocketAdapter):
    """
    Контроллер протокола является абстракцией уровня сообщений и реализует
    методы приема, отправки и валидации сообщений в рамках логики
    соответствующего протокола.
    """

    # Client/*
    def client_send(self, message_type, transaction_id, content_type, address=None, code=None, content=None):
        protocol_msg = SMPClientMessage(
            message_type=message_type, transaction_id=transaction_id,
            content_type=content_type, code=code, content=content,
            address=address
        )
        self.write(protocol_msg.repr.transport)

    def client_request(self, transaction_id, content_type, content=None):
        self.client_send(SMP_TYPE_REQUEST, transaction_id,
                         content_type, content=content)

    def client_partial(self, transaction_id, content_type, code=None, content=None, address=None):
        self.client_send(SMP_TYPE_PARTIAL, transaction_id, content_type,
                         code=code, content=content, address=address)

    def client_final(self, transaction_id, content_type, code=None, content=None, address=None):
        self.client_send(SMP_TYPE_FINAL, transaction_id, content_type,
                         code=code, content=content, address=address)

    def client_recv(self):
        """
        :return: сообщение протокола, либо исходное сообщение в случае ошибки
        """
        # читаем из сокета
        wire_msg = self.read()
        if self.messages.internal.errors:
            return wire_msg

        # парсер
        protocol_message = self.get_client_message(wire_msg)

        if self.messages.internal.errors:
            return wire_msg

        return protocol_message

    # LifeCycle/*
    def lifecycle_send(self, message_type, address=None):
        protocol_msg = SMPLifeCycleMessage(
            message_type=message_type, address=address
        )
        self.write(protocol_msg.repr.transport)

    def lifecycle_init(self):
        self.lifecycle_send(message_type=SMP_TYPE_LC_INIT)

    def lifecycle_heartbeat(self, address=None):
        self.lifecycle_send(
            message_type=SMP_TYPE_LC_HEARTBEAT, address=address
        )

    def lifecycle_reset(self, address=None):
        self.lifecycle_send(
            message_type=SMP_TYPE_LC_RESET, address=address
        )

    def lifecycle_shutdown(self, address=None):
        self.lifecycle_send(
            message_type=SMP_TYPE_LC_SHUTDOWN,
            address=address
        )

    def lifecycle_recv(self):
        """
        :return: сообщение протокола, либо исходное сообщение в случае ошибки
        """
        # читаем из сокета
        wire_msg = self.read()

        if self.messages.internal.errors:
            return wire_msg

        # парсер
        protocol_message = self.get_lifecycle_message(wire_msg)

        if self.messages.internal.errors:
            return wire_msg

        return protocol_message

    # Worker
    def worker_send(self, message_type, content_type, transaction_id=None, code=None, client_id=None,
                    content=None, address=None):
        protocol_msg = SMPWorkerMessage(
            message_type=message_type, transaction_id=transaction_id,
            content_type=content_type, code=code, client_id=client_id,
            content=content,
            address=address
        )
        self.write(protocol_msg.repr.transport)

    def worker_request(self, content_type, transaction_id=None, client_id=None, content=None, address=None):
        self.worker_send(
            message_type=SMP_TYPE_REQUEST,
            transaction_id=transaction_id,
            content_type=content_type,
            client_id=client_id,
            content=content,
            address=address
        )

    def worker_partial(self, content_type, transaction_id=None, code=None, client_id=None, content=None,
                       address=None):
        self.worker_send(
            message_type=SMP_TYPE_PARTIAL,
            transaction_id=transaction_id,
            content_type=content_type,
            code=code,
            client_id=client_id,
            content=content,
            address=address
        )

    def worker_final(self, content_type, transaction_id=None, code=None, client_id=None, content=None,
                     address=None):
        self.worker_send(
            message_type=SMP_TYPE_FINAL,
            transaction_id=transaction_id,
            content_type=content_type,
            code=code,
            client_id=client_id,
            content=content,
            address=address
        )

    def worker_recv(self, source=None):
        # читаем из сокета
        wire_msg = self.read()

        if self.messages.internal.errors:
            return wire_msg

        # парсер
        protocol_message = self.get_worker_message(wire_msg)

        if self.messages.internal.errors:
            return wire_msg

        return protocol_message
