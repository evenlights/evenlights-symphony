# -*- coding: utf-8 -*-
from symphony.routers.components import Router


DEFAULT_CONTENT_HANDLER = 'default'


class SMPMessageRouter(Router):
    """ Идея ``маршрутизатора сообщений`` состоит в том, чтобы разным типам
    [сообщений протокола] могли быть сопоставлены [обработчики] с различным
    поведением. В качестве критерия маршрутизации используется типа контента
    сообщения.

    Таким образом, мэппинг обработчиков выглядит прмерно следующим образом::

        {
            'default':          # обработчик по умолчанию
            0x01: handler1,     # прочие обработчики
            0x02: handler2',
            ...
        }

    Обработчик ``default`` используется для типов контента, не найденых в
    мэппинге.
    """

    name = 'smp_router'
    mapping_key = 'content_type'

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(SMPMessageRouter, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )

        if DEFAULT_CONTENT_HANDLER not in self.mapping:
            self.messages.warning('no default message handler')

        return self

    def handle(self, obj):
        """ Метод передает данные соответствующему обработчику, при условии, что
        таковой найден. """
        handler_name = self.get_handler_id(obj)
        self.messages.debug('routing id: %s' % handler_name)

        try:
            handler = self.mapping[handler_name]

        except KeyError:
            self.messages.debug(
                'no direct correspondence for %s, trying default' % handler_name
            )
            try:
                handler = self.mapping[DEFAULT_CONTENT_HANDLER]
            except KeyError:
                self.messages.error('handler not found: %s' % handler_name)
                return

        self.handler_call(handler, obj)

    def disable_handler_repr(self):
        """
        Отключает repr для всех обработчиков сообщений.
        :return:
        """
        for content_type in self.mapping:
            self.mapping[content_type]\
                .messages.runtime['settings']['append_name'] = False
