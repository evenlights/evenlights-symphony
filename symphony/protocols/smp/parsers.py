# -*- coding: utf-8 -*-
from symphony.protocols.parsers import ProtocolParserBase
from symphony.protocols.errors import ProtocolViolationError
from symphony.protocols.smp.containers import (
    SMPClientMessage, SMPLifeCycleMessage, SMPWorkerMessage
)
from symphony.protocols.smp.defaults import (
    SMP_SUB_CLIENT, SMP_SUB_LIFECYCLE, SMP_SUB_WORKER, SMP_SUB_ALL, SMP_SUB_HR,
    SMP_TYPE_REQUEST, SMP_TYPES_COMMON, SMP_TYPES_LIFECYCLE, SMP_TYPES_ALL, SMP_TYPES_HR,

    ADDRESS_LENGTH, TRANSACTION_ID_LENGTH, CLIENT_ID_LENGTH,
)


class SMPParserBase(ProtocolParserBase):
    """ Парсер протокола ``SMP`` является абстракцией уровня сообщений и реализует методы приема,
    отправки и валидации сообщений.
    """

    # Internal
    def validate_protocol_message(self, msg_source):
        """ Валидация начальной сигнатуры сообщения

        :param msg_source:
        :return:
        """
        start_index = 0

        # Начальная Сигнатура

        # 0: sub
        # если первый кадр -- не субпротокол, им должен быть второй кадр
        if not any([msg_source[0] in SMP_SUB_ALL, msg_source[1] in SMP_SUB_ALL]):
            self._message_or_raise(
                'no valid sub-protocol definition: %s' % msg_source[0:3],
                exception_class=ProtocolViolationError)
            return False

        if self.get_address(msg_source):
            address = msg_source[0]
            if isinstance(address, str):
                if not self.validate_max_length(
                        address, ADDRESS_LENGTH, var_name='address',
                        err_domain='initial signature'):
                    return False
            start_index = 1

        # если присутствует адрес, сдивгаем указатель
        if msg_source[1] in SMP_SUB_ALL:
            start_index = 1

        sub = msg_source[start_index]
        sub_hr = SMP_SUB_HR[sub]

        # 1: message_type
        # не целое число
        message_type = msg_source[start_index + 1]
        if not self.validate_datatype(
                message_type, int, var_name='message_type', err_domain='initial signature'):
            return False
        message_type = int(message_type)

        # не валидный тип
        if not self.validate_range(
                message_type, SMP_TYPES_ALL, var_name='message_type', err_domain='initial signature',
                hr_dict=SMP_TYPES_HR
        ):
            return False

        message_type_hr = SMP_TYPES_HR[message_type]

        # Длина Сообщения

        if sub == SMP_SUB_LIFECYCLE:
            msg_length = start_index + 2

        elif sub == SMP_SUB_CLIENT:
            if message_type == SMP_TYPE_REQUEST:
                msg_length = start_index + 5
            else:
                msg_length = start_index + 6

        elif sub == SMP_SUB_WORKER:
            if message_type == SMP_TYPE_REQUEST:
                msg_length = start_index + 6
            else:
                msg_length = start_index + 7

        if not self.validate_length(
                msg_source, msg_length, var_name='%s message' % message_type_hr,
                err_domain='%s sub-protocol' % sub_hr):
            return False

        if sub != SMP_SUB_LIFECYCLE:

            # 2: transaction_id
            transaction_id = msg_source[start_index + 2] or None
            if transaction_id and isinstance(transaction_id, str):
                if not self.validate_max_length(
                        transaction_id, TRANSACTION_ID_LENGTH, var_name='transaction_id',
                        err_domain='%s sub-protocol' % sub_hr):
                    return False

            # 3: content type
            content_type = msg_source[start_index + 3]
            if not self.validate_datatype(
                    content_type, int, var_name='content_type', err_domain='%s sub-protocol' % sub_hr):
                return False

            # note: ограничений на типа контента не накладывается

            # note: дальнейшая валидация специфична для разлиных типов сообщений и протоколов

        return True

    def get_address(self, msg_source):
        """ Получателем считается первый фрейм, если это не определение
        субпротокола, при условии, что вторйо кадр является этим определением.
        """
        return msg_source[0] if msg_source[0] not in SMP_SUB_ALL else None

    def get_sub(self, msg_source):
        start_index = 0 if not self.get_address(msg_source) else 1
        return msg_source[start_index]

    def get_message_type(self, msg_source):
        start_index = 0 if not self.get_address(msg_source) else 1
        return msg_source[start_index + 1]

    # Interface
    def get_client_message(self, msg_source, validate=True):
        """
        Сообщение ``REQUEST``::

            Frame 0: "SMPC01" (шесть байт, идентификатор субпротокола SMP/Client v0.1)
            Frame 1: message type (один байт, REQUEST)
            Frame 2: transaction id (строка, идентификатор операции)
            Frame 3: content type (один байт, SIGNAL/STREAM)
            Frame 4: content (бинарный формат)

        Сообщения ``PARTIAL/FINAL``::

            Frame 0: "SMPC01" (шесть байт, идентификатор субпротокола SMP/Client v0.1)
            Frame 1: message type (один байт, PARTIAL/FINAL)
            Frame 2: transaction id (строка, идентификатор операции)
            Frame 3: content type (один байт, NONE, STRING, MESSAGE, SIGNAL, FILE, STREAM)
            Frame 4: code (два байт,а код ответа)
            Frame 5: content (бинарный формат)

        :param msg_source:
        :param validate:
        :return:
        """
        if validate:
            if not self.validate_protocol_message(msg_source):
                return

        start_index = 0
        kwargs = {}
        if self.get_address(msg_source):
            kwargs['address'] = msg_source[0]
            start_index = 1

        # 0: sub
        sub = msg_source[start_index]
        if not self.validate_value(
                sub, SMP_SUB_CLIENT, var_name='sub', err_domain='Client sub-protocol',
                hr_dict=SMP_SUB_HR):
            return

        # 1: message_type
        kwargs['message_type'] = int(msg_source[start_index + 1])
        if not self.validate_range(
                kwargs['message_type'], SMP_TYPES_COMMON, var_name='message_type',
                err_domain='Client sub-protocol', hr_dict=SMP_TYPES_HR):
            return

        # 2: transaction_id
        kwargs['transaction_id'] = msg_source[start_index + 2]

        # 3: content_type
        kwargs['content_type'] = int(msg_source[start_index + 3])

        # 4, 5: код и контент
        if kwargs['message_type'] == SMP_TYPE_REQUEST:
            kwargs['content'] = msg_source[start_index + 4] or None
        else:
            kwargs['code'] = msg_source[start_index + 4] or None
            if kwargs['code']:
                if not self.validate_datatype(
                        kwargs['code'], int, var_name='code', err_domain='Client sub-protocol'):
                    return False
                kwargs['code'] = int(kwargs['code'])
            kwargs['content'] = msg_source[start_index + 5] or None

        return SMPClientMessage(**kwargs)

    def get_lifecycle_message(self, msg_source, validate=True):
        """
        Сообщение LifeCycle::

            Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
            Frame 1: message type (один байт, INIT/HEARTBEAT/RESET/SHUTDOWN)

        :param msg_source:
        :param validate:
        :return:
        """
        if validate:
            if not self.validate_protocol_message(msg_source):
                return

        start_index = 0
        kwargs = {}
        if self.get_address(msg_source):
            kwargs['address'] = msg_source[0]
            start_index = 1

        # 0: sub
        sub = msg_source[start_index]
        if not self.validate_value(
                sub, SMP_SUB_LIFECYCLE, var_name='sub', err_domain='LifeCycle sub-protocol',
                hr_dict=SMP_SUB_HR):
            return

        # 1: message_type
        kwargs['message_type'] = int(msg_source[start_index + 1])
        if not self.validate_range(
                kwargs['message_type'], SMP_TYPES_LIFECYCLE, var_name='message_type',
                err_domain='LifeCycle sub-protocol', hr_dict=SMP_TYPES_HR):
            return

        return SMPLifeCycleMessage(**kwargs)

    def get_worker_message(self, msg_source, validate=True):
        """
        Сообщение ``REQUEST``::

            Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
            Frame 1: message type (один байт, REQUEST)
            Frame 2: transaction id (строка, идентификатор операции)
            Frame 3: content type (один байт, SIGNAL/STREAM)
            Frame 4: client id (строка, идентификатор клиента)
            Frame 5: content (бинарный формат)

        Сообщения ``PARTIAL/FINAL``::

            Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
            Frame 1: message type (один байт, PARTIAL/FINAL)
            Frame 2: transaction id (строка, идентификатор операции)
            Frame 3: content type (один байт, NONE, STRING, MESSAGE, SIGNAL, FILE)
            Frame 4: code (два байт,а код ответа)
            Frame 5: client id (строка, идентификатор клиента)
            Frame 6: content (бинарный формат)

        :param msg_source:
        :param validate:
        :return:
        """
        if validate:
            if not self.validate_protocol_message(msg_source):
                return

        start_index = 0
        kwargs = {}
        if self.get_address(msg_source):
            kwargs['address'] = msg_source[0]
            start_index = 1

        # 0: sub
        sub = msg_source[start_index]
        if not self.validate_value(
                sub, SMP_SUB_WORKER, var_name='sub', err_domain='Worker sub-protocol',
                hr_dict=SMP_SUB_HR):
            return

        # 1: message_type
        kwargs['message_type'] = int(msg_source[start_index + 1])
        if not self.validate_range(
                kwargs['message_type'], SMP_TYPES_COMMON, var_name='message_type',
                err_domain='Worker sub-protocol', hr_dict=SMP_TYPES_HR):
            return

        # 2: transaction_id
        kwargs['transaction_id'] = msg_source[start_index + 2]

        # 3: content_type
        kwargs['content_type'] = int(msg_source[start_index + 3])

        # 4, 5, 6: код, client_id и контент
        if kwargs['message_type'] == SMP_TYPE_REQUEST:
            kwargs['client_id'] = msg_source[start_index + 4] or None
            kwargs['content'] = msg_source[start_index + 5] or None
        else:
            kwargs['code'] = msg_source[start_index + 4] or None
            if kwargs['code']:
                if not self.validate_datatype(
                        kwargs['code'], int, var_name='code', err_domain='Worker sub-protocol'):
                    return False
                kwargs['code'] = int(kwargs['code'])

            kwargs['client_id'] = msg_source[start_index + 5] or None
            kwargs['content'] = msg_source[start_index + 6] or None

        if kwargs['client_id'] and isinstance(kwargs['client_id'], str):
            if not self.validate_max_length(
                        kwargs['client_id'], CLIENT_ID_LENGTH, var_name='client_id',
                        err_domain='Worker sub-protocol'):
                return

        return SMPWorkerMessage(**kwargs)

    def get_message(self, msg_source):
        """
        :param msg_source:
        :return:
        """
        if not self.validate_protocol_message(msg_source):
            return

        if self.get_sub(msg_source) == SMP_SUB_CLIENT:
            return self.get_client_message(msg_source, validate=False)
        elif self.get_sub(msg_source) == SMP_SUB_LIFECYCLE:
            return self.get_lifecycle_message(msg_source, validate=False)
        elif self.get_sub(msg_source) == SMP_SUB_WORKER:
            return self.get_worker_message(msg_source, validate=False)
