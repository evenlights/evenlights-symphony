# -*- coding: utf-8 -*-
from symphony.messages.compat import MessageCompatibilityMixin
from symphony.protocols.errors import ProtocolViolationError


class ProtocolParserBase(MessageCompatibilityMixin, object):
    """ Парсер протокола ``SMP`` является абстракцией уровня сообщений и реализует методы приема,
    отправки и валидации сообщений.
    """
    def get_hr_value(self, value, hr_dict):
        try:
            return hr_dict[value]
        except KeyError:
            return value

    def validate_range(self, value, rng, var_name='value', err_domain=None, hr_dict=None):
        """
        :param value:
        :param rng:
        :param var_name:
        :param err_domain:
        :param hr_dict:
        :return:
        """
        if value not in rng:

            if hr_dict:
                value = self.get_hr_value(value, hr_dict)

            msg = '%s out of expected range: %s' % (var_name, value)

            if err_domain:
                msg = '%s: %s' % (err_domain, msg)

            self._message_or_raise(msg, exception_class=ProtocolViolationError)
            return False

        return True

    def validate_max_length(self, value, length, var_name='value', err_domain=None):
        """
        :param value:
        :param length:
        :param var_name:
        :param err_domain:
        :return:
        """
        if len(value) > length:
            actual = len(value)
            msg = '%s exceeds allowed length: expected %s, actual %s' % (var_name, length, actual)

            if err_domain:
                msg = '%s: %s' % (err_domain, msg)

            self._message_or_raise(msg, exception_class=ProtocolViolationError)
            return False

        return True

    def validate_min_length(self, value, length, var_name='value', err_domain=None):
        """
        :param value:
        :param length:
        :param var_name:
        :param err_domain:
        :return:
        """
        if len(value) < length:
            actual = len(value)
            msg = '%s is less than minimum length: expected %s, actual %s' % (
                var_name, length, actual
            )

            if err_domain:
                msg = '%s: %s' % (err_domain, msg)

            self._message_or_raise(msg, exception_class=ProtocolViolationError)
            return False

        return True

    def validate_length(self, value, length, var_name='value', err_domain=None):
        return self.validate_min_length(value, length, var_name=var_name, err_domain=err_domain) \
               and self.validate_max_length(value, length, var_name=var_name, err_domain=err_domain)

    def validate_value(self, actual, expected, var_name='value', err_domain=None, hr_dict=None):
        """
        :param actual:
        :param expected:
        :param var_name:
        :param err_domain:
        :param hr_dict:
        :return:
        """
        if actual != expected:

            if hr_dict:
                actual = self.get_hr_value(actual, hr_dict)
                expected = self.get_hr_value(expected, hr_dict)

            msg = '%s mismatch: expected %s, actual %s' % (var_name, expected, actual)

            if err_domain:
                msg = '%s: %s' % (err_domain, msg)

            self._message_or_raise(msg, exception_class=ProtocolViolationError)
            return False

        return True

    def validate_datatype(self, value, datatype, var_name='value', err_domain=None):
        """
        :param value:
        :param datatype:
        :param var_name:
        :param err_domain:
        :return:
        """
        try:
            datatype(value)
        except ValueError:
            msg = 'incorrect data type for %s: %s, expected %s' % (
                var_name, value, datatype.__name__
            )

            if err_domain:
                msg = '%s: %s' % (err_domain, msg)

            self._message_or_raise(msg, exception_class=ProtocolViolationError)
            return False

        return True
