# -*- coding: utf-8 -*-
import subprocess

from symphony.core.units import GenericInfraUnit


class BaseSystemBinding(GenericInfraUnit):
    """ Базовый системный юнит для создания привязок. Требует наличия модуля
    messages. """

    def __init__(self, *args, **kwargs):
        super(BaseSystemBinding, self).__init__(*args, **kwargs)

    def _execute(self, *args):
        """ Метод для вызова субпроцессов. В случае ошибки выполнения
        порождаются соответствующие ошибки во внотренней и центральной шине
        сообщений. При этом процедура вызова может выглядеть примерно следующим
        образом::

            def archive(self, path)
                self.messages.info('executing command: archive')
                self._execute('tar', '-cvzf', path)
        """
        # создаем буффер
        cmd_buffer = []
        cmd_buffer.extend(args)

        try:
            exit_code = subprocess.call(cmd_buffer)
            if exit_code != 0:
                self.messages.error('Failed to execute command: %s, exit code: %d' %
                                    (' '.join(cmd_buffer), exit_code))
            return exit_code

        except OSError:
            self.messages.error('Command not found: %s' % cmd_buffer[0])


SystemBinding = BaseSystemBinding
