# -*- coding: utf-8 -*-
from symphony.timers.containers import GenericTimer

from symphony.pools import defaults as pool_defaults
TIMEOUT = pool_defaults.WORKER_TIMEOUT


class WorkerInfo(object):
    def __init__(self, id, class_=None, name=None, init_args=None, init_kwargs=None,
                 instance=None, timeout=TIMEOUT):
        """
        :param id: системный идентификатор воркера
        :param class_: класс воркера
        :param name: имя воркера (трэда или процесса)
        :param init_args: аргументы инициализации
        :param init_kwargs: ключевые аргументы инициализации
        :param instance: инстанциированный класс
        :param timeout: период, по истечении которого удар сердца считается
            пропущенным
        """
        super(WorkerInfo, self).__init__()
        self.id = id
        self.is_engaged = False
        self.transaction_id = None

        self.hb_timer = GenericTimer(seconds=timeout)
        self._hb_timeout = timeout

        self.class_ = class_
        self.name = name
        self.init_args = init_args
        self.init_kwargs = init_kwargs
        self.instance = instance

        self.restart_timer = GenericTimer()
        self.restart_scheduled = False

    def __repr__(self):
        return 'Worker <%s>' % self.id

    # heartbeat
    def set_timeout(self, timeout):
        self.hb_timer.set_delta(seconds=timeout)

    @property
    def is_timed_out(self):
        # если значение 0, таймер не истекает
        if self._hb_timeout:
            return self.hb_timer.is_timed_out
        return False

    def heartbeat(self):
        self.hb_timer.reset()

    # pool restart
    def schedule_restart(self, s):
        self.restart_timer.set_delta(seconds=s)
        self.restart_scheduled = True
        self.restart_timer.reset()


class GenericWorkerPool(list):

    def __contains__(self, item):
        """ Проверка вхождения: стандартная, по id и имени. """
        orig_lookup = list.__contains__(self, item)
        id_lookup = list.__contains__([w.id for w in self], item)
        name_lookup = list.__contains__([w.name for w in self], item)
        return any([orig_lookup, id_lookup, name_lookup])

    def __getitem__(self, item):
        """ Получение элемента по нормальному индексу, id или имени """
        try:
            return [w for w in self if w.id == item][0]
        except IndexError:
            try:
                return [w for w in self if w.name == item][0]
            except IndexError:
                return list.__getitem__(self, item)

    def index(self, value, start=None, stop=None):
        """ Получение индекса элемента: стандартное, по id и имени. """
        try:
            return list.index(self, [w for w in self if w.id == value][0])
        except IndexError:
            try:
                return list.index(self, [w for w in self if w.name == value][0])
            except IndexError:
                return list.index(self, value)

    def instance(self, item):
        return self[item].instance

    def by_engagement(self, state):
        return [w for w in self[:] if w.is_engaged == state]

    # registration
    def register(self, id, timeout=TIMEOUT):
        if id in self[:]:
            raise RuntimeError('worker already registered: %s' % id)
        self.append(WorkerInfo(id=id, timeout=timeout))

    def unregister(self, id):
        self.pop(self.index(id))

    # engagement
    def engage(self, id, transaction_id=None):
        if self[id].is_engaged:
            raise RuntimeError('worker already is_engaged: %s' % id)
        self[id].is_engaged = True
        self[id].transaction_id = transaction_id
        return self[id].id

    def is_engaged(self, id):
        return self[id].is_engaged

    def release(self, id):
        if not self[id].is_engaged:
            raise RuntimeError('worker already released: %s' % id)

        # достаем воркер из очереди, освобождаем, ставим в конец (или начало :))
        self.append(self.pop(self.index(id)))
        self[id].is_engaged = False
        self[id].transaction_id = None

    def is_released(self, id):
        return not self[id].is_engaged

    @property
    def engaged(self):
        return [w.id for w in self.by_engagement(True)]

    @property
    def engaged_count(self):
        return len(self.by_engagement(True))

    def engage_lru(self):
        return self.engage(self.released[0])

    @property
    def released(self):
        return [w.id for w in self.by_engagement(False)]

    @property
    def released_count(self):
        return len(self.by_engagement(False))

    # heartbeat
    def heartbeat(self, id):
        self[id].hb_timer.reset()

    # TIMEOUT
    def is_timed_out(self, id):
        return self[id].hb_timer.is_timed_out

    @property
    def timed_out(self):
        return [o for o in self if o.is_timed_out]

    def purge(self):
        for w in self.timed_out:
            self.pop(w)

WorkerPool = GenericWorkerPool
