# -*- coding: utf-8 -*-
import uuid

from symphony.core.datatypes import DotDict as DotDict
from symphony.timers.containers import PPPTimer

from symphony.pools import defaults as pool_defaults
from symphony.protocols.smp import defaults as smp_defaults
SMP_TYPE_PARTIAL = smp_defaults.SMP_TYPE_PARTIAL

UNFINISHED_STATES = pool_defaults.UNFINISHED_STATES
FINISHED_STATES = pool_defaults.FINISHED_STATES
EXCEPTION_STATES = pool_defaults.EXCEPTION_STATES

STATE_INIT = pool_defaults.STATE_INIT
STATE_SUCCESS = pool_defaults.STATE_SUCCESS

TIMEOUT = pool_defaults.TRANSACTION_TIMEOUT
LIVENESS = pool_defaults.TRANSACTION_LIVENESS


class TransactionInfo(object):
    def __init__(self, id=None, group=None, name=None, args=None, kwargs=None, state=None,
                 meta=None, liveness=LIVENESS, timeout=TIMEOUT):
        super(TransactionInfo, self).__init__()
        self.id = id if id else uuid.uuid1().__str__()

        # параметры сигнала
        self.name = name
        self.group = group
        self.args = args
        self.kwargs = kwargs

        self.state = state

        self.response = DotDict({
            'partial': [],
            'final': None,
        })

        # дополнительно
        self.meta = DotDict()
        if meta:
            self.meta.update(meta)

        # контроль выполнения
        self.ppp_timer = PPPTimer(
            seconds=timeout, liveness=liveness, is_constant=False,
            mount_point=self
        )
        self._timeout = timeout

    def __repr__(self):
        return 'Transaction <%s>' % self.id

    def set_timeout(self, timeout):
        self.ppp_timer.set_delta(seconds=timeout)

    @property
    def is_timed_out(self):
        # истекшими считаются только транзакции, у которых значение таймаута не 0,
        # состояние не завершеное
        if self._timeout and self.state in UNFINISHED_STATES:
            return self.ppp_timer.is_timed_out
        return False

    @property
    def last_activity(self):
        return self.ppp_timer.last

    @property
    def liveness(self):
        return self.ppp_timer.liveness

    def register_activity(self):
        self.ppp_timer.reset()

    def refresh(self):
        self.ppp_timer.refresh()


class GenericTransactionPool(list):

    def __contains__(self, item):
        """ Проверка вхождения: стандартная, по id. """
        orig_lookup = list.__contains__(self, item)
        id_lookup = list.__contains__([t.id for t in self], item)
        return any([orig_lookup, id_lookup])

    def __getitem__(self, item):
        """ Получение элемента по нормальному индексу или id. """
        try:
            return [t for t in self if t.id == item][0]
        except IndexError:
            return list.__getitem__(self, item)

    def index(self, value, start=None, stop=None):
        """ Получение индекса элемента: стандартное и по id. """
        try:
            return list.index(self, [t for t in self if t.id == value][0])
        except IndexError:
            return list.index(self, value)

    # response
    def response_save(self, msg):

        # Client/PARTIAL
        if msg.message_type == SMP_TYPE_PARTIAL:
            self[msg.transaction_id].response.partial.append(msg)
        # Client/FINAL
        else:
            self[msg.transaction_id].response.final = msg

        self[msg.transaction_id].register_activity()

    # registration
    def register(self, id, name, group=None, args=None, kwargs=None, state=STATE_INIT,
                 meta=None, liveness=LIVENESS, timeout=TIMEOUT):
        if id in self[:]:
            raise RuntimeError('transaction already registered: %s' % id)

        self.append(
            TransactionInfo(
                id=id, name=name, group=group, args=args, kwargs=kwargs, state=state,
                meta=meta, timeout=timeout, liveness=liveness,
            )
        )

    def unregister(self, id):
        self.pop(self.index(id))

    # activity
    def register_activity(self, id):
        self[id].ppp_timer.reset()

    def last_activity(self, id):
        return self[id].last_activity

    def liveness(self, id):
        return self[id].liveness

    # timeout
    def refresh(self):
        list(map(lambda t: t.refresh, self))

    def is_timed_out(self, id):
        return self[id].is_timed_out

    @property
    def zero_liveness(self):
        return [t for t in self if t.liveness == 0]

    @property
    def timed_out(self):
        """ Метод возвращает транзакции, которые превысили интевал ожидания и
        имеют незавершенный статус (INIT, QUEUED, ASSIGNED, RETRY). """
        return [t for t in self if t.is_timed_out and t.state in UNFINISHED_STATES]

    @property
    def finished(self):
        """ Метод возвращает все транзакции, имеющие завершенный статус
        (STATE_SUCCESS, STATE_FAILURE, STATE_REVOKED). """
        return [t for t in self if t.state in FINISHED_STATES]

    @property
    def unfinished(self):
        """ Метод возвращает все транзакции, имеющие завершенный статус
        (STATE_SUCCESS, STATE_FAILURE, STATE_REVOKED). """
        return [t for t in self if t.state in UNFINISHED_STATES]

    @property
    def successful(self):
        """ Метод возвращает все транзакции, имеющие успешшный статус
        (STATE_SUCCESS). """
        return [t for t in self if t.state in STATE_SUCCESS]

    @property
    def failed(self):
        """ Метод возвращает все транзакции, имеющие статус ошибки
        (STATE_RETRY, STATE_FAILURE, STATE_REVOKED). """
        return [t for t in self if t.state in EXCEPTION_STATES]


TransactionPool = GenericTransactionPool
