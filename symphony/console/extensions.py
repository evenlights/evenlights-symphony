# -*- coding: utf-8 -*-
from termcolor import colored

from jinja2 import nodes
from jinja2.ext import Extension


class ColoredExtension(Extension):
    # a set of names that trigger the extension.
    tags = set(['colored'])

    def __init__(self, environment):
        super(ColoredExtension, self).__init__(environment)

        # add the defaults to the environment
        environment.extend(
            fragment_color='',
        )

    def parse(self, parser):
        lineno = next(parser.stream).lineno

        # color
        args = [parser.parse_expression()]

        # attrs
        if parser.stream.skip_if('comma'):
            args.append(parser.parse_expression())
        else:
            args.append(nodes.Const(None))

        body = parser.parse_statements(['name:endcolored'], drop_needle=True)
        return nodes.CallBlock(self.call_method('_colored', args),
                               [], [], body).set_lineno(lineno)

    def _colored(self, color, attrs, caller):
        rv = caller()
        return colored(rv, color, attrs=attrs)
