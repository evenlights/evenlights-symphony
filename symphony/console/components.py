# -*- coding: utf-8 -*-
from jinja2 import Environment, PackageLoader

from symphony.core.infra.generic import GenericComponent
from symphony.console.extensions import ColoredExtension


class Console(GenericComponent):
    """ """
    name = 'console'
    shared = True

    def __init__(self, **kwargs):
        super(Console, self).__init__(**kwargs)
        self.runtime['settings'] = {
            'no_encoding': True,
        }

    # Component
    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(Console, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.engine = Environment(
            loader=PackageLoader('symphony.console', 'templates'),
            extensions=[ColoredExtension],
            lstrip_blocks=True,
        )
        return self

    def shutdown(self):
        self.engine = None
        super(Console, self).shutdown()

    # Console
    def render(self, template_name, **kwargs):
        self.messages.debug('rendering: %s' % template_name)
        encoding = self.core.sys.stdout_encoding
        template = self.engine.get_template(template_name)
        output = template.render(
            term_width=self.core.sys.term_width,
            **kwargs
        )
        if not self.runtime['settings']['no_encoding']:
            output = output.encode(encoding)

        print(output)
