import json
import time
from typing import Iterable

from symphony.core.infra import GenericComponent
from symphony.api.decorators import (
    connection_success, endpoint_available,
    success_response, single_object_response, multiple_objects_response,

)
from symphony.api.http import GenericTokenClient


class GenericClient(GenericComponent):
    """
    """
    ENTITY_KEY_SINGULAR = None
    ENTITY_KEY_PLURAL = None

    name = 'api'
    shared = True
    headers: dict = None
    throttling = 0.2
    pagination = True

    class ConnectionError(BaseException):
        pass

    class EndpointUnavailable(BaseException):
        pass

    class UnexpectedResponseStatus(BaseException):
        pass

    class UnexpectedResponseContent(BaseException):
        pass

    class NotFound(BaseException):
        pass

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._client = None

    def activate(self):
        super().activate()

        self.messages.info(
            'connecting to API endpoint at: {}'.format(self.config.API_URL)
        )

        try:
            self._client = GenericTokenClient({
                'token': self.config.API_TOKEN,
                'url': self.config.API_URL,
            })

        except self.ConnectionError as e:
            self.messages.error('could not connect to API: {}'.format(e))

    @success_response
    @endpoint_available
    @connection_success
    def action(self, keys: Iterable, params: dict = None, request=None) -> dict:
        """
        Perform action.
        ---
        If we want pass `x-request-id` header, we should pass request too.

        :param keys: coreapi keys
        :param params: coreapi keys
        :param request: request object
        :return:
        """
        params = params or {}

        if self.headers:
            self._client._GenericTokenClient__transport._headers._data.update(
                self.headers
            )

        if self.throttling:
            time.sleep(self.throttling)

        return self._client.action(keys, params=params, request=request)

    @success_response
    @connection_success
    def get_client(self):
        self._client = GenericTokenClient({
            'token': self.config.API_TOKEN,
            'url': self.config.API_URL,
        })

    def _plain(self, data):
        return json.loads(json.dumps(data))

    def _print_schema(self):
        print(self._client._GenericTokenClient__schema)


class GenericCRUDEndpoint:
    BASE_KEYS = ()
    ENTITY_KEY_SINGULAR = None
    ENTITY_KEY_PLURAL = None

    def __init__(self, root_api: GenericClient, pagination=False):
        super().__init__()
        self._root_api = root_api
        self.ConnectionError = root_api.ConnectionError
        self.EndpointUnavailable = root_api.EndpointUnavailable
        self.UnexpectedResponseStatus = root_api.UnexpectedResponseStatus
        self.UnexpectedResponseContent = root_api.UnexpectedResponseContent

        if pagination:
            self.ENTITY_KEY_PLURAL = 'results'

    @multiple_objects_response
    def list(self, **kwargs):
        params = {'page_size': 'all'}
        params.update(kwargs)
        return self.action(['list'], params=params)

    @single_object_response
    def create(self, **kwargs):
        params = {}
        params.update(kwargs)
        return self.action(['create'], params=params)

    @single_object_response
    def update(self, partial=False, **kwargs):
        params = {}
        params.update(kwargs)
        if partial:
            return self.action(['partial_update'], params=params)
        else:
            return self.action(['update'], params=params)

    @single_object_response
    def get(self, **kwargs):
        params = {}
        params.update(kwargs)
        return self.action(['read'], params=params)

    def delete(self, **kwargs):
        params = {}
        params.update(**kwargs)
        return self.action(['delete'], params=params)

    def action(self, keys: Iterable, *args, **kwargs):
        # todo: try-except + message?
        return self._root_api.action(
            self.get_base_keys() + tuple(keys), *args, **kwargs
        )

    def get_base_keys(self) -> tuple:
        return self.BASE_KEYS

    @staticmethod
    def collect_kwargs(**kwargs):
        data = {}
        for key, value in kwargs.items():
            if value:
                data[key] = value

        return data
