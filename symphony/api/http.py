# -*- coding: utf-8 -*-
from typing import Iterable

import coreapi
from coreapi.auth import TokenAuthentication


class GenericTokenClient:
    __client = None
    __schema = None
    __transport = None

    def __init__(self, service_config):
        auth = TokenAuthentication(token=service_config['token'], scheme='Token')
        self.__transport = coreapi.transports.HTTPTransport(
            auth=auth, headers={}
        )
        self.__client = coreapi.Client(transports=[self.__transport])
        self.__schema = self.__client.get(service_config['url'])

    def action(self, keys: Iterable, params: dict = None, request=None) -> dict:
        """
        Perform action.
        ---
        If we want pass `x-request-id` header, we should pass request too.

        :param keys: coreapi keys
        :param params: coreapi keys
        :param request: request object
        :return:
        """
        params = params or {}

        self.__update_headers(request)
        return self.__client.action(self.__schema, keys, params)

    def __update_headers(self, request=None):
        """
        Update request headers.
        This is very dirty hack of the coreapi client, but at this time
        there is not other way to change headers after transport creation.
        """
        headers = {}

        if request:
            request_id = request.META.get('HTTP_X_REQUEST_ID')
            if request_id:
                headers.update({'X-Request-Id': request_id})

        self.__transport._headers._data.update(headers)
