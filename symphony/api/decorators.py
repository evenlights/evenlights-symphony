from urllib3.exceptions import MaxRetryError, NewConnectionError

from coreapi.exceptions import ErrorMessage, LinkLookupError
from requests.exceptions import ConnectionError


# Global Decorators

def connection_success(meth):

    def wrapper(*args, **kwargs):
        instance = args[0]

        try:
            return meth(*args, **kwargs)
        except (MaxRetryError, NewConnectionError, ConnectionError) as e:
            raise instance.ConnectionError(
                'could not connect to API: {}'.format(e)
            )

    return wrapper


def endpoint_available(meth):

    def wrapper(*args, **kwargs):
        instance = args[0]

        try:
            return meth(*args, **kwargs)
        except LinkLookupError as e:
            raise instance.EndpointUnavailable(
                'endpoint you are trying to call is not available, '
                'make sure it exists and you have enough permissions: '
                '{}'.format(e)
            )

    return wrapper


def success_response(meth):

    def wrapper(*args, **kwargs):
        instance = args[0]

        try:
            return meth(*args, **kwargs)
        except ErrorMessage as e:

            # not found
            try:
                if e.error._data['detail'] == 'Not found.':
                    raise instance.NotFound('Not found.')
            except (AttributeError, KeyError):
                pass

            raise instance.UnexpectedResponseStatus(
                'call to the endpoint returned unexpected code: {}'.format(e)
            )

    return wrapper


# Module-Level Decorators

def single_object_response(meth):

    def wrapper(*args, **kwargs):
        instance = args[0]
        data = meth(*args, **kwargs)

        entity_key = getattr(instance, 'ENTITY_KEY_SINGULAR', None)

        if entity_key:
            if entity_key not in data:
                raise instance.UnexpectedResponseContent(
                    'unexpected response from remote API, '
                    'entity key is missing: {}'.format(entity_key)
                )
            data = data[entity_key]

        if not isinstance(data, dict):
            raise instance.UnexpectedResponseContent(
                'unexpected response from remote API, '
                'expected dict, got {}'.format(type(data))
            )

        return data

    return wrapper


def multiple_objects_response(meth):

    def wrapper(*args, **kwargs):
        instance = args[0]
        data = meth(*args, **kwargs)

        entity_key = getattr(instance, 'ENTITY_KEY_PLURAL', None)

        if entity_key:
            if entity_key not in data:
                raise instance.UnexpectedResponseContent(
                    'unexpected response from remote API, '
                    'entity key is missing: {}'.format(entity_key)
                )
            data = data[entity_key]

        if not isinstance(data, list):
            raise instance.UnexpectedResponseContent(
                'unexpected response from remote API, '
                'expected list, got {}'.format(type(data))
            )

        return data

    return wrapper
