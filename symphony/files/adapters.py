# -*- coding: utf-8 -*-
import os

from symphony.adapters.files import BaseFileAdapter


class GenericFileAdapter(BaseFileAdapter):
    name = 'file_adapter'
    handler_class = open

    def create(self, target=None, lock_timeout=10):
        """ Метод создает файл и привязывает адаптер к нему.

        :param target: если указан, адаптер будет привязан к данному пути, если нет,
            будет использовано поле ``self._target``; если обе переменные пусты, метод
            породит сообщение об ошибке
        """

        if self.is_connected:
            self._raise('trying to create while attached to another storage: %s' % self.target,
                        exception_class=RuntimeError)

        if target:
            self.target = target

        if not self.target:
            self._raise('cannot create file, target unspecified', exception_class=RuntimeError)

        if os.path.isfile(self.target):
            self.messages.warning('file already exists, overwriting: %s' % self.target)

        try:
            self.messages.debug('creating file: %s' % self.target)
            self.handler_class(self.target, mode='a').close()

        except IOError:
            self.messages.error('I/O error when creating file: %s' % self.target)
            return

        self.connect(self.target)

    def read(self, chunk_size=None):
        """ Метод чтения из файла проверяет, привязан ли адаптер и можно ли считать
        указанный файл. В случае успеха метод возвращает содержимое файла. """
        super(BaseFileAdapter, self).read()

        if not self.handler:
            self._raise('error reading, file not opened', exception_class=RuntimeError)

        read_args = []
        if chunk_size:
            read_args.append(chunk_size)

        try:
            return self.handler.read(*read_args)
        except IOError:
            self.messages.error('I/O error when reading file: %s' % self.target)
            return

    def write(self, content, mode='w', lock_timeout=10, *args, **kwargs):
        """ Метод чтения из файла проверяет, привязан ли адаптер и можно ли записать
        в указанный файл. В случае успеха контрольная сумма контейнера будет
        обновлена.
        :param content: контент для записи
        :param mode: режим записи, по умолчанию 'w'
        """
        super(BaseFileAdapter, self).write()

        if not self.handler:
            self._raise('error reading, file not opened', exception_class=RuntimeError)

        try:
            self.handler.write(content)
        except IOError:
            self.messages.error('I/O error when writing to file: %s' % self.target)
            return

    # placement
    def copy(self, target, chunk_size=1024):
        """ Метод копирует содержимое текущего файла хранилища в указанный путь,
        по завершении адаптер остается привязан к исходному файлу.

        :param target: путь для создания копии
        """

        if not self.is_connected:
            self._raise('trying to copy with storage detached', exception_class=RuntimeError)

        if not self.handler:
            self._raise('trying to copy with file closed', exception_class=RuntimeError)

        try:
            with open(target, 'w') as target_file:
                while True:
                    data = self.read(chunk_size=chunk_size)
                    if not data:
                        break
                    target_file.write(data)
        except IOError:
            self.messages.error('I/O error when copying to file: %s' % target)
            return

    def move(self, target):
        """ Метод перемещает файл, к которому привязан адаптер, при этом меняется и
        путь привязки.

        :param target: путь для перемещения
        """
        if not self.is_connected:
            self._raise('trying to move with detached storage', exception_class=RuntimeError)

        try:
            os.rename(self.abspath, target)
            self.target = target
        except IOError:
            self.messages.error('I/O error when moving file to: %s' % target)
            return

FileAdapter = GenericFileAdapter
