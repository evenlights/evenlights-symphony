# -*- coding: utf-8 -*-
from symphony.core.infra.base import BaseComponent
from symphony.messages.containers import GenericMessageQueue

from symphony.messages.defaults import (
    COMPONENT_NAME,
    LEVEL_TRACE, LEVEL_DEBUG, LEVEL_INFO, LEVEL_WARNING, LEVEL_ERROR,
    LEVEL_CRITICAL
)


class Messages(BaseComponent):
    """
    Module of :ref:`infrastructure <symphony.apps.core.components module>`
    level to control the centralized management of message channels. Such
    channels can be an internal message queue, an MBus system message bus, a
    logging module, or similar objects.

    All channels receive messages in a standardized way: the ``info``,
    ``warning`` or similer method of the corresponding channel is called.
    """
    name = COMPONENT_NAME

    # Component

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        self.runtime['settings'] = {
            'enabled': True,
            'external_enabled': True,
            'append_name': True,
        }

        # используем реестр точки монтирования или ведем собственный
        try:
            self.runtime['channels'] = \
                mount_point.infra.registry['message_channels']
        except AttributeError:
            self.runtime['channels '] = {}

        # внутреняя шина сообщений
        self.internal = GenericMessageQueue()

        if core is not None:
            for channel in core.infra.registry['message_channels']:
                self.channel_register(channel)

        self.component.init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )

        return self

    def flush(self):
        self.internal.flush()

    def shutdown(self):
        del self.runtime['channels'][:]
        del self.internal[:]
        super(Messages, self).shutdown()

    # Channels
    def channel_in(self, channel):
        # обычное сравнение "in" может давать неверные результаты
        # при сравнении контейнеров
        return id(channel) in [id(ch) for ch in self.runtime['channels']]

    def channel_register(self, channel):
        """
        Метод для регистрации внешних каналов сообщений. При вызове одного
        из методов модуля (:meth:`info`, :meth:`warning`, :meth:`error`, ...),
        этот канал таже будет получать соответствующее сообщение.
        """
        # логгер ядра
        try:
            self.mount_point.logger.debug(
                '%s: registering external channel %s' %
                (self, channel.name if hasattr(channel, 'name') else repr(channel))
            )
        except AttributeError:
            pass

        if not self.channel_in(channel):
            self.runtime['channels'].append(channel)

        try:
            setattr(self, channel.name, channel)
        except AttributeError:
            pass

    def channel_unregister(self, channel):
        """
        :param channel:
        :return:
        """
        if not self.channel_in(channel):
            self._raise('channel not registered: %s' % channel.name)

        del self.runtime['channels'][self.runtime['channels'].index(channel)]
        delattr(self, channel.name)

    # Messages
    def emit(self, msg_class, msg):
        """

        :param msg_class:
        :param msg:
        :return:
        """
        if self.runtime['settings']['enabled']:
            if self.runtime['settings']['append_name']:
                msg = '%s: %s' % (self.mount_point, msg)

            # внутренний канал
            getattr(self.internal, msg_class)(msg)

            # внешние каналы
            if self.runtime['settings']['external_enabled']:
                for channel in self.runtime['channels']:
                    getattr(channel, msg_class)(msg)

    # getters
    @property
    def trace_messages(self):
        """ """
        return self.internal.debug_messages

    def debug_messages(self):
        """ """
        return self.internal.debug_messages

    @property
    def info_messages(self):
        """ """
        return self.internal.info_messages

    @property
    def warnings(self):
        """ """
        return self.internal.warnings

    @property
    def errors(self):
        """ """
        return self.internal.errors

    @property
    def critical_messages(self):
        """ """
        return self.internal.critical_messages

    @property
    def first_error_content(self):
        """ """
        return self.internal.first_error_content

    # emit
    def trace(self, msg):
        """ Метод порождения сообщения уровня ``trace``. """
        self.emit(LEVEL_TRACE, msg)

    def debug(self, msg):
        """ Метод порождения сообщения уровня ``debug``. """
        self.emit(LEVEL_DEBUG, msg)

    def info(self, msg):
        """ Метод порождения сообщения уровня ``info``. """
        self.emit(LEVEL_INFO, msg)

    def warning(self, msg):
        """ Метод порождения сообщения уровня ``warning``. """
        self.emit(LEVEL_WARNING, msg)

    def error(self, msg):
        """ Метод порождения сообщения уровня ``error``. """
        self.emit(LEVEL_ERROR, msg)

    def critical(self, msg):
        """ Метод порождения сообщения уровня ``critical``. """
        self.emit(LEVEL_CRITICAL, msg)
