# -*- coding: utf-8 -*-
import logging
import warnings


class MessageCompatibilityMixin(object):

    # Formatting
    @classmethod
    def _format_exception_message(cls, msg):
        return '%s: %s' % (cls.__name__, msg)

    def _format_bus_message(self, msg):
        return '%s: %s' % (self.name, msg)

    # Warnings
    @classmethod
    def _warn(cls, msg):
        warnings.warn(cls._format_exception_message(msg))

    def _message_or_warning(self, msg):
        try:
            self.messages.warning(msg)
        except AttributeError:
            self._warn(msg)

    # Errors
    @classmethod
    def _raise(cls, msg, exception_class=Exception):
        raise exception_class(cls._format_exception_message(msg))

    def _message_or_raise(self, msg, exception_class=Exception):
        try:
            self.messages.error(msg)
        except AttributeError:
            self._raise(msg, exception_class=exception_class)

    def _raise_and_message(self, msg, exception_class=Exception):
        self.messages.error(msg)
        self._raise(msg, exception_class=exception_class)

    # Logging
    def _log(self, msg, level='debug'):
        logger = logging.getLogger(self.name)
        getattr(logger, level)(self._format_bus_message(msg))

    def _log_debug(self, msg):
        self._log(msg, level='debug')

    def _log_info(self, msg):
        self._log(msg, level='info')

    def _log_error(self, msg):
        self._log(msg, level='error')
