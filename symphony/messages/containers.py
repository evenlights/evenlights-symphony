# -*- coding: utf-8 -*-
import datetime

from symphony.core.containers import ObjectContainer, ListContainer
from symphony.core.containers.fields import GenericDateTimeField

from symphony.messages.defaults import (
    LEVEL_TRACE, LEVEL_DEBUG, LEVEL_INFO, LEVEL_WARNING, LEVEL_ERROR, LEVEL_CRITICAL,
    REGISTERED_LEVELS
)


class GenericMessage(ObjectContainer):
    # тип данных, чтобы база обработчика решила вызвать _validate_datatype
    datatype = 'GenericMessage'

    def __init__(self, level=None, content=None, timestamp=None):
        if level and (level not in REGISTERED_LEVELS):
            raise ValueError('not a valid message level: %s' % level)

        self.level = level
        self.content = content

        if timestamp is not None:
            self.timestamp = timestamp
        else:
            self.timestamp = datetime.datetime.now()

        super(GenericMessage, self).__init__()

    class DataModel:
        structure = {
            'level': None,
            'content': None,
            'timestamp': GenericDateTimeField,
        }

    @classmethod
    def class_validate_datatype(cls, content):
        try:
            cls.from_transport(content)
            return True
        except:
            return False


class GenericMessageQueue(ListContainer):

    class DataModel:
        member_class = GenericMessage

    def by_level(self, level):
        msgs = []
        for nr, item in enumerate(self):
            if item.level == level:
                msgs.append(item)
        return msgs

    # getters
    @property
    def trace_messages(self):
        return self.by_level(LEVEL_TRACE) or None

    @property
    def debug_messages(self):
        return self.by_level(LEVEL_DEBUG) or None

    @property
    def info_messages(self):
        return self.by_level(LEVEL_INFO) or None

    @property
    def warnings(self):
        return self.by_level(LEVEL_WARNING) or None

    @property
    def errors(self):
        return self.by_level(LEVEL_ERROR) or None

    @property
    def critical_messages(self):
        return self.by_level(LEVEL_CRITICAL) or None

    def flush(self):
        del self[:]

    # setters
    def trace(self, msg):
        self.append(self.DataModel.member_class(level=LEVEL_TRACE, content=msg))

    def debug(self, msg):
        self.append(self.DataModel.member_class(level=LEVEL_DEBUG, content=msg))

    def info(self, msg):
        self.append(self.DataModel.member_class(level=LEVEL_INFO, content=msg))

    def warning(self, msg):
        self.append(self.DataModel.member_class(level=LEVEL_WARNING, content=msg))

    def error(self, msg):
        self.append(self.DataModel.member_class(level=LEVEL_ERROR, content=msg))

    def critical(self, msg):
        self.append(self.DataModel.member_class(level=LEVEL_CRITICAL, content=msg))

    @staticmethod
    def _strip_unit(s):
        """ Метод удаляет <имя модуля>: из текста сообщения """
        return ': '.join(s.split(': ')[1:])

    @property
    def first_error_content(self):
        return self._strip_unit(self.errors[0].content)

Message = GenericMessage
MessageQueue = GenericMessageQueue
