# -*- coding: utf-8 -*-
import logging

COLOR_DEBUG = 'white'
COLOR_INFO = 'cyan'
COLOR_WARNING = 'yellow'
COLOR_ERROR = 'red'
COLOR_CRITICAL = 'magenta'

COMPONENT_NAME = 'messages'
LEVEL_TRACE = 'trace'
LEVEL_DEBUG = 'debug'
LEVEL_INFO = 'info'
LEVEL_WARNING = 'warning'
LEVEL_ERROR = 'error'
LEVEL_CRITICAL = 'critical'

REGISTERED_LEVELS = [
    LEVEL_TRACE,
    LEVEL_DEBUG,
    LEVEL_INFO,
    LEVEL_WARNING,
    LEVEL_ERROR,
    LEVEL_CRITICAL,
]

LEVELS_HR2INT = {
    LEVEL_TRACE: 5,
    LEVEL_DEBUG: logging.DEBUG,
    LEVEL_INFO: logging.INFO,
    LEVEL_WARNING: logging.WARNING,
    LEVEL_ERROR: logging.ERROR,
    LEVEL_CRITICAL: logging.CRITICAL,
}
