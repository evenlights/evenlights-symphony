# -*- coding: utf-8 -*-
import time
import datetime


class GenericTimer(object):
    """ Generic-таймер предназначен для контроля неблокирующих процессов, имеющих
    срок истечения, например, ожидание замка. Он засекает время с момента последнего
    "нажатия на кнопку" сброса (:func:`reset`) и сообщает, не превышен ли
    установленный интервал выполнения (:func:`is_timed_out`).

    В момент инициализации сброс происходит автоматически.
    """

    def __init__(self, days=0, seconds=0, microseconds=0, milliseconds=0,
                 minutes=0, hours=0, weeks=0, mount_point=None):
        self.delta = datetime.timedelta(
            days=days, seconds=seconds, microseconds=microseconds, milliseconds=milliseconds,
            minutes=minutes, hours=hours, weeks=weeks
        )
        self.last = None
        self.mount_point = mount_point
        self.reset()

    def set_delta(self, days=0, seconds=0, microseconds=0, milliseconds=0,
                  minutes=0, hours=0, weeks=0):
        self.delta = datetime.timedelta(
            days=days, seconds=seconds, microseconds=microseconds, milliseconds=milliseconds,
            minutes=minutes, hours=hours, weeks=weeks
        )

    def reset(self):
        self.last = datetime.datetime.now()

    def reset_counter(self):
        self.last = datetime.datetime.now()

    def refresh(self):
        if self.is_timed_out:
            self.timeout_handle()

    @property
    def is_timed_out(self):
        return datetime.datetime.now() > self.last + self.delta

    def timeout_handle(self):
        self.on_timeout()

    def on_timeout(self):
        pass


class PPPTimer(GenericTimer):
    """ Таймер PPP (Paranoid Pirate Pattern) является более сложным таймером.
    Он используется для контроля операций, не имеющих срока истечения -- в
    основном, для контроля обязательных сетевых соединений, таких как
    LifeCycle-канал между брокером и воркерами или соединение с сервером
    транзакций. В этом случае попытки выполнять операции все же необходимо
    продолжать, однако уменьшая при этом интенсивность до определенного
    задаваемого уровня.

    В отличие от generic-таймера, PPP-таймер замеряет 2 интервала: интервал
    выполнения и интервал повтора, а также живучесть: количество интервалов
    выполнения, которое может быть пропущено, прежде чем интервал повтора
    будет удвоен.

    Иначе работает сброс -- помимо сброса точки отсчета сбрасывается занчение
    живучести и интервал повтора.

    :param liveness: живучесть, определяет количество повторный попыток
        выполнения операции
    :param retry_init: начальный интервал повтора для *постоянного* таймера
    :param retry_max: максимальный интервал повтора для *постоянного* таймера
    :param is_constant: параметр постоянного таймера: при значении ``True``
        живучесть будет восстанавливаться при достижении нулевого значения, а
        интервал повтора увеличиваться в 2 раза (но не более значения
        ``retry_max``), при значении ``False`` живучесть будет оставаться
        нулевой до сброса
    """

    def __init__(self, liveness=5, retry_init=1, retry_max=10, is_constant=True, *args, **kwargs):
        self.IS_CONSTANT = is_constant
        self.LIVENESS = liveness
        self.liveness = liveness

        self.RETRY_IN_INIT = retry_init
        self.RETRY_IN_MAX = retry_max
        self.retry_in = retry_init

        super(PPPTimer, self).__init__(*args, **kwargs)

    def sleep(self):
        time.sleep(self.retry_in)

    def reset(self):
        self.last = datetime.datetime.now()
        self.liveness = self.LIVENESS
        self.retry_in = self.RETRY_IN_INIT

    def timeout_handle(self):
        """ Обработка таймаута производится по следующим правилам:

        * если таймер постоянный (``is_constant=True``, по умолчанию), то при достижении
          таймаута живучесть будет уменьшена на 1, а при достижении нулевой живучести
          интервал повторна будет величен в два раза, но не более максимального значения,
          а живучесть восстановлена до начального занчения

        * если таймер единовременный (``is_constant=False``), то при достижении нелвой
          живучести таймер будет оставаться в этом состоянии до момента сброса.
        """
        # обработка таймаута производится только если живучесть не достигла нуля
        if self.liveness:
            self.on_timeout()
            self.liveness -= 1
            self.last = datetime.datetime.now()

            if self.liveness == 0:

                self.on_zero_liveness()

                # если таймер постоянный, живучесть восстанавливается с увеличением интервала повтора
                if self.IS_CONSTANT:
                    self.liveness = self.LIVENESS

                    if self.retry_in * 2 < self.RETRY_IN_MAX:
                        self.retry_in *= 2
                    else:
                        self.retry_in = self.RETRY_IN_MAX

    def on_zero_liveness(self):
        pass

    @property
    def retry_in_max(self):
        return self.retry_in == self.RETRY_IN_MAX

    @property
    def retry_in_min(self):
        return self.retry_in == self.RETRY_IN_INIT
