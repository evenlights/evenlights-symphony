# -*- coding: utf-8 -*-

# Core
LOGGER_FILE_FORMATTER = '%(asctime)-15s %(levelname)-8s %(module)s: %(message)s'
LOGGER_STDOUT_FORMATTER = '%(levelname)-8s: %(message)s'

# Transport
ZMQ_CONFIG_SECTION = 'ZMQ_TRANSPORT'

# CLI Client
CI_EXCLUDE_TRANSACTION_NAMES = ['help']
