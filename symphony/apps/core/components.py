# -*- coding: utf-8 -*-
import os
import sys
import locale
import platform

from symphony.packages.termsize import getTerminalSize

from symphony.core.infra.base import InfraComponent
from symphony.messages.components import Messages
from symphony.messages.containers import MessageQueue
from symphony.apps.core.logger import Logger

from symphony.configs.defaults import ENV_PROJECT_ENV, ENV_SETTINGS_MODULE


class SharedResourceManager(object):
    """ Менеджер ресурсов предназначем для регистрации общих произвольных
    единиц (обычно :ref:`блоков <core.units>`) с возвожностью разбивки по
    категориям.
    """
    def __init__(self, core):
        self._core = core
        self.registry = {}

    def register(self, resource, name=None):
        try:
            resource_name = name if name else resource.name
        except AttributeError:
            self._core._raise(
                'components without name cannot be registered: %s' % resource.__class__.__name__,
                exception_class=ValueError)

        if resource_name in self.registry:
            self._core._raise(
                'resource already registered: %s' % resource_name, exception_class=KeyError)

        self.registry[resource_name] = resource

    def unregister(self, resource):
        try:
            del self.registry[resource]
        except KeyError:
            try:
                del self.registry[resource.name]
            except AttributeError:
                self._core._raise(
                    'unable to identify resource: %s' % resource, exception_class=KeyError)

    def get(self, name, *args):
        return self.registry[name]


class SystemInformation(object):

    def __init__(self, core):
        self._core = core

    @property
    def pid(self):
        return os.getpid()

    @property
    def term_width(self):
        return getTerminalSize()[0]

    @property
    def term_height(self):
        return getTerminalSize()[1]

    @property
    def name(self):
        return platform.system()

    @property
    def preferred_encoding(self):
        return locale.getpreferredencoding()

    @property
    def locale(self):
        return locale.getdefaultlocale()[0]

    @property
    def language(self):
        if hasattr(self._core, 'config'):
            if hasattr(self._core.config, 'LOCAlE'):
                return self._core.config.LOCALE
        return self.locale

    @property
    def fs_encoding(self):
        return sys.getfilesystemencoding()

    @property
    def stdout_encoding(self):
        """ Кодировка стандартного устройства вывода. Может меняться при
        перенаправлении вывода в файл. """
        if sys.stdout.isatty():
            return sys.stdout.encoding
        else:
            return locale.getpreferredencoding()


class Environment(object):

    def __init__(self, core):
        self._core = core

    @property
    def name(self):
        return os.getenv(ENV_PROJECT_ENV, 'production')

    @property
    def settings_module(self):
        return os.getenv(ENV_SETTINGS_MODULE, 'settings')

    @staticmethod
    def get(key):
        return os.getenv(key)

    @staticmethod
    def set(key, value):
        os.putenv(key, value)


class AppCore(InfraComponent):
    """ Ядро приложения служит для организации совместной работы его
    :ref:`блоком <core.units>` и :ref:`модулей <core.modules>`. По сути, оно
    является обычным блоком приложения, однако модуль :ref:`инфраструктуры
    <core.app>` делает различие между ядром и прочими блоками.

    Сужествует ряд специализированных модулей ядра. К ним относятся:

    * :ref:`центральная шина <apps.core.modules.mbus>`

    * :ref:`модуль конфигурация ядра <apps.core.modules.config>`

    * :ref:`центральный логгер <apps.core.modules.logger>`

    * обычно ядро также монтирует :ref:`модуль сообщений <messages.modules>`

    Базовое ядро включает ряд постоянных модулей:

    * ``shared``: менеджер общих ресурсов регистрирует и обуспечивает
      доступ к общим блокам, ресурсам и плагинам

    * ``env``: отвечает за работу с переменными окружения

    * ``sys``: предоставляет информацию о лежащей в основе системе: размерах
      консоли, кодировках итд

    Помимо этого, базовое ядро имеет :ref:`runtime-контейнер <core.runtime>`.
    """
    name = 'core'

    class Infrastructure:
        components = [Logger, Messages, ]

    def init(self, core=None, config=None, registry=None, mount_point=None, **kwargs):
        self.sys = SystemInformation(core=self)
        self.env = Environment(core=self)
        self.shared = SharedResourceManager(core=self)

        self.mbus = MessageQueue()
        setattr(self.mbus, 'name', 'mbus')
        self.infra.registry['message_channels'].append(self.mbus)

        self.component.init(core=self, config=config, registry=registry, mount_point=mount_point, **kwargs)
        self.infra.init(
            core=self, config=self.config, registry=self.infra.components, mount_point=self
        )

        try:
            # system
            self.messages.debug(
                'system: name=%s, PID=%s, term_width=%d, term_height=%d' %
                (self.sys.name, self.sys.pid, self.sys.term_width, self.sys.term_height)
            )

            # encodings
            self.messages.debug(
                'encodings: preferred=%s, stdout=%s, filesystem=%s, locale=%s' %
                (self.sys.preferred_encoding, self.sys.stdout_encoding, self.sys.fs_encoding,
                 self.sys.locale)
            )

            # env
            self.messages.debug(
                'environment: name=%s, settings_module=%s' %
                (self.env.name, self.env.settings_module)
            )

        except AttributeError:
            pass

        return self

    def flush(self):
        self.mbus.flush()
        self.infra.flush()


Core = AppCore
