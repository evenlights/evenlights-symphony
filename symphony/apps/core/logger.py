# -*- coding: utf-8 -*-
import logging

import termcolor
from colorama import init as colorama_init

from symphony.core.infra.base import BaseComponent

from symphony.messages.defaults import (
    REGISTERED_LEVELS, LEVELS_HR2INT,
    LEVEL_TRACE,
    COLOR_DEBUG, COLOR_INFO, COLOR_WARNING, COLOR_ERROR, COLOR_CRITICAL
)

TRACE = LEVELS_HR2INT[LEVEL_TRACE]


class SymphonyLogger(logging.Logger):
    def __init__(self, name, level=logging.NOTSET):
        super(SymphonyLogger, self).__init__(name, level=level)

    def trace(self, msg, *args, **kwargs):
        if self.isEnabledFor(TRACE):
            self._log(TRACE, msg, args, **kwargs)


colorama_init()
logging.addLevelName(TRACE, LEVEL_TRACE.upper())
logging.setLoggerClass(SymphonyLogger)


def trace(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(TRACE):
        self._log(TRACE, message, args, **kws)
logging.Logger.trace = trace


class ColorSTDOUTFormatter(logging.Formatter):

    def __init__(self, *args, **kwargs):
        super(ColorSTDOUTFormatter, self).__init__(*args, **kwargs)

    color_mapping = {
        TRACE:            COLOR_DEBUG,
        logging.DEBUG:    COLOR_DEBUG,
        logging.INFO:     COLOR_INFO,
        logging.WARNING:  COLOR_WARNING,
        logging.ERROR:    COLOR_ERROR,
        logging.CRITICAL: COLOR_CRITICAL,
    }
    attr_mapping = {
        TRACE:            ['dark', 'bold'],
        logging.DEBUG:    ['dark'],
        logging.INFO:     ['bold'],
        logging.WARNING:  None,
        logging.ERROR:    None,
        logging.CRITICAL: None,
    }

    def format(self, record):
        text = super(ColorSTDOUTFormatter, self).format(record)
        # text = logging.StreamHandler().format(record)
        return termcolor.colored(
            text,
            self.color_mapping[record.levelno],
            attrs=self.attr_mapping[record.levelno]
        )


class CoreLogger(BaseComponent):
    """ Модуль ядра """
    name = 'logger'

    def __getattr__(self, item):
        """ При вызове таких методов, как ``info``, ``warning`` или
        ``error``, вызов будет переадресован объекту _logger модуля.
        """
        if item in REGISTERED_LEVELS:
            return getattr(self._logger, item)

        return self.__getattribute__(item)

    @property
    def logger_name(self):
        """ В качестве имени используется точка монтирования :ref:`инфраструктуры
        <core.app>` логгера, поскольку именно от лица этой точки монтирования
        должны поступать сообщения.
        """
        if self.mount_point:
            if self.mount_point.mount_point:
                return str(self.mount_point.mount_point)

            return str(self.mount_point)

        return ''

    def init(self, core=None, config=None, registry=None, mount_point=None, **kwargs):
        self.runtime['settings'] = {
            'enabled': True,
        }
        self.component.init(
            core=core, config=config, registry=registry, mount_point=mount_point, **kwargs
        )

        self._logger = logging.getLogger(self.logger_name)

        # нулевой обработчик используется если воркер-процесс не использует
        # собственных обработчиков, а мастер-процесс, который по идее должен
        # эти обработчики инициализировать, стартует позже
        self._null_handler = logging.NullHandler()
        self._logger.addHandler(self._null_handler)

        self.core.infra.registry['message_channels'].append(self)
        self._logger.debug('%s: core logger name: %s' % (self, self.logger_name))

        return self

    def shutdown(self):
        logger = logging.getLogger(self.logger_name)
        logger.removeHandler(self._null_handler)
        try:
            self.core.infra.registry['message_channels'].pop(
                self.core.infra.registry['message_channels'].index(self)
            )
        except ValueError:
            # логгер еще не был добавлен
            pass
        self.component.shutdown()

Logger = CoreLogger
