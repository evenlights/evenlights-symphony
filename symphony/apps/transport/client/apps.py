# -*- coding: utf-8 -*-
import sys
try:
    import readline
except ImportError:
    pass
from threading import Thread

from symphony.routers.components import QueueRouter
from symphony.ci.parsers import StringSignalParser
from symphony.protocols.smp.routers import SMPMessageRouter
from symphony.apps.handlers.messages import (
    DefaultMessageHandler,
    NoneMessageHandler, StringMessageHandler, MessageMessageHandler,
    FileMessageHandler
)
from symphony.apps.configs import CLIClientConfig
from symphony.apps.transport.base import ZMQTransportBase
from symphony.apps.transport.client.components import (
    ClientTransactionPool, ClientCI
)


class ClientBase(ZMQTransportBase):
    smp_handlers = [
        DefaultMessageHandler,
    ]

    class Infrastructure:
        components = [
            QueueRouter(
                name='ci_router', handler_source='signal_handlers', shared=True
            ),
            SMPMessageRouter(handler_source='smp_handlers', shared=True),
            ClientCI,
            ClientTransactionPool,
        ]

    def __init__(self, smp_handlers=None, *args, **kwargs):
        super(ClientBase, self).__init__(*args, **kwargs)

        if smp_handlers:
            self.smp_handlers = smp_handlers
        self.smp_handlers = self.smp_handlers or []

    def transaction_complete(self, transaction):
        """
        Метод для обработки завершенной транзакции. В зависимости от
        модификации клиент может передавать сообщения собственным обработчикам
        (например, для отображения), либо передавать дальше (например, при
        выполнении запросов REST-сервера).
        """

    def transaction_missing(self, msg):
        """
        Метод вызывается, если транзакция для сообщения не зарегистрирована.
        Такая ситуация может возникать по разным причинам, например, если
        клиент получил сообщение, после того, как закрыл удалил транзакцию как
        просроченную, либо если сервер инициирует передачу.
        """


class CLIClientBase(ClientBase):
    """
    ...
    Также клиентское приложение определяет единый порядок обработки:

    * обработка очереди роутера [интерфейса управления]: на этом этапе сигналы
      из очереди роутера, если она не пуста, либо выполняются при помощи
      собственного обарботчика приложения (если он зарегистрированы для данного
      сигнала), либо направляются удаленному узлу -- при этом для таких сигналов
      в регистрируются [транзакции] в соответствующем [пуле].

    * далее производится обработка пула транзакций: на этом этапе обрабатываются
      входящие сообщения для зарегистрированных транзакций, транзакции, для
      которых был превышел лимит ожидания отправляются повторно

    * завершенные транзакции направляются в роутер

    """
    config = CLIClientConfig
    remote_mapping = None
    smp_handlers = [
        DefaultMessageHandler,
        NoneMessageHandler, StringMessageHandler, MessageMessageHandler,
        FileMessageHandler
    ]

    def __init__(self, remote_mapping=None, *args, **kwargs):
        super(CLIClientBase, self).__init__(*args, **kwargs)
        self.shell_mode = True
        self.signal_parser = None

        if remote_mapping:
            self.remote_mapping = remote_mapping
        self.remote_mapping = self.remote_mapping or {}

    def init(self, **kwargs):
        super(CLIClientBase, self).init(**kwargs)
        self.mappings.update({
            'shell': {},
            'remote': self.remote_mapping,
        })

        if self.shell_mode or len(sys.argv) > 1:
            self.messages.debug('initializing signal parser')
            self.infra.join(StringSignalParser)

    def setup(self):
        # режим оболочки или параметры строки: инициализируем строковой парсер
        if self.shell_mode or len(sys.argv) > 1:

            self.messages.debug('merging signal mappings')
            self.mappings['shell'].update(self.ci_router.mapping)
            self.mappings['shell'].update(self.remote_mapping)

            self.messages.debug('shell mapping: %s' % self.mappings['shell'])
            self.signal_parser.mapping = self.mappings['shell']

        if self.shell_mode:
            self.messages.debug('applying shell mode settings')

            # отключаем repr для некоторых блоков
            self.signal_parser.messages.runtime['settings']['append_name'] = False
            self.core.messages.runtime['settings']['append_name'] = False
            self.ci_socket.messages.runtime['settings']['append_name'] = False

            self.smp_router.disable_handler_repr()

        # переключаем режим в зависимости от того, переданы ли аргументы
        if len(sys.argv) > 1:
            self.shell_mode = False
            signal_string = ' '.join(sys.argv[1:])
            signal = self.signal_parser.get_signal(signal_string)

            # ставим сигнал в очередь, если определился
            if signal:
                self.ci_router.queue_put(signal)

        super(CLIClientBase, self).setup()

    def handle(self, *args, **kwargs):
        super(CLIClientBase, self).handle(*args, **kwargs)
        if self.shell_mode:
            self.shell_handle()
        else:
            self.cycle_handle()

    def shell_handle(self):
        # интерактивный режим
        self.ci_get_signal()
        self.ci.handle_queue()

        while self.transactions.pool:
            self.socket_poll()
            self.ci.handle_socket()
            self.transactions.handle()

    def cycle_handle(self):
        self.ci.handle()
        self.transactions.handle()

    @property
    def is_running(self):
        # интерактивный режим: пока пользователь не выйдет либо есть активные
        # операции
        if self.shell_mode:
            return self.runtime['is_running'] \
                   or self.transactions.pool.unfinished

        # режим one-shot: пока есть активные операции либо сигналы в очереди
        else:
            return self.transactions.pool.unfinished \
                   or not self.ci_router.queue_empty()

    def ci_get_signal(self, signal_string=None):
        self.core.messages.debug('getting signal')

        signal_string = signal_string if signal_string else \
            input("[%s]$ " % self.name)

        signal = self.signal_parser.get_signal(signal_string)
        self.core.messages.debug('signal recognized as: %s ' % signal)

        # если сигнал удалось распознать
        if signal:
            # перестаем принимать сигналы при входе в состояние завершения
            if self.is_running:
                self.ci_router.queue_put(signal)
            else:
                self.core.messages.warning(
                    'cannot accept signal, shutdown in progress'
                )

    def transaction_complete(self, transaction):
        for msg in transaction.response.partial:
            self.smp_router.handle(msg)
        if transaction.response.final:
            self.smp_router.handle(transaction.response.final)

    def transaction_missing(self, msg):
        super(CLIClientBase, self).transaction_missing(msg)
        self.smp_router.handle(msg)


class EndpointClientBase(ClientBase):
    """
    Приложение, выполняющее роль обратного прокси-сервера.
    :class:`EndpointClientBase` осуществляет связь с [интерфейсом управления]
    того или иного [узла]. Клиент работает в автономном режиме, то есть,
    выполняет все поступающие запросы и возвращает ответы.
    """
    endpoint_handlers = None

    def __init__(self, endpoint_handlers=None, *args, **kwargs):
        super(EndpointClientBase, self).__init__(*args, **kwargs)
        self.api = None

        if endpoint_handlers:
            self.endpoint_handlers = endpoint_handlers

    def handle(self, *args, **kwargs):
        super(EndpointClientBase, self).handle(*args, **kwargs)
        self.ci.handle_socket()
        self.ci.handle_queue()
        self.transactions.handle()

    def transaction_complete(self, transaction):
        future = transaction.meta.pop('future')
        future.set_result(transaction.response)


class CLIClient(CLIClientBase):
    pass


class EndpointClient(EndpointClientBase, Thread):

    class Infrastructure:
        pass
        # modules = [CI, SMPRouter, LocalThreadAPI]

