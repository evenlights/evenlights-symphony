# -*- coding: utf-8 -*-
import uuid

from symphony.core.infra.generic import GenericComponent
from symphony.pools.transactions import GenericTransactionPool
from symphony.protocols.smp.containers import SMPClientMessage

from symphony.protocols.smp.defaults import (
    SMP_TYPE_FINAL,
    SMP_CONTENT_SIGNAL, SMP_CONTENT_STRING,
    SMP_CODE_GATEWAY_TIMEOUT,
)

from symphony.nodes.transactions.defaults import (
    STATE_QUEUED, STATE_SUCCESS, STATE_RETRY, STATE_FAILURE,
)


class ClientCI(GenericComponent):
    """ Обработка входящих сообщений сокета :ref:`интерфейс управления
    <ci.interfaces>` осуществляется только пока воркер находится рабочем
    состоянии -- при входе в состояние завершения новые сигналы в очередь
    добавляться перестают. Хотя, сигналы уже присутствующие в очереди, будут
    обработаны.

    На данный момент метод ожидает сигнал с типом содержимого
    ``SMP_CONTENT_SIGNAL``.
    """
    name = 'ci'

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(ClientCI, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.app = self.core.shared.get('app')

        self.messages.debug('initializing sockets')
        self.app.socket_join('ci_socket', registry=self.infra.components)
        self.socket = self.app.ci_socket
        self.router = self.app.ci_router

    def handle(self, *args, **kwargs):
        super(ClientCI, self).handle(*args, **kwargs)
        self.handle_queue()
        self.handle_socket()

    def handle_queue(self):
        """ Метод вызывает собственный обработчик, если таковой имеется ы
        собственном мэппигне сигналов, либо помещает его в пул запросов для
        (гипотетически) долгосрочного мониторинга и отправляет брокеру.

        Специальные ключевые аргументы сигнала:

        ``_transaction_id``: идентификатор транзакции, с которым будет отправлен
            исходящий сигнал
        ``_future``: объект :class:`Future` для неблокирующей обработки
        """
        transactions_ = self.core.shared.get('transactions')

        # выполняем следующий сигнал из очереди
        if not self.router.queue_empty():
            self.core.messages.debug('handling control interface queue')
            signal = self.router.queue_get()

            # если для сигнала в текущем мэппиге приложения зарегистрирован
            # обработчик, выполняем его, если нет -- перенаправляем

            if self.router.has_handler(signal):
                self.router.handle(signal)

            else:
                # id транзакции: получаем из ключевых аргументов или генерируем
                if signal.kwargs and '_transaction_id' in signal.kwargs:
                    transaction_id = signal.kwargs.pop('_transaction_id').__str__()
                else:
                    transaction_id = uuid.uuid1().__str__()

                # future: получаем из ключевых аргументов
                future = None
                if signal.kwargs and '_future' in signal.kwargs:
                    future = signal.kwargs.pop('_future')

                # отправляем запрос
                self.core.messages.info(
                    'remote call, transaction id: %s' % transaction_id
                )
                self.send(
                    transaction_id=transaction_id, content=signal.repr.json
                )
                transactions_.pool.register(
                    id=transaction_id, name=signal.name, args=signal.args,
                    kwargs=signal.kwargs, state=STATE_QUEUED,
                    meta={'cache': signal.repr.json},
                    timeout=self.config.REPLY_TIMEOUT,
                    liveness=self.config.REQUEST_RETRIES
                )

                if future:
                    transactions_.pool[transaction_id].meta.future = future

            self.router.task_done()

    def handle_socket(self):
        transactions_ = self.core.shared.get('transactions')

        if self.app.socket_poll_in(self.socket):
            msg = self.socket.client_recv()

            # обработка критических ошибок
            if self.socket.messages.errors:
                error_domain = 'CI socket'
                error_message = self.socket.messages.first_error_content
                self.messages.critical(
                    '%s: %s, disconnecting' % (error_domain, error_message)
                )

                # пытаемся очистить очередь, если она не пуста
                self.clear_queue()
                transactions_.clear_pool()
                self.app.exit()
                return

            # регистрируем активность операций
            if msg.transaction_id in transactions_.pool:
                transaction = transactions_.pool[msg.transaction_id]

                transactions_.save_response(msg)

                if msg.message_type == SMP_TYPE_FINAL:
                    transactions_.success(transaction)

            else:
                transactions_.missing(msg)

    def clear_queue(self):
        if not self.app.ci_router.queue_empty():
            while not not self.app.ci_router.queue_empty():
                self.app.ci_router.queue_get()
                self.app.ci_router.task_done()

    def send(self, transaction_id, content):
        self.socket.client_request(
            transaction_id=transaction_id,
            content_type=SMP_CONTENT_SIGNAL,
            content=content
        )


class ClientTransactionPool(GenericComponent):
    """ Обработка входящих сообщений сокета :ref:`интерфейс управления
    <ci.interfaces>` осуществляется только пока воркер находится рабочем
    состоянии -- при входе в состояние завершения новые сигналы в очередь
    добавляться перестают. Хотя, сигналы уже присутствующие в очереди, будут
    обработаны.

    На данный момент метод ожидает сигнал с типом содержимого
    ``SMP_CONTENT_SIGNAL``.
    """
    name = 'transactions'
    shared = True

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(ClientTransactionPool, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.app = self.core.shared.get('app')

        self.messages.debug('initializing transaction pool')
        self.pool = GenericTransactionPool()
        self.router = self.app.smp_router

    def handle(self, *args, **kwargs):
        """ """
        super(ClientTransactionPool, self).handle(*args, **kwargs)

        if self.pool:

            # просроченные операции
            if self.pool.timed_out:
                self.messages.debug(
                    '%d transaction(s) timed-out' % len(self.pool.timed_out))
                self.app.ci_socket.reconnect()

                for transaction in self.pool.timed_out:

                    # если попытки остались, отсылаем повторно, статус RETRY
                    if transaction.liveness:
                        self.retry(transaction)

                    # попытки закончились
                    else:
                        self.failed(transaction)

            if self.pool.finished:
                for transaction in self.pool.finished:
                    self.complete(transaction)

    # State Control

    def success(self, transaction):
        self.messages.info('transaction successful: %s' % transaction.id)
        self.pool[transaction.id].state = STATE_SUCCESS

    def failed(self, transaction):
        err_msg = 'transaction failed to complete ' \
                  'due to gateway timeout: %s@%s' \
                  % (transaction.id, self.app.ci_socket.target)

        self.messages.error(err_msg)
        transaction.state = STATE_FAILURE

        # FINAL-сообщение об ошибке
        self.pool.response_save(SMPClientMessage(
            message_type=SMP_TYPE_FINAL,
            transaction_id=transaction.id,
            content_type=SMP_CONTENT_STRING,
            content=err_msg,
            code=SMP_CODE_GATEWAY_TIMEOUT
        ))

    def retry(self, transaction):
        self.messages.warning(
            're-sending transaction: %s, %d attempts left'
            % (transaction.id, transaction.liveness)
        )
        transaction.state = STATE_RETRY
        self.app.ci.send(
            transaction_id=transaction.id,
            content=transaction.meta.cache
        )
        transaction.refresh()

    def complete(self, transaction):
        """ Метод для обработки завершенной транзакции. В зависимости от
        модификации клиентможет передавать сообщения собственным обработчикам
        (например, для отображения), либо передавать дальше (например, при
        выполнении запросов REST-сервера). """
        self.pool.unregister(transaction.id)
        self.app.transaction_complete(transaction)

    def missing(self, msg):
        """ Метод вызывается, если транзакция для сообщения не зарегистрирована.
        Такая ситуация может возникать по разным причинам, например, если клиент
        получил сообщение, после того, как закрыл удалил транзакцию как
        просроченную, либо если сервер инициирует передачу. """
        self.core.messages.warning(
            'message for a non-registered transaction: %s' % msg.transaction_id
        )
        self.app.transaction_missing(msg)

    # Response Handling

    def save_response(self, msg):
        self.messages.debug(
            'saving response for transaction: %s' % msg.transaction_id)
        self.app.transactions.pool.response_save(msg)

    def clear_pool(self):
        del self.pool[:]
