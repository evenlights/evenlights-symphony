# -*- coding: utf-8 -*-
import zmq
from jsonschema.exceptions import best_match

from symphony.core.exceptions import ImproperlyConfigured
from symphony.protocols.smp.adapters import SMPSocketAdapter
from symphony.apps.transport.schemas import ZTCDraft4Validator, ZTCSchema
from symphony.apps.base import BaseApp

from symphony.apps.defaults import ZMQ_CONFIG_SECTION


class ZMQTransportBase(BaseApp):
    """
    Итак, что нужно для транспортной базы...
    * сокеты, помимо инфраструктуры, должна регистрироваться
    * конфиг транспорта 2/ZTC
    """

    def __init__(self, context=None, **kwargs):
        """ """
        super(ZMQTransportBase, self).__init__(**kwargs)
        self.runtime.update_defaults({
            'transport': {
                'sockets': {},
                'own_context': True,
                'socket_poll': {},
                'config': None
            }
        })
        # Context (ZMQ context is thread-safe)
        if context:
            self.context = context
            self.runtime['transport']['own_context'] = False
        else:
            self.context = None

    def init(self, **kwargs):
        super(ZMQTransportBase, self).init(**kwargs)
        self.messages.debug('ZMQ transport init')

        if not self.context:
            self.messages.debug('ZMQ context init')
            self.context = zmq.Context()
        else:
            self.messages.debug('using existing context')

        self.messages.debug('validating ZMQ transport config')
        if ZMQ_CONFIG_SECTION not in self.config:
            self._raise_and_message(
                'no transport config section: %s' % ZMQ_CONFIG_SECTION,
                exception_class=ImproperlyConfigured
            )

        # note: на данный момент поддерживается только этот способ зугрузки ZTC

        ztc_error = best_match(
            ZTCDraft4Validator(ZTCSchema).iter_errors(self.config[ZMQ_CONFIG_SECTION])
        )
        if ztc_error:
            self._raise_and_message(ztc_error.message, exception_class=ImproperlyConfigured)

        if self.name not in self.config[ZMQ_CONFIG_SECTION]['apps']:
            self._raise_and_message(
                'no transport configuration for app: %s' % self.name,
                exception_class=ImproperlyConfigured
            )

        if 'context' in self.transport_config:
            for name, value in list(self.transport_config.context.items()):
                self.messages.debug('applying context option: %s=%s' % (name, value))
                self.context.setsockopt(getattr(zmq, name.upper()), value)

        self.messages.debug('socket polling init')
        self.poller = zmq.Poller()

        return self

    def handle(self, *args, **kwargs):
        self.socket_poll()
        super(ZMQTransportBase, self).handle(*args, **kwargs)

    def shutdown(self):
        # сокеты должны свернуться на стадии завершения инфраструктуры и удалить себя из всех реестров
        sockets = self.runtime['transport']['sockets']
        if sockets:
            self._raise_and_message(
                'sockets still registered during shutdown: %s' % ', '.join([n for n in sockets])
            )

        self.poller = None

        if self.runtime['transport']['own_context']:
            self.context.term()
        self.context = None
        del self.runtime['transport']

        super(ZMQTransportBase, self).shutdown()

    # Transport
    @property
    def transport_config(self):
        config = self.config[ZMQ_CONFIG_SECTION]['apps'][self.name]

        @property
        def context():
            return config['context']

        @property
        def sockets():
            return config['sockets']

        return config

    def socket_join(self, name, **kwargs):
        """ Метод добавления нового сокета к текущей инфраструктуре. Поиск конфигурации происходит по
        имени в разделе ``sockets`` соответствующего приложения. Если сокет не найден, будет порождено
        соответствующее исключение.

        :param name: имя нового сокета
        :return: созданный сокет
        :raises: KeyError
        """
        try:
            self.messages.debug('joining socket: %s' % name)
            config = self.transport_config.sockets[name]
            self.infra.join(
                SMPSocketAdapter(name=name), config=config,
                context=self.context, poller=self.poller,
                socket_registry=self.runtime['transport']['sockets'],
                **kwargs
            )
            return getattr(self, name)

        except KeyError:
            self._raise_and_message('no configuration for socket: %s' % name, exception_class=KeyError)

    def socket_poll(self):
        """ Метод опроса сокетов. """
        # fixme: на данный момент, несмотря на наличие схем, дефолтные значения не присваиваются
        self.runtime['transport']['socket_poll'] = dict(
            self.poller.poll(self.transport_config['poll_interval'])
        )

    def socket_poll_in(self, adapter):
        """ Метод для проверки входящих сообщений в сокете.
        :returns: ``True``, если сокет заданного адаптера находится в текущем опрсе, иначе ``False``
        :param adapter: адаптер для проверки """
        return adapter.socket in self.runtime['transport']['socket_poll']
