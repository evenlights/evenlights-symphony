# -*- coding: utf-8 -*-
from symphony.core.containers import container_validate
from symphony.core.infra.generic import GenericComponent
from symphony.core.exceptions import ImproperlyConfigured
from symphony.timers.containers import GenericTimer
from symphony.protocols.smp.containers import SMPSignal

from symphony.protocols.smp.defaults import (
    SMP_TYPE_FINAL, SMP_TYPE_LC_INIT, SMP_TYPE_LC_HEARTBEAT,
    SMP_TYPE_LC_SHUTDOWN,
    SMP_CONTENT_SIGNAL, SMP_CONTENT_TYPES_HR,
)
from symphony.nodes.transactions.defaults import (
    STATE_QUEUED, STATE_ASSIGNED, STATE_SUCCESS, STATE_RETRY, STATE_FAILURE
)


class WorkerHBTimer(GenericTimer):
    """ По исчтечении интервала таймер отправляет сердцебиение и сбрасывает
    счетчик. """

    def on_timeout(self):
        for worker in self.mount_point.worker_pool:
            self.mount_point.lifecycle_be.lifecycle_heartbeat(address=worker.id)
        self.reset()


class BrokerCIFE(GenericComponent):
    """ Метод обрабатывает входящие сообщения сокета интерфейса управления
    и, если они валидны, помещает сигнал в очередь контрольного интерфейса.

    Логика того, какие сигналы выполняются обработчиком мастер-приложения,
    а какие проксируются интерфейсам воркер-процессов, описывается в
    абстрактном методе :func:`ci_fe_handle`
    """
    name = 'ci_fe_unit'

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(BrokerCIFE, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.app = self.core.shared.get('app')

        self.messages.debug('initializing sockets')
        self.app.socket_join('ci_fe', registry=self.infra.components)
        self.socket = self.app.ci_fe

    def handle(self, *args, **kwargs):
        super(BrokerCIFE, self).handle(*args, **kwargs)

        if self.app.is_running:
            if self.app.socket_poll_in(self.socket):
                msg = self.socket.client_recv()

                # обработка критических ошибок
                error_domain = 'CI frontend socket'
                error_message = None

                # ошибка протокола: код 400
                if self.socket.messages.errors:
                    error_message = self.socket.messages.first_error_content

                # неверный тип контента
                elif not self.socket.validate_value(
                        msg.content_type, SMP_CONTENT_SIGNAL,
                        var_name='content_type', hr_dict=SMP_CONTENT_TYPES_HR
                ):
                    error_message = self.socket.messages.first_error_content

                # неверный формат контейнера
                elif not container_validate(SMPSignal, msg.content):
                    error_message = 'invalid signal container received'

                if error_message:
                    self.messages.error(
                        '%s: %s' % (error_domain, error_message)
                    )
                    return

                signal = SMPSignal.from_json(msg.content)
                signal.address = msg.address
                signal.transaction_id = msg.transaction_id

                # fixme: транзакции
                # создание/обновление транзакции: QUEUED/RETRY
                # try:
                #     # todo: https://bitbucket.org/evenlights/symphony/issues/3/appsbroker-queued-retry
                #     resp = self.transactions_api.single('transaction__read', signal.transaction_id)
                #     if resp:
                #         state = STATE_RETRY if resp.code == SMP_CODE_OK else STATE_QUEUED
                #         # fixme: transacion_wite не очень хорош...
                #         self.transactions_api.transaction_write(signal, state=state)
                # except AttributeError:
                #     pass

                self.app.ci_router.queue_put(signal)


class BrokerCIBE(GenericComponent):
    """ Обработка активности бэкэнда :ref:`интерфейса управления
    <ci.interfaces>`, если он подключен, производится в любом случае вне
    зависимости от состояния завершения. Таким образом, поскольку цикл
    приложения продолжается до тех пор, пока не освободится воркер-пул, все
    ответы, посыламые воркерами будут доставлены клиентам.

    В определенных модификациях приложений брокер может работать и без воркеров,
    выполняя запросы клиентов самостоятельно. В этом случае следует использовать
    :ref:`соответствующий тип обработчиков сигналов
    <protocols.smp.ci.handlers>`.
    """
    name = 'ci_be_unit'

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(BrokerCIBE, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.app = self.core.shared.get('app')

        # проверяем конфигурацию
        be_sockets = [
            'CI_BE_CONF' in self.config, 'LIFECYCLE_BE_CONF' in self.config
        ]
        if not (all(be_sockets) or not any(be_sockets)):
            self._raise_and_message(
                'Control Interface and LifeCycle backend sockets should '
                'be used both or not at all',
                exception_class=ImproperlyConfigured
            )

        self.messages.debug('initializing sockets')
        self.app.socket_join('ci_be', registry=self.infra.components)
        self.socket = self.app.ci_be
        self.worker_pool = self.app.worker_pool

    def handle(self, *args, **kwargs):
        super(BrokerCIBE, self).handle(*args, **kwargs)

        # брокер может работать как серверное приложение со своими обработчиками
        if self.socket and self.app.socket_poll_in(self.socket):
            msg = self.socket.worker_recv()

            error_domain = 'CI backend socket'
            error_message = None

            # ошибка протокола
            if self.socket.messages.errors:
                error_message = self.socket.messages.first_error_content

            if error_message:
                self.messages.error('%s: %s, trying LifeCycle/SHUTDOWN'
                                    % (error_domain, error_message))
                # в случае ошибки адаптер возвращает "сырое" сообщение, если
                # воркер с таким id зарегистрирован, отправляем сигнал
                # shutdown и удаляем из пула

                worker_id = msg[0]
                if worker_id in self.worker_pool:
                    self.app.lifecycle_be.lifecycle_shutdown(address=worker_id)
                    self.worker_pool.unregister(worker_id)
                return

            # fixme: транзакции
            # обновление транзакции: SUCCESS/FAILURE
            # try:
            #     state = STATE_SUCCESS if msg.code < 300 else STATE_FAILURE
            #     self.transactions_api.single(
            #         'transaction__write', msg.transaction_id, state=state)
            # except AttributeError:
            #     pass

            # перенаправляем сообщение клиенту
            self.app.ci_fe.client_send(
                message_type=msg.message_type,
                transaction_id=msg.transaction_id,
                content_type=msg.content_type,
                code=msg.code,
                content=msg.content,
                address=msg.client_id
            )

            # освобождаем воркер, если это его последнее сообщение
            if msg.message_type == SMP_TYPE_FINAL:
                worker_id = msg.address
                transaction_id = msg.transaction_id
                self.messages.info(
                    'releasing worker: %s, transaction id: %s' %
                    (worker_id, transaction_id)
                )

                # если воркер в воркер-пуле, пытаемся освободить его
                if worker_id in self.worker_pool:

                    if worker_id in self.worker_pool.engaged:
                        self.worker_pool.release(worker_id)
                        self.worker_pool.heartbeat(worker_id)
                    else:
                        # нарушение протокола: повторный Worker/FINAL
                        self.messages.warning(
                            'Worker/FINAL from released worker, '
                            'sending LifeCycle/SHUTDOWN: %s' % worker_id
                        )
                        self.app.lifecycle_be.lifecycle_shutdown(
                            address=worker_id
                        )

                # если воркер уходил в оффлайн, регистрируем его повторно
                else:
                    self.messages.warning(
                        'Worker/FINAL from unregistered worker, '
                        'registering: %s' % worker_id
                    )

                    # использовать реконнект для backend-сокета мы не можем,
                    # поэтому в контроле сердцебиения участвует только таймаут
                    self.worker_pool.register(
                        worker_id,
                        timeout=self.config.HEARTBEAT_INTERVAL_WORKERS
                    )


class BrokerLifecycleBE(GenericComponent):
    """ Обработка активности бэкэнда жизненного цикла, если он подключен,
    производится в любом случае вне зависимости от состояния завершения.
    Поскольку цикл приложения продолжается до тех пор, пока не освободится
    воркер-пул, обратная связь с жизненными циклами воркеров совершенно
    необходима.

    В определенных модификациях приложений брокер может работать и без воркеров,
    выполняя запросы клиентов самостоятельно.
    """
    name = 'lifecycle_be_unit'

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(BrokerLifecycleBE, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.app = self.core.shared.get('app')

        # проверяем конфигурацию
        be_sockets = [
            'CI_BE_CONF' in self.config, 'LIFECYCLE_BE_CONF' in self.config
        ]
        if not (all(be_sockets) or not any(be_sockets)):
            self._raise_and_message(
                'Control Interface and LifeCycle backend sockets should '
                'be used both or not at all',
                exception_class=ImproperlyConfigured
            )

        self.messages.debug('initializing sockets')
        self.app.socket_join('lifecycle_be', registry=self.infra.components)
        self.socket = self.app.lifecycle_be

        self.messages.debug('initializing control timers')
        self.app.timers['heartbeat_self'] = WorkerHBTimer(
            mount_point=self.app, seconds=self.config.HEARTBEAT_INTERVAL_SELF,
        )

    def handle(self, *args, **kwargs):
        super(BrokerLifecycleBE, self).handle(*args, **kwargs)

        if self.socket:
            if self.app.socket_poll_in(self.socket):
                msg = self.socket.lifecycle_recv()

                error_domain = 'LifeCycle backend socket'
                error_message = None

                # ошибка протокола
                if self.socket.messages.errors:
                    error_message = self.socket.messages.first_error_content

                if error_message:
                    self.messages.error(
                        '%s: %s, trying LifeCycle/SHUTDOWN' %
                        (error_domain, error_message)
                    )
                    # в случае ошибки адаптер возвращает "сырое" сообщение,
                    # если воркер с таким id зарегистрирован, отправляем сигнал
                    # shutdown и удаляем из пула

                    worker_id = msg[0]
                    if worker_id in self.app.worker_pool:
                        self.socket.lifecycle_shutdown(address=worker_id)
                        self.app.worker_pool.unregister(worker_id)
                    return

                worker_id = msg.address

                # LifeCycle/INIT
                if msg.message_type == SMP_TYPE_LC_INIT:

                    # воркер подключился, добавляем в пул
                    if worker_id not in self.app.worker_pool:
                        self.messages.info('worker signed in: %s' % worker_id)
                        self.app.worker_pool.register(
                            worker_id,
                            timeout=self.config.HEARTBEAT_INTERVAL_WORKERS
                        )
                    else:
                        # восстановление: воркер вернулся раньше,
                        # чем был очищен из пула
                        self.messages.warning(
                            'worker recovered: %s' % worker_id
                        )
                        self.app.worker_pool.heartbeat(worker_id)

                # LifeCycle/HEARTBEAT
                elif msg.message_type == SMP_TYPE_LC_HEARTBEAT:

                    if worker_id in self.app.worker_pool:
                        self.app.worker_pool.heartbeat(worker_id)
                    else:
                        # восстановление: HEARTBEAT от незарегистрированного
                        # воркера
                        self.messages.warning(
                            'worker recovered: %s' % worker_id
                        )
                        self.app.worker_pool.register(
                            worker_id,
                            timeout=self.config.HEARTBEAT_INTERVAL_WORKERS
                        )

                # LifeCycle/SHUTDOWN
                elif msg.message_type == SMP_TYPE_LC_SHUTDOWN:
                    # воркер сообщил о завершении, удаляем из пула, если он там
                    if worker_id in self.app.worker_pool:
                        self.messages.info('worker signed out: %s' % worker_id)
                        self.app.worker_pool.unregister(worker_id)
                    else:
                        self.messages.warning(
                            'LifeCycle/SHUTDOWN error, worker not signed in, '
                            'ignoring: %s' % worker_id
                        )

            # отправка собственного сердцебиения
            self.app.timers['heartbeat_self'].refresh()

            # очистка воркер-пула
            if self.app.worker_pool.timed_out:
                for worker in self.app.worker_pool.timed_out:
                    self.messages.warning(
                        'LifeCycle/HEARTBEAT failure, no heartbeat '
                        'from worker: %s' % worker.id)
                    self.app.worker_pool.unregister(worker.id)
