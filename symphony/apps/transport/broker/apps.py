# -*- coding: utf-8 -*-
from symphony.core.exceptions import ImproperlyConfigured
from symphony.routers.components import QueueRouter
from symphony.pools.workers import GenericWorkerPool
from symphony.apps.transport.base import ZMQTransportBase
from symphony.apps.transport.broker.components import (
    BrokerCIFE, BrokerCIBE, BrokerLifecycleBE
)
# from symphony.nodes.logger.api import Logger
# from symphony.nodes.transactions.api import Transactions

from symphony.protocols.smp.defaults import SMP_CONTENT_SIGNAL
from symphony.nodes.transactions.defaults import (
    STATE_ASSIGNED, STATE_SUCCESS, STATE_FAILURE
)


class BrokerBase(ZMQTransportBase):
    """ Типичными случаями мастер-приложений являются балансеры,
    проксирующие приложения, менеджеры очередей и подобные им единицы.

    Базовый класс мастер-приложения использует 4 типа сокетов, 2 из которых
    обязательны, а 2 опциональны:
    * интерфейс управления (фронт-энд)
    * бэкэнд для воркер-процесссов
    * сокет жизненного цикла (опциональный)
    * сокет логгера (опциональный)

    Также, класс предоставляет общие инструменты для работы с этими сокетами,
    такие как валидация и обработка.
    """

    # fixme: починить API
    class Infrastructure:
        # modules = [Logger, Transactions]
        components = [BrokerCIFE, BrokerCIBE, BrokerLifecycleBE]

    def init(self, **kwargs):
        super(BrokerBase, self).init(**kwargs)

        self.messages.debug('initializing control interface')
        self.infra.join(
            QueueRouter(name='ci_router', handlers=self.signal_handlers, shared=True)
        )

        self.core.messages.debug('initializing worker pool')
        self.worker_pool = GenericWorkerPool()

    def handle(self, *args, **kwargs):
        super(BrokerBase, self).handle(self, *args, **kwargs)
        self.ci_queue_handle()

    @property
    def is_running(self):
        return self.runtime['is_running'] or self.worker_pool

    # Control Interface

    def ci_queue_handle(self):
        """ Абстрактный метод для обработки очереди. В зависимости от
        модификации мастер-приложения это может быть либо FQ-проксирование, либо
        LRU-балансировка нагрузки.
        """
        raise NotImplementedError

    def exit(self):
        super(BrokerBase, self).exit()

        # второе условие выполнения цикла
        if self.worker_pool:

            self.core.messages.info(
                'sending LifeCycle/SHUTDOWN downstream: %d workers' %
                len(self.worker_pool)
            )
            for worker in self.worker_pool:
                self.lifecycle_be.lifecycle_shutdown(address=worker.id)


class LRUBrokerBase(BrokerBase):
    """ """

    def ci_queue_handle(self):
        """ За один цикл выполнения метод
        * пополняет очередь контрольного интерфейса, если есть входящие сигналы
        * выполняет следующий сигнал из очереди, если она не пуста

        Проксируемые сигналы выполняются только если есть свободные
        воркер-процессы.
        """

        # выполняем следующий сигнал из очереди
        if not self.ci_router.queue_empty():

            # получаем следующий сигнал
            signal = self.ci_router.queue_get()

            # если для сигнала в текущем мэппиге приложения зарегистрирован
            # обработчик, выполняем его
            if self.ci_router.has_handler(signal):

                self.core.mbus.flush()
                self.ci_router.handle(signal)

                # fixme: transactions
                # обновление транзакции: SUCCESS/FAILURE
                # if self.transactions_api:
                #     trx_state = STATE_SUCCESS if not self.core.mbus.errors \
                #         else STATE_FAILURE
                #     self.transactions_api.transaction_write(signal, state=trx_state)

            # перенаправляем, если есть свободные воркеры
            elif self.worker_pool.released:

                # получаем адрес следующего воркера
                worker_id = self.worker_pool.engage_lru()
                self.core.messages.info(
                    'engaging worker: %s, transaction id: %s' %
                    (worker_id, signal.transaction_id)
                )

                # fixme: transactions
                # если есть сокет транзакций, обновление транзакции: ASSIGNED
                # if self.transactions_api:
                #     self.transactions_api.transaction_write(
                #         signal, state=STATE_ASSIGNED
                #     )

                self.ci_be.worker_request(
                    content_type=SMP_CONTENT_SIGNAL,
                    transaction_id=signal.transaction_id,
                    client_id=signal.address,
                    content=signal.repr.json,
                    address=worker_id
                )

            self.ci_router.task_done()


Broker = LRUBrokerBase
