# -*- coding: utf-8 -*-
from jsonschema.validators import Draft4Validator, extend
from jsonschema.exceptions import ValidationError
from zmq.sugar.constants import ctx_opt_names, bytes_sockopt_names, int_sockopt_names


ZTCSchema = {
    'title': 'ZeroMQ Transport Configuration Schema',
    'description': 'ZeroMQ Transport Configuration Schema implements RFC 2/ZTC',

    # Определения
    'definitions': {

        # Сокеты
        'sockets': {

            # generic-сокет
            'generic': {
                'type': 'object',
                'required': ['type'],
                'properties': {
                    # Тип Сокета
                    'type': {
                        'type': 'string',
                        'enum': [
                            'PAIR', 'PUB', 'SUB', 'REQ', 'REP', 'DEALER', 'ROUTER', 'XREQ',
                            'XREP', 'PULL', 'PUSH', 'XPUB', 'XSUB', 'UPSTREAM', 'DOWNSTREAM',
                            'STREAM', 'CLIENT', 'SERVER'
                        ],
                    },

                    # в качестве адреса допускаются строки и массивы строк
                    'bind': {
                        'anyOf': [
                            {'type': 'string'},
                            {'type': 'array', 'items': {'type': 'string'}, 'uniqueItems': True},
                        ]
                    },
                    'connect': {
                        'anyOf': [
                            {'type': 'string'},
                            {'type': 'array', 'items': {'type': 'string'}, 'uniqueItems': True},
                        ]
                    },

                    # Опции Сокета
                    'option': {
                        'type': 'object',
                        'properties': {
                            'reconnect_delay': {
                                'type': 'number',
                                'default': 0.2,
                            },
                        },
                        'additionalProperties': {
                            'anyOf': [
                                # в качестве опций допускаются строки, массивы строк и целые числа
                                {'type': 'string'},
                                {'type': 'array', 'items': {'type': 'string'}, 'uniqueItems': True},
                                {'type': 'integer'},
                            ]
                        },
                    },
                },

                # прочие опции в секциях сокетов не разрешены
                'additionalProperties': False
            },

            # метод bind
            'bind': {
                'allOf': [{'$ref': '#/definitions/sockets/generic'}, {'required': ['bind']}]
            },

            # метод connect
            'connect': {
                'allOf': [{'$ref': '#/definitions/sockets/generic'}, {'required': ['connect']}]
            },
        }
    },

    # Схема
    '$schema': 'http://json-schema.org/schema#',
    'type': 'object',
    'required': ['version', 'apps'],
    'properties': {

        # Версия
        'version': {'type': 'number', 'enum': [1.0]},

        # Приложения
        'apps': {
            'type': 'object',
            'additionalProperties': {
                'type': 'object',
                'properties': {

                    # Контекст
                    'context': {
                        'type': 'object',
                        # опции контекста допускают только целочисленные значения
                        'additionalProperties': {'type': 'integer'},
                    },

                    # Сокеты
                    'sockets': {
                        'type': 'object',
                        'additionalProperties': {
                            'oneOf': [
                                {'$ref': '#/definitions/sockets/bind'},
                                {'$ref': '#/definitions/sockets/connect'},
                            ],
                        },
                    },

                    # Прочие опции
                    'poll_interval': {'type': 'integer', 'default': 1000},
                },

                # прочие опции в секциях приложений не разрешены
                'additionalProperties': False,
            },

        },
    },

    # прочие опции в корневой секции не разрешены
    'additionalProperties': False,
}


additionalPropertiesOrig = Draft4Validator.VALIDATORS['additionalProperties']


def additionalPropertiesZTC(validator, aP, instance, schema):

    # оргинальная валидация
    for error in additionalPropertiesOrig(validator, aP, instance, schema):
        yield error

    # действительные секции context не будут иметь additionalProperties
    if not aP and isinstance(instance, dict) and 'context' in list(instance.keys()):
        for name, value in list(instance['context'].items()):

            # валидируем только имена, поскольку значения ограничены схемой
            if name.upper() not in ctx_opt_names:
                yield ValidationError('Unexpected ZMQ context option: %s' % name)

    # действительные секции option не будут иметь additionalProperties
    if not aP and isinstance(instance, dict) and 'option' in list(instance.keys()):
        for name, value in list(instance['option'].items()):
            if name.upper() in bytes_sockopt_names:
                if not (isinstance(value, str) or isinstance(value, list)):
                    yield ValidationError('Unexpected value for socket string option: %s=%s' % (name, value))
            elif name.upper() in int_sockopt_names:
                if not isinstance(value, int):
                    yield ValidationError('Unexpected value for socket int option: %s=%s' % (name, value))
            else:
                yield ValidationError('Unexpected socket option: %s' % name, )


ZTCDraft4Validator = extend(Draft4Validator, {'additionalProperties': additionalPropertiesZTC})
