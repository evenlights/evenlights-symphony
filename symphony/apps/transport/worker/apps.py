# -*- coding: utf-8 -*-
from threading import Thread

from symphony.routers.components import QueueRouter
from symphony.apps.transport.base import ZMQTransportBase
from symphony.apps.transport.worker.components import WorkerCI, WorkerLC

from symphony.protocols.smp.defaults import (
    SMP_CONTENT_STRING, SMP_CODE_BAD_REQUEST
)


class WorkerBase(ZMQTransportBase):

    # fixme: починить API
    class Infrastructure:
        # modules = [Logger]
        components = [WorkerCI, WorkerLC]

    def __init__(self, **kwargs):
        super(WorkerBase, self).__init__(**kwargs)
        self.runtime.update_defaults({'is_engaged': False})

    def init(self, **kwargs):
        super(WorkerBase, self).init(**kwargs)

        self.messages.debug('initializing control interface')
        self.infra.join(
            QueueRouter(name='ci_router', handlers=self.signal_handlers, shared=True)
        )

    def handle(self, *args, **kwargs):
        super(WorkerBase, self).handle(self, *args, **kwargs)
        self.ci_queue_handle()

    @property
    def is_engaged(self):
        return self.runtime['is_engaged']

    @property
    def is_running(self):
        return self.runtime['is_running'] or not self.ci_router.queue_empty()

    def ci_queue_handle(self):
        """ Обычно именно воркер, в  противоположность :ref:`брокеру
        <apps.broker>`, является конечной точкой выполнения сигналов, поэтому
        обработка очереди фактически идентична обработке интерфейса управления.

        .. note::

           Важной особенностью обработчиков сигналов воркер-процессов является
           тот факт, что они не могут возвращать "голых" значений, которые могли
           бы быть без изменения быть перенаправлены брокеру, поскольку тип
           контента и код ответа различаются от задачи к задаче. Вместо этого
           они должны сами направить в сокет ИУ-фронтэнда сигнал ответ, для чего
           они располагают всеми необходимыми данными.
        """
        if not self.is_engaged and not self.ci_router.queue_empty():
            signal = self.ci_router.queue_get()
            handler_id = self.ci_router.get_handler_id(signal)
            handler = self.ci_router.get_handler(handler_id)

            if handler:
                # с этого момента все сообщения относятся к транзакции
                try:
                    self.logger_api.handler.transaction = signal.transaction_id
                except AttributeError:
                    pass

                self.ci_router.handle(signal)

                # далее логгирование без привязки к транзакции
                try:
                    self.logger_api.handler.transaction = None
                except AttributeError:
                    pass

            else:
                self.ci_socket.worker_final(
                    transaction_id=signal.transaction_id,
                    content_type=SMP_CONTENT_STRING,
                    code=SMP_CODE_BAD_REQUEST,
                    client_id=signal.address,
                    content=self.ci_router.messages.first_error_content
                )
            self.ci_router.task_done()


class ThreadWorker(WorkerBase, Thread):
    pass


Worker = ThreadWorker
