# -*- coding: utf-8 -*-
from symphony.core.containers import container_validate
from symphony.core.infra.generic import GenericComponent
from symphony.timers.containers import GenericTimer, PPPTimer
from symphony.protocols.smp.containers import SMPSignal

from symphony.protocols.smp.defaults import (
    SMP_TYPE_LC_HEARTBEAT, SMP_TYPE_LC_RESET, SMP_TYPE_LC_SHUTDOWN,
    SMP_CONTENT_SIGNAL, SMP_CONTENT_TYPES_HR,
)


class WorkerHBTimer(GenericTimer):
    """ По исчтечении интервала таймер отправляет сердцебиение и сбрасывает
    счетчик. """

    def on_timeout(self):
        self.mount_point.lifecycle_socket.lifecycle_heartbeat()
        self.reset()


class BrokerHBTimer(PPPTimer):
    """ При очередном достижении нулевой живучести таймер ждет заданный
    интервал и повтрно подключает сокеты интерфейса управления и жизенного
    цикла. """

    def on_zero_liveness(self):
        self.mount_point.messages.warning(
            'LifeCycle/HEARTBEAT failure, cannot reach broker process'
        )
        self.mount_point.messages.info('retrying in %d seconds' % self.retry_in)
        self.sleep()
        self.mount_point.ci_socket.reconnect()
        self.mount_point.lifecycle_socket.reconnect()


class WorkerCI(GenericComponent):
    """ Обработка входящих сообщений сокета :ref:`интерфейс управления
    <ci.interfaces>` осуществляется только пока воркер находится рабочем
    состоянии -- при входе в состояние завершения новые сигналы в очередь
    добавляться перестают. Хотя, сигналы уже присутствующие в очереди, будут
    обработаны.

    На данный момент метод ожидает сигнал с типом содержимого
    ``SMP_CONTENT_SIGNAL``.
    """
    name = 'ci'

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(WorkerCI, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.app = self.core.shared.get('app')

        self.messages.debug('initializing sockets')
        self.app.socket_join('ci_socket', registry=self.infra.components)
        self.socket = self.app.ci_socket
        self.router = self.app.ci_router

    def handle(self, *args, **kwargs):
        super(WorkerCI, self).handle(*args, **kwargs)

        if self.app.is_running:
            if self.app.socket_poll_in(self.socket):
                msg = self.socket.worker_recv()

                self.app.timers['heartbeat_broker'].reset()

                # обработка критических ошибок
                error_domain = 'CI socket'
                error_message = None

                # ошибка протокола
                if self.socket.messages.errors:
                    error_message = self.socket.messages.first_error_content

                # неверный тип контента
                elif not self.socket.validate_value(
                        msg.content_type, SMP_CONTENT_SIGNAL,
                        var_name='content_type', hr_dict=SMP_CONTENT_TYPES_HR
                ):
                    error_message = self.socket.messages.first_error_content

                # неверный формат контейнера
                elif not container_validate(SMPSignal, msg.content):
                    error_message = 'invalid signal container received'

                if error_message:
                    self.messages.critical(
                        '%s: %s, disconnecting' % (error_domain, error_message)
                    )
                    self.app.exit()
                    return

                signal = SMPSignal.from_json(msg.content)
                signal.address = msg.client_id
                signal.transaction_id = msg.transaction_id

                self.router.queue_put(signal)


class WorkerLC(GenericComponent):
    """ Обработка активности сокета жизненного цикла, если он подключен,
    производится в любом случае вне зависимости от состояния завершения.
    Поскольку цикл брокера продолжается до тех пор, пока не освободится
    воркер-пул, обратная связь с жизненными циклами воркеров совершенно
    необходима.

    В определенных модификациях приложений брокер может работать и без
    канала LifeCycle. В этом случае завершения воркера должно
    осуществляться соответствующим сигналом.
    """
    name = 'lifecycle_unit'

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(WorkerLC, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.app = self.core.shared.get('app')

        self.messages.debug('initializing sockets')
        self.app.socket_join('lifecycle_socket', registry=self.infra.components)
        self.socket = self.app.lifecycle_socket

        self.messages.debug('initializing control timers')
        self.app.timers['heartbeat_self'] = WorkerHBTimer(
            mount_point=self.app, seconds=self.config.HEARTBEAT_INTERVAL_SELF,
        )
        self.app.timers['heartbeat_broker'] = BrokerHBTimer(
            mount_point=self.app,
            seconds=self.config.HEARTBEAT_INTERVAL_BROKER,
            liveness=self.config.HEARTBEAT_LIVENESS_BROKER,
            retry_init=self.config.HEARTBEAT_RECONNECT_INIT,
            retry_max=self.config.HEARTBEAT_RECONNECT_MAX,
        )

    def activate(self):
        super(WorkerLC, self).activate()
        self.messages.info('sending LifeCycle/INIT upstream')
        self.socket.lifecycle_init()

    def handle(self, *args, **kwargs):
        super(WorkerLC, self).handle(*args, **kwargs)

        # получение
        if self.app.socket_poll_in(self.socket):
            msg = self.socket.lifecycle_recv()

            # критические ошибки
            error_domain = 'LifeCycle socket'
            error_message = None

            # ошибка протокола
            if self.socket.messages.errors:
                error_message = self.socket.messages.first_error_content

            if error_message:
                self.messages.critical(
                    '%s: %s, disconnecting' % (error_domain, error_message)
                )
                self.app.exit()
                return

            # LifeCycle/HEARTBEAT
            if msg.message_type == SMP_TYPE_LC_HEARTBEAT:
                self.app.timers['heartbeat_broker'].reset()

            # LifeCycle/RESET
            elif msg.message_type == SMP_TYPE_LC_RESET:
                self.messages.info('LifeCycle/RESET signal received')
                self.app.timers['heartbeat_broker'].reset()
                self.app.reset()

            # LifeCycle/SHUTDOWN
            elif msg.message_type == SMP_TYPE_LC_SHUTDOWN:
                self.messages.info('LifeCycle/SHUTDOWN signal received')
                self.app.exit()
                return

        # отправка собственного сердцебиения
        self.app.timers['heartbeat_self'].refresh()

        # PPP (проверка сердцебиения брокера)
        self.app.timers['heartbeat_broker'].refresh()

    def deactivate(self):
        self.messages.info('sending LifeCycle/SHUTDOWN upstream')
        self.socket.lifecycle_shutdown()

        super(WorkerLC, self).deactivate()
