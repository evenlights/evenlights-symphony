# -*- coding: utf-8 -*-
import time

from symphony.apps.base import BaseApp
from symphony.apps.configs import ThreadPoolConfig
from symphony.pools.workers import GenericWorkerPool, WorkerInfo


class ThreadPool(BaseApp):
    config = ThreadPoolConfig
    threads = []

    def __init__(self, name='thread-pool', *args, **kwargs):
        super(ThreadPool, self).__init__(name=name, *args, **kwargs)
        self.thread_pool = None

    def _base_setup(self):
        super(ThreadPool, self)._base_setup()

        self.core.messages.debug('initializing thread pool')
        self.thread_pool = GenericWorkerPool()

        # ЗАПУСК ПОТОКОВ
        self.core.messages.info('registering threads')
        for thread in self.threads:
            thread_class = thread['class']
            thread_args = self.resolve_vars(thread['args']) if 'args' in thread else []
            thread_kwargs = self.resolve_vars(thread['kwargs']) if 'kwargs' in thread else {}

            thread_instance = thread['class'](*thread_args, **thread_kwargs)
            thread_name = thread_instance.name

            self.core.messages.debug('adding thread: %s' % thread_name)
            self.thread_pool.append(WorkerInfo(
                id=thread_instance.sys_id,
                name=thread_name,
                class_=thread_class,
                init_args=thread_args,
                init_kwargs=thread_kwargs,
                instance=thread_instance,
            ))

        self.core.messages.info('starting threads')
        for thread in self.thread_pool:
            self.thread_start(thread)

    def _base_cycle(self):
        super(ThreadPool, self)._base_cycle()
        time.sleep(self.core.config.CYCLE_INTERVAL)

        for thread in self.thread_pool:

            # трэд инстациирован, помер, и перезапуск не запланирован
            if thread.instance and not thread.instance.is_alive() \
                    and not thread.restart_scheduled:
                RESTART_IN = self.core.config.THREAD_RESTART_INTERVAL

                self.core.messages.warning(
                    'thread is dead, scheduling restart in %d seconds: %s'
                    % (RESTART_IN, thread.instance.name))
                self.thread_stop(thread)
                thread.schedule_restart(RESTART_IN)

            # перезапуск запалнирован, интервал истек
            elif thread.restart_scheduled and thread.restart_timer.is_timed_out:
                self.thread_start(thread)

    def _base_clean(self):
        super(ThreadPool, self)._base_clean()
        for thread in self.thread_pool:
            self.thread_stop(thread)

    # todo: https://bitbucket.org/evenlights/symphony/issues/4/appsrunnersthreadpool-config-templating
    @staticmethod
    def strip_var(var):
        return var.strip().replace('{{', '').replace('}}', '').replace(' ', '')

    def resolve_vars(self, var):

        if isinstance(var, list):
            new_var = []
            for item in var:
                if isinstance(item, str) and '{{' and '}}' in item:
                        new_var.append(eval(self.strip_var(item)))
                else:
                    new_var.append(item)

        elif isinstance(var, dict):
            new_var = {}
            for item_name in var:
                if isinstance(var[item_name], str) and'{{' and '}}' in var[item_name]:
                    new_var[item_name] = eval(self.strip_var(var[item_name]))
                else:
                    new_var[item_name] = var[item_name]

        else:
            new_var = var

        return new_var

    def thread_start(self, thread):
        if not thread.instance:
            thread.instance = thread.class_(*thread.init_args, **thread.init_kwargs)

        self.core.messages.debug('starting thread: %s' % thread.instance.name)
        thread.instance.start()

        self.core.messages.debug('waiting for thread: %s' % thread.instance.name)

        while not thread.instance.is_running:
            time.sleep(0.2)

        thread.restart_scheduled = False

        self.core.messages.info('thread started successfully: %s' % thread.instance.name)

    def thread_stop(self, thread):
        # трэд инстанциирован
        if thread.instance:
            # трэд инстанциирован, жив и работает
            if thread.instance.is_running and thread.instance.is_alive():
                self.core.messages.info('normal thread stop: %s' % thread.instance.name)
                try:
                    thread.instance.shutdown()
                    thread.instance.join()
                except AttributeError:
                    pass
                finally:
                    thread.instance = None

            # трэд инстанциирован, помер, но состояние "работает"
            elif thread.instance.is_running and not thread.instance.is_alive():
                self.core.messages.warning('emergency thread stop: %s' % thread.instance.name)
                try:
                    thread.instance.shutdown()
                    thread.instance._clean()
                except AttributeError:
                    pass
                finally:
                    thread.instance = None
        else:
            self.core.messages.error('thread is not instantiated: %s' % thread)
