# -*- coding: utf-8 -*-
import logging
import logging.config

from symphony.messages.components import Messages
from symphony.apps.core.components import AppCore
from symphony.core.infra.base import InfraComponent


class BaseApp(InfraComponent):
    """ Приложение является [инфраструктурой] верхнего уровня. Базовое приложение описывает 3
    основных аспекта приложения: начальную загрузку, общую стратегию жизненного цикла и механизм
    контроля основного цикла (фаза обработки).

    Начальная загрузка: инфраструктурным компонентам, в том числе и самому приложению, необходимы
    [ядро] и [конфигарция], которые приложения инициализирует в ходе начальной загрузки до собственной
    инициализации и инициализации инфраструктуры. Также на этом этапе приложение монтирует в ручном
    режиме компонент сообщений, поскольку контроль выполнения дальнейших шагов осуществляется при
    помощи [центральной шины].

    Стратегия жизненного цикла: реализует общую философию 5 фаз жизни инфраструктуры: инициализации,
    активации, обработки, деактивации и завершения. С момента инициализации до перехода в фазу
    обработки приложение проверяет [центральную шину] на предмет ошибок, чтобы исключить запкуск
    приложения с компонентами, которые не смогли произвести инициализацию, настройку или активацию.

    Контроль основного цикла: также базовое приложение вводит механизмы для основного цикла:  обработка
    продолжается до тех пор, пока метод :meth:`is_running` возвращает значение ``True``.
    """
    id = None
    config = None
    signal_handlers = None

    def __init__(self, id=None, **kwargs):
        super(BaseApp, self).__init__(**kwargs)

        if id:
            self.id = id

        self.runtime.update_defaults({'is_running': False})

        if not self.signal_handlers:
            self.signal_handlers = []

    # Init Phase

    def bootstrap(self):
        """ Метод предварительной зрагрузки инициализирует основные элементы  инфраструктуры:
        конфигурацию и ядро. Также метод настраивает логгирование, если указан параметр
        конфигурации ``LOGGING``, и подключает компонент сообщений. """

        # конфигурация
        self._log_debug('config init')
        if self.config:
            config = self.config()
            self.config = None
            config.init(registry=self.infra.components, mount_point=self)
            self.config.load()

        self._run_hook('bootstrap_intro_hook')

        # logging
        if 'LOGGING' in self.config:
            self._log_debug('logging init')
            logger = logging.getLogger(self.name)

            self._run_hook('bootstrap_logging_hook')

            logging.config.dictConfig(self.config.LOGGING)
            if self.config.DEBUG:
                logger.setLevel(logging.DEBUG)

        # ядро
        self._log_debug('core init')
        AppCore().init(
            config=self.config, registry=self.infra.components, mount_point=self
        )

        # сообщения
        self._log_debug('messages init')
        Messages().init(
            core=self.core, config=self.config, registry=self.infra.components,
            mount_point=self
        )

    def init(self, **kwargs):
        self.core.shared.register(self, name='app')

        # id
        self.messages.debug('setting id')
        if not self.id:
            # порядок разрешения: конфиг, собственное имя
            try:
                self.id = self.core.config.ID

            except KeyError:
                self.id = self.name

            except AttributeError:
                self.id = self.name
        self.messages.info('using id: %s' % self.id)

        # mappings
        self.messages.debug('mapping container init')
        self.mappings = {}

        # timers
        self.messages.debug('timer container init')
        self.timers = {}

        self.component.state['init_successful'] = True

        return self

    # Handle Phase

    @property
    def is_running(self):
        return self.runtime['is_running']

    def exit(self):
        self.runtime['is_running'] = False

    # Shutdown Phase

    def shutdown(self):
        """ В отношении метода :meth:`shutdown` приложения важно помниить, что он выполняется
        **после** завершения финраструктуры, поэтому здесь больше не доступны [ядро],
        [сообщения] и [конфигурация]. В этой связи, если какой-либо из перечисленных компонентов
        необходим в процессе завершения, навпример, для вывода обзора о выполненных операциях,
        следует воспользоваться методом :meth:`deactivate`.

        Также в классах-потомках желательно придерживаться порядка, при котором
        класс **сначала** выполняет собственное завершение, а **затем** делает super-вызов к
        родительскому классу.
        """
        self.mappings = None
        self.timers = None

        self.component.state['shutdown_successful'] = True

    # Handle Phase

    def run(self):

        # Bootstrap

        self._log_debug('bootstrap phase in progress')
        self.bootstrap()

        # Init

        self.messages.debug('init phase in progress')
        self.init()
        if self.core.mbus.errors:
            raise RuntimeError(self.core.mbus.first_error_content)

        self.messages.debug('infrastructure init')
        self.infra.init(
            core=self.core, config=self.config, registry=self.infra.components,
            mount_point=self)
        if self.core.mbus.errors:
            raise RuntimeError(self.core.mbus.first_error_content)

        # Activation

        self.messages.debug('activation phase in progress')
        self.activate()
        if self.core.mbus.errors:
            raise RuntimeError(self.core.mbus.first_error_content)

        # Setup

        self.messages.debug('setup phase in progress')
        self.setup()
        if self.core.mbus.errors:
            raise RuntimeError(self.core.mbus.first_error_content)

        self.messages.debug('check phase in progress')
        self.check()
        if self.core.mbus.errors:
            raise RuntimeError(self.core.mbus.first_error_content)

        try:
            self.messages.debug('handle phase in progress')
            self.runtime['is_running'] = True

            while self.is_running:
                self.handle()
                self.flush()

        except KeyboardInterrupt:
            self.messages.warning('KeyboardInterrupt caught, initiating shutdown')
            self.exit()

        finally:

            # Cleanup

            self.messages.debug('cleanup phase in progress')
            self.cleanup()

            # Deactivate

            self.messages.debug('deactivation phase in progress')
            self.deactivate()

            # Shutdown

            self.messages.debug('shutdown phase in progress')
            self.messages.debug('infrastructure shutdown')

            self.infra.shutdown()
            self.shutdown()

    def reset(self):
        """ Сброс описывает развитие событий, в ходе которого прерываются все
        текущие процессы, и юнит возвращается в исходное состояние. Примером
        сброса может быть прерывание выполенния задачи воркер-процессом. """
        pass

    def _run_hook(self, name):
        hook_method = getattr(self, name, None)
        if hook_method:
            hook_method()
