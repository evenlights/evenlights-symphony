# -*- coding: utf-8 -*-
from jinja2 import Template

from symphony.ci.handlers import SignalHandler


class ExitSignalHandler(SignalHandler):
    name = 'exit'
    aliases = ['close', 'shutdown']

    def handle(self):
        self.messages.info('trying graceful shutdown')
        app = self.core.shared.get('app')
        app.exit()


class HelpSignalHandler(SignalHandler):
    name = 'help'
    aliases = ['man']

    def prepare(self):
        signal_parser = self.core.shared.get('signal_parser')

        self.runtime['args'] = self.runtime['object'].args
        self.runtime['kwargs'] = self.runtime['object'].kwargs

        if self.runtime['args']:
            signal_parser.flush()
            self.runtime['handler_name'] = \
                signal_parser.pop_name(self.runtime['args'])

            if signal_parser.messages.errors:
                self.error_response(
                    'no help available for: %s' %
                    ', '.join(self.runtime['args'])
                )
                return

            if len(self.runtime['args']):
                self.error_response(
                    'could not interpret rest of the args: %s' %
                    ', '.join(self.runtime['args'])
                )
                return

    def handle(self):
        console = self.core.shared.get('console')

        # задан конкретный обработчик
        if 'handler_name' in self.runtime:
            entries = [self.get_help_entry(self.runtime['handler_name'])]
            template = 'symphony_help_detail.html'

        # выводим список
        else:
            ci = self.core.shared.get('ci')
            entries = [
                self.get_help_entry(n)
                for n, h in list(ci.mapping.items()) if h != self
            ]
            template = 'symphony_help_list.html'

        console.render(template, entries=entries)

    def render_template_safe(self, template):
        console = self.core.shared.get('console')

        content = None
        if isinstance(template, str):
            content = console.engine.from_string(template).render()

        elif isinstance(template, Template):
            content = template.render()

        return content

    def get_help_entry(self, name):
        ci = self.core.shared.get('ci')

        handler = ci.mapping[name]
        short_description = self.render_template_safe(handler.short_description)
        full_description = self.render_template_safe(handler.full_description)

        return {
            'name': name,
            'short_description': short_description,
            'full_description': full_description,
        }
