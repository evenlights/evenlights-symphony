# -*- coding: utf-8 -*-
from symphony.core.containers import container_validate
from symphony.messages.containers import GenericMessage
from symphony.protocols.smp import defaults as smp_defaults
from symphony.protocols.smp.handlers import SMPMessageHandler
from symphony.protocols.smp.routers import DEFAULT_CONTENT_HANDLER

from symphony.core import defaults as core_defaults


class BaseMessageHandler(SMPMessageHandler):

    # message methods
    @staticmethod
    def is_partial(msg):
        return msg.message_type == smp_defaults.SMP_TYPE_FINAL

    @staticmethod
    def is_final(msg):
        return msg.message_type == smp_defaults.SMP_TYPE_FINAL

    @staticmethod
    def code_repr(msg):
        code = msg.code
        code_hr = smp_defaults.SMP_CODES_HR[code] if code in smp_defaults.SMP_CODES_HR else 'UNKNOWN'
        return '%d [%s]' % (code, code_hr)

    @staticmethod
    def level_by_code(msg):
        # пытаемся определить уровень
        level = 'info'
        if msg.code in range(300, 399):
            level = 'warning'
        elif msg.code in range(400, 599):
            level = 'error'
        return level


class DefaultMessageHandler(BaseMessageHandler):
    """ """
    name = 'default_smp_handler'
    content_type = DEFAULT_CONTENT_HANDLER

    def handle(self):
        msg = self.runtime['object']
        message = 'remote response:'

        if msg.code:
            message += ' ' + self.code_repr(msg)

        if msg.content:
            message += ': ' + msg.content

        level = self.level_by_code(msg)

        self.messages.emit(level, message)


# LEVEL 0 HANDLERS
class NoneMessageHandler(BaseMessageHandler):
    """ """
    name = 'none_smp_handler'
    content_type = smp_defaults.SMP_CONTENT_NONE

    def validate(self, msg):
        if msg.content is not None:
            self.messages.error('unexpected content frame')
            return False
        if msg.code is None:
            self.messages.error('code frame empty')
            return False

        return True

    def handle(self):
        msg = self.runtime['object']
        message = 'remote response:'

        if msg.code:
            message += ' ' + self.code_repr(msg)

        level = self.level_by_code(msg)
        self.messages.emit(level, message)


class StringMessageHandler(DefaultMessageHandler):
    """
    """
    name = 'string_smp_handler'
    content_type = smp_defaults.SMP_CONTENT_STRING

    def validate(self, msg):
        if msg.content is None:
            self.messages.error('content frame empty')
            return False
        return True


class FileMessageHandler(BaseMessageHandler):
    """
    """
    name = 'file_smp_handler'
    mode = 'w'
    content_type = smp_defaults.SMP_CONTENT_FILE

    def validate(self, msg):
        if msg.content is None:
            self.messages.error('content frame empty')
            return False
        return True

    def handle(self):
        msg = self.runtime['object']
        target_dir = self.mount_point.config.TMP_DIR
        target_path = '%s/%s' % (target_dir, msg.transaction_id)
        self.messages.info('saving file as: %s' % target_path)
        with open(target_path, mode=self.mode) as outfile:
            outfile.write(
                msg.content.encode(core_defaults.DEFAULT_CODEC)
            )


# LEVEL 1 HANDLERS
class MessageMessageHandler(BaseMessageHandler):
    """
    """
    name = 'message_smp_handler'
    content_type = smp_defaults.SMP_CONTENT_MESSAGE

    def validate(self, msg):

        if msg.content is None:
            self.messages.error('content frame empty')
            return False

        if not container_validate(GenericMessage, msg.content):
            self.messages.error('invalid message container')
            return False

        return True

    def handle(self):
        msg = self.runtime['object']
        container = GenericMessage.from_json(msg.content)

        message = 'remote response: '
        if msg.code:
            code = msg.code
            message += '%d [%s]: ' % \
                       (code, smp_defaults.SMP_CODES_HR[code]
                        if code in smp_defaults.SMP_CODES_HR else 'UNKNOWN')
        message += container.content
        self.messages.emit(container.level, message)

# LEVEL 2 HANDLERS
# todo: https://bitbucket.org/evenlights/symphony/issues/2/appshandlerssmp-stream-handler
