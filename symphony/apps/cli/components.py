# -*- coding: utf-8 -*-
import uuid

from symphony.core.infra.generic import GenericComponent
from symphony.pools.transactions import GenericTransactionPool

from symphony.apps.defaults import CI_EXCLUDE_TRANSACTION_NAMES as EXCLUDE_NAMES
from symphony.pools.defaults import STATE_SUCCESS, STATE_QUEUED, STATE_FAILURE


class ClientCI(GenericComponent):
    """ Обработка входящих сообщений сокета :ref:`интерфейс управления
    <ci.interfaces>` осуществляется только пока воркер находится рабочем
    состоянии -- при входе в состояние завершения новые сигналы в очередь
    добавляться перестают. Хотя, сигналы уже присутствующие в очереди, будут
    обработаны.

    На данный момент метод ожидает сигнал с типом содержимого
    ``SMP_CONTENT_SIGNAL``.
    """
    name = 'ci'
    shared = True

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(ClientCI, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.app = self.core.shared.get('app')
        self.router = self.app.ci_router
        self.transactions = self.app.transactions

        self.queue_clear = self.router.queue_clear
        self.queue_empty = self.router.queue_empty
        self.queue_get = self.router.queue_get
        self.queue_put = self.router.queue_put
        self.queue_size = self.router.queue_size
        self.task_done = self.router.task_done
        self.mapping = self.router.mapping

    def handle(self, *args, **kwargs):
        """ """
        # выполняем следующий сигнал из очереди
        if not self.router.queue_empty():
            self.messages.debug('handling control interface queue')

            # получаем следующий сигнал
            signal = self.router.queue_get()

            # регистрируем транзакцию
            transaction_id = uuid.uuid1().__str__()

            if signal.name not in EXCLUDE_NAMES:
                self.messages.info(
                    'registering transaction: %s' % transaction_id
                )
                self.transactions.register(
                    id=transaction_id, name=signal.name, args=signal.args,
                    kwargs=signal.kwargs,
                    state=STATE_QUEUED
                )

            self.router.handle(signal)
            self.router.task_done()

            if signal.name not in EXCLUDE_NAMES:
                if not self.core.mbus.errors:
                    self.messages.info(
                        'transaction complete: %s' % transaction_id)
                    self.transactions.set_state(transaction_id, STATE_SUCCESS)
                else:
                    self.messages.error(
                        'transaction failed: %s' % transaction_id)
                    self.transactions.set_state(transaction_id, STATE_FAILURE)


class ClientTransactionPool(GenericComponent):
    """ Обработка входящих сообщений сокета :ref:`интерфейс управления
    <ci.interfaces>` осуществляется только пока воркер находится рабочем
    состоянии -- при входе в состояние завершения новые сигналы в очередь
    добавляться перестают. Хотя, сигналы уже присутствующие в очереди, будут
    обработаны.

    На данный момент метод ожидает сигнал с типом содержимого
    ``SMP_CONTENT_SIGNAL``.
    """
    name = 'transactions'
    shared = True

    def init(self, core=None, config=None, registry=None, mount_point=None,
             **kwargs):
        super(ClientTransactionPool, self).init(
            core=core, config=config, registry=registry,
            mount_point=mount_point, **kwargs
        )
        self.app = self.core.shared.get('app')

        self.messages.debug('initializing transaction pool')
        self.pool = GenericTransactionPool()

        self.register = self.pool.register

    def set_state(self, transaction_id, state):
        self.pool[transaction_id].state = state
