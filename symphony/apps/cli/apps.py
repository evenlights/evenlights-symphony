# -*- coding: utf-8 -*-
import sys
try:
    import readline
except ImportError:
    pass

from symphony.console.components import Console
from symphony.ci.parsers import StringSignalParser
from symphony.routers.components import QueueRouter
from symphony.ci.containers import GenericSignal
from symphony.apps.base import BaseApp
from symphony.apps.cli.components import ClientCI, ClientTransactionPool

from symphony import __version__


class CLIApp(BaseApp):
    """ """

    class Infrastructure:
        components = [
            QueueRouter(
                name='ci_router', handler_source='signal_handlers', shared=True
            ),
            StringSignalParser,
            ClientTransactionPool,
            ClientCI,
            Console,
        ]

    def setup(self):
        self.signal_parser.mapping = self.ci.mapping

        # переключаем режим в зависимости от того, переданы ли аргументы
        if len(sys.argv) > 1:
            signal_string = ' '.join(sys.argv[1:])
            signal = self.signal_parser.get_signal(signal_string)

            # ставим сигнал в очередь, если определился
            if signal:
                self.ci.queue_put(signal)

        if self.ci.queue_empty():
            signal = GenericSignal(name='help')
            self.ci.queue_put(signal)

        super(CLIApp, self).setup()

    def handle(self, *args, **kwargs):
        self.ci.handle()

    @property
    def is_running(self):
        """
        Приложение работает до тех пор, пока в очереди ИУ есть необработанные
        сигналы.
        """
        return not self.ci.queue_empty()

    def deactivate(self):
        super(CLIApp, self).deactivate()
        console = self.core.shared.get('console')

        if self.transactions.pool.finished:
            console.render(
                'symphony_pool_stats.html',
                total_count=len(self.transactions.pool.finished),
                successful_count=len(self.transactions.pool.successful),
                failed_count=len(self.transactions.pool.failed),
            )
        console.render('symphony_footer.html', version=__version__)
