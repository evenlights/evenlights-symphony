import time
from threading import Thread

from symphony.apps.transport.base import ZMQTransportBase


class UnboundWorkerBase(ZMQTransportBase):
    """ todo """
    # fixme: нафига козе баян? если врокер не использует транспорт, зачем транспортная механика?

    def _base_cycle(self):
        time.sleep(self.core.config.SOCKET_POLLING_INTERVAL / 1000)


class UnboundThreadWorker(UnboundWorkerBase, Thread):
    pass
    # class Infrastructure:
    #     modules = [Logger]

UnboundWorker = UnboundThreadWorker
