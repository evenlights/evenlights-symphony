# -*- coding: utf-8 -*-
import simplejson as json

from symphony.core.containers.fields import StringField, IntegerField, BooleanField
from symphony.ci.handlers import WorkerSignalHandler, RESTEndpointMixin

from symphony.protocols.smp import defaults as smp_defaults


class SenderWriteHandler(RESTEndpointMixin, WorkerSignalHandler):
    """ Сигнатура вызываемой процедуры::

        def sender_write(self, id, name=None, max_messages=None)

    :param name: имя отправителя (узла системы или клиента)
    :param max_messages: максимальное количество сообщений, хранимое для
        данного отправителя
    """
    group = 'sender'
    name = 'write'
    args = [StringField]
    kwargs = {
        'name': StringField,
        'max_messages': IntegerField,
    }
    http_method = 'post'

    def handle(self, signal):
        logger_ = self.infra.core.shared.units.logger

        sender_existed = logger_.sender_exists(*self.runtime.args)

        logger_.sender_write(*self.runtime.args, **self.runtime.kwargs)
        if logger_.messages.internal.errors:
            self.messages.error(logger_.messages.internal.first_error_content)
            return

        if sender_existed:
            self.accepted_response(final=True)
        else:
            self.created_response(final=True)


class SenderReadHandler(RESTEndpointMixin, WorkerSignalHandler):
    """ Сигнатура вызываемой процедуры::

        def sender_read(self, id)
    """
    group = 'sender'
    name = 'read'
    args = [StringField]

    def handle(self, signal):
        logger_ = self.infra.core.shared.units.logger

        sender = logger_.sender_read(*self.runtime.args)
        if logger_.messages.internal.errors:
            self.messages.error(logger_.messages.internal.first_error_content)
            return

        self.ok_response(
            content_type=smp_defaults.SMP_CONTENT_JSON, content=sender.repr.json, final=True
        )


class SenderListHandler(RESTEndpointMixin, WorkerSignalHandler):
    """ Сигнатура вызываемой процедуры::

        def sender_list(self):
    """
    group = 'sender'
    name = 'list'

    def handle(self, signal):
        logger_ = self.infra.core.shared.units.logger

        sender_list_db = logger_.sender_list()

        if logger_.messages.internal.errors:
            self.messages.error(logger_.messages.internal.first_error_content)
            return

        sender_list_json = json.dumps(
            [s.repr.transport for s in sender_list_db]
        )

        self.ok_response(
            content_type=smp_defaults.SMP_CONTENT_JSON,
            content=sender_list_json,
            final=True
        )


class SenderDeleteHandler(RESTEndpointMixin, WorkerSignalHandler):
    """ Сигнатура вызываемой процедуры::

        def sender_delete(self, id, cascade_messages=False,
                          cascade_transactions=False):

    :param id: идентификатор отправителя

    :param cascade_messages: удалить все сообщения данного отправителя
    :param cascade_transactions: удалитьвсе транзакции и связанные с ними
        сообщения
    """
    group = 'sender'
    name = 'delete'
    args = [StringField]
    kwargs = {
        'cascade_messages': BooleanField,
        'cascade_transactions': BooleanField,
    }
    http_method = 'delete'

    def handle(self, signal):
        logger_ = self.infra.core.shared.units.logger

        logger_.sender_delete(*self.runtime.args, **self.runtime.kwargs)

        if logger_.messages.internal.errors:
            self.messages.error(logger_.messages.internal.first_error_content)
            return

        self.no_content_response(final=True)
