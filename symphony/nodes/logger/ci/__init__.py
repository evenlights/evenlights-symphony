# -*- coding: utf-8 -*-
from .message import (
    MessageLogHandler, MessageListHandler, MessageDeleteHandler
)
from .sender import (
    SenderWriteHandler, SenderReadHandler, SenderDeleteHandler, SenderListHandler
)

ALL_HANDLERS = [
    MessageLogHandler, MessageListHandler, MessageDeleteHandler,
    SenderWriteHandler, SenderReadHandler, SenderDeleteHandler, SenderListHandler
]
