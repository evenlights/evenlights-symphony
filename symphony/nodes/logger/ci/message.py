# -*- coding: utf-8 -*-
import simplejson as json

from symphony.core.containers.fields import SimpleDateTimeField, StringField
from symphony.messages.containers import GenericMessage
from symphony.ci.handlers import WorkerSignalHandler, RESTEndpointMixin

from symphony.protocols.smp.defaults import SMP_CONTENT_JSON
from symphony.messages.defaults import LEVELS_HR2INT


class MessageLogHandler(RESTEndpointMixin, WorkerSignalHandler):
    """ Сигнатура вызываемой процедуры::

        def message_log(self, sender_id, message_container, transaction_id=None)

    :param sender_id: идентификатор отправителя, иными словами узла,
        породившего это сообщение
    :param GenericMessage message_container: JSON-представление контейнера
        сообщения
    :param transaction_id: идентификатор транзакции может отсутстовать,
        поскольку не все сообщения обязательно совершаются в рамках транзакций,
        например, сообщения о готовности узла к работе не принадлежат ни к
        какой логической операции.
    """
    group = 'message'
    name = 'log'
    args = [StringField, GenericMessage]
    kwargs = {
        'transaction_id': StringField,
    }
    http_method = 'post'

    def handle(self, signal):
        logger_ = self.infra.core.shared.units.logger

        logger_.message_log(*self.runtime.args, **self.runtime.kwargs)
        if logger_.messages.internal.errors:
            self.messages.error(logger_.messages.internal.first_error_content)
            return

        self.created_response(final=True)


class MessageListHandler(RESTEndpointMixin, WorkerSignalHandler):
    """ Сигнатура вызываемой процедуры::

        def message_list(self, sender_id=None, transaction_id=None,
                         level='debug', from_date=None, to_date=None)

    :param sender_id: идентификатор отправителя иными словами узла,
        породившего это сообщение: аргумент не обязателен, поскольку иногда,
        например, необходимо получить сообщения для транзакции, а они включают
        различных отправителей
    :param transaction_id: идентификатор транзакции: может отсутстовать,
        если, например, необходимо получить все сообщения отправителя
    :param level: минимальный уровень поулчаемых сообщений, ``dubug`` по
        умолчанию
    :param from_date: нижний порог сообщений по времени
    :param from_date: верхний порог сообщений по времени
    """
    group = 'message'
    name = 'list'
    kwargs = {
        'sender_id': StringField,
        'transaction_id': StringField,
        'level': StringField,
        'from_date': SimpleDateTimeField,
        'to_date': SimpleDateTimeField,
    }

    def validate(self, signal):
        if 'level' in self.runtime.kwargs:
            if self.runtime.kwargs.level not in LEVELS_HR2INT:
                self.messages.error('invalid message level: %s' % self.runtime.kwargs.level)
                return False

        # если переданы обе даты, убеждаемся, что from_date <= to_date
        if 'from_date' and 'to_date' in self.runtime.kwargs:
            if not (self.runtime.kwargs.from_date <= self.runtime.kwargs.to_date):
                self.messages.error('from_date should be less or equal to to_date')
                return False

        return True

    def handle(self, signal):
        logger_ = self.infra.core.shared.units.logger

        msg_list_db = logger_.message_list(**self.runtime.kwargs)
        if logger_.messages.internal.errors:
            self.messages.error(logger_.messages.internal.first_error_content)
            return

        msg_list_json = json.dumps(
            [m.repr.transport for m in msg_list_db]
        )

        self.ok_response(content_type=SMP_CONTENT_JSON, content=msg_list_json, final=True)


class MessageDeleteHandler(RESTEndpointMixin, WorkerSignalHandler):
    """ Сигнатура вызываемой процедуры::

        def message_delete(self, sender_id=None, transaction_id=None,
                           level='debug', from_date=None, to_date=None)

    :param sender_id: идентификатор отправителя иными словами узла,
        породившего это сообщение: аргумент не обязателен, поскольку иногда,
        например, необходимо получить сообщения для транзакции, а они включают
        различных отправителей
    :param transaction_id: идентификатор транзакции: может отсутстовать,
        если, например, необходимо удалить все сообщения отправителя
    :param level: минимальный уровень удаляемых сообщений, ``dubug`` по
        умолчанию
    :param from_date: нижний порог сообщений по времени
    :param to_date: верхний порог сообщений по времени
    """
    group = 'message'
    name = 'delete'
    http_method = 'delete'
    kwargs = {
        'sender_id': StringField,
        'transaction_id': StringField,
        'level': StringField,
        'from_date': SimpleDateTimeField,
        'to_date': SimpleDateTimeField,
    }

    def validate(self, signal):
        if 'level' in self.runtime.kwargs:
            if self.runtime.kwargs.level not in LEVELS_HR2INT:
                self.messages.error(
                    'invalid message level: %s' % self.runtime.kwargs.level
                )
                return False

        # если переданы обе даты, убеждаемся, что from_date <= to_date
        if 'from_date' and 'to_date' in self.runtime.kwargs:
            if not (self.runtime.kwargs.from_date <= self.runtime.kwargs.to_date):
                self.messages.error(
                    'from_date should be less or equal to to_date'
                )
                return False

        return True

    def handle(self, signal):
        logger_ = self.infra.core.shared.units.logger

        logger_.message_delete(**self.runtime.kwargs)

        if logger_.messages.internal.errors:
            self.messages.error(logger_.messages.internal.first_error_content)
            return

        self.no_content_response(final=True)
