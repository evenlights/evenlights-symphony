# -*- coding: utf-8 -*-
from symphony.apps.configs import WorkerConfig
from symphony.apps.transport.worker import Worker, UnboundWorker
from symphony.nodes.logger.ci import ALL_HANDLERS
from symphony.nodes.logger.units import LoggerUnit, CleanerUnit
from symphony.timers.containers import GenericTimer


class LoggerWorkerConfig(WorkerConfig):
    """ Моудль конфигурации

    :param DB_NAME: имя базы дынных, обязательный параметр

    :param DB_HOST: хост
    :param DB_USER: имя пользователя
    :param DB_PASSWORD: пароль
    """
    defaults = WorkerConfig.extend_defaults({
        'DB_NAME': 'logger',
        'SERVICE_THREAD_INTERVAL': 120,
    })


class LoggerWorker(Worker):
    """ todo """
    config = LoggerWorkerConfig
    signal_handlers = ALL_HANDLERS

    def __init__(self, *args, **kwargs):
        super(LoggerWorker, self).__init__(*args, **kwargs)
        self.logger = None

    def setup(self):
        self.logger = LoggerUnit(core=self.core)

    def flush(self):
        self.logger.flush()


class ServiceThread(UnboundWorker):
    """ todo """
    config = LoggerWorkerConfig

    def __init__(self, *args, **kwargs):
        super(ServiceThread, self).__init__(*args, **kwargs)
        self.timer = None
        self.cleaner = None

    def setup(self):
        class _Timer(GenericTimer):
            def on_timeout(self):
                self.mount_point.core.messages.info('service thread awake')
                self.mount_point.cleaner.run()
                self.reset()
        self.timer = _Timer(
            mount_point=self, seconds=self.core.config.SERVICE_THREAD_INTERVAL
        )
        self.cleaner = CleanerUnit(core=self.core)

    def cycle(self):
        self.timer.refresh()
