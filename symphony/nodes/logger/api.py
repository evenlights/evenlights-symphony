# -*- coding: utf-8 -*-
import uuid
import logging

from symphony.messages.containers import GenericMessage
from symphony.ci.containers import GenericSignal
from symphony.ci.api import RemoteAPI
from symphony.protocols.smp import defaults as smp_defaults


class RemoteLoggerHandler(logging.Handler):
    """ todo """
    def __init__(self, socket, sys_id):
        logging.Handler.__init__(self)
        self.sys_id = sys_id
        self.socket = socket
        self.transaction = None

    def emit(self, record):
        content = self.format(record)

        # подавляем сообщения собственного сокета
        if self.socket.sys_name not in content:
            signal = GenericSignal(
                group='message',
                name='log',
                args=[
                    self.sys_id,
                    GenericMessage(
                        level=record.levelname.lower(), content=content
                    ).repr.transport
                ],
                kwargs={
                    'transaction_id': self.transaction
                } if self.transaction else None
            )
            self.socket.client_request(
                transaction_id=uuid.uuid4().__str__(),
                content_type=smp_defaults.SMP_CONTENT_SIGNAL,
                content=signal.repr.json
            )


class RemoteLoggerAPI(RemoteAPI):
    name = 'logger'
    sys_name = 'logger_api'

    def __init__(self, *args, **kwargs):
        super(RemoteLoggerAPI, self).__init__(*args, **kwargs)
        self.handler = None

    def mount(self, mount_point):
        super(RemoteLoggerAPI, self).mount(mount_point=mount_point)

        # если вызов к RemoteAPI.mount() отработал
        if self._mount_state:
            core_ = self.infra.core
            mp_ = self._mount_point

            # отключаем адаптер от шины сообщений
            self.messages.debug('adding remote handler')
            self.socket.messages.channel_unregister(core_.mbus)
            self.socket.messages.channel_unregister(core_.logger)

            logger = core_.logger._logger
            self.handler = RemoteLoggerHandler(socket=self.socket, sys_id=mp_.sys_id)
            logger.addHandler(self.handler)

    def umount(self):
        # если вызов к RemoteAPI.mount() отработал
        if self._mount_state:
            core_ = self.infra.core
            logger = core_.logger._logger
            logger.removeHandler(self.handler)
            self.socket.messages.channel_register(core_.mbus)
            self.socket.messages.channel_register(core_.logger)
            super(RemoteLoggerAPI, self).umount()

Logger = RemoteLoggerAPI
