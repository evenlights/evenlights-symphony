# -*- coding: utf-8 -*-

SYS_NAME = 'logger'
MONGODB_DATE_FORMATTER = '%Y-%m-%d %H:%M:%S.%f'
SIGNAL_DATE_FORMATTER = '%Y-%m-%d %H:%M:%S'
