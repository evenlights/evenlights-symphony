# -*- coding: utf-8 -*-
import datetime

import mongoengine as mongo

from symphony.core.containers import GenericObjectContainer
from symphony.core.containers.fields import GenericDateTimeField


class Sender(GenericObjectContainer, mongo.Document):
    """ Отправителем может быть узел системы, выполняющий клиент"""

    update = mongo.Document.update

    class DataModel:
        structure = {
            'id': None,
            'name': None,
            'max_messages': None,
        }

    id = mongo.StringField(primary_key=True)
    name = mongo.StringField()
    max_messages = mongo.IntField(default=0)

    @property
    def messages(self):
        return Message.objects(sender=self)

    @property
    def transactions(self):
        return Transaction.objects(sender=self.id)


class Transaction(mongo.Document):
    """ Транзакция имеет отправителя (инициатора) и включает все сообщения,
    которые были порождены всеми участниками процесса в ходе ее выполнения. """
    id = mongo.StringField(primary_key=True)
    sender = mongo.ReferenceField(Sender)

    @property
    def messages(self):
        return Message.objects(transaction=self)


class Message(GenericObjectContainer, mongo.Document):

    update = mongo.Document.update

    class DataModel:
        structure = {
            'level': None,
            'content': None,
            'timestamp': GenericDateTimeField,
        }

    level = mongo.StringField()
    content = mongo.StringField()
    timestamp = mongo.DateTimeField(default=datetime.datetime.now())

    # smp fields
    transaction = mongo.ReferenceField(Transaction)
    sender = mongo.ReferenceField(Sender)
