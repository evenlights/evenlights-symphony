# -*- coding: utf-8 -*-
import datetime

import mongoengine as mongo

from symphony.core.units import GenericInfraUnit
from symphony.nodes.logger.containers import Sender, Transaction, Message

from symphony.messages.defaults import LEVELS_HR2INT
from symphony.nodes.logger.defaults import SYS_NAME, MONGODB_DATE_FORMATTER

# todo: https://bitbucket.org/evenlights/symphony/issues/9/nodesloggerunits-rotate


class LoggerUnit(GenericInfraUnit):
    shared = True
    sys_name = SYS_NAME

    def __init__(self, *args, **kwargs):
        super(LoggerUnit, self).__init__(*args, **kwargs)

        # db
        db_name = self.infra.core.config.DB_NAME
        self.messages.debug('using database: %s' % db_name)

        db_kwargs = {}

        if hasattr(self.infra.core.config, 'DB_HOST'):
            db_kwargs['host'] = self.infra.core.config.DB_HOST
            self.messages.debug('connecting to host: %s' % db_kwargs['host'])

        if hasattr(self.infra.core.config, 'DB_PORT'):
            db_kwargs['port'] = self.infra.core.config.DB_PORT
            self.messages.debug('using port: %s' % db_kwargs['port'])

        if hasattr(self.infra.core.config, 'DB_USER'):
            db_kwargs['username'] = self.infra.core.config.DB_USER
            self.messages.debug('username: present')

        if hasattr(self.infra.core.config, 'DB_PASSWORD'):
            db_kwargs['password'] = self.infra.core.config.DB_PASSWORD
            self.messages.debug('password: present')

        self.messages.debug('connecting to mongodb engine')
        mongo.connect(db_name, **db_kwargs)

    @staticmethod
    def timestamp_truncate(timestamp):
        """ Метод округляет микросекунды до 100000, как mongodb округляет при
        сохранении.
        """
        time_string = '%s000' % timestamp.strftime(MONGODB_DATE_FORMATTER)[:-3]
        return datetime.datetime.strptime(time_string, MONGODB_DATE_FORMATTER)

    # операции с отправителями
    def sender_exists(self, id):
        """ Возвращает объект соответствубщего отправителя. """
        self.messages.debug('checking sender existence: %s' % id)

        try:
            Sender.objects.get(id=id)
            return True

        except Sender.DoesNotExist:
            return False

    def sender_list(self):
        """ Метод возвращает список всех имеющихся записей отправителей. Если
        список записей пуст, возвращается пустой список, поскольку запрашивающий
        узел все же должен получить ответ. """
        self.messages.debug('getting sender list')
        return Sender.objects

    def sender_read(self, id):
        """ Возвращает объект соответствубщего отправителя. """
        self.messages.debug('reading sender id: %s' % id)

        try:
            return Sender.objects.get(id=id)

        except Sender.DoesNotExist:
            self.messages.error('sender not found: %s' % id)

    def sender_write(self, id, name=None, max_messages=None):
        """ Метод :func:`write` служит для создания и обновления отправителей.

        :param id: идентификатор отправителя используется для создания нового
            объекта либо обновления уже существующего
        :param name: имя отправителя
        :param max_messages: максимальное количество хранимых сообщений,
            по умолчанию 0 (количество не ограничено)
        """
        self.messages.debug('writing sender id: %s' % id)

        try:
            sender = Sender.objects.get(id=id)
        except Sender.DoesNotExist:
            self.messages.debug('sender not present, creating')
            sender = Sender(id=id).save(force_insert=True)

        update_kwargs = {}
        if name:
            update_kwargs['set__name'] = name
        if max_messages:
            update_kwargs['set__max_messages'] = max_messages

        if update_kwargs:
            sender.modify(**update_kwargs)

        return sender

    def sender_delete(self, id,
                      cascade_messages=False, cascade_transactions=False):
        """ Удаление отправителя и всех его сообщений.
        :param cascade_messages: удаление всех сообщений отправителя
        :param cascade_transactions: удаление всех транзакций отправителя и
            связанных сообщений
        """
        self.messages.debug('deleting sender id: %s' % id)

        try:
            sender = Sender.objects.get(id=id)

        # либо сообщаем, что лог не найден
        except Sender.DoesNotExist:
            self.messages.error('sender not found: %s' % id)
            return False

        if cascade_messages:
            self.messages.debug('cascade-deleting sender messages: %s' % id)
            list(map(lambda m: m.delete(), sender.messages))

        if cascade_transactions:
            self.messages.debug('cascade-deleting sender transactions: %s' % id)
            for transaction in sender.transactions:
                list(map(lambda m: m.delete(), transaction.messages))
                transaction.delete()

        return sender.delete()

    # операции с сообщениями
    def message_log(self, sender_id, message_container, transaction_id=None):
        """ Метод добавления записи в журнал соответствующей системной единицы.

        :param sender_id: идентификатор отправителя сообщения
        :param message_container: контейнер сообщения
        :param transaction_id: идентификатор транзакции; может отсутсвовать в
            ситуациях, когда, например, компоненты узла сообщают о готовнсти
        """
        self.messages.debug('logging a message for sender: %s' % sender_id)
        if transaction_id:
            self.messages.debug('transaction id: %s' % transaction_id)

        # получаем существующий лог или создаем новый
        self.messages.debug('getting sender')
        try:
            sender = Sender.objects.get(id=sender_id)
        except Sender.DoesNotExist:
            self.messages.debug('sender not present, creating')
            sender = Sender(id=sender_id).save(force_insert=True)

        if transaction_id:
            self.messages.debug('getting transaction')
            try:
                transaction = Transaction.objects.get(id=transaction_id)
            except Transaction.DoesNotExist:
                transaction = Transaction(id=transaction_id).save(force_insert=True)

        self.messages.debug('logging message')
        msg_kwargs = {
            'level': message_container.level,
            'content': message_container.content,
            'timestamp': message_container.timestamp,
            'sender': sender,
        }
        if transaction_id:
            msg_kwargs['transaction'] = transaction
        Message(**msg_kwargs).save()

    def message_list(self, sender_id=None, transaction_id=None,
                     level='debug', from_date=None, to_date=None):
        """
        Возвращает сообщения, удоавлетворяющие заданным критериям.

        :param sender_id: идентификатор отправителя
        :param transaction_id: идентификатор транзакции
        :param level: минимальный уровень сообщения
        :param from_date: минимальная дата
        :param to_date: максимальная дата
        """
        self.messages.debug('getting message list')

        filter_kwargs = {}

        if sender_id:
            self.messages.debug('filtering by sender id: %s' % sender_id)
            try:
                sender = Sender.objects.get(id=sender_id)
                filter_kwargs['sender'] = sender
            except Sender.DoesNotExist:
                self.messages.error('sender does not exist: %s' % sender_id)
                return

        if transaction_id:
            self.messages.debug('filtering by transaction id: %s' % transaction_id)
            try:
                transaction = Transaction.objects.get(id=transaction_id)
                filter_kwargs['transaction'] = transaction
            except Transaction.DoesNotExist:
                self.messages.error('transaction does not exist: %s' % transaction_id)
                return

        if from_date:
            self.messages.debug('filtering by from_date: %s' % from_date)
            filter_kwargs['timestamp__gte'] = self.timestamp_truncate(from_date)

        if to_date:
            self.messages.debug('filtering by to_date: %s' % to_date)
            filter_kwargs['timestamp__lte'] = self.timestamp_truncate(to_date)

        messages = Message.objects(**filter_kwargs)

        if level:
            self.messages.debug('filtering level: %s' % level)
            messages = [r for r in messages if LEVELS_HR2INT[r.level] >= LEVELS_HR2INT[level]]

        return messages

    def message_delete(self, sender_id=None, transaction_id=None,
                       level='debug', from_date=None, to_date=None):
        self.messages.debug('deleting messages')
        messages = self.message_list(
            sender_id=sender_id, transaction_id=transaction_id, level=level,
            from_date=from_date, to_date=to_date
        )
        if messages:
            list(map(lambda m: m.delete(), messages))


class CleanerUnit(GenericInfraUnit):
    shared = True
    sys_name = 'cleaner'

    def run(self):
        self.messages.debug('cleaner unit active')

        for sender in Sender.objects.all():
            # если достигнуто ограничение, удаяем лишние записи
            if sender.max_messages:
                if len(sender.messages) > sender.max_messages:
                    extra_messages_count = len(sender.messages) - sender.max_messages + 1
                    self.messages.info('cleaning %d messages for sender %s' % (extra_messages_count, sender.id))
                    extra_messages = sender.messages.order_by('timestamp')[:extra_messages_count]
                    list(map(lambda m: m.delete(), extra_messages))
