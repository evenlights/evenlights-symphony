# -*- coding: utf-8 -*-
from symphony.apps.configs import BrokerConfig
from symphony.apps.transport.broker import Broker
from symphony.configs.loaders import module_loader


class GenericBrokerConfig(BrokerConfig):
    """ todo """
    loader = module_loader
    source = 'settings'


class GenericBroker(Broker):
    """ todo """
    config = GenericBrokerConfig
    signal_handlers = []
