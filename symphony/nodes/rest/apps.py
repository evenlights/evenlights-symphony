# -*- coding: utf-8 -*-
from threading import Thread

import tornado.web
import tornado.ioloop

from symphony.core.exceptions import ImproperlyConfigured
from symphony.apps.configs import CLIAppConfig
from symphony.apps.base import BaseApp
from symphony.nodes.rest.handlers import SMPProxyHandler


class RESTServerConfig(CLIAppConfig):
    """ Моудль конфигурации

    :param BIND_ADDR:
    :param BIND_PORT:
    """
    source = 'settings'
    defaults = {
        'BIND_ADDR': 'localhost',
        'BIND_PORT': 8080,
    }


class RESTServerBase(BaseApp):
    """ ``RESTServerBase`` объединяет клиентские [endpoint-приложения].
    """
    config = RESTServerConfig
    endpoints = []
    pool = None

    def __init__(self, endpoints=None, pool=None, *args, **kwargs):
        super(RESTServerBase, self).__init__(*args, **kwargs)
        if endpoints:
            self.endpoints = endpoints
        self.pool = pool
        self.http_server = None

    def setup(self):
        handlers = self.get_handlers()
        self.core.messages.debug('server handlers: %s' % str(handlers))
        self.http_server = tornado.web.Application(handlers=handlers)
        addr = self.core.config.BIND_ADDR
        port = self.core.config.BIND_PORT
        self.core.messages.info('starting http server at: %s:%d' % (addr, port))
        self.http_server.listen(address=addr, port=port)

    def cycle(self):
        tornado.ioloop.IOLoop.current().start()

    def shutdown(self):
        tornado.ioloop.IOLoop.current().stop()
        self.is_running = False

    def get_handlers(self):
        handlers = []
        for ep in self.endpoints:

            if not ep.endpoint_handlers:
                raise ImproperlyConfigured('endpoint_handlers cannot be empty')

            for handler_class in ep.endpoint_handlers:
                handler_id = getattr(handler_class, handler_class.id_field)
                handler_group = getattr(handler_class, handler_class.group_field) or ''
                handler_name = '_'.join([ep.name, handler_group, handler_id])

                handlers.append(

                    tornado.web.url(
                        handler_class.as_url(node_name=ep.name),
                        SMPProxyHandler,
                        dict(
                            pool=self.pool,
                            ep_name=ep.name,
                            handler_name=handler_name,
                            api_handler_class=handler_class,
                            api_func_name='__'.join([handler_group, handler_id])
                        ),
                        name=handler_name
                    )
                )
        return handlers


class ThreadRESTServer(RESTServerBase, Thread):
    pass

RESTServer = ThreadRESTServer
