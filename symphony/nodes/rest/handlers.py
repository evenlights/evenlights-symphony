# -*- coding: utf-8 -*-
import uuid
import logging

import simplejson as json
import tornado.gen
import tornado.web

from symphony.protocols.smp import defaults as smp_defaults


class SMPProxyHandler(tornado.web.RequestHandler):
    """
    Обработчик инициализирует сокет, подключается к прокси-сокету клиента (ROUTER)
    со случайным ID или по ID транзакции, клиент забирает сигнал оттуда, и туда
    же возвращает ответ(ы). все красиво.
    """

    def initialize(self, pool, ep_name, handler_name, api_handler_class, api_func_name):
        self.logger = logging.getLogger('tornado.general.%s' % handler_name)

        instance = pool.thread_pool.instance(ep_name)
        assert instance is not None
        self.logger.debug('endpoint client instance: %s' % instance)

        self.api = instance.api

        self.api_handler_class = api_handler_class
        self.api_func_name = api_func_name
        self.query_kwargs = None

    def prepare(self):
        header_origin = self.request.headers.get('Origin')
        if header_origin:
            self.set_header('Access-Control-Allow-Origin', header_origin)

        kwargs = {
            k: self.get_query_argument(k, default=None)
            for k in list(self.api_handler_class.kwargs.keys())
            } if self.api_handler_class.kwargs else {}
        non_empty = [k for k in kwargs if kwargs[k] is not None]
        self.query_kwargs = {k: kwargs[k] for k in non_empty}

        # if self.request.headers["Content-Type"].startswith("application/json"):
        #     self.json_args = json.loads(self.request.body)
        # else:
        #     self.json_args = None

    @tornado.gen.coroutine
    def get(self, *args):
        """ Функции, вызываемые методом ``GET`` должны соответствовать спецификации
        ``HTTP``, то есть не производить изменений в данных. Обычно такими это обработчики
        операций ``read`` и ``list``.

        При этом подразумевается, что как позиционные аргументы, так и ключевые не слишком
        велики и могут быть переданы как regex-группы или параметры строки запроса.

        Поэтому для обработчиков сигналов, вызываемых методом ``GET``, аргументы ожидаются
        в виде regex-групп, а ключевые аргументы в виде ``?key1=value1&key2-value2&...``
        """
        trx_id = uuid.uuid1().__str__()

        self.logger.debug('local thread API call: %s' % self.api_func_name)
        resp = yield self.api.async(self.api_func_name, _transaction_id=trx_id, *args, **self.query_kwargs)

        self.logger.debug('response obtained: %s' % self.api_func_name)

        if resp.final.code < 300:
            if resp.final.content_type == smp_defaults.SMP_CONTENT_JSON:
                self.set_header("Content-Type", "application/json")

        self.set_status(resp.final.code)

        if resp.final.content:
            self.write(resp.final.content)

    @tornado.gen.coroutine
    def post(self, *args):
        """ Функции, вызываемые методом ``POST`` должны использоваться для изменения или
        создания данных. Обычно это обработчики операций ``write``.

        При этом подразумевается, что позиционные аргументы могут быть как достаточно большими
        (например, передача файлов), и тогда они должны передаваться в теле запроса, так и не слишком
        большими, и могут тогда передаваться как regex-группы, а ключевые аргументы всегда не слишком
        велики, и могут быть пепреданы как параметры строки запроса.

        В этой связи необходимо учитывать порядок аргументов обработчика: небольшие
        аргументы должны идти сначала (и передаваться как regex-группы), а большие
        аргументы следовать за ними и передаваться в теле запроса в виде JSON-списка.

        Таким образом, для обработчиков сигналов, вызываемых методом ``POST``, аргументы
        ожидаются в в теле сообщения в виде JSON=строки, а а ключевые аргументы в виде
        ``?key1=value1&key2-value2&...``
        """
        trx_id = uuid.uuid1().__str__()

        args = list(args)
        body_args = [a for a in json.load(self.request.body)] if self.request.body else []
        args += body_args

        resp = yield self.api.async(self.api_func_name, _transaction_id=trx_id, *args, **self.query_kwargs)

        if resp.final.code < 300:
            if resp.final.content_type == smp_defaults.SMP_CONTENT_JSON:
                self.set_header("Content-Type", "application/json")

        self.set_status(resp.final.code)

        if resp.final.content:
            self.write(resp.final.content)

    @tornado.gen.coroutine
    def delete(self, *args):
        """ Метод ``DELETE`` аналогичен методу ``GET`` в смысле разрешения аргусментов, то есть,
        как позиционные аргументы, так и ключевые не слишком велики и могут быть переданы как
        regex-группы или параметры строки запроса.

        Для обработчиков сигналов, вызываемых методом ``DELETE``, аргументы ожидаются
        в виде regex-групп, а ключевые аргументы в виде ``?key1=value1&key2-value2&...``
        """
        trx_id = uuid.uuid1().__str__()

        resp = yield self.api.async(self.api_func_name, _transaction_id=trx_id, *args, **self.query_kwargs)

        if resp.final.code < 300:
            if resp.final.content_type == smp_defaults.SMP_CONTENT_JSON:
                self.set_header("Content-Type", "application/json")

        self.set_status(resp.final.code)

        if resp.final.content:
            self.write(resp.final.content)

    @tornado.gen.coroutine
    def options(self, *args):
        self.set_header('Access-Control-Allow-Methods', self.api_handler_class.http_method.upper())
