# -*- coding: utf-8 -*-
import datetime

import mongoengine as mongo

from symphony.core.containers import GenericObjectContainer
from symphony.core.containers.fields import GenericDateTimeField
from symphony.nodes.transactions.defaults import STATE_INIT


class Sender(GenericObjectContainer, mongo.Document):
    """ Под отправителем транзакции подразуемвается ее инициатор. Обычно это
    клиент, хотя теоретически создавать транзакции могут и другие узлы
    системы. """
    update = mongo.Document.update

    class DataModel:
        structure = {
            'id': None,
            'name': None,
            'max_transactions': None,
        }

    id = mongo.StringField(primary_key=True)
    name = mongo.StringField()
    max_transactions = mongo.IntField(default=0)

    @property
    def transactions(self):
        return Transaction.objects(sender=self.id)


class Transaction(GenericObjectContainer, mongo.Document):
    """ Транзакция имеет отправителя (инициатора) и включает все сообщения,
    которые были порождены всеми участниками процесса в ходе ее выполнения. """

    # параметры smp
    id = mongo.StringField(primary_key=True)
    sender = mongo.ReferenceField(Sender)

    # параметры сигнала
    name = mongo.StringField()
    group = mongo.StringField()
    args = mongo.ListField()
    kwargs = mongo.DictField()

    # дополнительные параметры
    state = mongo.StringField(default=STATE_INIT)
    timestamp = mongo.DateTimeField(default=datetime.datetime.now())

    update = mongo.Document.update

    @property
    def sender_id(self):
        return self.sender.id if self.sender else None

    @sender_id.setter
    def sender_id(self, value):
        sender = Sender.objects.get(id=value)
        self.sender = sender

    class DataModel:
        structure = {
            'id': None,
            'sender_id': None,

            'name': None,
            'group': None,
            'args': None,
            'kwargs': None,

            'state': None,
            'timestamp': GenericDateTimeField,

        }
