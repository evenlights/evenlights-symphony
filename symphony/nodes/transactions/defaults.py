# -*- coding: utf-8 -*-
"""
Общая логика выполнения транзакции описывыает процесс с момента ее
инициализации клиентом до получения им ответа.

В ходе этого процесса запрос проходит через несколько стадий, включающих
постановку в очередь брокером (``QUEUED``), передачу воркеру (``ASSIGNED``) и
возврат результата клиенту (``SUCCESS``, ``FAILURE``).

Помимо этого, клиент может повторить запрос (``RETRY``), либо отозвать его (
``REVOKED``).
"""

# транзакция инициирована (клиентом)
STATE_INIT = 'INIT'

# транзакция поставлена в очередь (получена брокером)
STATE_QUEUED = 'QUEUED'

# транзакция передана воркеру
STATE_ASSIGNED = 'ASSIGNED'

# тарнзакция исполнена
STATE_SUCCESS = 'SUCCESS'

# ошибка при выполнении
STATE_FAILURE = 'FAILURE'

# назначено повторное выполнение
STATE_RETRY = 'RETRY'

# транзакция отозвана
STATE_REVOKED = 'REVOKED'


FINISHED_STATES = [STATE_SUCCESS, STATE_FAILURE, STATE_REVOKED]

UNFINISHED_STATES = [STATE_INIT, STATE_QUEUED, STATE_ASSIGNED, STATE_RETRY]

EXCEPTION_STATES = [STATE_RETRY, STATE_FAILURE, STATE_REVOKED]

ALL_STATES = [STATE_INIT, STATE_QUEUED, STATE_ASSIGNED, STATE_SUCCESS,
              STATE_FAILURE, STATE_RETRY, STATE_REVOKED]

MONGODB_DATE_FORMATTER = '%Y-%m-%d %H:%M:%S.%f'
