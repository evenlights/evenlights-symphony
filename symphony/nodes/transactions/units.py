# -*- coding: utf-8 -*-
import datetime

import mongoengine as mongo

from symphony.core.units import GenericInfraUnit
from symphony.nodes.transactions.containers import Sender, Transaction

from symphony.nodes.transactions import defaults as trx_defaults

DEFAULT_FORMATTER = trx_defaults.MONGODB_DATE_FORMATTER


class TransactionPoolUnit(GenericInfraUnit):
    shared = True
    sys_name = 'transaction_pool'

    def __init__(self, *args, **kwargs):
        super(TransactionPoolUnit, self).__init__(*args, **kwargs)

        # db
        db_name = self.infra.core.config.DB_NAME
        self.messages.debug('using database: %s' % db_name)

        db_kwargs = {}

        if hasattr(self.infra.core.config, 'DB_HOST'):
            db_kwargs['host'] = self.infra.core.config.DB_HOST
            self.messages.debug('connecting to host: %s' % db_kwargs['host'])

        if hasattr(self.infra.core.config, 'DB_PORT'):
            db_kwargs['port'] = self.infra.core.config.DB_PORT
            self.messages.debug('using port: %s' % db_kwargs['port'])

        if hasattr(self.infra.core.config, 'DB_USER'):
            db_kwargs['username'] = self.infra.core.config.DB_USER
            self.messages.debug('username: present')

        if hasattr(self.infra.core.config, 'DB_PASSWORD'):
            db_kwargs['password'] = self.infra.core.config.DB_PASSWORD
            self.messages.debug('password: present')

        self.messages.debug('connecting to mongodb engine')
        mongo.connect(db_name, **db_kwargs)

    @staticmethod
    def timestamp_truncate(timestamp):
        """ Метод округляет микросекунды до 100000, как mongodb округляет при
        сохранении.
        """
        time_string = '%s000' % timestamp.strftime(DEFAULT_FORMATTER)[:-3]
        return datetime.datetime.strptime(time_string, DEFAULT_FORMATTER)

    # операции с отправителями
    def sender_exists(self, id):
        """ Возвращает объект соответствубщего отправителя. """
        self.messages.debug('checking sender existence: %s' % id)

        try:
            Sender.objects.get(id=id)
            return True

        except Sender.DoesNotExist:
            return False

    def sender_list(self):
        """ Метод возвращает список всех имеющихся записей отправителей. Если
        список записей пуст, возвращается пустой список, поскольку запрашивающий
        узел все же должен получить ответ. """
        self.messages.debug('getting sender list')
        return Sender.objects

    def sender_read(self, id):
        """ Возвращает объект соответствубщего отправителя. """
        self.messages.debug('reading sender id: %s' % id)

        try:
            return Sender.objects.get(id=id)

        except Sender.DoesNotExist:
            self.messages.error('sender not found: %s' % id)

    def sender_write(self, id, name=None, max_transactions=None):
        """ Метод :func:`write` служит для создания и обновления отправителей.

        :param id: идентификатор отправителя используется для создания нового
            объекта либо обновления уже существующего
        :param name: имя отправителя
        :param max_transactions: максимальное количество хранимых транзакций,
            по умолчанию 0 (количество не ограничено)
        """
        self.messages.debug('writing sender id: %s' % id)

        try:
            sender = Sender.objects.get(id=id)
        except Sender.DoesNotExist:
            self.messages.debug('sender not present, creating')
            sender = Sender(id=id).save(force_insert=True)

        update_kwargs = {}
        if name:
            update_kwargs['set__name'] = name
        if max_transactions:
            update_kwargs['set__max_transactions'] = max_transactions

        if update_kwargs:
            sender.modify(**update_kwargs)

        return sender

    def sender_delete(self, id, cascade_transactions=False):
        """ Удаление отправителя и всех его сообщений """
        self.messages.debug('deleting sender id: %s' % id)

        try:
            sender = Sender.objects.get(id=id)

        # либо сообщаем, что лог не найден
        except Sender.DoesNotExist:
            self.messages.error('sender not found: %s' % id)
            return False

        if cascade_transactions:
            self.messages.debug('cascade-deleting sender transactions: %s' % id)
            list(map(lambda t: t.delete(), sender.transactions))
        return sender.delete()

    # операции с транзакциями
    def transaction_exists(self, id):
        """ Метод чтения отдельной транзакции. """
        self.messages.debug('checking transaction existence: %s' % id)

        try:
            Transaction.objects.get(id=id)
            return True

        except Transaction.DoesNotExist:
            return False

    def transaction_read(self, id):
        """ Метод чтения отдельной транзакции. """
        self.messages.debug('reading transaction id: %s' % id)

        try:
            return Transaction.objects.get(id=id)

        except Transaction.DoesNotExist:
            self.messages.error('transaction not found: %s' % id)

    def transaction_write(self, id, sender_id=None,
                          group=None, name=None, args=None, kwargs=None,
                          state=None, timestamp=None):
        """ Метод создания и обновления транзакций.
        :param id: идентификатор транзакции
        :param sender_id: идентификатор инициатора транзакции

        :param name: имя исходного сигнала
        :param group: группа исходного сигнала
        :param args: аргументы исходного сигнала
        :param kwargs: ключевые аргументы исходного сигнала

        :param state: состояние транзакции
        :param timestamp: время создания транзакции
        """
        self.messages.debug('writing transaction id: %s' % id)

        self.messages.debug('getting transaction')
        try:
            transaction = Transaction.objects.get(id=id)
        except Transaction.DoesNotExist:
            self.messages.debug('transaction not present, creating')
            ts = timestamp if timestamp else datetime.datetime.now()
            transaction = Transaction(id=id, timestamp=ts).save(force_insert=True)

        update_kwargs = {}
        if sender_id:
            # получаем существующий лог или создаем новый
            self.messages.debug('getting sender')
            try:
                sender = Sender.objects.get(id=sender_id)
            except Sender.DoesNotExist:
                self.messages.debug('sender not present, creating')
                sender = Sender(id=sender_id).save(force_insert=True)

            update_kwargs['set__sender'] = sender

        # параметры сигнала
        if name:
            update_kwargs['set__name'] = name

        if group:
            update_kwargs['set__group'] = group

        if args:
            update_kwargs['set__args'] = args

        if kwargs:
            update_kwargs['set__kwargs'] = kwargs

        # дополнительные параметры
        if state:
            update_kwargs['set__state'] = state

        if timestamp:
            update_kwargs['set__timestamp'] = timestamp

        if update_kwargs:
            transaction.modify(**update_kwargs)

        return transaction

    def transaction_list(self, sender_id=None,
                         group=None, name=None, args=None, kwargs=None,
                         state=None, from_date=None, to_date=None):
        """ Метод :func:`write` для создания и обновления транзакций.

        :param sender_id: идентификатор инициатора транзакции

        :param name: поиск по имени сигнала
        :param group: поиск по группе сигнала
        :param args: поиск по аргументам сигнала
        :param kwargs: поиск по ключевым аргументам сигнала

        :param state: состояние транзакции
        :param from_date: нижняя граница времени
        :param to_date: верхняя граница времени
        """
        self.messages.debug('getting transaction list')

        call_kwargs = {}

        if sender_id:
            self.messages.debug('filtering by sender id: %s' % sender_id)
            try:
                sender = Sender.objects.get(id=sender_id)
                call_kwargs['sender'] = sender
            except Sender.DoesNotExist:
                self.messages.error('sender does not exist: %s' % sender_id)
                return

        if group:
            self.messages.debug('filtering by signal group: %s' % group)
            call_kwargs['group'] = group

        if name:
            self.messages.debug('filtering by signal name: %s' % name)
            call_kwargs['name'] = name

        if args:
            self.messages.debug('filtering by signal args: %s' % args)
            if isinstance(args, list):
                call_kwargs['args__in'] = args
            else:
                call_kwargs['args'] = args

        if kwargs:
            self.messages.debug('filtering by signal kwargs: %s' % kwargs)
            for key in kwargs:
                call_kwargs['kwargs__' + key] = kwargs[key]

        if state:
            self.messages.debug('filtering by transaction state: %s' % state)
            call_kwargs['state'] = state

        if from_date:
            self.messages.debug('filtering by from_date: %s' % from_date)
            call_kwargs['timestamp__gte'] = self.timestamp_truncate(from_date)

        if to_date:
            self.messages.debug('filtering by to_date: %s' % to_date)
            call_kwargs['timestamp__lte'] = self.timestamp_truncate(to_date)

        self.messages.debug('call kwargs: %s' % call_kwargs)
        transactions = Transaction.objects(**call_kwargs)

        return transactions

    def transaction_delete(self, id=None, sender_id=None,
                           group=None, name=None, args=None, kwargs=None,
                           state=None, from_date=None, to_date=None):
        """ Удаление транзакций. Использует те же параметры, что и метод
        :func:`transaction_list`.

        :param id: идентификатор транзакции
        :param sender_id: идентификатор инициатора транзакции

        :param name: поиск по имени сигнала
        :param group: поиск по группе сигнала
        :param args: поиск по аргументам сигнала
        :param kwargs: поиск по ключевым аргументам сигнала

        :param state: состояние транзакции
        :param from_date: нижняя граница времени
        :param to_date: верхняя граница времени
        """
        self.messages.debug('deleting single transaction: %s' % id)
        if id:
            try:
                transaction = Transaction.objects(id=id)
                transaction.delete()
                return
            except Transaction.DosNotExist:
                self.messages.error('transaction does not exist: %s' % id)
                return

        self.messages.debug('deleting transactions')
        transactions = self.transaction_list(
            sender_id=sender_id,
            group=group, name=name, args=args, kwargs=kwargs,
            state=state, from_date=from_date, to_date=to_date
        )

        if transactions:
            list(map(lambda m: m.delete(), transactions))


class CleanerUnit(GenericInfraUnit):
    shared = True
    sys_name = 'cleaner'

    def run(self):
        self.messages.debug('cleaner unit active')

        for transaction in Transaction.objects.all():
            if transaction.sender:

                sender = transaction.sender
                try:
                    if sender.max_transactions:
                        if len(sender.transactions) > sender.max_transactions:
                            extra_transactions_count = len(sender.transactions) - sender.max_transactions + 1
                            self.messages.info('cleaning %d transactions for sender %s' % (extra_transactions_count, sender.id))
                            extra_transactions = sender.transactions.order_by('timestamp')[:extra_transactions_count]
                            # fixme: логично как-то удалять и сообщения этих транзакций. API?
                            list(map(lambda m: m.delete(), extra_transactions))

                except AttributeError:
                    # sender -- просто строка
                    pass
