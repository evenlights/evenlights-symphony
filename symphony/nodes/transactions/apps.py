# -*- coding: utf-8 -*-
from symphony.apps.configs import WorkerConfig
from symphony.apps.transport.worker import Worker, UnboundWorker
from symphony.nodes.transactions.ci import ALL_HANDLERS
from symphony.nodes.transactions.units import TransactionPoolUnit, CleanerUnit
from symphony.timers.containers import GenericTimer


class TransactionPoolWorkerConfig(WorkerConfig):
    """ Моудль конфигурации

    :param DB_NAME: имя базы дынных, обязательный параметр

    :param DB_HOST: хост
    :param DB_USER: имя пользователя
    :param DB_PASSWORD: пароль
    """
    defaults = WorkerConfig.extend_defaults({
        'DB_NAME': 'transactions',
        'SERVICE_THREAD_INTERVAL': 120,
    })


class TransactionPoolWorker(Worker):
    """ todo """
    config = TransactionPoolWorkerConfig
    signal_handlers = ALL_HANDLERS

    def __init__(self, *args, **kwargs):
        super(TransactionPoolWorker, self).__init__(*args, **kwargs)
        self.transaction_pool = None

    def setup(self):
        self.transaction_pool = TransactionPoolUnit(core=self.core)

    def flush(self):
        self.transaction_pool.flush()


class ServiceThread(UnboundWorker):
    """ todo """
    config = TransactionPoolWorkerConfig

    def __init__(self, *args, **kwargs):
        super(ServiceThread, self).__init__(*args, **kwargs)
        self.timer = None
        self.cleaner = None

    def setup(self):

        class _Timer(GenericTimer):
            def on_timeout(self):
                self.mount_point.core.messages.info('service thread awake')
                self.mount_point.cleaner.run()
                self.reset()
        self.timer = _Timer(
            mount_point=self, seconds=self.core.config.SERVICE_THREAD_INTERVAL
        )

        self.cleaner = CleanerUnit(core=self.core)

    def cycle(self):
        self.timer.refresh()
