# -*- coding: utf-8 -*-
from symphony.ci.api import RemoteAPI
from symphony.nodes.transactions import defaults as trx_defaults


class RemoteTransactionsAPI(RemoteAPI):
    name = 'transactions'
    sys_name = 'transactions_api'

    def transaction_write(self, signal, state=trx_defaults.STATE_INIT):
        # отправитель транзакции: '_sender_id' из ключевых аргументов сигнала или
        # адрес отправителя сообщения
        sender_id = signal.kwargs.pop('_sender_id') if '_sender_id' in signal.kwargs \
            else signal.address

        return self.single(
            'transaction__write', signal.transaction_id,
            sender_id=sender_id,
            group=signal.group,
            name=signal.name,
            args=signal.args,
            kwargs=signal.kwargs,
            state=state,
        )

Transactions = RemoteTransactionsAPI
