# -*- coding: utf-8 -*-
import simplejson as json

from symphony.core.containers.fields import StringField, IntegerField, BooleanField
from symphony.ci.handlers import RESTEndpointMixin
from symphony.ci.handlers.smp import GenericWorkerHandler

from symphony.protocols.smp import defaults as smp_defaults


class SenderWriteHandler(RESTEndpointMixin, GenericWorkerHandler):
    """
    Сигнатура вызываемой процедуры::

        def sender_write(self, id, name=None, max_transactions=None)

    :param id: идентификатор отправителя используется для создания нового
        объекта либо обновления уже существующего
    :param name: имя отправителя
    :param max_transactions: максимальное количество хранимых транзакций,
        по умолчанию 0 (количество не ограничено)
    """
    group = 'sender'
    name = 'write'
    args = [StringField]
    kwargs = {
        'name': StringField,
        'max_transactions': IntegerField,
    }
    http_method = 'post'

    def handle(self, signal):
        transaction_pool_ = self.infra.core.shared.units.transaction_pool

        sender_existed = transaction_pool_.sender_exists(*self.runtime.args)

        transaction_pool_.sender_write(*self.runtime.args, **self.runtime.kwargs)
        if transaction_pool_.messages.internal.errors:
            self.messages.error(transaction_pool_.messages.internal.first_error_content)
            return

        if sender_existed:
            self.accepted_response(final=True)
        else:
            self.created_response(final=True)


class SenderReadHandler(RESTEndpointMixin, GenericWorkerHandler):
    """
    Сигнатура вызываемой процедуры:

        def sender_read(self, id)

    :param id: идентификатор отправителя
    """
    group = 'sender'
    name = 'read'
    args = [StringField]

    def handle(self, signal):
        transaction_pool_ = self.infra.core.shared.units.transaction_pool

        sender = transaction_pool_.sender_read(*self.runtime.args)
        if transaction_pool_.messages.internal.errors:
            self.messages.error(transaction_pool_.messages.internal.first_error_content)
            return

        # возвращаем код OK, если дошли до этого места
        self.ok_response(
            content_type=smp_defaults.SMP_CONTENT_JSON,
            content=sender.repr.json,
            final=True
        )


class SenderListHandler(RESTEndpointMixin, GenericWorkerHandler):
    """
    Сигнатура вызываемой процедуры::

        def sender_list(self):
    """
    group = 'sender'
    name = 'list'

    def handle(self, signal):
        transaction_pool_ = self.infra.core.shared.units.transaction_pool

        sender_list_db = transaction_pool_.sender_list()
        if transaction_pool_.messages.internal.errors:
            self.messages.error(transaction_pool_.messages.internal.first_error_content)
            return

        sender_list_json = json.dumps([s.repr.transport for s in sender_list_db])

        # возвращаем код OK, если дошли до этого места
        self.ok_response(
            content_type=smp_defaults.SMP_CONTENT_JSON,
            content=sender_list_json,
            final=True
        )


class SenderDeleteHandler(RESTEndpointMixin, GenericWorkerHandler):
    """
    Сигнатура вызываемой процедуры::

        def sender_delete(self, id, cascade_messages=False,
                          cascade_transactions=False):

    :param id: идентификатор отправителя

    :param cascade_messages: удалить все сообщения данного отправителя
    :param cascade_transactions: удалитьвсе транзакции и связанные с ними
        сообщения
    """
    group = 'sender'
    name = 'delete'
    args = [StringField]
    kwargs = {
        'cascade_transactions': BooleanField,
    }
    http_method = 'delete'

    def handle(self, signal):
        transaction_pool_ = self.infra.core.shared.units.transaction_pool

        transaction_pool_.sender_delete(*self.runtime.args, **self.runtime.kwargs)
        if transaction_pool_.messages.internal.errors:
            self.messages.error(transaction_pool_.messages.internal.first_error_content)
            return

        # код NO_CONTENT, тип контента CONTENT_NONE
        self.no_content_response(final=True)
