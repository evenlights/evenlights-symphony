# -*- coding: utf-8 -*-
import simplejson as json

from symphony.core.containers.fields import (
    StringField, GenericDateTimeField, SimpleDateTimeField,
    GenericListField, GenericDictField
)
from symphony.ci.handlers import RESTEndpointMixin
from symphony.ci.handlers.smp import GenericWorkerHandler

from symphony.protocols.smp import defaults as smp_defaults


class TransactionWriteHandler(RESTEndpointMixin, GenericWorkerHandler):
    """ Сигнатура вызываемой процедуры::

        def transaction_write(self, id, sender_id=None,
                              group=None, name=None, args=None, kwargs=None,
                              state=None, timestamp=None)

    :param id: идентификатор отправителя используется для создания нового
        объекта либо обновления уже существующего

    :param sender_id: идентификатор инициатора транзакции

    :param group: группа сигнала
    :param name: имя сигнала
    :param args: аргументы сигнала
    :param kwargs: ключевые аргументы сигнала

    :param state: состояние транзакции
    :param timestamp: время создания транзакции
    """
    group = 'transaction'
    name = 'write'
    args = [StringField]
    kwargs = {
        'sender_id': StringField,

        'group': StringField,
        'name': StringField,
        'args': GenericListField,
        'kwargs': GenericDictField,

        'state': StringField,
        'timestamp': GenericDateTimeField,
    }
    http_method = 'post'

    def handle(self, signal):
        transaction_pool_ = self.infra.core.shared.units.transaction_pool

        transaction_existed = transaction_pool_.transaction_exists(self.runtime.args[0])

        transaction_pool_.transaction_write(*self.runtime.args, **self.runtime.kwargs)
        if transaction_pool_.messages.internal.errors:
            self.messages.error(transaction_pool_.messages.internal.first_error_content)
            return

        if transaction_existed:
            self.accepted_response(final=True)
        else:
            self.created_response(final=True)


class TransactionReadHandler(RESTEndpointMixin, GenericWorkerHandler):
    """ Сигнатура вызываемой процедуры::

        def transaction_read(self, id)

    :param id: идентификатор отправителя
    """
    group = 'transaction'
    name = 'read'
    args = [StringField]

    def handle(self, signal):
        transaction_pool_ = self.infra.core.shared.units.transaction_pool

        transaction = transaction_pool_.transaction_read(*self.runtime.args)
        if transaction_pool_.messages.internal.errors:
            self.messages.error(transaction_pool_.messages.internal.first_error_content)
            return

        # возвращаем код OK, если дошли до этого места
        self.ok_response(
            content_type=smp_defaults.SMP_CONTENT_JSON,
            content=transaction.repr.json,
            final=True
        )


class TransactionListHandler(RESTEndpointMixin, GenericWorkerHandler):
    """ Сигнатура вызываемой процедуры::

        def message_list(self, sender_id=None, transaction_id=None,
                         level='debug', from_date=None, to_date=None)

    :param sender_id: идентификатор инициатора транзакции

    :param group: группа сигнала
    :param name: имя сигнала
    :param args: аргументы сигнала (строка или список)
    :param kwargs: ключевые аргументы сигнала

    :param state: состояние транзакции

    :param from_date: нижний порог сообщений по времени
    :param to_date: верхний порог сообщений по времени
    """
    group = 'transaction'
    name = 'list'
    kwargs = {
        'sender_id': StringField,

        'group': StringField,
        'name': StringField,
        'args': None,
        'kwargs': GenericDictField,

        'state': StringField,
        'from_date': SimpleDateTimeField,
        'to_date': SimpleDateTimeField,
    }

    def validate(self, signal):
        # аргументы -- строки или список
        if 'args' in self.runtime.kwargs:
            if not (isinstance(self.runtime.kwargs.args, (str, list))):
                self.messages.error('args parameter should be string or list')
                return False

        # если переданы обе даты, убеждаемся, что from_date <= to_date
        if 'from_date' and 'to_date' in self.runtime.kwargs:
            if not (self.runtime.kwargs.from_date <= self.runtime.kwargs.to_date):
                self.messages.error('from_date should be less or equal to to_date')
                return False

        return True

    def handle(self, signal):
        transaction_pool_ = self.infra.core.shared.units.transaction_pool

        trx_list_db = transaction_pool_.transaction_list(**self.runtime.kwargs)
        if transaction_pool_.messages.internal.errors:
            self.messages.error(transaction_pool_.messages.internal.first_error_content)
            return

        trx_list_json = json.dumps([m.repr.transport for m in trx_list_db])

        # возвращаем код CREATED, если дошли до этого места
        self.ok_response(
            content_type=smp_defaults.SMP_CONTENT_JSON,
            content=trx_list_json,
            final=True)


class TransactionDeleteHandler(RESTEndpointMixin, GenericWorkerHandler):
    """ Сигнатура вызываемой процедуры::

        def message_list(self, sender_id=None, transaction_id=None,
                         level='debug', from_date=None, to_date=None)

    :param sender_id: идентификатор инициатора транзакции

    :param group: группа сигнала
    :param name: имя сигнала
    :param args: аргументы сигнала (строка или список)
    :param kwargs: ключевые аргументы сигнала

    :param state: состояние транзакции

    :param from_date: нижний порог сообщений по времени
    :param to_date: верхний порог сообщений по времени
    """
    group = 'transaction'
    name = 'delete'
    kwargs = {
        'id': StringField,
        'sender_id': StringField,

        'group': StringField,
        'name': StringField,
        'args': None,
        'kwargs': GenericDictField,

        'state': StringField,
        'from_date': SimpleDateTimeField,
        'to_date': SimpleDateTimeField,
    }
    http_method = 'delete'

    def validate(self, signal):
        # аргументы -- строки или список
        if 'args' in self.runtime.kwargs:
            if not (isinstance(self.runtime.kwargs.args, (str, list))):
                self.messages.error('args parameter should be string or list')
                return False

        # если переданы обе даты, убеждаемся, что from_date <= to_date
        if 'from_date' and 'to_date' in self.runtime.kwargs:
            if not (self.runtime.kwargs.from_date <= self.runtime.kwargs.to_date):
                self.messages.error('from_date should be less or equal to to_date')
                return False

        return True

    def handle(self, signal):
        transaction_pool_ = self.infra.core.shared.units.transaction_pool

        transaction_pool_.transaction_delete(**self.runtime.kwargs)
        if transaction_pool_.messages.internal.errors:
            self.messages.error(transaction_pool_.messages.internal.first_error_content)
            return

        # возвращаем код NO_CONTENT, если дошли до этого места
        self.no_content_response(final=True)
