# -*- coding: utf-8 -*-
from symphony.core.datatypes import DotDict
from symphony.core.infra.base import InfraManager
from symphony.messages.components import Messages
from symphony.apps.core.components import AppCore
from symphony.tests.base import BaseTest


class InfraTestCase(BaseTest):

    def __str__(self):
        return 'infra-test'

    def setUp(self):
        super(InfraTestCase, self).setUp()
        self.runtime = DotDict({})
        self.config = DotDict({
            'DEBUG': True,
        })

        self.messages.info('infrastructure manager init')
        self.infra = InfraManager(self)

        # ядро
        self.messages.info('core init')
        AppCore().init(config=self.config, registry=self.infra.components, mount_point=self)

        # сообщения
        self.messages.info('messages init')
        self.__messages = self.messages
        delattr(self, 'messages')
        self.infra.join(Messages)
        self.default_mbus = self.core.mbus

        # регистрируем тест как приложение
        self.core.shared.register(self, name='app')

    def tearDown(self):
        self.infraShutdown()
        super(InfraTestCase, self).tearDown()

    # Infrastructure

    def infraFlush(self):
        self.infra.flush()

    def infraShutdown(self):
        self.infra.shutdown()
        self.messages = self.__messages


InfraTest = InfraTestCase
