# -*- coding: utf-8 -*-
import logging
import signal

TIMEOUT = 3


class timeout(object):
    """ http://charlesnagy.info/it/python/useful-python-class-based-decorators-and-context-managers
    """

    class TimeoutException(Exception):
        pass

    def __init__(self, seconds=TIMEOUT, msg=None):
        self.timeout = seconds
        self.timed_out = False
        self.msg = msg
        super(timeout, self).__init__()

    def _start(self):
        self.sig_handler = signal.signal(signal.SIGALRM, self.__handler)
        signal.alarm(self.timeout)

    def _cleanup(self):
        signal.signal(signal.SIGALRM, self.sig_handler)
        signal.alarm(0)

    def _timed_out(self):
        if self.msg:
            logging.error('Timeout after %d seconds (%s)' % (self.timeout, self.msg))
        else:
            logging.error('Function timed out after %d seconds.' % self.timeout)

    def __enter__(self):
        self._start()
        logging.debug('Timeout context started with %d seconds.' % self.timeout)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._cleanup()
        if exc_type == timeout.TimeoutException:
            self.timed_out = True
            self._timed_out()
            return True

    def __call__(self, fn):
        def wrapped(*args, **kwargs):
            self._start()
            retval = None
            exception = None
            try:
                retval = fn(*args, **kwargs)
            except timeout.TimeoutException as e:
                self._timed_out()
                exception = e
            finally:
                self._cleanup()

            if exception:
                raise exception

            return retval

        return wrapped

    def __handler(self, signum, frame):
        raise timeout.TimeoutException()
