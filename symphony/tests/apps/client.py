# -*- coding: utf-8 -*-
from symphony.apps.configs import CLIClientConfig
from symphony.apps.transport.client import CLIClient
from symphony.configs.loaders import dict_loader


class TestClientConfig(CLIClientConfig):
    loader = dict_loader
    source = {
        'SYS_ID': 'e2e-test-cli-client',

        'CI_FE_CONF': './sockets/client_ci_fe.json',
        'REPLY_TIMEOUT': 10,
        'REQUEST_RETRIES': 5,

        'DEBUG': True,
    }


class TestCLIClient(CLIClient):
    config = TestClientConfig
