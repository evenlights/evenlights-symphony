# -*- coding: utf-8 -*-
import sys
from threading import Thread

import zmq

from symphony.protocols.smp.adapters import SMPSocketAdapter

from symphony.protocols.smp.defaults import SMP_CONTENT_NONE, SMP_CODE_OK


class MockNode(Thread):
    def __init__(self, context, socket_conf, debug=True, *args, **kwargs):
        super(MockNode, self).__init__(*args, **kwargs)
        self.debug = debug

        if context:
            self.context = context
            self._external_context = True
        else:
            self.context = None
            self._external_context = False

        self.context = context
        self.socket = None
        self.poller = None
        self._socket_poll = None
        self._socket_conf = socket_conf

        self.is_running = True
        self.is_active = True

    def tprint(self, msg):
        """like print, but won't get newlines confused with multiple core"""
        if self.debug:
            sys.stdout.write(msg + '\n')
            sys.stdout.flush()

    def log(self, msg):
        self.tprint('%s: %s' % (self.name, msg))

    def setup(self):
        if not self.context:
            self.context = zmq.Context()

        self.poller = zmq.Poller()
        self.socket = SMPSocketAdapter(
            sys_name='%s_socket' % self.name,
            config_source=self._socket_conf,
            context=self.context,
            poller=self.poller,
        )
        self.socket.attach()

    def poll_sockets(self):
        self._socket_poll = dict(self.poller.poll(1000))

    def poll_in(self):
        return self.socket._socket in self._socket_poll

    def pause(self):
        self.is_active = False

    def resume(self):
        self.is_active = True

    def shutdown(self):
        self.log('shutdown received')
        self.is_running = False

    def client_final(self, address, content=None, code=SMP_CODE_OK,
                     content_type=SMP_CONTENT_NONE, transaction_id=None):
        """ По умолчанию узел отвечает кодом OK на любой запрос. """
        self.socket.client_final(
            address=address,
            transaction_id=transaction_id,
            content_type=content_type,
            content=content,
            code=code
        )

    def reply(self, msg):
        self.log('sending reply to %s, transaction id %s' % (msg.address, msg.transaction_id))
        self.client_final(msg.address, transaction_id=msg.transaction_id)

    def run(self):
        self.setup()

        while self.is_running:
            self.poll_sockets()

            if self.poll_in():
                msg = self.socket.client_recv()

                if self.is_active:
                    self.reply(msg)

        self.log('main cycle exit')

        self.socket.detach()

        if not self._external_context:
            self.context.term()
