# -*- coding: utf-8 -*-
from symphony.tests.base import BaseTest


class SymphonyAppTestMixin(object):
    app_class = None

    def setUp(self):
        super(SymphonyAppTestMixin, self).setUp()
        self.messages.info('initializing app')
        try:
            name = self.app_class.name
        except AttributeError:
            name = None
        if not name:
            name = 'test-app'

        app_kwargs = {
            'name': name
        }

        try:
            app_kwargs['context'] = self.context
        except AttributeError:
            pass

        self.app = self.app_class(**app_kwargs)

    def tearDown(self):
        self.appExit()
        self.appCleanup()
        self.appDeactivate()
        self.appShutdown()
        super(SymphonyAppTestMixin, self).tearDown()

    # App
    def appInit(self):
        self.messages.info('init phase in progress')
        self.app.bootstrap()
        self.app.init()
        self.app.infra.init(
            core=self.app.core, config=self.app.config,
            registry=self.app.infra.components, mount_point=self.app
        )
        self.default_mbus = self.app.core.mbus

    def appActivate(self):
        self.messages.info('activation phase in progress')
        self.app.activate()

    def appSetup(self):
        self.messages.info('setup phase in progress')
        self.app.setup()

    def appSetRunning(self):
        self.app.runtime['is_running'] = True

    def appHandle(self):
        if not self.app.runtime['is_running']:
            self.app.runtime['is_running'] = True
        self.app.handle()

    def appExit(self):
        # фаза обработки могла быть не достигнута,
        # например, проверялтся только метод init
        if self.app.component.state['init_successful']:
            if self.app.is_running:
                self.app.exit()

    def appCleanup(self):
        self.messages.info('cleanup phase in progress')
        self.app.cleanup()

    def appDeactivate(self):
        # приложение могло быть не было активировано,
        # например, проверялтся только метод init
        if self.app.component.state['activation_successful']:

            # деактивация могла быть поризведена в тесте
            if not self.app.component.state['deactivation_successful']:
                self.messages.info('deactivation phase in progress')
                self.app.infra.deactivate()
                self.app.component.deactivate()

    def appShutdown(self):
        if self.app.component.state['init_successful']:
            if not self.app.component.state['shutdown_successful']:
                self.messages.info('shutdown phase in progress')
                self.app.infra.shutdown()
                self.app.shutdown()

    def appEnterRunning(self):
        self.messages.info('entering run phase')
        self.appInit()
        self.appActivate()
        self.appSetup()
        self.appSetRunning()


class SymphonyAppTestCase(SymphonyAppTestMixin, BaseTest):
    pass


AppMixin = SymphonyAppTestMixin
AppTest = SymphonyAppTestCase
