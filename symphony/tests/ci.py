# -*- coding: utf-8 -*-
from symphony.protocols.smp.containers import SMPClientMessage

from symphony.protocols.smp import defaults


class SymphonyCITestMixin(object):

    def setUp(self):
        super(SymphonyCITestMixin, self).setUp()

        class DummyClientInterface(object):
            def __init__(self, core=None):
                self.core = core
                self.request_content = None

                self.partial_content = []

                self.final_content = None

            def client_request(self, *args, **kwargs):
                self.request_content = kwargs['content'] if 'content' in kwargs else None

            def client_partial(self, *args, **kwargs):
                self.partial_content.append(kwargs['content'] if 'content' in kwargs else None)

            def client_final(self, *args, **kwargs):
                self.final_content = kwargs['content'] if 'content' in kwargs else None

            def flush(self):
                self.request_content = None
                self.partial_content = []
                self.final_content = None

            def client_recv(self):
                return SMPClientMessage(message_type=defaults.SMP_TYPE_FINAL)

        class DummyWorkerInterface(object):

            def __init__(self, core=None):
                self.core = core

                self.request_content = None

                self.partial_content = []
                self.partial_content_type = []
                self.partial_code = []

                self.final_content = None
                self.final_content_type = None
                self.final_code = None

            def worker_request(self, *args, **kwargs):
                self.request_content = kwargs['content'] if 'content' in kwargs else None

            def worker_partial(self, *args, **kwargs):
                self.partial_content.append(kwargs['content'] if 'content' in kwargs else None)
                self.partial_content_type.append(kwargs['content_type'] if 'content_type' in kwargs else None)
                self.partial_code.append(kwargs['code'] if 'code' in kwargs else None)

            def worker_final(self, *args, **kwargs):
                self.final_content = kwargs['content'] if 'content' in kwargs else None
                self.final_content_type = kwargs['content_type'] if 'content_type' in kwargs else None
                self.final_code = kwargs['code'] if 'code' in kwargs else None

            def flush(self):
                self.request_content = None

                self.partial_content = []
                self.partial_content_type = []
                self.partial_code = []

                self.final_content = None
                self.final_content_type = None
                self.final_code = None

        self.DummyClientInterface = DummyClientInterface
        self.DummyWorkerInterface = DummyWorkerInterface


CITestMixin = SymphonyCITestMixin
