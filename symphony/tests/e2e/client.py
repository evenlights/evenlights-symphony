# -*- coding: utf-8 -*-
from symphony.tests.infra import InfraTest
from symphony.tests.apps.client import TestCLIClient
from symphony.protocols.smp.containers import SMPSignal


class SymphonyE2EClientTestCase(InfraTest):

    def setUp(self):
        super(SymphonyE2EClientTestCase, self).setUp()
        self.messages.info('initializing client')
        self.client = TestCLIClient(name='e2e-test-cli-client')
        self.client.shell_mode = False

    def tearDown(self):
        super(InfraTest, self).tearDown()

    def executeRequest(self, signal):
        self.client.core.flush()

        # помещаем сигнал в очередь и отправляем
        self.messages.info('sending client request')
        self.client.ci.queue_put(signal)
        self.client.ci_queue_handle()

        # получаем ответ
        self.messages.info('receiving response')
        while self.client.transaction_pool.unfinished:
            self.client.socket_poll()
            self.client.ci_fe_handle()
            self.client.transaction_pool_handle()

    def clientStart(self):
        self.messages.info('starting client')
        self.client._setup()
        self.client.is_running = True

    def clientStop(self):
        self.messages.info('stopping client')
        self.client.shutdown()
        self.client._clean()

E2EClientTest = SymphonyE2EClientTestCase
