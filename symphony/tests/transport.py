# -*- coding: utf-8 -*-
import zmq

from symphony.protocols.smp.adapters import SMPSocketAdapter
from symphony.tests.infra import InfraTest
from symphony.tests.utils import timeout as timeout_context

from symphony.protocols.smp.defaults import (
    SMP_SUB_CLIENT, SMP_SUB_WORKER, SMP_SUB_LIFECYCLE
)


class SymphonyZMQTransportTest(InfraTest):
    SOCKET_POLLING_INTERVAL = 1000
    REPLY_TIMEOUT = 3
    sockets = None                     # в формате {name: config}

    def __str__(self):
        return 'zmq-transport-test'

    def setUp(self):
        super(SymphonyZMQTransportTest, self).setUp()
        self.runtime['transport'] = {
            'sockets': {},
            'own_context': True,
            'socket_poll': {},
        }
        self.messages.info('creating ZMQ context')
        self.context = zmq.Context()
        self.poller = zmq.Poller()

        self.socketsInit()

    def tearDown(self):
        self.socketsShutdown()

        self.messages.info('terminating context')
        self.context.term()
        super(SymphonyZMQTransportTest, self).tearDown()

    # Sockets
    def socketInit(self, name, config):
        self.messages.info('connecting %s socket' % name)

        SMPSocketAdapter(name=name).init(
            core=self.core, config=config, registry=self.infra.components,
            mount_point=self,
            context=self.context, poller=self.poller,
            socket_registry=self.runtime['transport']['sockets']
        )

    def socketConnect(self, socket):
        socket.connect()

    def socketsInit(self):
        if self.sockets:
            for name, config in list(self.sockets.items()):
                self.socketInit(name, config)

    def socketsConnect(self):
        if self.runtime['transport']['sockets']:
            for name, socket in list(self.runtime['transport']['sockets'].items()):
                self.messages.info('connecting socket: %s ' % name)
                socket.connect()

    def socketsShutdown(self):
        if self.runtime['transport']['sockets']:
            for name, socket in list(self.runtime['transport']['sockets'].items()):
                self.messages.info('disconnecting socket: %s ' % name)
                socket.shutdown()

    def pollSockets(self):
        self.runtime['transport']['socket_poll'] = dict(self.poller.poll(self.SOCKET_POLLING_INTERVAL))

    def pollIn(self, socket):
        return socket.socket in self.runtime['transport']['socket_poll']

    # Protocol Messages
    def assertSMPMessage(self, socket, timeout=REPLY_TIMEOUT, msg=None, **kwargs):

        with timeout_context(seconds=timeout):
            wire_msg = socket.read()
            protocol_message = socket.get_message(wire_msg)

            # все атрибуты должны присутствовать и соответствовать заданным значениями
            message_map = [hasattr(protocol_message, n) and getattr(protocol_message, n) == kwargs[n] for n in kwargs]

            if not all(message_map):
                failed_attrs = {}
                for name, value in list(kwargs.items()):
                    if not (hasattr(protocol_message, name) and getattr(protocol_message, name) == value):
                        failed_attrs[name] = value
                attrs_repr = ', '.join(['%s=%s' % (n, failed_attrs[n]) for n in failed_attrs])
                msg = self._formatMessage(
                    msg,
                    "following message attributes are missing or have incorrect value: %s" % attrs_repr
                )
                raise self.failureException(msg)

    def assertClientMessage(self, socket, timeout=REPLY_TIMEOUT, **kwargs):
        return self.assertSMPMessage(socket=socket, timeout=timeout, sub=SMP_SUB_CLIENT, **kwargs)

    def assertWorkerMessage(self, socket, timeout=REPLY_TIMEOUT, **kwargs):
        return self.assertSMPMessage(socket=socket, timeout=timeout, sub=SMP_SUB_WORKER, **kwargs)

    def assertLifeCycleMessage(self, scoket, timeout=REPLY_TIMEOUT, **kwargs):
        return self.assertSMPMessage(socket=scoket, timeout=timeout, sub=SMP_SUB_LIFECYCLE, **kwargs)


ZMQTransportTest = SymphonyZMQTransportTest
