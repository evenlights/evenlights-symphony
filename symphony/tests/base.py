# -*- coding: utf-8 -*-
import os
import sys
import time
import random
import string
import shutil
import logging
import unittest
import datetime

from symphony.tests.modules import TestCaseMessages
from symphony.apps.defaults import LOGGER_STDOUT_FORMATTER
from symphony.messages.defaults import LEVELS_HR2INT


class SymphonyBaseTestCase(unittest.TestCase):

    # test settings
    CYCLE_LENGTH = 10
    SLEEP_INTERVAL = 1

    LOG_NAME = ''
    LOG_LEVEL = logging.DEBUG

    def __str__(self):
        return 'base-test'

    def setUp(self):
        # PyCharm добавляет свои параметры
        sys.argv = [str(os.path.abspath(__file__))]

        self.messages = TestCaseMessages(mount_point=self)
        print('='*80)
        print(unittest.TestCase.id(self))
        print('-'*80)

        self.messages.info('random seed init')
        random.seed()

        self.messages.info('logging init')
        self.initLogging(name=self.LOG_NAME, level=self.LOG_LEVEL)

        self.default_mbus = None

    def tearDown(self):
        self.messages.info('logging shutdown')
        self.cleanLogging()
        print('-'*80)

    # random
    def randomString(self, n):
        return ''.join(
            random.choice(string.ascii_letters + string.digits)
            for _ in range(n)
        )

    def randomInt(self, n1, n2):
        return random.randint(n1, n2)

    def randomChoice(self, *args):
        return random.choice(*args)

    def randomRange(self, *args):
        return random.randrange(*args)

    # Logging
    def getLogger(self):
        return logging.getLogger(name=self._logger_name)

    def initLogging(self, name=LOG_NAME, level=LOG_LEVEL):
        self._logger_name = name
        logger = self.getLogger()
        logger.setLevel(level=level)
        handler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(logging.Formatter(LOGGER_STDOUT_FORMATTER))
        logger.addHandler(handler)

    def loggingSetLevel(self, level_name):
        logger = self.getLogger()
        logger.setLevel(LEVELS_HR2INT[level_name.lower()])

    def cleanLogging(self):
        logger = logging.getLogger(self._logger_name)
        list(map(logger.removeHandler, logger.handlers[:]))
        list(map(logger.removeFilter, logger.filters[:]))

    # Messages
    def getBusMessages(self, bus, bus_getter=None):
        if bus is None:
            bus = self.default_mbus

        if bus is None:
            raise self.failureException("both passed and default message buses resolve to None")

        return getattr(bus, bus_getter) if bus_getter else bus

    @staticmethod
    def findMessage(s, bus_messages):
        messages = bus_messages if bus_messages is not None else []

        if isinstance(s, list):
            match = []
            for match_str in s:
                match += [m for m in messages if match_str in m.content]

        else:
            match = [m for m in messages if s in m.content]

        return match

    def assertMessage(self, s, bus, bus_getter=None, msg=None):
        bus_messages = self.getBusMessages(bus, bus_getter=bus_getter)

        # сообщения присутствуют в шине
        if not bus_messages:
            msg = self._formatMessage(msg, "bus or filter result in no messages: %s" % s)
            raise self.failureException(msg)

        match = self.findMessage(s, bus_messages)

        if not match:
            msg = self._formatMessage(msg, "bus message is missing: %s" % s)
            raise self.failureException(msg)

    def assertNoMessage(self, s, bus, bus_getter=None, msg=None):
        bus_messages = self.getBusMessages(bus, bus_getter=bus_getter)
        match = self.findMessage(s, bus_messages)

        if match:
            msg = self._formatMessage(
                msg,
                "bus message is present: %s" % s
            )
            raise self.failureException(msg)

    def assertTraceMessage(self, msg, bus=None):
        self.assertMessage(msg, bus=bus, bus_getter='trace_messages')

    def assertNoTraceMessage(self, msg, bus=None):
        self.assertNoMessage(msg, bus=bus, bus_getter='trace_messages')

    def assertDebugMessage(self, msg, bus=None):
        self.assertMessage(msg, bus=bus, bus_getter='debug_messages')

    def assertNoDebugMessage(self, msg, bus=None):
        self.assertNoMessage(msg, bus=bus, bus_getter='debug_messages')

    def assertInfoMessage(self, msg, bus=None):
        self.assertMessage(msg, bus=bus, bus_getter='info_messages')

    def assertNoInfoMessage(self, msg, bus=None):
        self.assertNoMessage(msg, bus=bus, bus_getter='info_messages')

    def assertWarningMessage(self, msg, bus=None):
        self.assertMessage(msg, bus=bus, bus_getter='warnings')

    def assertNoWarningMessage(self, msg, bus=None):
        self.assertNoMessage(msg, bus=bus, bus_getter='warnings')

    def assertErrorMessage(self, msg, bus=None):
        self.assertMessage(msg, bus=bus, bus_getter='errors')

    def assertNoErrorMessage(self, msg, bus=None):
        self.assertNoMessage(msg, bus=bus, bus_getter='errors')

    def assertCriticalMessage(self, msg, bus=None):
        self.assertMessage(msg, bus=bus, bus_getter='critical_messages')

    def assertNoCriticalMessage(self, msg, bus=None):
        self.assertNoMessage(msg, bus=bus, bus_getter='critical_messages')

    # Time

    def sleep(self, interval=SLEEP_INTERVAL):
        time.sleep(interval)

    def datetimeNow(self):
        return datetime.datetime.now()

    # File System

    def cleanPath(self, path):
        self.messages.info('cleaning path: %s' % path)
        if os.path.exists(path):
            shutil.rmtree(path, ignore_errors=True)

    def copyPath(self, from_path, to_path):
        shutil.copytree(from_path, to_path)

    def assertFileExists(self, path):
        self.assertTrue(os.path.isfile(path))


BaseTest = SymphonyBaseTestCase
