# -*- coding: utf-8 -*-
import logging
import sys


class TestCaseMessages(object):
    def __init__(self, mount_point=None):
        self.mount_point = mount_point
        self.DEBUG = True

    def tprint(self, msg):
        """like print, but won't get newlines confused with multiple core"""
        if hasattr(self.mount_point, 'DEBUG'):
            _debug = self.mount_point.DEBUG
        else:
            _debug = self.DEBUG

        if _debug:
            sys.stdout.write(msg + '\n')
            sys.stdout.flush()

    def info(self, message):
        self.tprint('TEST    : ' + str(message))
