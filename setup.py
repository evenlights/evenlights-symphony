#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    # generic
    # 'importlib',
    'pytz',
    'simplejson',
    'jsonschema',
    'kitchen',

    # terminal
    'jinja2',
    'colorama',
    'termcolor',

    # transport
    'pyzmq',
    'requests',
    'coreapi',
    # 'tornado',

    # mongo
    # 'mongoengine',
]

setup_requirements = []

test_requirements = []

setup(
    author='George Makarovsky',
    author_email='development@evenlights.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    description='Symphony is an application and infrastructure framework based '
                'on ZeroMQ',
    install_requires=requirements,
    license='BSD license',
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='symphony',
    name='symphony',
    packages=find_packages(include=['symphony']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/mcaffee/symphony',
    version='1.1.1',
    zip_safe=False,
)
