# -*- coding: utf-8 -*-
from symphony.configs.components import ModuleConfig
from symphony.configs.loaders import module_loader
from symphony.tests.infra import InfraTest


TEST_CONFIG_KEYS = [
    'ROOT_DIR', 'SETTINGS_DIR', 'SAMPLES_DIR', 'SOCKET_CONF_DIR', 'TMP_DIR',
    'LOG_DIR', 'SOCK_DIR', 'DEBUG'
]


class TestModuleConfig(InfraTest):

    def setUp(self):
        super(TestModuleConfig, self).setUp()

        class TestConfig(ModuleConfig):
            required_keys = ['ROOT_DIR']
            loader = module_loader
            source = 'tests.test_settings'

        self.infra.join(TestConfig(name='test_config'))

    def test_init(self):
        """ Изначально правильными должны быть параметры: DEBUG, _source_dict и
        required_keys. Также должен быть смонтирован модуль messages.
        """

        # DEBUG=False
        self.assertIn('DEBUG', self.test_config)
        self.assertFalse(self.test_config.DEBUG)

        # required_keys
        self.assertEqual(self.test_config.required_keys, ['ROOT_DIR'])

        # messages module
        self.assertIn('messages', self.test_config)

    def test_load(self):
        """ При зугрузке должен загрузиться обязательный параметр 'ROOT_DIR',
        параметр 'DEBUG' должен иметь значение True, также должны быть загружены
        все остальные параметры
        """

        self.test_config.load()

        # ROOT_DIR
        self.assertIn('ROOT_DIR', self.test_config)

        # DEBUG=True
        self.assertIn('DEBUG', self.test_config)
        self.assertTrue(self.test_config.DEBUG)

        # все ключи
        for key in TEST_CONFIG_KEYS:
            self.assertIn(key, self.test_config)

    # validation
    def test_required_key_missing(self):

        # в качестве обязательного параметра указываем несуществующий
        self.test_config.required_keys = ['MISSING_PARAM']
        self.test_config.load()

        # проверяем, есть ли сообщение
        self.assertErrorMessage(
            'required parameter not found: MISSING_PARAM', self.core.mbus
        )
