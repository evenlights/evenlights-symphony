# -*- coding: utf-8 -*-
from symphony.tests.base import SymphonyBaseTestCase
from symphony.configs.loaders import (
    module_loader, json_file_loader, cfg_file_loader
)
import tests.test_settings as test_settings


class TestGenericConfigLoader(SymphonyBaseTestCase):

    def test_from_module(self):
        config_dict = module_loader(None, 'tests.test_settings')
        self.assertEqual(config_dict['ROOT_DIR'], test_settings.ROOT_DIR)
        self.assertEqual(config_dict['SAMPLES_DIR'], test_settings.SAMPLES_DIR)
        self.assertEqual(config_dict['DEBUG'], test_settings.DEBUG)

    def test_from_json_file(self):
        config_dict = json_file_loader(None, test_settings.SETTINGS_DIR + '/sample.json')
        self.assertEqual(config_dict['ROOT_DIR'], test_settings.ROOT_DIR)
        self.assertEqual(config_dict['SAMPLES_DIR'], test_settings.SAMPLES_DIR)
        self.assertEqual(config_dict['DEBUG'], test_settings.DEBUG)

    def test_from_cfg_file(self):
        config_dict = cfg_file_loader(None, test_settings.SETTINGS_DIR + '/sample.cfg')
        self.assertEqual(config_dict['ROOT_DIR'], test_settings.ROOT_DIR)
        self.assertEqual(config_dict['SAMPLES_DIR'], test_settings.SAMPLES_DIR)
        self.assertEqual(config_dict['DEBUG'], test_settings.DEBUG)
