# -*- coding: utf-8 -*-
from unittest import skip

from symphony.tests.base import SymphonyBaseTestCase
from symphony.configs.base import ConfigBase


class TestConfig(ConfigBase):
    # Объект будет загружать поля с именами 'a', 'b' и 'c' в качестве
    # обязательных. 'd' будет опциональным полем
    required_keys = ['A', 'B', 'C']

    # загрузчик будет просто возвращать исходный параметр
    loader = lambda slf, src: src


class TestConfigBase(SymphonyBaseTestCase):
    """ Тест должен проверять функционал базы:
    * инициализацию
    * загрузку обязательных полей
    * загрузку оставшихся полей
    """

    def setUp(self):
        super(TestConfigBase, self).setUp()
        test_source = {
            'A': 1,
            'B': 2,
            'C': 3,
            'D': 4
        }
        self.config_base = TestConfig(source=test_source)

    # @skip('')
    def test_init(self):
        # все элементы источника на месте
        self.assertEqual(self.config_base.source, {'A': 1, 'C': 3, 'B': 2, 'D': 4})

        # обязательные ключи переданы правильно
        self.assertEqual(self.config_base.required_keys, ['A', 'B', 'C'])

    # @skip('')
    def test_load(self):
        # импортируме обязательные поля
        self.config_base.load()

        # в буфере источника остался пустой словарь
        self.assertEqual(self.config_base.source, {})

        # соответствующие поля объекта установлены
        self.assertTrue(hasattr(self.config_base, 'A'))
        self.assertTrue(hasattr(self.config_base, 'B'))
        self.assertTrue(hasattr(self.config_base, 'C'))
        self.assertTrue(hasattr(self.config_base, 'D'))

        # поля объекта имеют правильные значения
        self.assertEqual(self.config_base.A, 1)
        self.assertEqual(self.config_base.B, 2)
        self.assertEqual(self.config_base.C, 3)
        self.assertEqual(self.config_base.D, 4)
