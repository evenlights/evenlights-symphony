# -*- coding: utf-8 -*-
import os

from symphony.configs.components import DictConfig
from symphony.configs.loaders import dict_loader
from symphony.apps.configs import (
    BrokerConfig, WorkerConfig, CLIClientConfig,
    ThreadPoolConfig
)

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# settings
SETTINGS_DIR = ROOT_DIR + '/test_settings'
SOCKET_CONF_DIR = SETTINGS_DIR + '/sockets'
SAMPLES_DIR = SETTINGS_DIR + '/samples'

# tmp
TMP_DIR = ROOT_DIR + '/test_tmp'
LOG_DIR = TMP_DIR + '/log'
SOCK_DIR = TMP_DIR + '/sock'

DEBUG = True


class TestBrokerConfig(BrokerConfig):
    # для теста мы используем модуль с загрзкой из словаря
    loader = dict_loader
    source = {
        'SYS_ID': 'test-broker',
        'ZMQ_TRANSPORT': {
            'version': 1.0,
            'apps': {
                'test-broker': {
                    'sockets': {
                        'ci_fe': {
                            'type': 'ROUTER',
                            'bind': 'inproc://broker_ci_fe',
                            'option': {
                                'identity': 'test-broker',
                            },
                        },
                        'ci_be': {
                            'type': 'ROUTER',
                            'bind': 'inproc://broker_ci_be',
                            'option': {
                                'identity': 'test-broker',
                            },
                        },
                        'lifecycle_be': {
                            'type': 'ROUTER',
                            'bind': 'inproc://broker_lc_be',
                            'option': {
                                'identity': 'test-broker',
                            },
                        },
                    },
                    'poll_interval': 1000,
                },
            }
        },
        # 'LOGGER_SOCKET_CONF': SOCKET_CONF_DIR + '/client_logger.json',
        # 'LOGGER_SOCKET_CONF': SOCKET_CONF_DIR + '/client_logger_staging.json',
        # 'TRANSACTIONS_SOCKET_CONF': SOCKET_CONF_DIR + '/client_transactions.json',
        # 'TRANSACTIONS_SOCKET_CONF': SOCKET_CONF_DIR + '/client_transactions_staging.json',

        'HEARTBEAT_INTERVAL_SELF': 1,
        'HEARTBEAT_INTERVAL_WORKERS': 10,
        'DEBUG': DEBUG,
    }


class TestWorkerConfig(DictConfig):
    required_keys = DictConfig.extend_required_keys([
        'HEARTBEAT_RECONNECT_INIT', 'HEARTBEAT_RECONNECT_MAX',
        'HEARTBEAT_INTERVAL_SELF',
        'HEARTBEAT_INTERVAL_BROKER', 'HEARTBEAT_LIVENESS_BROKER',
    ])
    source = {
        'ID': 'test-worker',
        'ZMQ_TRANSPORT': {
            'version': 1.0,
            'apps': {
                'test-worker': {
                    'sockets': {
                        'ci_socket': {
                            'type': 'DEALER',
                            'connect': 'inproc://broker_ci_be',
                            'option': {
                                'identity': 'test-worker',
                            },
                        },
                        'lifecycle_socket': {
                            'type': 'DEALER',
                            'connect': 'inproc://broker_lc_be',
                            'option': {
                                'identity': 'test-worker',
                            },
                        },
                    },
                    'poll_interval': 1000,
                },
            }
        },
        # 'LOGGER_SOCKET_CONF': SOCKET_CONF_DIR + '/client_logger_staging.json',
        # 'LOGGER_SOCKET_CONF': SOCKET_CONF_DIR + '/client_logger.json',

        'HEARTBEAT_RECONNECT_INIT': 1,
        'HEARTBEAT_RECONNECT_MAX': 3,
        'HEARTBEAT_INTERVAL_SELF': 1,
        'HEARTBEAT_INTERVAL_BROKER': 1,
        'HEARTBEAT_LIVENESS_BROKER': 3,

        'DEBUG': DEBUG,
    }


class TestClientConfig(CLIClientConfig):
    # для теста мы используем модуль с загрзкой из словаря
    loader = dict_loader
    source = {
        'ID': 'test-client',
        'ZMQ_TRANSPORT': {
            'version': 1.0,
            'apps': {
                'test-client': {
                    'sockets': {
                        'ci_socket': {
                            'type': 'DEALER',
                            'connect': 'inproc://broker_ci_fe',
                            'option': {
                                'identity': 'test-client',
                            },
                        },
                    },
                    'poll_interval': 1000,
                },
            }
        },
        'REPLY_TIMEOUT': 1,
        'REQUEST_RETRIES': 3,
        'DEBUG': DEBUG,
    }


class TestThreadPoolConfig(ThreadPoolConfig):
    loader = dict_loader
