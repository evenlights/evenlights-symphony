# -*- coding: utf-8 -*-
import unittest

from symphony.osutils.bindings import SystemBinding


class TestBaseSystemBindingUnit(unittest.TestCase):
    """ Тест должен проверять функционал базы:
    * инициализацию
    * загрузку обязательных полей
    * загрузку оставшихся полей
    """

    def setUp(self):
        print('='*80)
        print(unittest.TestCase.id(self))
        print('-'*80)

        class Binding(SystemBinding):
            sys_name = 'test_binding'

        self.test_binding = Binding()

    def test_exit_code_0(self):
        """ В этом тесте выполняется существующая комманда с верными
        параметрами. Код выхода должен быть равен нулю, во внутренней шине
        сообщений не должно быть ничего.
        """

        code = self.test_binding._execute('ls', '-l')

        # проверяем код
        self.assertEqual(code, 0)

        # проверяем внутренюю шину
        self.assertEqual(len(self.test_binding.messages.internal), 1)

    # validation
    def test_exit_code_2(self):
        """ В тесте выполняется существующая комманда с неверными параметрами.
        Код выхода должен быть равен 2, во внутренней шине сообщений одно
        сообщение уровня error.
        """
        code = self.test_binding._execute('ls', 'l')

        # проверяем код
        self.assertEqual(code, 2)

        # проверяем внутренюю шину
        self.assertEqual(len(self.test_binding.messages.internal), 2)

        # проверяем уровень сообщения
        self.assertEqual(len(self.test_binding.messages.internal.errors), 1)

    def test_os_error(self):
        """ В тесте выполняется несуществующая комманда с неверными параметрами,
        что должно породить ошибку OSError. Во внутренней шине сообщений одно
        сообщение уровня error. Код выхода не возвращается.
        """

        code = self.test_binding._execute('ls1', 'l')

        # проверяем внутренюю шину
        self.assertEqual(len(self.test_binding.messages.internal), 2)

        # проверяем уровень сообщения
        self.assertEqual(len(self.test_binding.messages.internal.errors), 1)
