# -*- coding: utf-8 -*-
from symphony.core.datatypes import DotDict
from symphony.core.infra.generic import GenericComponent
from symphony.routers.components import Router, QueueRouter
from symphony.tests.infra import InfraTest


class DummyHandler(GenericComponent):
    def init(self, core=None, config=None, registry=None, mount_point=None, **kwargs):
        super(DummyHandler, self).init(
            core=core, config=config, registry=registry, mount_point=mount_point, **kwargs)
        mapping = kwargs.get('mapping')
        mapping[self.name] = self

    def prepare(self):
        pass

    def error_response(self, msg):
        self.messages.error(msg)


class ErrorHandler(DummyHandler):
    def handle(self):
        raise Exception


only_name_signal = DotDict({'name': 'name1'})
group_name_signal = DotDict({'name': 'group1__name1'})
error_signal = DotDict({'name': 'error'})


class TestRouter(InfraTest):

    def setUp(self):
        super(TestRouter, self).setUp()

        self.router = Router(
            name='router',
            shared=True,
            handlers=[
                DummyHandler(name='name1'),
                DummyHandler(name='group1__name1'),
                ErrorHandler(name='error')
            ],
        ).init(core=self.core, registry={}, mount_point=self)

    def tearDown(self):
        self.router.shutdown()
        super(TestRouter, self).tearDown()

    def test_init(self):
        # зарегистрирован как общий ресурс
        self.assertIn('router', self.core.shared.registry)
        self.assertEqual(self.core.shared.get('router'), self.router)

        # все обработчики в инфраструктуре
        for handler in self.router.handlers:
            self.assertIn(handler.name, self.router.infra.components)

        # мэппинг заполнен
        for handler in self.router.handlers:
            self.assertIn(handler.name, self.router.mapping)

    def test_get_handler_name(self):
        # только имя
        only_name = self.router.get_handler_id(only_name_signal)
        self.assertEqual(only_name, 'name1')

        # имя и группа
        group_name = self.router.get_handler_id(group_name_signal)
        self.assertEqual(group_name, 'group1__name1')

        self.core.mbus.flush()

        # объект без имени
        self.router.get_handler_id(object())
        self.assertErrorMessage('error getting id field name for the object, available are: ')

    def test_handle(self):
        self.router.handle(only_name_signal)
        self.assertIsNone(self.core.mbus.errors)

        self.router.handle(group_name_signal)
        self.assertIsNone(self.core.mbus.errors)

        self.router.handle(error_signal)
        self.assertErrorMessage('Traceback (most recent call last):')


class TestQueueRouter(InfraTest):

    def setUp(self):
        super(TestQueueRouter, self).setUp()

        self.router = QueueRouter(
            name='router',
            shared=True,
            handlers=[
                DummyHandler(name='name1'),
                DummyHandler(name='group1__name1'),
                ErrorHandler(name='error')
            ],
        ).init(core=self.core, registry={}, mount_point=self)

    def tearDown(self):
        self.router.shutdown()
        super(TestQueueRouter, self).tearDown()

    def test_queue_put(self):
        for _ in range(self.CYCLE_LENGTH):
            self.router.queue_put(self.randomChoice([only_name_signal, group_name_signal, error_signal]))
        self.sleep(0.1)
        self.assertEqual(self.router.queue_size(), 10)

    def test_queue_get(self):
        for _ in range(self.CYCLE_LENGTH):
            self.router.queue_put(self.randomChoice([only_name_signal, group_name_signal, error_signal]))

        for _ in range(self.CYCLE_LENGTH):
            self.router.queue_get()

        self.sleep(0.1)
        self.assertEqual(self.router.queue_size(), 0)

    def test_task_done(self):
        for _ in range(self.CYCLE_LENGTH):
            self.router.queue_put(self.randomChoice([only_name_signal, group_name_signal, error_signal]))

        for _ in range(self.CYCLE_LENGTH):
            self.router.queue_get()
            self.router.task_done()

        self.sleep(0.1)
        self.assertEqual(self.router.queue_size(), 0)

    def test_handle_next(self):
        for _ in range(self.CYCLE_LENGTH):
            self.router.queue_put(self.randomChoice([only_name_signal, group_name_signal, error_signal]))

        for _ in range(self.CYCLE_LENGTH):
            self.router.handle_next()

        self.sleep(0.1)
        self.assertEqual(self.router.queue_size(), 0)
