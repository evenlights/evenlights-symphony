# -*- coding: utf-8 -*-
from symphony.core.datatypes import DotDict
from symphony.routers.components import Router
from symphony.routers.handlers import BaseHandler
from symphony.tests.infra import InfraTest

info_signal = DotDict({'name': 'info'})
warning_signal = DotDict({'name': 'warning'})
error_signal = DotDict({'name': 'error'})


class TestBaseHandler(InfraTest):

    def setUp(self):
        super(TestBaseHandler, self).setUp()

        class InfoHandler(BaseHandler):
            name = 'info'
            aliases = ['super_test_ok']

            def handle(self):
                self.messages.info('handling!')

        class WarningHandler(BaseHandler):
            name = 'warning'

            def prepare(self):
                self.warning_response('this is a warning')

            def handle(self):
                self.messages.info('handling!')

        class ErrorHandler(BaseHandler):
            name = 'error'

            def prepare(self):
                self.error_response('error! not valid!')

            def handle(self):
                self.info_response('handling!')

        self.router = Router(
            name='router',
            shared=True,
            handlers=[InfoHandler, WarningHandler, ErrorHandler],
        ).init(core=self.core, mount_point=self)

    def tearDown(self):
        super(TestBaseHandler, self).tearDown()

    def test_init(self):
        # основное имя
        self.assertIn('info', self.router.mapping)

        # алиасы
        self.assertIn('super_test_ok', self.router.mapping)

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

    def test_shutdown(self):
        router = self.router
        self.router.shutdown()

        # все обработчики удалили себя из мэппинга
        self.assertFalse(len(router.mapping))

    def test_info_response(self):
        self.router.handle(info_signal)
        self.assertInfoMessage('handling!', self.core.mbus)
        self.assertFalse(self.core.mbus.warnings)
        self.assertFalse(self.core.mbus.errors)

    def test_warning_response(self):
        self.router.handle(warning_signal)
        self.assertWarningMessage('this is a warning', self.core.mbus)
        self.assertInfoMessage('handling!', self.core.mbus)
        self.assertFalse(self.core.mbus.errors)

    def test_error_response(self):
        self.router.handle(error_signal)
        self.assertErrorMessage('error! not valid!', self.core.mbus)
        self.assertFalse(self.core.mbus.info_messages)
