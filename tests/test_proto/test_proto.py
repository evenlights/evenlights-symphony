# -*- coding: utf-8 -*-
from symphony.apps.broker import Broker
from symphony.apps.client import CLIClient
from test_apps.test_common import ASignalHandler, BSignalHandler
from test_settings import (
    TestBrokerConfig, TestWorkerConfig, TestClientConfig,
    SOCKET_CONF_DIR
)

from symphony.apps.handlers.signals import ExitSignalHandler
from symphony.apps.transport.worker import Worker
from symphony.tests.apps.mock import MockNode
from symphony.tests.transport import ZMQTransportTest


class TestProtoApp(ZMQTransportTest):

    def setUp(self):
        super(TestProtoApp, self).setUp()

        # заготовка мастера
        class TemplateBroker(Broker):
            config = TestBrokerConfig

        self.TemplateMaster = TemplateBroker

        # заготовка воркера
        class TemplateWorker(Worker):
            config = TestWorkerConfig

            # ворке-приложение умеет обрабатывать сигналы A и B с поризвольным
            # набором позиционных и ключевых аргументов, а также обработчик
            # сигнала SHUTDOWN
            signal_handlers = [
                ASignalHandler, BSignalHandler,
                ]

        self.TemplateWorker = TemplateWorker

        class TemplateClient(CLIClient):
            """ Шаблон клиентского приложения
            """
            config = TestClientConfig
            signal_handlers = [ExitSignalHandler, ]
            remote_mapping = {
                'a': None,
            }
        self.TemplateClient = TemplateClient

        # logger
        self.messages.info('starting logger')
        self.logger = MockNode(
            name='mock-logger',
            context=self.context,
            socket_conf=SOCKET_CONF_DIR + '/logger_ci.json',
            debug=False,
        )
        self.logger.start()

        # transactions
        self.messages.info('starting transactions')
        self.transactions = MockNode(
            name='mock-transactions',
            context=self.context,
            socket_conf=SOCKET_CONF_DIR + '/transactions_ci.json',
            debug=False,
        )
        self.transactions.start()

    def tearDown(self):

        self.messages.info('stopping logger')
        self.logger.shutdown()
        while self.logger.is_alive():
            self.sleep()

        self.messages.info('stopping transactions')
        self.transactions.shutdown()
        while self.transactions.is_alive():
            self.sleep()

        super(TestProtoApp, self).tearDown()

    def N_test_broker_worker(self):
        """ Тест проверяет стыковку брокер-воркер """

        self.messages.info('initializing broker')
        broker = self.TemplateMaster(name='test-broker', context=self.context)

        # запускаем трэд мастера
        self.messages.info('starting broker')
        broker.start()

        self.messages.info('initializing worker')
        worker = self.TemplateWorker(name='test-worker', context=self.context)

        # запускаем трэд
        self.messages.info('starting worker')
        worker.start()

        self.messages.info('waiting for broker to come up')
        while not broker.is_running:
            pass

        self.messages.info('waiting for worker to sign in')
        while len(broker.worker_pool) != 1:
            pass

        self.sleep(5)

        # ждем завершения мастера
        self.messages.info('waiting for broker to stop')
        broker.shutdown()
        broker.join()

    def test_client_ci(self):
        """ Тест проверяет стыковку клиент-брокер-воркер """
        self.messages.info('initializing broker')
        broker = self.TemplateMaster(name='test-broker', context=self.context)

        # запускаем трэд мастера
        self.messages.info('starting broker')
        broker.start()

        self.messages.info('initializing worker')
        worker = self.TemplateWorker(name='test-worker', context=self.context)

        # запускаем трэд воркера
        self.messages.info('starting worker')
        worker.start()

        # ждем запуска мастера
        self.messages.info('waiting for broker to come up')
        while not broker.is_running:
            pass

        # ждем, когда подключится воркер
        self.messages.info('waiting for worker to sign in')
        while len(broker.worker_pool) != 1:
            pass

        self.messages.info('initializing client')
        client = self.TemplateClient(name='test-client', context=self.context)
        client._setup()
        client.is_running = True

        # отправляем запрос
        self.messages.info('sending a client request')
        client.ci_get_signal('a')
        # отправка сообщения
        client.ci_queue_handle()

        # получаем ответ
        self.messages.info('receiving response')
        while not client.transaction_pool.finished:
            client.socket_poll()
            client.ci_fe_handle()

        self.messages.info('broker shutdown')
        broker.shutdown()
        broker.join()

        # ждем завершения мастера
        self.messages.info('waiting for worker to stop')
        worker.join()

        client._clean()
