# -*- coding: utf-8 -*-
from unittest import skip

from symphony.core.generators import GenericKeyGenerator
from symphony.ci.containers import GenericSignal
from symphony.protocols.smp.adapters import SMPSocketAdapter
from symphony.apps.transport.broker.apps import LRUBrokerBase

from symphony.tests.apps.base import AppMixin
from symphony.tests.apps.mock import MockNode
from symphony.tests.utils import timeout
from symphony.tests.transport import ZMQTransportTest

from symphony.protocols.smp.defaults import (
    SMP_TYPE_LC_HEARTBEAT, SMP_TYPE_LC_SHUTDOWN,
    SMP_CODE_OK,
    SMP_CONTENT_SIGNAL, SMP_CONTENT_RAW,
)

from tests.test_apps.common import (
    ASignalHandler, BSignalHandler,
    signal_a, signal_b, signal_worker_task,
)
from tests.test_settings import TestBrokerConfig


class App(LRUBrokerBase):
    name = 'test-broker'
    config = TestBrokerConfig
    signal_handlers = [
        ASignalHandler, BSignalHandler
    ]


class TestLRUBrokerBase(AppMixin, ZMQTransportTest):
    """ Тест брокера проверяет:
    * обработку сигналов фронтэнда Интерфейса Управления (клиенты)
    * обработку сигналов бэкэнда Интерфейса Управления (воркеры)
    * обработка сигналов бэкэнда Жизненного Цикла (воркеры)
    """
    app_class = App
    sockets = {
        'client': {
            'type': 'DEALER',
            'connect': 'inproc://broker_ci_fe',
        }
    }

    def setUp(self):
        super(TestLRUBrokerBase, self).setUp()
        self.WORKER_COUNT = self.CYCLE_LENGTH
        # # logger
        # self.messages.info('starting logger')
        # self.logger = MockNode(
        #     context=self.context, name='mock-logger',
        #     socket_conf=SOCKET_CONF_DIR+'/logger_ci.json',
        #     debug=False,
        # )
        # self.logger.start()
        #
        # # transactions
        # self.messages.info('starting transactions')
        # self.transactions = MockNode(
        #     context=self.context, name='mock-transactions',
        #     socket_conf=SOCKET_CONF_DIR+'/transactions_ci.json',
        #     debug=False,
        # )
        # self.transactions.start()

    def tearDown(self):
        # self.messages.info('stopping logger')
        # self.logger.shutdown()
        # while self.logger.is_alive():
        #     self.sleep()
        #
        # self.messages.info('stopping transactions')
        # self.transactions.shutdown()
        # while self.transactions.is_alive():
        #     self.sleep()

        super(TestLRUBrokerBase, self).tearDown()

    # @skip('')
    def test_control_interface_fe(self):
        self.socketsConnect()
        self.appEnterRunning()
        mbus = self.app.core.mbus

        # ошибок при инициализации нет
        self.assertIsNone(mbus.errors)

        # посылаем серию случайных сигналов
        self.messages.info('sending random client signals')
        for _ in range(self.WORKER_COUNT):
            self.client.client_request(
                transaction_id=str(GenericKeyGenerator()),
                content_type=SMP_CONTENT_SIGNAL,
                content=self.randomChoice([
                    signal_a.repr.json, signal_b.repr.json
                ]),
            )

        self.messages.info('waiting for broker to receive requests')
        with timeout(seconds=1):
            while self.app.ci_router.queue_size() != self.WORKER_COUNT:
                self.app.socket_poll()
                self.app.ci_fe_unit.handle()

        # ошибок нет
        self.assertIsNone(mbus.errors)

        self.messages.info('waiting for broker to process requests')
        with timeout(seconds=1):
            while not self.app.ci_router.queue_empty():
                self.app.ci_queue_handle()

    # @skip('')
    def test_lifecycle_be(self):
        """ Тест проверяет соединение мастер-приложения и воркеров при помощи
        монитор-бэкэнда.
        """
        self.socketsConnect()
        self.appEnterRunning()
        mbus = self.app.core.mbus

        # ошибок при инициализации нет
        self.assertIsNone(mbus.errors)

        self.messages.info('initializing workers')
        worker_sockets = [
            [
                SMPSocketAdapter(name='worker-socket-%s' % _).init(
                    core=self.core,
                    config={
                        'type': 'DEALER',
                        'connect': 'inproc://broker_lc_be',
                        'option': {
                            'identity': 'test-worker-%s' % _,
                        },
                    },
                    registry=self.infra.components,
                    mount_point=self,
                    context=self.context,
                    poller=self.poller,
                    socket_registry=self.runtime['transport']['sockets']
                ),
                True
            ]
            for _ in range(self.WORKER_COUNT)
        ]

        # ошибок при инициализации нет
        self.assertIsNone(mbus.errors)

        # приветствие
        self.messages.info('connecting workers')
        for ws in worker_sockets:
            ws[0].connect()
            ws[0].lifecycle_init()

        # ждем, пока подключатся все воркеры
        self.messages.info('waiting for workers to sign in')
        with timeout(seconds=self.REPLY_TIMEOUT):
            while len(self.app.worker_pool) != self.WORKER_COUNT:
                self.app.socket_poll()
                self.app.lifecycle_be_unit.handle()

        # HEARTBEATS
        n_heartbeats = 5
        self.runtime['counters'] = [0 for _ in range(self.WORKER_COUNT)]
        self.messages.info('waiting for %d LC/HEARTBEAT signals' % n_heartbeats)

        with timeout(seconds=self.REPLY_TIMEOUT * n_heartbeats):
            while not all([c == n_heartbeats for c in self.runtime['counters']]):
                self.app.socket_poll()
                self.app.lifecycle_be_unit.handle()

                self.pollSockets()
                for index, ws in enumerate(worker_sockets):
                    if self.pollIn(ws[0]):
                        lc_msg = ws[0].lifecycle_recv()
                        self.assertEqual(lc_msg.message_type, SMP_TYPE_LC_HEARTBEAT)

                        # увеличиваем только до n_heartbeats
                        if self.runtime['counters'][index] < n_heartbeats:
                            self.runtime['counters'][index] += 1

                        ws[0].lifecycle_heartbeat()

                self.messages.debug(
                    'heartbeats received: %s' % self.runtime['counters']
                )

        # посылаем сигнал SHUTDOWN
        self.messages.info('broker shutdown')
        self.app.exit()

        # прощание
        self.messages.info(
            'waiting for workers get SHUTDOWN signal and sign out'
        )
        with timeout(seconds=self.REPLY_TIMEOUT):
            while len(self.app.worker_pool):
                self.app.socket_poll()
                self.app.lifecycle_be_unit.handle()

                self.pollSockets()

                for ws in worker_sockets:
                    if ws[1]:
                        ws[0].lifecycle_heartbeat()

                        # ждем сигнал завершения
                        if self.pollIn(ws[0]):
                            self.messages.info('%s sign out' % ws[0].name)
                            msg = ws[0].lifecycle_recv()
                            if msg.message_type == SMP_TYPE_LC_SHUTDOWN:
                                ws[0].lifecycle_shutdown()
                                ws[1] = False
                                # note: не вытыкаем сокеты, иначе сообщения
                                # могут быть не доставлены

                self.messages.debug('...')

    # @skip('')
    def test_control_interface_be(self):

        self.socketsConnect()
        self.appEnterRunning()
        mbus = self.app.core.mbus

        # ошибок при инициализации нет
        self.assertIsNone(mbus.errors)

        self.messages.info('initializing workers')
        worker_sockets = [
            [
                SMPSocketAdapter(name='worker-socket-ci-%s' % _).init(
                    core=self.core,
                    config={
                        'type': 'DEALER',
                        'connect': 'inproc://broker_ci_be',
                        'option': {
                            'identity': 'test-worker-%s' % _,
                        },
                    },
                    registry=self.infra.components,
                    mount_point=self,
                    context=self.context,
                    poller=self.poller,
                    socket_registry=self.runtime['transport']['sockets']
                ),
                SMPSocketAdapter(name='worker-socket-lc-%s' % _).init(
                    core=self.core,
                    config={
                        'type': 'DEALER',
                        'connect': 'inproc://broker_lc_be',
                        'option': {
                            'identity': 'test-worker-%s' % _,
                        },
                    },
                    registry=self.infra.components,
                    mount_point=self,
                    context=self.context,
                    poller=self.poller,
                    socket_registry=self.runtime['transport']['sockets']
                ),
                True
            ]
            for _ in range(self.WORKER_COUNT)
        ]

        # ошибок при инициализации нет
        self.assertIsNone(mbus.errors)

        # приветствие
        self.messages.info('connecting workers')
        for ws in worker_sockets:
            ws[0].connect()
            ws[1].connect()
            ws[1].lifecycle_init()

        # ждем, пока подключатся все воркеры
        self.messages.info('waiting for workers to sign in')
        with timeout(seconds=self.REPLY_TIMEOUT * 3):
            while len(self.app.worker_pool) != self.WORKER_COUNT:
                self.app.socket_poll()
                self.app.lifecycle_be_unit.handle()

        # шлем сигналы во фронтэн-ИУ брокера
        self.messages.info('sending signals to broker ci fe')
        for _ in range(self.CYCLE_LENGTH):

            # получаем адрес следующего LRU-воркера
            worker_sys_id = self.app.worker_pool[0].id
            self.messages.info('%s sending' % worker_sys_id)

            # отсылаем клиентом сигнал во фронтэнд, чтобы получить ответ от
            # ci_be_handle_socket()
            self.client.client_request(
                transaction_id=str(GenericKeyGenerator()),
                content_type=SMP_CONTENT_SIGNAL,
                content=signal_worker_task.repr.json,
            )

            # ответ воркера
            self.messages.info('worker getting signal')
            success = False
            with timeout(seconds=self.REPLY_TIMEOUT):
                while not success:
                    self.pollSockets()
                    self.app.socket_poll()
                    self.app.ci_fe_unit.handle()
                    self.app.ci_queue_handle()

                    for ws in worker_sockets:
                        if self.pollIn(ws[0]):
                            msg = ws[0].worker_recv()
                            signal = GenericSignal.from_json(msg.content)
                            ws[0].worker_final(
                                transaction_id=msg.transaction_id,
                                content_type=SMP_CONTENT_RAW,
                                code=SMP_CODE_OK,
                                client_id=msg.client_id,
                                content='executed command %s' % signal.name,
                            )
                            self.messages.info('worker sending reply')
                            success = True
                        ws[1].lifecycle_heartbeat()

                # ждем, пока воркер освободится
                self.messages.info('worker waiting to be released')
                while self.app.worker_pool[worker_sys_id].is_engaged:
                    self.app.socket_poll()
                    self.app.ci_be_unit.handle()

                self.messages.info('client getting reply')
                success = False
                while not success:
                    self.pollSockets()

                    if self.pollIn(self.client):
                        protocol_msg = self.client.client_recv()
                        self.assertEqual(
                            protocol_msg.content, 'executed command worker_task'
                        )
                        success = True

        # посылаем сигнал SHUTDOWN
        self.messages.info('broker shutdown')
        self.app.exit()

        # прощание
        self.messages.info(
            'waiting for workers get SHUTDOWN signal and sign out'
        )
        with timeout(seconds=self.REPLY_TIMEOUT * 3):
            while len(self.app.worker_pool):
                self.app.socket_poll()
                self.app.lifecycle_be_unit.handle()

                self.pollSockets()

                for ws in worker_sockets:
                    if ws[2]:
                        ws[1].lifecycle_heartbeat()

                        # ждем сигнал завершения
                        if self.pollIn(ws[1]):
                            msg = ws[1].lifecycle_recv()
                            if msg.message_type == SMP_TYPE_LC_SHUTDOWN:
                                ws[1].lifecycle_shutdown()
                                ws[2] = False
                                # note: не вытыкаем сокеты, иначе сообщения
                                # могут быть не доставлены
