# -*- coding: utf-8 -*-
import copy
from unittest import skip

from jsonschema.exceptions import best_match

from symphony.apps.transport.schemas import ZTCSchema, ZTCDraft4Validator
from symphony.tests.base import BaseTest


class TestZTCSchema(BaseTest):
    """ """

    def setUp(self):
        super(TestZTCSchema, self).setUp()

        self.config = {
            'version': 1.0,
            'apps': {
                'test-app': {
                    'context': {
                        'io_threads': 1,
                    },
                    'sockets': {
                        'ci_socket1': {
                            'type': 'DEALER',
                            'bind': 'inproc://ci_sock1',
                            'option': {
                                'identity': 'test-app',
                                'subscribe': ['110', '111'],
                            },
                        },
                    },
                    'poll_interval': 1000,
                }
            },
        }
        self.validator = ZTCDraft4Validator(ZTCSchema)

    def test_check_schema(self):
        ZTCDraft4Validator.check_schema(ZTCSchema)

    def test_validate_ok(self):
        # нормальный конфиг
        config = copy.deepcopy(self.config)
        error = best_match(self.validator.iter_errors(config))
        self.assertIsNone(error)

    def test_validate_root(self):
        # лишний ключ
        config = copy.deepcopy(self.config)
        config['aaa'] = 1
        error = best_match(self.validator.iter_errors(config))
        self.assertIsNotNone(error)
        self.assertEqual(
            "Additional properties are not allowed ('aaa' was unexpected)", error.message
        )

        # отсутствует обязательный ключ
        config = copy.deepcopy(self.config)
        del config['version']
        error = best_match(self.validator.iter_errors(config))
        self.assertIsNotNone(error)
        self.assertEqual(
            "'version' is a required property", error.message
        )

        # недопустимая версия
        config = copy.deepcopy(self.config)
        config['version'] = 2
        error = best_match(self.validator.iter_errors(config))
        self.assertIsNotNone(error)
        self.assertEqual(
            '2 is not one of [1.0]', error.message
        )

    def test_validate_context(self):
        # не целое число
        config = copy.deepcopy(self.config)
        config['apps']['test-app']['context']['max_sockets'] = 'a'
        error = best_match(self.validator.iter_errors(config))
        self.assertIsNotNone(error)
        self.assertEqual(
            "'a' is not of type 'integer'", error.message
        )

        # неизвестная опция
        config = copy.deepcopy(self.config)
        config['apps']['test-app']['context']['iio_threads'] = 1
        error = best_match(self.validator.iter_errors(config))
        self.assertIsNotNone(error)
        self.assertEqual(
            "Unexpected ZMQ context option: iio_threads", error.message
        )

    def test_validate_sockets(self):

        # некорректный тип
        config = copy.deepcopy(self.config)
        config['apps']['test-app']['sockets']['ci_socket1']['option']['identity'] = {}
        error = best_match(self.validator.iter_errors(config))
        self.assertIsNotNone(error)
        self.assertEqual(
            "{} is not of type 'string'", error.message
        )

        # неизвестная опция
        config = copy.deepcopy(self.config)
        config['apps']['test-app']['sockets']['ci_socket1']['option']['abc'] = 1
        error = best_match(self.validator.iter_errors(config))
        self.assertIsNotNone(error)
        self.assertEqual(
            "Unexpected socket option: abc", error.message
        )

        # число вместо строки
        config = copy.deepcopy(self.config)
        config['apps']['test-app']['sockets']['ci_socket1']['option']['identity'] = 12
        error = best_match(self.validator.iter_errors(config))
        self.assertIsNotNone(error)
        self.assertEqual(
            "Unexpected value for socket string option: identity=12", error.message
        )

        # строка вместо числа
        config = copy.deepcopy(self.config)
        config['apps']['test-app']['sockets']['ci_socket1']['option']['sndhwm'] = 'abc'
        error = best_match(self.validator.iter_errors(config))
        self.assertIsNotNone(error)
        self.assertEqual(
            "Unexpected value for socket int option: sndhwm=abc", error.message
        )
