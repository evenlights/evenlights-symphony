# -*- coding: utf-8 -*-
import datetime
from unittest import skip

from symphony.apps.transport.worker.apps import WorkerBase
from symphony.tests.apps.base import AppMixin
from symphony.tests.transport import ZMQTransportTest
from symphony.tests.utils import timeout

from symphony.protocols.smp.defaults import (
    SMP_TYPE_LC_INIT, SMP_TYPE_LC_HEARTBEAT,
    SMP_CONTENT_SIGNAL,
)

from tests.test_apps.common import (
    ASignalHandler, BSignalHandler, signal_a, signal_b
)
from tests.test_settings import TestWorkerConfig


class App(WorkerBase):
    name = 'test-worker'
    config = TestWorkerConfig
    signal_handlers = [ASignalHandler, BSignalHandler]


class TestWorkerBase(AppMixin, ZMQTransportTest):
    app_class = App
    sockets = {
        'broker_ci_be': {
            'type': 'ROUTER',
            'bind': 'inproc://broker_ci_be',
        },
        'broker_lc_be': {
            'type': 'ROUTER',
            'bind': 'inproc://broker_lc_be',
        },
        'logger_ci_fe': {
            'type': 'ROUTER',
            'bind': 'inproc://logger_ci_fe',
        },
    }

    # @skip('')
    def test_activate(self):
        self.socketsConnect()
        self.appInit()
        self.appActivate()

        # сообщение отправлено
        self.assertLifeCycleMessage(
            self.broker_lc_be, message_type=SMP_TYPE_LC_INIT
        )

    # @skip('')
    def test_ci_socket_handle_protocol_error(self):
        self.socketsConnect()
        self.appInit()
        self.appActivate()
        self.appSetRunning()

        self.broker_ci_be.write([self.app.id, 1, 2, 3])

        msg = "CI socket: no valid sub-protocol definition: " \
              "['1', '2', '3'], disconnecting"

        with timeout(seconds=self.REPLY_TIMEOUT):
            while not self.findMessage(
                    msg, self.app.ci.messages.critical_messages):
                self.app.socket_poll()
                self.app.ci.handle()

        self.assertCriticalMessage(msg)

    # @skip('')
    def test_ci_socket_handle_wrong_content_type(self):
        self.socketsConnect()
        self.appEnterRunning()

        self.broker_ci_be.worker_request(
            address=self.app.id, content_type=15, client_id='client_%d' % 1,
            content=self.randomChoice([signal_a.repr.json, signal_b.repr.json]),
        )
        msg = 'CI socket: content_type mismatch: expected CONTENT SIGNAL, ' \
              'actual 15, disconnecting'
        mbus = self.app.core.mbus

        with timeout(seconds=self.REPLY_TIMEOUT):
            while not self.findMessage(msg, mbus.critical_messages):
                self.app.socket_poll()
                self.app.ci.handle()

        self.assertCriticalMessage(msg)

    # @skip('')
    def test_ci_socket_handle_wrong_container(self):
        self.socketsConnect()
        self.appEnterRunning()

        self.broker_ci_be.worker_request(
            address=self.app.id, content_type=SMP_CONTENT_SIGNAL,
            client_id='client_%d' % 1, content='{"a": 1}',
        )

        msg = 'CI socket: invalid signal container received, disconnecting'

        with timeout(seconds=self.REPLY_TIMEOUT):
            while not self.findMessage(msg, self.app.core.mbus.critical_messages):
                self.app.socket_poll()
                self.app.ci.handle()

        self.assertCriticalMessage(msg)

    # @skip('')
    def test_ci_socket_handle_normal_flow(self):
        self.socketsConnect()
        self.appEnterRunning()

        # посылаем серию случайных сигналов
        self.messages.info('sending random signals from broker ci')
        for _ in range(self.CYCLE_LENGTH):
            self.broker_ci_be.worker_request(
                address=self.app.id,
                content_type=SMP_CONTENT_SIGNAL,
                client_id='client_%d' % _,
                content=self.randomChoice([
                    signal_a.repr.json, signal_b.repr.json
                ]),

            )

        # убеждаемся, что все получены
        self.messages.info('waiting for worker to receive signals')
        with timeout(seconds=self.REPLY_TIMEOUT*2):
            while not self.app.ci_router.queue_size() == self.CYCLE_LENGTH:
                self.app.socket_poll()
                self.app.ci.handle()

        self.assertEqual(self.CYCLE_LENGTH, self.app.ci_router.queue_size())

    # @skip('')
    def test_lifecycle_socket_handle_protocol_error(self):
        self.socketsConnect()
        self.appEnterRunning()

        self.broker_lc_be.write([self.app.id, 1, 2, 3])

        msg = "LifeCycle socket: no valid sub-protocol definition: " \
              "['1', '2', '3'], disconnecting"
        mbus = self.app.core.mbus

        with timeout(seconds=self.REPLY_TIMEOUT):
            while not self.findMessage(msg, mbus.critical_messages):
                self.app.socket_poll()
                self.app.lifecycle_unit.handle()

        self.assertCriticalMessage(msg)

    # @skip('')
    def test_lifecycle_socket_handle_heartbeat_in(self):
        self.socketsConnect()
        self.appEnterRunning()

        # посылаем серию сердцебиений, убеждаемся, что таймер сбрасывается
        self.messages.info('sending HEARTBEAT signals from broker lifecycle')
        for _ in range(self.CYCLE_LENGTH):
            self.broker_lc_be.lifecycle_heartbeat(address=self.app.id)
            time_sent = self.datetimeNow()

            self.app.socket_poll()
            self.app.lifecycle_unit.handle()
            # даем 1000 микросекунд
            self.assertAlmostEqual(
                time_sent, self.app.timers['heartbeat_broker'].last,
                delta=datetime.timedelta(microseconds=1000)
            )
            self.sleep()

    # @skip('')
    def test_lifecycle_socket_handle_reset(self):
        self.socketsConnect()
        self.appEnterRunning()

        self.messages.info('sending RESET signal from broker lifecycle')
        self.broker_lc_be.lifecycle_reset(address=self.app.id)

        msg = 'LifeCycle/RESET signal received'

        with timeout(seconds=self.REPLY_TIMEOUT):
            while not self.findMessage(msg, self.app.core.mbus.info_messages):
                self.app.socket_poll()
                self.app.lifecycle_unit.handle()

        self.assertInfoMessage(msg)

    # @skip('')
    def test_lifecycle_socket_handle_shutdown(self):
        self.socketsConnect()
        self.appEnterRunning()

        self.messages.info('sending SHUTDOWN signal from broker lifecycle')
        self.broker_lc_be.lifecycle_shutdown(address=self.app.id)

        msg = 'LifeCycle/SHUTDOWN signal received'

        with timeout(seconds=self.REPLY_TIMEOUT):
            while not self.findMessage(msg, self.app.core.mbus.info_messages):
                self.app.socket_poll()
                self.app.lifecycle_unit.handle()

        self.assertInfoMessage(msg)
        self.assertFalse(self.app.is_running)

    # @skip('')
    def test_lifecycle_socket_handle_heartbeat_out(self):
        self.socketsConnect()
        self.appEnterRunning()

        # забиаерм сообщение INIT
        self.assertLifeCycleMessage(
            self.broker_lc_be, message_type=SMP_TYPE_LC_INIT
        )

        self.messages.info('receiving HEARTBEAT signals on broker lifecycle')
        for _ in range(self.CYCLE_LENGTH):
            self.broker_lc_be.lifecycle_heartbeat(address=self.app.id)
            self.sleep()

            self.app.lifecycle_unit.handle()
            self.assertLifeCycleMessage(
                self.broker_lc_be, message_type=SMP_TYPE_LC_HEARTBEAT
            )

    # @skip('')
    def test_lifecycle_socket_handle_broker_heartbeat_in(self):
        self.socketsConnect()
        self.appEnterRunning()

        self.messages.info('broker stops sending HEARTBEAT signals')
        for _ in range(
            self.app.config.HEARTBEAT_INTERVAL_BROKER *
            self.app.config.HEARTBEAT_LIVENESS_BROKER +
            1
        ):
            self.sleep()
            self.app.socket_poll()
            self.app.lifecycle_unit.handle()

        self.assertWarningMessage(
            'LifeCycle/HEARTBEAT failure, cannot reach broker'
        )
        self.assertInfoMessage('ci_socket: reconnecting in 0.20 seconds')
        self.assertInfoMessage('lifecycle_socket: reconnecting in 0.20 seconds')

        self.messages.info('broker sending HEARTBEAT signal again')
        self.broker_lc_be.lifecycle_heartbeat(address=self.app.id)
        time_sent = self.datetimeNow()
        self.app.socket_poll()
        self.app.lifecycle_unit.handle()

        # note: предположительно, интервал существенно больше из-за реконнекта

        # воркер должен восстановить счетчик
        self.assertAlmostEqual(
            time_sent, self.app.timers['heartbeat_broker'].last,
            delta=datetime.timedelta(seconds=2)
        )

    # @skip('')
    def test_ci_queue_handle(self):
        self.socketsConnect()
        self.appEnterRunning()

        # посылаем серию случайных сигналов
        self.messages.info('sending random signals from broker ci')
        for _ in range(self.CYCLE_LENGTH):
            self.broker_ci_be.worker_request(
                address=self.app.id,
                content_type=SMP_CONTENT_SIGNAL,
                client_id='client_%d' % _,
                content=self.randomChoice([
                    signal_a.repr.json, signal_b.repr.json
                ]),
            )

        # убеждаемся, что все получены
        self.messages.info('waiting for worker to receive signals')
        signals_handled = 0
        mbus = self.app.core.mbus
        with timeout(seconds=self.REPLY_TIMEOUT*2):
            while signals_handled != self.CYCLE_LENGTH:
                self.app.core.mbus.flush()
                self.app.socket_poll()
                self.app.ci.handle()
                self.app.ci_queue_handle()

                if self.findMessage('handler call to: ', mbus.debug_messages):
                    signals_handled += 1

        # все сигналы получены
        self.assertEqual(self.CYCLE_LENGTH, signals_handled)
