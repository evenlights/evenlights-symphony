# -*- coding: utf-8 -*-
import datetime
from unittest import skip


from symphony.tests.apps.base import AppMixin
from symphony.apps.handlers.messages import (
    DefaultMessageHandler, NoneMessageHandler
)
from symphony.apps.handlers.signals import ExitSignalHandler
from symphony.apps.transport.client.apps import CLIClientBase
from symphony.protocols.smp.defaults import (
    SMP_TYPE_REQUEST,
    SMP_CONTENT_SIGNAL, SMP_CONTENT_NONE,
    SMP_CODE_ACCEPTED,
)
from symphony.tests.transport import ZMQTransportTest
from tests.test_apps.common import signal_remote_test
from tests.test_settings import TestClientConfig


class App(CLIClientBase):
    name = 'test-client'
    config = TestClientConfig
    signal_handlers = [ExitSignalHandler, ]
    content_handlers = [DefaultMessageHandler, NoneMessageHandler, ]
    remote_mapping = {
        'remote_test': None,
    }


class TestCLIClientSync(AppMixin, ZMQTransportTest):
    app_class = App
    sockets = {
        'broker_ci_fe': {
            'type': 'ROUTER',
            'bind': 'inproc://broker_ci_fe',
        },
    }

    def setUp(self):
        super(TestCLIClientSync, self).setUp()
        self.socketsConnect()
        self.appEnterRunning()

    # @skip('')
    def test_ci_get_signal(self):
        self.app.ci_get_signal('shutdown')

        # 1 команда в очереди ИУ
        self.assertEqual(self.app.ci_router.queue_size(), 1)

    # @skip('')
    def test_ci_queue_handle_local_mapping(self):
        self.app.ci_get_signal('shutdown')
        self.app.ci.handle_queue()

        self.assertDebugMessage(
            'test-client.ci_router.exit: control obtained', self.app.core.mbus
        )

    # @skip('')
    def test_ci_queue_handle_remote_mapping(self):
        self.app.ci_get_signal('remote_test')
        self.app.ci.handle_queue()

        # 1 сигнал в пуле операций
        self.assertEqual(len(self.app.transactions.pool), 1)

        transaction = self.app.transactions.pool[0]

        # представление сигналов совпадает
        self.assertEqual(signal_remote_test.repr.json, transaction.meta.cache)

        # id присвоен
        self.assertIsNotNone(transaction.id)

        # последняя активность "почти сейчас"
        self.assertAlmostEqual(
            datetime.datetime.now(),
            transaction.last_activity,
            delta=datetime.timedelta(milliseconds=2)
        )

        # таймаут операции выставлен верно
        self.assertEqual(
            datetime.timedelta(seconds=self.app.config.REPLY_TIMEOUT),
            transaction.ppp_timer.delta
        )

        # количество попыток выставлено верно
        self.assertEqual(self.app.config.REQUEST_RETRIES, transaction.liveness)

    # @skip('')
    def test_add_transaction_to_pool_and_send(self):
        self.app.ci_get_signal('remote_test')
        self.app.ci.handle_queue()

        transaction = self.app.transactions.pool[0]

        # клиент отправил сигнал
        self.pollSockets()
        self.assertTrue(self.pollIn(self.broker_ci_fe))

        # сигнал имеет верный вид
        self.assertClientMessage(
            self.broker_ci_fe,
            message_type=SMP_TYPE_REQUEST,
            transaction_id=transaction.id,
            content_type=SMP_CONTENT_SIGNAL,
        )

    # @skip('')
    def test_transaction_pool_timeout(self):
        self.app.ci_get_signal('remote_test')
        self.app.ci.handle_queue()

        # клиент отправил сигнал
        self.pollSockets()
        self.assertTrue(self.pollIn(self.broker_ci_fe))
        self.broker_ci_fe.client_recv()

        # даем таймауту истечь
        self.messages.info('letting the transaction timeouts expire')
        self.sleep()
        self.app.transactions.handle()

        # сообщение о таймауте присутствует
        self.assertDebugMessage('transaction(s) timed-out', self.app.core.mbus)

        transaction = self.app.transactions.pool[0]

        # количество попыток уменьшено
        self.assertEqual(
            transaction.liveness, self.app.config.REQUEST_RETRIES-1
        )

        # ждем повторного сообщения
        self.messages.info('waiting for message to be resent')
        while True:
            self.pollSockets()
            self.app.ci.handle_queue()
            self.app.transactions.handle()

            if self.pollIn(self.broker_ci_fe):
                self.messages.info('client has resent message')
                break

    # @skip('')
    def test_transaction_zero_liveness(self):
        self.app.ci_get_signal('remote_test')
        self.app.ci.handle_queue()

        # даем таймауту истечь
        self.messages.info('letting the retries count to be exceeded')
        while self.app.transactions.pool:
            self.app.transactions.handle()
            self.sleep()

        self.assertErrorMessage(
            'transaction failed to complete due to gateway timeout',
            self.app.core.mbus
        )

    # @skip('')
    def test_ci_fe_handle_partial(self):
        """ При ответе Client/PARTIAL должна быть зарегистрирована активность
        по операции и вызван сообтветствующий обработчик
        """
        self.app.ci_get_signal('remote_test')
        self.app.ci.handle_queue()

        # получаем удаленный сигнал
        client_msg = self.broker_ci_fe.client_recv()
        transaction = self.app.transactions.pool[0]

        # говорим клиенту, что сообщение получено
        self.messages.info('tell the client that message has been accepted')
        self.broker_ci_fe.client_partial(
            transaction_id=client_msg.transaction_id,
            content_type=SMP_CONTENT_NONE,
            code=SMP_CODE_ACCEPTED,
            address=client_msg.address
        )

        # получаем сообщение
        self.messages.info('waiting for message on client')

        while not self.app.transactions.pool:
            self.app.socket_poll()
            self.app.ci.handle()

        # последняя активность "почти сейчас"
        self.assertAlmostEqual(
            datetime.datetime.now(),
            transaction.last_activity,
            delta=datetime.timedelta(milliseconds=2)
        )

    # @skip('')
    def test_ci_fe_handle_final(self):
        """ При ответе Client/FINAL операция должна быть удалена из пула. """

        self.app.ci_get_signal('remote_test')
        self.app.ci.handle_queue()
        client_msg = self.broker_ci_fe.client_recv()

        self.messages.info('tell the client transaction is finished')
        self.broker_ci_fe.client_final(
            transaction_id=client_msg.transaction_id,
            content_type=SMP_CONTENT_NONE,
            code=SMP_CODE_ACCEPTED,
            address=client_msg.address
        )

        self.messages.info('waiting for final signal')
        while not self.app.transactions.pool.finished:
            self.app.socket_poll()
            self.app.ci.handle()
