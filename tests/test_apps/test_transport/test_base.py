# -*- coding: utf-8 -*-
from unittest import skip

import zmq

from symphony.core.infra.generic import GenericComponent
from symphony.apps.transport.base import ZMQTransportBase
from symphony.ci.handlers import SignalHandler
from symphony.configs.components import DictConfig
from symphony.tests.apps.base import AppTest


class Config(DictConfig):
    source = {
        'DEBUG': True,
        'ZMQ_TRANSPORT': {
            'version': 1,
            'apps': {
                'test-app': {
                    'context': {
                        'io_threads': 1,
                    },
                    'sockets': {
                        'ci_socket': {
                            'type': 'DEALER',
                            'connect': 'inproc://ci',
                            'option': {
                                'identity': 'app'
                            }
                        }
                    },
                    'poll_interval': 1000,
                }
            },

        }
    }


class App(ZMQTransportBase):
    class Infrastructure(object):
        components = [GenericComponent(name='ci')]

    config = Config
    signal_handlers = [SignalHandler(name='a'), SignalHandler(name='b')]

    def handle(self, *args, **kwargs):
        self.ci.messages.info('handling')
        self.socket_join('ci_socket')
        self.ci_socket.connect()
        self.exit()


class TestZMQTransportBase(AppTest):
    app_class = App

    def socketConnect(self):
        self.app.socket_join('ci_socket')
        self.app.ci_socket.connect()

    def test_init(self):
        self.appInit()

        # ошибок нет
        self.assertFalse(self.app.core.mbus.errors)

        # опции контекста установлены
        self.assertDebugMessage('applying context option: io_threads=1', bus=self.app.core.mbus)

    def test_transport_config(self):
        self.appInit()

        self.assertEqual(
            Config.source['ZMQ_TRANSPORT']['apps']['test-app'],
            self.app.transport_config
        )
        self.assertEqual(
            Config.source['ZMQ_TRANSPORT']['apps']['test-app']['context'],
            self.app.transport_config.context
        )
        self.assertEqual(
            Config.source['ZMQ_TRANSPORT']['apps']['test-app']['sockets'],
            self.app.transport_config.sockets
        )

    def test_socket_add(self):
        self.appInit()
        self.app.socket_join('ci_socket')
        self.app.ci_socket.activate()

        socket_conf = self.app.transport_config.sockets['ci_socket']

        # сокет зарегистрирован
        self.assertIn('ci_socket', self.app.runtime['transport']['sockets'])

        # параметры переданы верно
        self.assertEqual(getattr(zmq, socket_conf['type']), self.app.ci_socket.type)
        self.assertEqual('connect', self.app.ci_socket.method)
        self.assertEqual(socket_conf['connect'], self.app.ci_socket.target)
        self.assertEqual(b'app', self.app.ci_socket.socket.identity)

    def test_activate(self):
        self.appInit()
        self.app.socket_join('ci_socket')

        self.messages.info('activation phase in progress')
        self.app.activate()

        self.assertTrue(hasattr(self.app, 'ci_socket'))

    def test_deactivate(self):
        self.appInit()
        self.app.socket_join('ci_socket')
        self.appActivate()
        self.appDeactivate()

        self.assertDebugMessage('ci_socket: disconnect', bus=self.app.core.mbus)
        self.assertFalse(self.app.ci_socket.is_connected)
