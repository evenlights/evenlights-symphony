# -*- coding: utf-8 -*-
from symphony.core.infra.generic import GenericComponent
from symphony.apps.base import BaseApp
from symphony.ci.handlers import SignalHandler
from symphony.configs.components import DictConfig
from symphony.tests.base import BaseTest


class TestAppBase(BaseTest):
    """ """

    def setUp(self):
        super(TestAppBase, self).setUp()

        class Config(DictConfig):
            source = {
                'DEBUG': True,
            }
        self.Config = Config

        class App(BaseApp):
            class Infrastructure:
                components = [GenericComponent(name='ci')]

            config = Config
            signal_handlers = [SignalHandler(name='a'), SignalHandler(name='b')]

            def __init__(self, **kwargs):
                super(App, self).__init__(**kwargs)
                self.counter = 1

            def handle(self, *args, **kwargs):
                self.ci.messages.info('handling %d' % self.counter)
                self.counter -= 1

                if not self.counter:
                    self.exit()

                super(App, self).handle()

        self.app = App(name='test-app')

    def test_bootstrap(self):
        self.app.bootstrap()

        # конфиг
        self.assertIsInstance(self.app.config, self.Config)
        self.assertIn('config', self.app.infra.components)

        # ядро
        self.assertTrue(hasattr(self.app, 'core'))
        self.assertIn('core', self.app.infra.components)

        # сообщения
        self.assertTrue(hasattr(self.app, 'messages'))
        self.assertIn('messages', self.app.infra.components)
        self.assertEqual(2, len(self.app.messages.runtime['channels']))
        self.assertTrue(self.app.messages.channel_in(self.app.core.mbus))
        self.assertTrue(self.app.messages.channel_in(self.app.core.logger))

    def test_shutdown(self):

        self.app.run()

        self.assertIsNone(self.app.mappings)
        self.assertIsNone(self.app.timers)

        self.assertFalse(hasattr(self.app, 'core'))
        self.assertFalse(isinstance(self.app.config, self.Config))

        self.assertEqual({}, self.app.infra.registry['components'])
        self.assertEqual([], self.app.infra.registry['message_channels'])
