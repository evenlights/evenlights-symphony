# -*- coding: utf-8 -*-
from symphony.tests.base import BaseTest
from symphony.core.infra.generic import GenericComponent
from symphony.apps.core.components import AppCore


class TestAppCore(BaseTest):

    # enable trace
    LOG_LEVEL = 5

    def setUp(self):
        super(TestAppCore, self).setUp()

        class TestApp(object):

            def __str__(self):
                return 'test-app'

        self.app = TestApp()

    def tearDown(self):
        super(TestAppCore, self).tearDown()

    # todo: проверка регистрации общих ресурсов

    def test_init(self):
        core = AppCore()
        core.init(mount_point=self.app)

        # комплектация: логгер и сообщения примонтированы и зарегистрированы
        self.assertTrue(hasattr(core, 'logger'))
        self.assertIn(core.logger, list(core.infra.components.values()))

        self.assertTrue(hasattr(core, 'messages'))
        self.assertIn(core.messages, list(core.infra.components.values()))

        # сообщения: все внешние каналы подцеплены
        self.assertEqual(core.messages.runtime['channels'], core.infra.registry['message_channels'])

        # сообщения: количество сообщений в центральной шине и внетренней очереди совпадают
        self.assertEqual(len(core.messages.internal), len(core.messages.mbus))

        # сообщения: все внешние и внутренний канал доспутны
        self.assertTrue(hasattr(core.messages, 'logger'))
        self.assertTrue(hasattr(core.messages, 'mbus'))
        self.assertTrue(hasattr(core.messages, 'internal'))

    def test_flush(self):
        core = AppCore().init(mount_point=self.app)
        core.flush()

        # внутренние сообщения и центральная шина сброшены
        self.assertFalse(len(core.messages.internal))
        self.assertFalse(len(core.messages.mbus))

    def test_logger(self):
        core = AppCore()
        core.init(mount_point=self.app)

        unit = GenericComponent(name='test-unit')
        unit.init(core=core, mount_point=self.app)

        core.logger._logger.info('core.logger._logger')
        core.logger.info('core.logger')

        unit.messages.info('unit.messages')
        unit.messages.logger.info('unit.messages.logger')

    def test_messages(self):
        core = AppCore().init(mount_point=self.app)
        unit = GenericComponent(name='test-unit').init(core=core, mount_point=self.app)
        core.flush()
        unit.flush()

        msg = 'unit.messages.trace'
        unit.messages.trace(msg)
        self.assertTraceMessage(msg, unit.messages.internal)
        self.assertTraceMessage(msg, core.mbus)

        msg = 'unit.messages.debug'
        unit.messages.debug(msg)
        self.assertDebugMessage(msg, unit.messages.internal)
        self.assertDebugMessage(msg, core.mbus)

        msg = 'unit.messages.info'
        unit.messages.info(msg)
        self.assertInfoMessage(msg, unit.messages.internal)
        self.assertInfoMessage(msg, core.mbus)

        msg = 'unit.messages.warning'
        unit.messages.warning(msg)
        self.assertWarningMessage(msg, unit.messages.internal)
        self.assertWarningMessage(msg, core.mbus)

        msg = 'unit.messages.error'
        unit.messages.error(msg)
        self.assertErrorMessage(msg, unit.messages.internal)
        self.assertErrorMessage(msg, core.mbus)

        msg = 'unit.messages.critical'
        unit.messages.critical(msg)
        self.assertCriticalMessage(msg, unit.messages.internal)
        self.assertCriticalMessage(msg, core.mbus)
