# -*- coding: utf-8 -*-
import sys
from unittest import skip

from symphony.ci.handlers import SignalHandler
from symphony.configs.components import DictConfig
from symphony.apps.cli.apps import CLIApp
from symphony.apps.handlers.signals import HelpSignalHandler
from symphony.tests.apps.base import AppTest


class Handler(SignalHandler):
    short_description = \
        "{% colored None, ['bold'] %}Использование: convert <сигнатура входных файлов> " \
        "region=<имя региона> [from_date=2015082815, to_date=2015082816]" \
        "{% endcolored %}" \
        "\n" \
        "Конвертирует данные U- и V-ветра из формата ре-анализа NCEP в формат SWAN"

    full_description = \
        "{% colored 'cyan', ['bold'] %}Аргументы:{% endcolored %}" \
        "\n\n" \
        "* сигнатура входных файлов: сигнатура, при помощи которой будут составлены " \
        "имена входных файлов по образцу 'uwnd.<сигнатура>.nc'. При формировании" \
        "полных путей используются переменные настроек 'SOURCE_UWIND_DIR' и " \
        "'SOURCE_VWIND_DIR'." \
        "\n\n" \
        "{% colored 'cyan', ['bold'] %}Ключевые аргументы:{% endcolored %}" \
        "\n\n" \
        "* region: обязательный аргемнт, файл с указанием региона; поиск будет " \
        "произведен по пути, указанному в перемнной настроек 'REGIONS_DIR'." \
        "\n\n" \
        "* from_date: дата начала выборки в формате '2016-01-02 00.00.00' или " \
        "'2016010200', используется только вместе с аргументом 'to_date'." \
        "\n\n" \
        "* to_date: дата конца выборки в формате '2016-01-02 00.00.00' или " \
        "'2016010200', используется только вместе с аргументом 'from_date'."


class Config(DictConfig):
    source = {
        'DEBUG': True,
    }


class App(CLIApp):
    config = Config
    signal_handlers = [
        HelpSignalHandler, Handler(name='a'), Handler(name='a__b')
    ]


class TestCLIApp(AppTest):
    """ """
    app_class = App

    # @skip('')
    def test_init_plain(self):
        self.appEnterRunning()

        self.assertTrue(hasattr(self.app, 'transactions'))
        self.assertTrue(hasattr(self.app, 'ci_router'))
        self.assertTrue(hasattr(self.app, 'ci'))
        self.assertTrue(hasattr(self.app, 'signal_parser'))
        self.assertTrue(hasattr(self.app, 'console'))

        signal = self.app.ci.queue_get()
        self.assertEqual('help', signal.name)

    # @skip('')
    def test_init_args(self):
        sys.argv.extend(['a'])

        self.appEnterRunning()

        signal = self.app.ci.queue_get()
        self.assertEqual('a', signal.name)

    # @skip('')
    def test_handle_args(self):
        sys.argv.extend(['a'])

        self.appEnterRunning()

        self.app.handle()

        self.assertEqual(1, len(self.app.transactions.pool.finished))
        self.assertEqual(1, len(self.app.transactions.pool.successful))
        self.assertInfoMessage(
            'registering transaction', bus=self.app.core.mbus
        )
        self.assertInfoMessage('transaction complete', bus=self.app.core.mbus)

    # @skip('')
    def test_handle_help(self):
        sys.argv.extend(['help', 'a'])

        self.appEnterRunning()

        self.app.console.runtime['settings']['no_encoding'] = True
        self.app.handle()

        self.assertEqual(0, len(self.app.transactions.pool.finished))
        self.assertDebugMessage(
            'rendering: symphony_help_detail.html', bus=self.app.core.mbus
        )
