# -*- coding: utf-8 -*-
from symphony.messages.containers import GenericMessage
from symphony.ci.containers import GenericSignal
from symphony.ci.handlers import SignalHandler
from symphony.protocols.smp import defaults as smp_defaults


# SIGNALS
signal_a = GenericSignal(name='a')
signal_b = GenericSignal(name='b')
signal_shutdown = GenericSignal(name='shutdown')
signal_worker_task = GenericSignal(name='worker_task')
signal_remote_test = GenericSignal(name='remote_test')

# MESSAGES
message_info = GenericMessage(
    level='info',
    content='Everything is going fine!'
)


# HANDLERS
class ASignalHandler(SignalHandler):
    sys_name = 'a_signal_handler'
    name = 'a'

    def handle(self):
        self.info_response('a signal handle')


class BSignalHandler(SignalHandler):
    sys_name = 'b_signal_handler'
    name = 'b'

    def handle(self):
        self.info_response('b signal handle')
