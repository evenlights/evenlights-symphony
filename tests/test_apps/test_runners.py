# -*- coding: utf-8 -*-
import time
from threading import Thread
from symphony.apps.runners import ThreadPool
from symphony.tests.base import SymphonyBaseTestCase
from test_settings import TestThreadPoolConfig


class TestThreadPool(SymphonyBaseTestCase):
    """
    """
    CYCLE_LENGTH = 10

    def setUp(self):
        super(TestThreadPool, self).setUp()

        # воркер
        class TestWorker(Thread):
            def __init__(self, sys_id=None, exception_time=5, *args, **kwargs):
                super(TestWorker, self).__init__(*args, **kwargs)
                self.sys_id = sys_id
                self.is_running = False
                self.exception_time = exception_time

            def run(self):
                self.is_running = True
                while self.is_running:
                    time.sleep(self.exception_time)
                    raise Exception('Nasty exception in %s' % self.name)

            def shutdown(self):
                self.is_running = False

            def _clean(self):
                print(self.name, 'emergency stop handling')

        class Pool(ThreadPool):
            config = TestThreadPoolConfig
            threads = [
                {
                    'class': TestWorker,
                    'kwargs': {'sys_id': 'sys_id_1', 'name': 'worker-1', 'exception_time': 3},
                },

                {
                    'class': TestWorker,
                    'kwargs': {'sys_id': 'sys_id_2', 'name': 'worker-2', 'exception_time': 6},
                },
            ]

        self.pool = Pool()

    def test_init(self):
        self.pool.run()
