# -*- coding: utf-8 -*-
import os
import sys
import logging

from symphony.tests.infra import InfraTest

from symphony.core.generators import GenericKeyGenerator
from symphony.core.datatypes import DotDict
from symphony.messages.containers import GenericMessage
from symphony.protocols.smp import defaults as smp_defaults
from symphony.protocols.smp.containers import SMPClientMessage
from symphony.apps.core.components import AppCore
from symphony.apps.handlers.messages import (
    DefaultMessageHandler,
    NoneMessageHandler, StringMessageHandler, FileMessageHandler,
    MessageMessageHandler
)
from test_settings import TMP_DIR


class TestContentHandlers(InfraTest):
    """ """

    def setUp(self):
        super(TestContentHandlers, self).setUp()
        self.core.config.update({
            'TMP_DIR': TMP_DIR
        })

    # LEVEL 0 HANDLERS
    # NONE
    def test_content_none_validation(self):
        handler = NoneMessageHandler(core=self.core, mount_point=self)
        # без кода
        message = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=str(GenericKeyGenerator()),
            content_type=smp_defaults.SMP_CONTENT_NONE,
        )
        handler.validate(message)

        # ошибка присутствует
        self.assertIsNotNone(handler.messages.internal.errors)

        # сообщение верное
        self.assertRegex(
            handler.messages.internal.errors[0].content,
            'code frame empty'
        )

        # контент
        handler.messages.internal.flush()
        message.code = smp_defaults.SMP_CODE_OK
        message.content = 'content'
        handler.validate(message)

        # ошибка присутствует
        self.assertIsNotNone(handler.messages.internal.errors)

        # сообщение верное
        self.assertRegex(
            handler.messages.internal.errors[0].content,
            'unexpected content frame'
        )

    def test_content_none(self):
        handler = NoneMessageHandler(core=self.core, mount_point=self)
        message = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=str(GenericKeyGenerator()),
            content_type=smp_defaults.SMP_CONTENT_NONE,
            code=smp_defaults.SMP_CODE_OK,
        )

        # обрабатываем сигнал OK
        handler.handle(message)

        # ошибок нет, валидация прошла успешно, сообщение нужного уровня
        self.assertIsNone(handler.messages.internal.errors)
        self.assertIsNotNone(handler.messages.internal.info_messages)
        self.assertEqual(len(handler.messages.internal.info_messages), 1)

        # обрабатываем сигнал 300
        message.code = 301
        handler.handle(message)

        # ошибок нет, валидация прошла успешно, сообщение нужного уровня
        self.assertIsNone(handler.messages.internal.errors)
        self.assertIsNotNone(handler.messages.internal.warnings)
        self.assertEqual(len(handler.messages.internal.warnings), 1)

        # обрабатываем сигнал BAD_REQUEST
        message.code = smp_defaults.SMP_CODE_BAD_REQUEST
        handler.handle(message)

        # ошибок нет, валидация прошла успешно, сообщение нужного уровня
        self.assertIsNotNone(handler.messages.internal.errors)
        self.assertEqual(len(handler.messages.internal.errors), 1)

    # STRING
    def test_content_string_validation(self):
        handler = StringMessageHandler(core=self.core, mount_point=self)
        # вообщени без кода
        message = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=str(GenericKeyGenerator()),
            content_type=smp_defaults.SMP_CONTENT_NONE,
        )
        handler.validate(message)

        # ошибка присутствует
        self.assertIsNotNone(handler.messages.internal.errors)

        # сообщение верное
        self.assertRegex(
            handler.messages.internal.errors[0].content,
            'content frame empty'
        )

    def test_content_string(self):
        handler = StringMessageHandler(core=self.core, mount_point=self)
        message = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=str(GenericKeyGenerator()),
            content_type=smp_defaults.SMP_CONTENT_STRING,
            content='string'
        )

        # сообщение без кода
        handler.handle(message)

        # ошибок нет, валидация прошла успешно, сообщение нужного уровня
        self.assertIsNone(handler.messages.internal.errors)
        self.assertIsNotNone(handler.messages.internal.info_messages)
        self.assertEqual(len(handler.messages.internal.info_messages), 1)

        # код 200
        handler.messages.internal.flush()
        message.code = smp_defaults.SMP_CODE_OK
        handler.handle(message)

        # ошибок нет, валидация прошла успешно, сообщение нужного уровня
        self.assertIsNone(handler.messages.internal.errors)
        self.assertIsNotNone(handler.messages.internal.info_messages)
        self.assertEqual(len(handler.messages.internal.info_messages), 1)

        # код 300
        handler.messages.internal.flush()
        message.code = 301
        handler.handle(message)

        # ошибок нет, валидация прошла успешно, сообщение нужного уровня
        self.assertIsNone(handler.messages.internal.errors)
        self.assertIsNotNone(handler.messages.internal.warnings)
        self.assertEqual(len(handler.messages.internal.warnings), 1)

        # код 400
        handler.messages.internal.flush()
        message.code = smp_defaults.SMP_CODE_BAD_REQUEST
        handler.handle(message)

        # ошибок нет, валидация прошла успешно, сообщение нужного уровня
        self.assertIsNotNone(handler.messages.internal.errors)
        self.assertEqual(len(handler.messages.internal.errors), 1)

    # FILE
    def test_content_file_validation(self):
        handler = FileMessageHandler(core=self.core, mount_point=self)
        message = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=str(GenericKeyGenerator()),
            content_type=smp_defaults.SMP_CONTENT_NONE,
        )
        handler.validate(message)

        # ошибка присутствует
        self.assertIsNotNone(handler.messages.internal.errors)

        # сообщение верное
        self.assertRegex(
            handler.messages.internal.errors[0].content,
            'content frame empty'
        )

    def test_content_file(self):
        handler = FileMessageHandler(core=self.core, mount_point=self)
        message = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=str(GenericKeyGenerator()),
            content_type=smp_defaults.SMP_CONTENT_STRING,
            content='file'
        )

        handler.handle(message)
        path = self.core.config.TMP_DIR + '/%s' % message.transaction_id

        # фалй существует
        self.assertTrue(os.path.exists(path))

        # содержание записано верно
        with open(path) as infile:
            self.assertEqual(infile.read(), 'file')

        os.remove(path)

    # LEVEL 1 HANDLERS
    # MESSAGE
    def test_content_message_validation(self):
        handler = MessageMessageHandler(core=self.core, mount_point=self)

        # контент отсутствует
        message = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=str(GenericKeyGenerator()),
            content_type=smp_defaults.SMP_CONTENT_FILE,
        )
        handler.validate(message)

        # ошибка присутствует
        self.assertIsNotNone(handler.messages.internal.errors)

        # сообщение верное
        self.assertRegex(
            handler.messages.internal.errors[0].content,
            'content frame empty'
        )

        # невалидный контейнер
        handler.messages.internal.flush()
        message.content = '{"invalid": "container"}'
        handler.validate(message)

        # ошибка присутствует
        self.assertIsNotNone(handler.messages.internal.errors)

        # сообщение верное
        self.assertRegex(
            handler.messages.internal.errors[0].content,
            'invalid message container'
        )

    def test_content_message(self):
        handler = MessageMessageHandler(core=self.core, mount_point=self)

        # контент отсутствует
        message = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=str(GenericKeyGenerator()),
            content_type=smp_defaults.SMP_CONTENT_FILE,
            code=smp_defaults.SMP_CODE_OK
        )

        # info
        message.content = GenericMessage(level='info', content='info').repr.json
        handler.handle(message)

        # ошибок нет, валидация прошла успешно, сообщение нужного уровня
        self.assertIsNone(handler.messages.internal.errors)
        self.assertIsNotNone(handler.messages.internal.info_messages)
        self.assertEqual(len(handler.messages.internal.info_messages), 1)

        # warning
        handler.messages.internal.flush()
        message.content = GenericMessage(level='warning', content='warning').repr.json
        handler.handle(message)

        # ошибок нет, валидация прошла успешно, сообщение нужного уровня
        self.assertIsNone(handler.messages.internal.errors)
        self.assertIsNotNone(handler.messages.internal.warnings)
        self.assertEqual(len(handler.messages.internal.warnings), 1)

        # error
        message.content = GenericMessage(level='error', content='error').repr.json
        handler.handle(message)

        # ошибок нет, валидация прошла успешно, сообщение нужного уровня
        self.assertIsNotNone(handler.messages.internal.errors)
        self.assertEqual(len(handler.messages.internal.errors), 1)
