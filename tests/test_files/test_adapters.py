# -*- coding: utf-8 -*-
from unittest import skip
import os
from symphony.files.adapters import GenericFileAdapter
from symphony.tests.infra import InfraTestCase
from tests.test_settings import TMP_DIR, SAMPLES_DIR


class TestGenericFileAdapter(InfraTestCase):

    def setUp(self):
        super(TestGenericFileAdapter, self).setUp()
        self.TMP_FILE = '%s/tmp.txt' % TMP_DIR
        self.TMP_FILE_LOCK = '%s/tmp.txt.lock' % TMP_DIR
        self.TMP_COPY_FILE = '%s/tmp_copy.txt' % TMP_DIR
        self.SAMPLE_FILE = '%s/sample_file.txt' % SAMPLES_DIR

        self.adapter = self.infra.join(GenericFileAdapter)

    def tearDown(self):
        if os.path.isfile(self.TMP_FILE_LOCK):
            os.remove(self.TMP_FILE_LOCK)

        if os.path.isfile(self.TMP_FILE):
            os.remove(self.TMP_FILE)

        if os.path.isfile(self.TMP_COPY_FILE):
            os.remove(self.TMP_COPY_FILE)

        super(TestGenericFileAdapter, self).tearDown()

    def test_connect(self):
        self.adapter.connect(self.SAMPLE_FILE)

        # цель верная
        self.assertEqual(self.adapter.target, self.SAMPLE_FILE)

        # состояние верное
        self.assertTrue(self.adapter.is_connected)

        # сообщение
        self.assertDebugMessage('attaching to target: %s' % repr(self.SAMPLE_FILE),
                                self.adapter.messages.internal)

    def test_disconnect(self):
        self.adapter.connect(self.SAMPLE_FILE)
        self.adapter.disconnect()

        # состояние
        self.assertFalse(self.adapter.is_connected)

        # сообщение
        self.assertDebugMessage('detaching from target', self.adapter.messages.internal)

    def test_open(self):
        self.adapter.connect(self.SAMPLE_FILE)

        self.adapter.open()

        # сообщений об ошибках нет
        self.assertIsNone(self.adapter.messages.errors)

        # дискриптор открыт
        self.assertTrue(self.adapter.handler)

    def test_open_context_manager(self):
        self.adapter.connect(self.SAMPLE_FILE)

        with self.adapter.open(mode='r'):

            # сообщений об ошибках нет
            self.assertIsNone(self.adapter.messages.errors)

            # дискриптор открыт
            self.assertTrue(self.adapter.handler)

        # дискриптор закрыт
        self.assertFalse(self.adapter.handler)

    def test_create(self):
        self.adapter.create(self.TMP_FILE)

        # файл существует
        self.assertTrue(os.path.isfile(self.TMP_FILE))

        # адаптер привязан
        self.assertTrue(self.adapter.is_connected)

        # привязка верная
        self.assertEqual(self.adapter.target, self.TMP_FILE)

    def test_read(self):
        self.adapter.connect(self.SAMPLE_FILE)
        self.adapter.open()

        # содержание верное
        self.assertEqual(self.adapter.read(), '12345')

    def test_write(self):
        self.adapter.create(self.TMP_FILE)
        with self.adapter.open(mode='w') as outfile:
            outfile.write('123456789')

        with self.adapter.open(mode='r') as infile:
            # содержание верное
            self.assertEqual(infile.read(), '123456789')

    def test_delete(self):
        self.adapter.create(self.TMP_FILE)
        self.adapter.delete()

        # файл удален
        self.assertFalse(os.path.isfile(self.TMP_FILE))

        # хранилище отвязано
        self.assertFalse(self.adapter.is_connected)

    # @skip('')
    def test_copy(self):
        self.adapter.create(self.TMP_FILE)
        self.adapter.open()
        self.adapter.copy(self.TMP_COPY_FILE)

        # копия создана
        self.assertTrue(os.path.isfile(self.TMP_COPY_FILE))

        # адаптер привязан к исходному файлу
        self.assertEqual(self.adapter.target, self.TMP_FILE)

    # @skip('')
    def test_move(self):
        self.adapter.create(self.TMP_FILE)

        self.adapter.move(self.TMP_COPY_FILE)

        # файл существует
        self.assertTrue(os.path.isfile(self.TMP_COPY_FILE))

        # адаптер привязан к новому файлу
        self.assertEqual(self.adapter.target, self.TMP_COPY_FILE)
