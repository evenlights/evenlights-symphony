# -*- coding: utf-8 -*-
from symphony.tests.base import SymphonyBaseTestCase
from symphony.core.runtime import RuntimeStorage


class TestBaseRuntimeObject(SymphonyBaseTestCase):

    def setUp(self):
        super(TestBaseRuntimeObject, self).setUp()
        self.init_dict = {
            'string': 'string',
            'list': [1, 2, 3, 4, 5],
            'dict': {'a': 1},
        }
        self.runtime = RuntimeStorage(self.init_dict)
        self.runtime.reset()

    def test_init(self):
        self.assertIn('string', self.runtime)
        self.assertIn('list', self.runtime)
        self.assertTrue('dict', self.runtime)

        self.assertEqual('string', self.runtime['string'])
        self.assertEqual([1, 2, 3, 4, 5], self.runtime['list'])
        self.assertEqual({'a': 1}, self.runtime['dict'])

    def test_reset(self):
        self.runtime.string = 'new string'
        self.runtime.list = [1, 2, 3]
        self.runtime.dict = {'b': 2}

        self.runtime.flush()

        # исходные значения восстановлены
        self.assertEqual('string', self.runtime['string'])
        self.assertEqual([1, 2, 3, 4, 5], self.runtime['list'])
        self.assertEqual({'a': 1}, self.runtime['dict'])

    def test_update_defaults(self):
        new_dafaults = {
            'dict_new': {'c': 3}
        }
        self.runtime.update_defaults(new_dafaults)

        new_dafaults['dict_new']['c'] = '10'
        self.runtime.flush()

        # после изменения исходного словаря значение по умолчанию
        # должно остаться прежним
        self.assertEqual({'c': 3}, self.runtime['dict_new'])

