# -*- coding: utf-8 -*-
import random
from symphony.tests.base import SymphonyBaseTestCase
from symphony.core.resolvers import resolve_comparison, resolve_name


class TestComparisonResolver(SymphonyBaseTestCase):

    def setUp(self):
        super(TestComparisonResolver, self).setUp()
        self._value = random.randint(1, 1000)

    def test_gt(self):
        self.assertFalse(
            resolve_comparison('value__gt', self._value)(self._value)
        )
        self.assertFalse(
            resolve_comparison('value__gt', self._value)(self._value - 1)
        )
        self.assertTrue(
            resolve_comparison('value__gt', self._value)(self._value + 1)
        )

    def test_gte(self):
        self.assertTrue(
            resolve_comparison('value__gte', self._value)(self._value)
        )
        self.assertFalse(
            resolve_comparison('value__gte', self._value)(self._value - 1)
        )
        self.assertTrue(
            resolve_comparison('value__gte', self._value)(self._value + 1)
        )

    def test_lt(self):
        self.assertFalse(
            resolve_comparison('value__lt', self._value)(self._value)
        )
        self.assertTrue(
            resolve_comparison('value__lt', self._value)(self._value - 1)
        )
        self.assertFalse(
            resolve_comparison('value__lt', self._value)(self._value + 1)
        )

    def test_lte(self):
        self.assertTrue(
            resolve_comparison('value__lte', self._value)(self._value)
        )
        self.assertTrue(
            resolve_comparison('value__lte', self._value)(self._value - 1)
        )
        self.assertFalse(
            resolve_comparison('value__lte', self._value)(self._value + 1)
        )


class TestNameResolver(SymphonyBaseTestCase):

    def test_cmp(self):
        self.assertEqual(resolve_name('value__gt'), 'value')
        self.assertEqual(resolve_name('value__gte'), 'value')
        self.assertEqual(resolve_name('value__lt'), 'value')
        self.assertEqual(resolve_name('value__lte'), 'value')

    def test_minus(self):
        self.assertEqual(resolve_name('-value'), 'value')

    def test_dot_getter(self):
        self.assertEqual(resolve_name('value1__value2'), 'value1.value2')
