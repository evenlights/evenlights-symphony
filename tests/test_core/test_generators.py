# -*- coding: utf-8 -*-
from unittest import skip

from symphony.tests.base import SymphonyBaseTestCase
from symphony.core.generators import (
    GenericKeyGenerator, GenericTokenGenerator
)


class TestGenerators(SymphonyBaseTestCase):

    def setUp(self):
        super(TestGenerators, self).setUp()

    # @skip('')
    def test_data_from_dict(self):
        key = str(GenericKeyGenerator())
        self.assertEqual(len(key), 32)

    # @skip('')
    def test_token_from_args(self):
        # equal args
        token1 = GenericTokenGenerator('arg1', 12, None, 165)
        token2 = GenericTokenGenerator('arg1', 12, None, 165)
        self.assertEqual(str(token1), str(token2))

        # different order
        token1 = GenericTokenGenerator('arg1', 12, None, 165)
        token2 = GenericTokenGenerator(165, 'arg1', 12, None)
        self.assertNotEqual(str(token1), str(token2))

        # different agr
        token1 = GenericTokenGenerator('arg1', 12, None, 165)
        token2 = GenericTokenGenerator('arg2', 12, None, 165)
        self.assertNotEqual(str(token1), str(token2))

    # @skip('')
    def test_token_from_kwargs(self):
        # equal kwargs
        token1 = GenericTokenGenerator(
            param1='arg1', param2=12, param3=None, param4=165
        )
        token2 = GenericTokenGenerator(
            param1='arg1', param2=12, param3=None, param4=165
        )
        self.assertEqual(str(token1), str(token2))

        # different order
        token1 = GenericTokenGenerator(
            param1='arg1', param2=12, param3=None, param4=165
        )
        token2 = GenericTokenGenerator(
            param4=165, param1='arg1', param2=12, param3=None
        )
        self.assertEqual(str(token1), str(token2))

        # different kwarg
        token1 = GenericTokenGenerator(
            param1='arg1', param2=12, param3=None, param4=165
        )
        token2 = GenericTokenGenerator(
            param1_1='arg1', param2=12, param3=None, param4=165
        )
        self.assertNotEqual(str(token1), str(token2))

    # @skip('')
    def test_token_from_args_and_kwargs(self):
        token1 = GenericTokenGenerator(
            'arg1', 12, None, 165,
            param1='arg1', param2=12, param3=None, param4=165
        )
        token2 = GenericTokenGenerator(
            'arg1', 12, None, 165,
            param1='arg1', param2=12, param3=None, param4=165
        )
        self.assertEqual(str(token1), str(token2))
