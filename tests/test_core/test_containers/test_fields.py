# -*- coding: utf-8 -*-
import datetime

from symphony.tests.base import SymphonyBaseTestCase
from symphony.core.containers.fields import (
    GenericDateTimeField, SimpleDateTimeField,
    IntegerField, FloatField, BooleanField,
    StringField, JSONStringField
)


class TestGenericDateTimeField(SymphonyBaseTestCase):

    def setUp(self):
        super(TestGenericDateTimeField, self).setUp()
        self.field_class = GenericDateTimeField
        self.valid_transport_repr = '2015-10-30 13:22:39.469000'
        self.invalid_transport_repr = '2015-10-30 13:22:39.abc'

    def test_class_validate_format(self):

        # валидный формат
        self.assertTrue(self.field_class.validate_format(self.valid_transport_repr))

        # невалидный формат
        self.assertFalse(self.field_class.validate_format(self.invalid_transport_repr))

    def test_class_from_transport_repr(self):

        # верный тип
        self.assertIsInstance(
            self.field_class.from_transport(self.valid_transport_repr),
            datetime.datetime
        )

    def test_class_to_transport_repr(self):
        restored_value = self.field_class.from_transport(self.valid_transport_repr)
        transport_repr = self.field_class.to_transport(restored_value)

        # верный тип
        self.assertIsInstance(transport_repr, str)

        # значения совпадают
        self.assertEqual(self.valid_transport_repr, transport_repr)


class TestSimpleDateTimeField(SymphonyBaseTestCase):

    def setUp(self):
        super(TestSimpleDateTimeField, self).setUp()
        self.field_class = SimpleDateTimeField
        self.valid_transport_repr = '2015-10-30 13:22:39'
        self.invalid_transport_repr = '2015-10-30 13:22:sdfg'

    def test_class_validate_format(self):

        # валидный формат
        self.assertTrue(self.field_class.validate_format(self.valid_transport_repr))

        # невалидный формат
        self.assertFalse(self.field_class.validate_format(self.invalid_transport_repr))

    def test_class_from_transport_repr(self):
        # верный тип
        self.assertIsInstance(
            self.field_class.from_transport(self.valid_transport_repr),
            datetime.datetime
        )

    def test_class_to_transport_repr(self):
        restored_value = self.field_class.from_transport(self.valid_transport_repr)
        transport_repr = self.field_class.to_transport(restored_value)

        # верный тип
        self.assertIsInstance(transport_repr, str)

        # значения совпадают
        self.assertEqual(self.valid_transport_repr, transport_repr)


class TestIntegerField(SymphonyBaseTestCase):

    def setUp(self):
        super(TestIntegerField, self).setUp()
        self.field_class = IntegerField
        self.valid_transport_repr = 12
        self.invalid_transport_repr = 'abc'

    def test_class_validate_datatype(self):

        # валидный формат
        self.assertTrue(self.field_class.validate_datatype(self.valid_transport_repr))

        # строка также восстанавливается
        self.assertTrue(self.field_class.validate_datatype('12'))

        # невалидный формат
        self.assertFalse(self.field_class.validate_datatype(self.invalid_transport_repr))

    def test_class_from_transport_repr(self):

        # верный тип
        self.assertIsInstance(
            self.field_class.from_transport(self.valid_transport_repr),
            int
        )

    def test_class_to_transport_repr(self):
        restored_value = self.field_class.from_transport(self.valid_transport_repr)
        transport_repr = self.field_class.to_transport(restored_value)

        # верный тип
        self.assertIsInstance(transport_repr, int)

        # значения совпадают
        self.assertEqual(self.valid_transport_repr, transport_repr)


class TestFloatField(SymphonyBaseTestCase):

    def setUp(self):
        super(TestFloatField, self).setUp()
        self.field_class = FloatField
        self.valid_transport_repr = 12.127556
        self.invalid_transport_repr = 'abc'

    def test_class_validate_datatype(self):

        # валидный формат
        self.assertTrue(self.field_class.validate_datatype(self.valid_transport_repr))

        # строка также восстанавливается
        self.assertTrue(self.field_class.validate_datatype('12'))

        # невалидный формат
        self.assertFalse(self.field_class.validate_datatype(self.invalid_transport_repr))

    def test_class_from_transport_repr(self):

        # верный тип
        self.assertIsInstance(
            self.field_class.from_transport(self.valid_transport_repr),
            float
        )

    def test_class_to_transport_repr(self):
        restored_value = self.field_class.from_transport(self.valid_transport_repr)
        transport_repr = self.field_class.to_transport(restored_value)

        # верный тип
        self.assertIsInstance(transport_repr, float)

        # значения совпадают
        self.assertEqual(self.valid_transport_repr, transport_repr)


class TestBooleanField(SymphonyBaseTestCase):

    def setUp(self):
        super(TestBooleanField, self).setUp()
        self.field_class = BooleanField
        self.valid_transport_repr = True
        self.invalid_transport_repr = 'abc'

    def test_class_validate_range(self):

        # валидный формат
        self.assertTrue(self.field_class.validate_range(self.valid_transport_repr))

        # невалидный формат
        self.assertFalse(self.field_class.validate_range(self.invalid_transport_repr))

    def test_class_from_transport_repr(self):

        # верный тип
        self.assertIsInstance(
            self.field_class.from_transport(self.valid_transport_repr),
            bool
        )

    def test_class_to_transport_repr(self):
        restored_value = self.field_class.from_transport(self.valid_transport_repr)
        transport_repr = self.field_class.to_transport(restored_value)

        # верный тип
        self.assertIsInstance(transport_repr, bool)

        # значения совпадают
        self.assertEqual(self.valid_transport_repr, transport_repr)


class TestStringField(SymphonyBaseTestCase):

    def setUp(self):
        super(TestStringField, self).setUp()
        self.field_class = StringField
        self.valid_transport_repr = 'Превед!'
        self.invalid_transport_repr = [1, 2, 3, 4]

    def test_class_validate_datatype(self):

        # валидный тип данных
        self.assertTrue(self.field_class.validate_datatype(self.valid_transport_repr))

        # невалидный тип данных
        self.assertFalse(self.field_class.validate_datatype(self.invalid_transport_repr))

    def test_class_from_transport_repr(self):

        # верный тип
        self.assertIsInstance(
            self.field_class.from_transport(self.valid_transport_repr),
            str
        )

    def test_class_to_transport_repr(self):
        restored_value = self.field_class.from_transport(self.valid_transport_repr)
        transport_repr = self.field_class.to_transport(restored_value)

        # верный тип
        self.assertIsInstance(transport_repr, str)

        # значения совпадают
        self.assertEqual(self.valid_transport_repr, transport_repr)


class TestJSONStringField(SymphonyBaseTestCase):

    def setUp(self):
        super(TestJSONStringField, self).setUp()
        self.field_class = JSONStringField
        self.valid_transport_repr = '{"a": 1}'
        self.invalid_transport_repr = '{a: 1}'

    def test_class_validate_datatype(self):

        # валидный тип данных
        self.assertTrue(self.field_class.validate_datatype(self.valid_transport_repr))

        # невалидный тип данных
        self.assertFalse(self.field_class.validate_datatype(self.invalid_transport_repr))

    def test_class_from_transport_repr(self):

        # восстановлен верный тип
        self.assertIsInstance(
            self.field_class.from_transport(self.valid_transport_repr),
            dict
        )

    def test_class_to_transport_repr(self):
        restored_value = self.field_class.from_transport(self.valid_transport_repr)
        transport_repr = self.field_class.to_transport(restored_value)

        # значение представлено в верном типе (JSON-строка)
        self.assertIsInstance(transport_repr, str)

        # значения совпадают
        self.assertEqual(self.valid_transport_repr, transport_repr)
