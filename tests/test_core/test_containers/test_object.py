# -*- coding: utf-8 -*-
import simplejson
from symphony.tests.base import SymphonyBaseTestCase
from symphony.core.containers.object import ObjectContainer
from symphony.core.exceptions import ImproperlyConfigured


class TestObjectContainer(SymphonyBaseTestCase):

    def setUp(self):
        super(TestObjectContainer, self).setUp()

        class NestedConteiner(ObjectContainer):

            class DataModel:
                structure = {
                    'dict_field': None,
                    'list_field': None,
                    'str_field': None,
                    'unicode_field': None,
                }

        nested_container = NestedConteiner(
            dict_field={'key': 'value', },
            list_field=[1, 2, 3, 4, ],
            str_field='string',
            unicode_field='nested привет'
        )

        class Container(ObjectContainer):

            class DataModel:
                root_key = 'test'
                structure = {
                    'dict_field': dict,
                    'list_field': list,
                    'str_field': str,
                    'unicode_field': str,
                    'nested_object_container': NestedConteiner
                }

        self.container = Container(
            dict_field={'key': 'value', },
            list_field=[1, 2, 3, 4, ],
            str_field='string',
            unicode_field='привет',
            nested_object_container=nested_container
        )

    # Init
    def test_init(self):
        d = self.container.repr.transport['test']
        self.assertIn('dict_field', d)
        self.assertIn('list_field', d)
        self.assertIn('str_field', d)
        self.assertIn('unicode_field', d)
        self.assertIn('nested_object_container', d)

        self.assertIsInstance(d['dict_field'], dict)
        self.assertIsInstance(d['list_field'], list)
        self.assertIsInstance(d['str_field'], str)
        self.assertIsInstance(d['unicode_field'], str)

    # Repr
    def test_transport_repr(self):
        self.assertIsInstance(self.container.repr.transport, dict)
        self.messages.info(self.container.repr.transport)

    def test_json_repr(self):
        self.assertIsInstance(self.container.repr.json, str)
        self.messages.info(self.container.repr.json)

    def test__repr__(self):
        self.messages.info(self.container.__repr__())

    def test__str__(self):
        self.messages.info(str(self.container))
        self.messages.info(type(str(self.container)))
        self.assertIsInstance(str(self.container), str)

    def test__unicode__(self):
        self.messages.info(str(self.container))
        self.assertIsInstance(str(self.container), str)

    # Constructor
    def test_from_json(self):
        restored = self.container.from_json(self.container.repr.json)
        self.assertEqual(str(self.container), str(restored))

    def test_update(self):
        restored = self.container.from_json(self.container.repr.json)
        restored.dict_field = {'key_new': 'value_new'}
        restored.str_field = 'string new'
        self.container.update(restored)

        # make sure object is updated
        self.assertEqual(self.container.dict_field, {'key_new': 'value_new'})
        self.assertEqual(self.container.str_field, 'string new')

        # make sure the other object doesn't influence the first one
        restored.dict_field = {'key_new1': 'value_new1'}
        restored.str_field = 'string new1'
        self.assertEqual(self.container.dict_field, {'key_new': 'value_new'})
        self.assertEqual(self.container.str_field, 'string new')

    # Validation
    def test_transport_validation_no_class(self):
            class TestObjectContainer(ObjectContainer):
                pass
            self.assertRaises(ImproperlyConfigured, TestObjectContainer)

    def test_transport_validation_no_strucure(self):
            class TestObejctContainer(ObjectContainer):
                class DataModel:
                    pass
            self.assertRaises(ImproperlyConfigured, TestObejctContainer)

    def test_source_validation_no_class(self):
            class TestObjectContainer(ObjectContainer):
                pass
            json = self.container.repr.json
            self.assertRaises(ImproperlyConfigured, TestObjectContainer.from_json, json)

    def test_source_validation_no_strucure(self):
            class TestObjectContainer(ObjectContainer):
                class DataModel:
                    pass
            json = self.container.repr.json
            self.assertRaises(ImproperlyConfigured, TestObjectContainer.from_json, json)

    def test_source_validation_no_root_key(self):
            class TestObjectContainer(ObjectContainer):
                class DataModel:
                    root_key = 'test'
                    structure = {
                        'dict_field': dict,
                        'list_field': list,
                        'str_field': str,
                        'unicode_field': str,
                        'nested_object_container': None
                    }
            # remove root key
            d = self.container.repr.transport['test']
            json = simplejson.dumps(d)
            self.assertRaises(KeyError, TestObjectContainer.from_json, json)

    def test_source_validation_redundant_root_key(self):
            class TestObjectContainer(ObjectContainer):
                class DataModel:
                    structure = {
                        'dict_field': dict,
                        'list_field': list,
                        'str_field': str,
                        'unicode_field': str,
                        'nested_dict_container': None
                    }
            # with root key
            json = self.container.repr.json
            self.assertRaises(KeyError, TestObjectContainer.from_json, json)

    def test_source_validation_missing_field(self):
            class TestObjectContainer(ObjectContainer):
                class DataModel:
                    structure = {
                        'dict_field': dict,
                        'list_field': list,
                        'str_field': str,
                        'unicode_field': str,
                        'nested_object_container': None,
                        'missing_field': None
                    }
            json = self.container.repr.json
            self.assertRaises(KeyError, TestObjectContainer.from_json, json)

    def test_source_validation_redundant_field(self):
            class TestObjectContainer(ObjectContainer):
                class DataModel:
                    structure = {
                        'dict_field': dict,
                        'list_field': list,
                        'str_field': str,
                        'unicode_field': str,
                    }
            json = self.container.repr.json
            self.assertRaises(KeyError, TestObjectContainer.from_json, json)
