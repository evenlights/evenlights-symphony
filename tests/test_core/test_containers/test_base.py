# -*- coding: utf-8 -*-
from symphony.tests.base import BaseTest
from symphony.core.containers.base import BaseContainer


class TestGenericDateTimeField(BaseTest):

    def setUp(self):
        super(TestGenericDateTimeField, self).setUp()
        self.field_class = BaseContainer
        self.valid_transport_repr = '2015-10-30 13:22:39.469000'
        self.invalid_transport_repr = '2015-10-30 13:22:39.abc'

    def test_init(self):
        print(self.field_class.from_transport)
