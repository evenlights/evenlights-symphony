# -*- coding: utf-8 -*-
import simplejson
from symphony.tests.base import SymphonyBaseTestCase
from symphony.core.containers.object import ObjectContainer
from symphony.core.containers.list import ListContainer


class TestListContainer(SymphonyBaseTestCase):

    def setUp(self):
        super(TestListContainer, self).setUp()

        class ElementClass(ObjectContainer):
            field_a = None
            field_b = None

            class DataModel:
                structure = {
                    'field_a': None,
                    'field_b': None,
                }

        class NoDMContainer(ListContainer):
            pass

        class RKContainer(ListContainer):

            class DataModel:
                root_key = 'test_list'

        class RKMCContainer(ListContainer):

            class DataModel:
                root_key = 'test_list'
                member_class = ElementClass

        self.NoDMContainer = NoDMContainer
        self.RKContainer = RKContainer
        self.RKMCContainer = RKMCContainer
        self.container = RKMCContainer(
            [ElementClass(field_a='a%d' % nr, field_b='b%d' % nr) for nr in range(1, 5)])

    def tearDown(self):
        super(TestListContainer, self).tearDown()

    # Init
    def test_init_empty(self):
        container = self.NoDMContainer()
        self.assertIsInstance(container.repr.transport, list)
        self.assertEqual(len(container.repr.transport), 0)

        container = self.RKContainer()
        self.assertIsInstance(container.repr.transport, dict)
        self.assertIn('test_list', container.repr.transport)

    def test_init_source(self):
        self.assertIn('test_list', self.container.repr.transport)
        self.assertEqual(len(self.container.repr.transport['test_list']), 4)
        self.assertEqual(len(self.container), 4)

    # Repr
    def test_json_repr(self):
        self.assertIsInstance(self.container.repr.json, str)
        self.messages.info(self.container.repr.json)

    def test__repr__(self):
        self.messages.info(self.container.__repr__())

    def test__str__(self):
        self.messages.info(str(self.container))
        self.messages.info(type(str(self.container)))
        self.assertIsInstance(str(self.container), str)

    def test__unicode__(self):
        self.messages.info(str(self.container))
        self.assertIsInstance(str(self.container), str)

    # Restoration
    def test_from_json(self):
        restored = self.container.from_json(self.container.repr.json)
        self.assertEqual(str(self.container), str(restored))

    # Validation
    def test_source_validation_no_root_key(self):
        json = simplejson.dumps(self.container.repr.transport['test_list'])
        self.assertRaises(TypeError, self.RKContainer.from_json, json)

    def test_source_validation_redundant_root_key(self):
        json = self.container.repr.json
        self.assertRaises(TypeError, self.NoDMContainer.from_json, json)
