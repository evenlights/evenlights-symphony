# -*- coding: utf-8 -*-
import copy

from symphony.core.infra.base import InfraComponent
from symphony.tests.base import BaseTest


class TestInfraComponent(BaseTest):

    def setUp(self):
        super(TestInfraComponent, self).setUp()

        class Component(InfraComponent):

            class Infrastructure:
                components = [
                    InfraComponent(name='mod%d' % self.randomInt(1, 100)),
                    InfraComponent(name='mod%d' % self.randomInt(1, 100))
                ]

            def init(self, core=None, config=None, registry=None, mount_point=None, **kwargs):
                self.infra.init(
                    core=core, config=config, registry=self.infra.components, mount_point=self
                )

            def shutdown(self):
                self.infra.shutdown()

        self.component = Component(name='test-component')

    def test_init(self):
        core = 'test-core'
        config = {'a': 1}
        self.component.init(core=core, config=config)

        # инфра-менеджер подцепился
        self.assertTrue(hasattr(self.component, 'infra'))

        components = self.component.infra.registry['components']

        # компоненты зарегистрированы
        self.assertEqual(len(components), 2)

        # это не один объект
        mod1 = list(components.values())[0]
        mod2 = list(components.values())[1]
        self.assertFalse(mod1 == mod2)

        # проверяем вид модулей
        for module in list(components.values()):

            # инфраструктура присоединена
            self.assertTrue(module.core == core)
            self.assertTrue(module.component.state['plug_successful'])

            # конфигурация успешна
            self.assertTrue(module.component.state['config_successful'])

            # регистрация произведена
            self.assertTrue(module.name in components)
            self.assertTrue(module.registry == self.component.infra.registry['components'])
            self.assertTrue(module.component.state['registration_successful'])

            # монтирование выполнено
            self.assertTrue(hasattr(self.component, module.name))
            self.assertTrue(module.component.state['mount_successful'])

            # инициализация успешна
            self.assertTrue(module.component.state['init_successful'])

    def test_shutdown(self):
        self.component.init(core='test-core', config={'a': 1})

        components = copy.copy(self.component.infra.registry['components'])

        self.component.shutdown()

        # проверяем вид модулей
        for module in list(components.values()):

            # отмонтирование произведено
            self.assertFalse(hasattr(self.component, module.name))
            self.assertTrue(module.component.state['unmount_successful'])

            # отмена регистрации
            self.assertFalse(module.name in self.component.infra.registry['components'])
            self.assertTrue(module.registry == None)
            self.assertTrue(module.component.state['unregistration_successful'])

            # инфраструктура отсоединена
            self.assertTrue(module.core == None)
            self.assertTrue(module.component.state['unplug_successful'])

            # завершение успешно
            self.assertTrue(module.component.state['shutdown_successful'])

        # реестр очищен
        self.assertEqual(len(self.component.infra.registry['components']), 0)
