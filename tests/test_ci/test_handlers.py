# -*- coding: utf-8 -*-
from symphony.core.datatypes import DotDict
from symphony.core.containers.fields import (
    SimpleDateTimeField,
    BooleanField, IntegerField, StringField,
)
from symphony.ci.containers import GenericSignal
from symphony.ci.handlers import SignalHandler, RESTEndpointMixin
from symphony.tests.infra import InfraTest


class TestSignalHandler(InfraTest):

    def setUp(self):
        super(TestSignalHandler, self).setUp()

        class Handler(RESTEndpointMixin, SignalHandler):
            name = 'test'
            args = [StringField, SimpleDateTimeField]
            kwargs = {
                'a': IntegerField,
            }

        self.mapping = {}
        self.handler = Handler().init(core=self.core, mapping=self.mapping)
        self.signal = GenericSignal(name='test')

    def tearDown(self):
        super(TestSignalHandler, self).tearDown()

    def test_validate_value(self):
        # тип данных: валидные значения
        self.assertTrue(self.handler.validate_value(IntegerField, 1))

        # тип данных: невалидные значения
        self.assertFalse(self.handler.validate_value(IntegerField, 'a'))
        self.assertErrorMessage("a is not a valid representation of the data type: int")

        # формат: валидные значения
        self.assertTrue(self.handler.validate_value(SimpleDateTimeField, '2015-10-30 16:07:00'))

        # формат: невалидные значения
        self.assertFalse(self.handler.validate_value(SimpleDateTimeField, '2015-10-30 1700:07:00'))
        self.assertErrorMessage("2015-10-30 1700:07:00 doesn't match the format specified: %Y-%m-%d %H:%M:%S")

        # диапазон: валидные значения
        for v in [0, 1, True, False]:
            self.assertTrue(self.handler.validate_value(BooleanField, v))

        # диапазон: невалидные значения
        self.assertFalse(self.handler.validate_value(BooleanField, 2))
        self.assertErrorMessage("2 doesn't match the range specified: [1, 0, True, False]")

    def test_validate_args(self):

        # валидные аргументы
        self.handler.runtime['object'] = DotDict({'args': ['test', '2015-10-30 16:07:00']})
        self.assertTrue(self.handler.validate_args())
        self.assertIsNone(self.handler.messages.internal.errors)

        # невалидные аргументы
        self.handler.runtime['object'] = DotDict({'args': ['test', '2015-10-30 16:07:00.abc']})
        self.assertFalse(self.handler.validate_args())
        self.assertErrorMessage("2015-10-30 16:07:00.abc doesn't match the format specified: %Y-%m-%d %H:%M:%S")

        # обработчик не принимает аргументы
        self.handler.args = []
        self.handler.runtime['object'] = DotDict({'args': ['test', '2015-10-30 16:07:00.abc']})
        self.assertFalse(self.handler.validate_args())
        self.assertErrorMessage('test takes no arguments')

        # неверное количество аргументов
        self.handler.args = [SimpleDateTimeField]
        self.handler.runtime['object'] = DotDict({'args': ['test', '2015-10-30 16:07:00.abc']})
        self.assertFalse(self.handler.validate_args())
        self.assertErrorMessage('test takes exactly 1 argument(s) (2 given)')

    def test_validate_kwargs(self):

        # валидные аргументы
        self.handler.runtime['object'] = DotDict({'kwargs': {'a': 10}})
        self.handler.validate_kwargs()
        self.assertIsNone(self.handler.messages.internal.errors)

        # лишний аргумент
        self.handler.runtime['object'] = DotDict({'kwargs': {'a': 10, 'b': 20}})
        self.handler.validate_kwargs()
        self.assertErrorMessage('test got an unexpected keyword argument: b')

    def test_collect_args(self):
        self.handler.args = [StringField, StringField]
        self.handler.runtime['object'] = DotDict({'args': ['this is a cool string', 'this is another cool string']})
        self.handler.collect_args()

        # количество верное
        self.assertEqual(2, len(self.handler.runtime['args']))

        # порядок совпадает
        self.assertEqual(self.handler.runtime['args'][0], 'this is a cool string')
        self.assertEqual(self.handler.runtime['args'][1], 'this is another cool string')

    def test_collect_kwargs(self):
        self.handler.kwargs.update({'b': StringField})

        # переданы не все аргументы
        self.handler.runtime['object'] = DotDict({'kwargs': {'a': 10}})
        self.handler.collect_kwargs()

        # собраны только те, что пепеданы
        self.assertIn('a', self.handler.runtime['kwargs'])
        self.assertNotIn('b', self.handler.runtime['kwargs'])

    def test_prepare(self):
        pass


class TestRESTEndpointMixin(InfraTest):
    def setUp(self):
        super(TestRESTEndpointMixin, self).setUp()

        class HTTPHandler(RESTEndpointMixin, SignalHandler):
            name = 'test'
        self.HTTPHandler = HTTPHandler

    def tearDown(self):
        super(TestRESTEndpointMixin, self).tearDown()

    def test_group(self):
        class Handler(self.HTTPHandler):
            name = 'name__group'
            args = []

        handler = Handler()

        # __ резолвится нормально
        self.assertEqual('/name/group/', handler.as_url())

    def test_get_no_args(self):
        class Handler(self.HTTPHandler):
            args = []

        handler = Handler()

        # в строке обе группы
        self.assertEqual('/test/', handler.as_url())

    def test_get_args(self):
        class Handler(self.HTTPHandler):
            args = [IntegerField, StringField]

        handler = Handler()

        # в строке обе группы
        self.assertEqual('/test/(\d+)/([a-zA-Z0-9._-]+)/', handler.as_url())

    def test_get_args_kwargs(self):
        class Handler(self.HTTPHandler):
            args = [IntegerField, StringField]
            kwargs = {
                'int': IntegerField,
                'str': StringField,
            }

        handler = Handler()

        # в строке обе группы, ключевые аргументы не повлияли на URL
        self.assertEqual('/test/(\d+)/([a-zA-Z0-9._-]+)/', handler.as_url())

    def test_post_no_args(self):
        class Handler(self.HTTPHandler):
            http_method = 'post'

        handler = Handler()

        # в строке обе группы
        self.assertEqual('/test/', handler.as_url())

    def test_post_args(self):
        class Handler(self.HTTPHandler):
            http_method = 'post'
            args = [IntegerField, StringField, SimpleDateTimeField]

        handler = Handler()

        # в строке 2 группы, которые могут быть представленые в строке
        # третий аргумент, который не может быть использован в строке, опущен
        # (должен быть передан в теле запроса)
        self.assertEqual('/test/(\d+)/([a-zA-Z0-9._-]+)/', handler.as_url(), )

    def test_post_args_kwargs(self):
        class Handler(self.HTTPHandler):
            http_method = 'post'
            args = [IntegerField, StringField, SimpleDateTimeField]
            kwargs = {
                'int': IntegerField,
                'str': StringField,
            }

        handler = Handler()

        # в строке 2 группы, которые могут быть представленые в строке
        # третий аргумент, который не может быть использован в строке, опущен
        # (должен быть передан в теле запроса)
        # ключевые аргументы не повлияли на URL
        self.assertEqual('/test/(\d+)/([a-zA-Z0-9._-]+)/', handler.as_url(), )
