# -*- coding: utf-8 -*-
from symphony.tests.infra import InfraTest
from symphony.tests.transport import ZMQTransportMixin
from symphony.tests.apps.mock import MockNode

from symphony.messages.containers import GenericMessage
from symphony.ci.api import RemoteAPI

from symphony.protocols.smp.defaults import SMP_CODE_OK

from test_settings import SOCKET_CONF_DIR


class TestRemoteAPI(ZMQTransportMixin, InfraTest):
    sys_id = 'logger-test'

    def setUp(self):
        super(TestRemoteAPI, self).setUp()

        # config
        self.core.config.update({
            'SOCKET_POLLING_INTERVAL': 1000,

            'LOGGER_SOCKET_CONF': SOCKET_CONF_DIR + '/client_logger.json',
            'LOGGER_REPLY_TIMEOUT': 1,
            'LOGGER_REQUEST_RETRIES': 5,
            'LOGGER_RECONNECT': 10,
        })

        # logger
        self.messages.info('starting logger')
        self.logger = MockNode(
            context=self.context, name='mock-logger',
            socket_conf=SOCKET_CONF_DIR+'/logger_ci.json',
        )
        self.logger.start()

    def tearDown(self):

        self.messages.info('stopping logger')
        self.logger.shutdown()
        while self.logger.is_alive():
            self.sleep()

        if self.logger_api:
            self.messages.info('unmounting API module')
            self.logger_api.umount()

        super(TestRemoteAPI, self).tearDown()

    def test_mount(self):
        RemoteAPI(core=self.core, name='logger', sys_name='logger_api').mount(self)

        self.assertTrue(hasattr(self, 'logger_api'))
        self.assertIsNotNone(self.logger_api)

    def test_mount_no_conf(self):
        self.core.config.pop('LOGGER_SOCKET_CONF')
        RemoteAPI(core=self.core, name='logger', sys_name='logger_api').mount(self)

        self.assertTrue(hasattr(self, 'logger_api'))
        self.assertEqual(self.logger_api, None)

    def test_request(self):
        RemoteAPI(core=self.core, name='logger', sys_name='logger_api').mount(self)
        resp = self.logger_api.single('test_req', 'arg1', kwarg1='kwarg')

        # ошибок нет
        self.assertFalse(self.core.mbus.errors)

        # повторных запросов не было
        self.assertFalse(self.core.mbus.warnings)

        # ответ верный
        self.assertEqual(resp.code, SMP_CODE_OK)

    def test_request_failure(self):
        RemoteAPI(core=self.core, name='logger', sys_name='logger_api').mount(self)

        self.messages.info('pausing logger')
        self.logger.pause()
        msg = GenericMessage(level='info', content='test_msg')
        # клиент постоянно отправляет запросы к логгеру
        while True:

            self.logger_api.single(
                'message__log', self.sys_id, msg.repr.transport,
                transaction_id='test-transaction-id'
            )

            # достигнут наибольший интервал ожидания, включаем логгер
            if self.logger_api.ppp_timer.retry_in_max:
                self.messages.info('maximum reconnect interval reached')
                break

            self.sleep(0.5)

        self.messages.info('resuming logger')
        self.logger.resume()

        self.messages.info('waiting for API to recover')
        while not self.logger_api.is_active:
            self.logger_api.handle()
            self.sleep(0.5)

        self.messages.info('sending request')
        resp = self.logger_api.single(
            'message__log', self.sys_id, msg.repr.transport,
            transaction_id='test-transaction-id'
        )
        self.assertEqual(resp.code, SMP_CODE_OK)

