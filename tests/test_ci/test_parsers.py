# -*- coding: utf-8 -*-
from symphony.tests.infra import InfraTest
from symphony.ci.parsers import StringSignalParser


class TestStringSignalParser(InfraTest):
    """ """

    def setUp(self):
        super(TestStringSignalParser, self).setUp()
        self.mapping = {
            'run': None,
            'test': None,
            'test__run': None
        }
        self.parser = StringSignalParser().init(
            core=self.core,
            mapping=self.mapping,
        )

    # name
    def test_pop_name_token(self):

        # имя корневого уровня
        tokens = self.parser.split('run')
        self.assertEqual('run', self.parser.pop_name(tokens))

        # имя, совпадающее по форме с именем группы, но являющееся именем корневого уровня + параметр
        tokens = self.parser.split('test value1')
        self.assertEqual('test', self.parser.pop_name(tokens))

        # имя + группа
        tokens = self.parser.split('test run')
        self.assertEqual('test__run', self.parser.pop_name(tokens))

        # несуществующее имя
        tokens = self.parser.split('bbb')
        self.parser.pop_name(tokens)
        self.assertErrorMessage('no matching handler found')
        self.core.mbus.flush()

        # несуществующие имя и группа или параметр
        tokens = self.parser.split('bbb test')
        self.parser.pop_name(tokens)
        self.assertErrorMessage('no matching handler found')

    # args
    def test_get_args(self):

        # позиционные и ключевые
        tokens = self.parser.split('value1 value2 kwarg1=1 kwarg2=2')
        self.assertEqual(['value1', 'value2'], self.parser.get_args(tokens))

        # только позиционные
        tokens = self.parser.split('value1 value2')
        self.assertEqual(['value1', 'value2'], self.parser.get_args(tokens))

        # только ключевые
        tokens = self.parser.split('kwarg1=1 kwarg2=2')
        self.assertEqual(None, self.parser.get_args(tokens))

        # никаких ругментов
        tokens = []
        self.assertEqual(None, self.parser.get_args(tokens))

    # kwargs
    def test_get_kwargs(self):
        # позиционные и ключевые
        tokens = self.parser.split('value1 value2 kwarg1=1 kwarg2=2.002 kwarg3=true kwarg4=false')
        self.assertEqual(
            self.parser.get_kwargs(tokens),
            {'kwarg1': 1, 'kwarg2': 2.002, 'kwarg3': True, 'kwarg4': False}
        )
        tokens = self.parser.split('value1 value2')
        self.assertIsNone(self.parser.get_kwargs(tokens))

    # validation
    def test_validate_arg_tokens(self):
        tokens = self.parser.split('run value1 kwarg1=1 value2 kwarg2=2')
        self.parser.validate_arg_tokens(tokens)
        self.assertIsNotNone(self.core.mbus.errors)
        self.assertErrorMessage('positional arguments should always precede keyword arguments')

    # signal
    def test_get_signal(self):
        signal = self.parser.get_signal('test run 1 значение2 kwarg1=1 kwarg2=2.002 kwarg3=true kwarg4=no')
        self.assertEqual(signal.name, 'test__run')
        self.assertEqual(signal.args, [1, 'значение2'])
        self.assertEqual(
            signal.kwargs,
            {"kwarg1": 1, "kwarg2": 2.002, "kwarg3": True, "kwarg4": False}
        )
