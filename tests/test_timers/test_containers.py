# -*- coding: utf-8 -*-
import time
from symphony.tests.base import SymphonyBaseTestCase
from symphony.timers.containers import PPPTimer


class TestPPPTimer(SymphonyBaseTestCase):

    def setUp(self):
        super(TestPPPTimer, self).setUp()
        self.timer = PPPTimer(seconds=1)

    def tearDown(self):
        super(TestPPPTimer, self).tearDown()

    def N_test_ppp_instant(self):

        # основной цикл приложения
        while True:
            self.messages.info('is timed out: %s' % self.timer.is_timed_out)
            self.messages.info('liveness: %d' % self.timer.liveness)
            self.messages.info('retry in: %d' % self.timer.retry_in)

            self.timer.refresh()

            if self.timer.retry_in_max:
                self.messages.info('maximum retry in reached')
                break
            time.sleep(1)

        self.messages.info('timer reset')
        self.timer.reset()

        self.messages.info('is timed out: %s' % self.timer.is_timed_out)
        self.messages.info('liveness: %d' % self.timer.liveness)
        self.messages.info('retry in: %d' % self.timer.retry_in)

    def test_ppp_one_shot(self):

        self.timer.IS_CONSTANT = False

        # основной цикл приложения
        while True:
            self.messages.info('is timed out: %s' % self.timer.is_timed_out)
            self.messages.info('liveness: %d' % self.timer.liveness)
            self.messages.info('retry in: %d' % self.timer.retry_in)

            self.timer.refresh()

            if self.timer.liveness == 0:
                self.messages.info('zero liveness reached')
                break
            time.sleep(1)

        self.messages.info('timer reset')
        self.timer.reset()

        self.messages.info('is timed out: %s' % self.timer.is_timed_out)
        self.messages.info('liveness: %d' % self.timer.liveness)
        self.messages.info('retry in: %d' % self.timer.retry_in)
