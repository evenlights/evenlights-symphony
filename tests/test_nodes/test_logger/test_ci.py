# -*- coding: utf-8 -*-
import uuid
import simplejson as json

from symphony.tests.infra import InfraTest
from symphony.tests.ci import CITestMixin

from symphony.messages.containers import GenericMessage
from symphony.protocols.smp.containers import SMPSignal
from symphony.protocols.smp import defaults as smp_defaults
from symphony.nodes.logger.containers import Sender, Transaction, Message
from symphony.nodes.logger.units import LoggerUnit
from symphony.nodes.logger.ci.message import MessageListHandler, MessageLogHandler
from symphony.nodes.logger.ci.sender import (
    SenderWriteHandler, SenderReadHandler, SenderListHandler,
    SenderDeleteHandler
)


_BaseLoggerCITest = object


class BaseLoggerCITest(CITestMixin, InfraTest):
    DB_NAME = 'test_logger'
    handler_class = None

    def setUp(self):
        super(BaseLoggerCITest, self).setUp()
        assert self.handler_class
        self.core.config.update({
            'DB_NAME': self.DB_NAME,
        })
        self.ci_fe = self.DummyWorkerInterface()
        self.logger = LoggerUnit(core=self.core)
        self.handler = self.handler_class(core=self.core, mount_point=self)

    def tearDown(self):
        super(BaseLoggerCITest, self).tearDown()
        list(map(lambda s: s.delete(), Sender.objects))
        list(map(lambda t: t.delete(), Transaction.objects))
        list(map(lambda m: m.delete(), Message.objects))

    def generateMessages(self, sender_id, transaction_id=None, level='debug'):
        for _ in range(self.CYCLE_LENGTH):
            message = GenericMessage(
                level=level, content='test message %d' % _
            )

            self.logger.message_log(
                sender_id=sender_id,
                message_container=message,
                transaction_id=transaction_id
            )

    def generateSenders(self, transaction_id=None):
        for _ in range(self.CYCLE_LENGTH):
            sender_id = uuid.uuid4().__str__()
            self.generateMessages(sender_id, transaction_id=transaction_id)


# SENDER
class TestSenderWriteHandler(BaseLoggerCITest):
    handler_class = SenderWriteHandler

    def test_handle(self):
        # сообщения всех отправителей будут в рамках одной транзакции
        sender_id = uuid.uuid4().__str__()

        signal = SMPSignal(
            group='sender',
            name='write',
            args=[sender_id],
        )

        # ОРИГИНАЛЬНЫЙ ОБЪЕКТ
        self.handler._handle(signal)
        sender = Sender.objects.get(id=sender_id)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertEqual(sender.max_messages, 0)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_CREATED)

        # ОБНОВЛЯЕМ ИМЯ
        signal.kwargs = {'name': 'new name'}
        self.handler._handle(signal)
        sender.reload()

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertEqual(sender.name, 'new name')
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_ACCEPTED)

        # ОБНОВЛЯЕМ MAX_TRANSACTIONS
        signal.kwargs = {'max_messages': 10,}
        self.handler._handle(signal)
        sender.reload()

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertEqual(sender.max_messages, 10)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_ACCEPTED)


class TestSenderReadHandler(BaseLoggerCITest):
    handler_class = SenderReadHandler

    def test_handle(self):
        # сообщения всех отправителей будут в рамках одной транзакции
        sender_id = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()
        sender = Sender(id=sender_id).save(force_insert=True)
        Transaction(id=transaction_id, sender=sender).save(force_insert=True)

        signal = SMPSignal(
            group='sender',
            name='read',
            args=[sender_id],
        )

        # ОРИГИНАЛЬНЫЙ ОБЪЕКТ
        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNotNone(self.ci_fe.final_content)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_OK)

        # содержание овета
        self_response = json.loads(self.ci_fe.final_content)
        self.assertIsInstance(self_response, dict)

        print(self_response)


class TestSenderDeleteHandler(BaseLoggerCITest):
    handler_class = SenderDeleteHandler

    def test_handle(self):
        # сообщения всех отправителей будут в рамках одной транзакции
        sender_id = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()
        sender = Sender(id=sender_id).save(force_insert=True)
        Transaction(id=transaction_id, sender=sender).save(force_insert=True)

        signal = SMPSignal(
            group='sender',
            name='delete',
            args=[sender_id],
            kwargs={'cascade_transactions': True}
        )

        # ОРИГИНАЛЬНЫЙ ОБЪЕКТ
        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNone(self.ci_fe.final_content)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_NO_CONTENT)

        # содержание базы
        self.assertEqual(len(Sender.objects), 0)
        self.assertEqual(len(Transaction.objects), 0)


class TestSenderListHandler(BaseLoggerCITest):
    handler_class = SenderListHandler

    def test_handle(self):
        self.generateSenders()

        signal = SMPSignal(group='sender', name='list')

        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNotNone(self.ci_fe.final_content)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_OK)

        # содержание овета
        self_response = json.loads(self.ci_fe.final_content)
        self.assertIsInstance(self_response, list)
        self.assertEqual(len(self_response), self.CYCLE_LENGTH)

        print(self_response)


# MESSAGE
class TestMessageLogHandler(BaseLoggerCITest):
    handler_class = MessageLogHandler

    def test_handle(self):

        sender_id = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()
        message = GenericMessage(level='debug', content='test info message')

        signal = SMPSignal(
            group='message',
            name='log',
            args=[sender_id, message.repr.transport],
            kwargs={'transaction_id': transaction_id}
        )

        self.handler._handle(signal)

        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ОТВЕТ ОБРАБОТЧИКА
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_CREATED)


class TestMessageListHandler(BaseLoggerCITest):
    handler_class = MessageListHandler

    def test_handle(self):
        sender_id = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()
        self.generateMessages(sender_id, transaction_id=transaction_id)

        signal = SMPSignal(
            group='message',
            name='list',
            kwargs={
                'sender_id': sender_id,
                'transaction_id': transaction_id,
            }
        )

        self.handler._handle(signal)

        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ОТВЕТ ОБРАБОТЧИКА

        # обработчик вернул ответ
        self.assertIsNotNone(self.ci_fe.final_content)

        self_response = json.loads(self.ci_fe.final_content)

        # правильный тип данных
        self.assertIsInstance(self_response, list)

        # правильная длина
        self.assertEqual(len(self_response), self.CYCLE_LENGTH)

        print(self_response)

