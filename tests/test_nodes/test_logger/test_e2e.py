# -*- coding: utf-8 -*-
import datetime
from symphony.messages.containers import GenericMessage
from symphony.protocols.smp.containers import SMPSignal

from symphony.tests.e2e.client import SymphonyE2EClientTestCase


class LoggerE2ETest(SymphonyE2EClientTestCase):
    UUID = 'c41f4316-80c7-48e1-99e9-5de8f6e7395e'

    def setUp(self):
        super(LoggerE2ETest, self).setUp()

    def tearDown(self):
        super(LoggerE2ETest, self).tearDown()
        signal = SMPSignal(group='message', name='list')

    def _test_message_log(self):
        # сообщения всех отправителей будут в рамках одной транзакции
        signal = SMPSignal(
            group='message',
            name='log',
            args=[self.UUID, GenericMessage(level='info', content='test info message').repr.transport],
        )

        self.executeRequest(signal=signal)

    def test_cleaner_thread(self):
        """ В течение 1 минуты 10 секунд пишем сообщения и ждем, пока клинер-трэд очистит лишние
        """
        dt_now = datetime.datetime.now()
        delta = datetime.timedelta(seconds=70)

        while datetime.datetime.now() < dt_now + delta:

            signal = SMPSignal(
                group='message',
                name='log',
                args=[self.UUID, GenericMessage(level='info', content='test info message').repr.transport],
            )
            self.client.control_interface.queue.put(signal)
            self.client.ci_queue_handle()
