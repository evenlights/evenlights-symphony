# -*- coding: utf-8 -*-
import uuid
import mongoengine as mongo
from symphony.tests.base import SymphonyBaseTestCase
from symphony.nodes.logger.containers import Message, Transaction, Sender


class TestLoggerContainers(SymphonyBaseTestCase):
    DB_NAME = 'test_logger'     # имя базы

    def setUp(self):
        super(TestLoggerContainers, self).setUp()
        mongo.connect(self.DB_NAME)

    def tearDown(self):
        super(TestLoggerContainers, self).tearDown()
        for m in Message.objects:
            m.delete()
        for t in Transaction.objects:
            t.delete()
        for s in Sender.objects:
            s.delete()

    def test_messages(self):
        message = Message(level='info', content='content').save()

        # сообщение в базе
        self.assertIn(message, Message.objects)

    def test_transaction(self):
        transaction = Transaction(id=uuid.uuid4().__str__()).save()

        # транзакция в базе
        self.assertIn(transaction, Transaction.objects)

        message = Message(level='info', content='content', transaction=transaction).save()

        # сообщение связано с правильной транзакцией
        self.assertEqual(message.transaction, transaction)

        # сообщение в списке сообщений данной транзакции
        self.assertIn(message, transaction.messages)

    def test_sender(self):
        sender = Sender(id=uuid.uuid4().__str__(), name='test sender').save()

        # отправитель в базе
        self.assertIn(sender, Sender.objects)

        transaction = Transaction(id=uuid.uuid4().__str__(), sender=sender).save()
        message = Message(level='info', content='content', transaction=transaction, sender=sender).save()

        # сообщение связано с правильным отправителем
        self.assertEqual(message.sender, sender)

        # транзакция связана с правильным отправителем
        self.assertEqual(transaction.sender, sender)

        # сообщение принадлежит правильному отправителю
        self.assertIn(message, sender.messages)

        # транзакция принадлежит правильному отправителю
        self.assertIn(transaction, sender.transactions)
