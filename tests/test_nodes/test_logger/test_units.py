# -*- coding: utf-8 -*-
import uuid
import datetime

from symphony.tests.infra import InfraTestCase
from symphony.apps.core.components import AppCore

from symphony.configs.loaders import dict_loader
from symphony.messages.containers import GenericMessage
from symphony.nodes.logger.units import LoggerUnit
from symphony.nodes.logger.containers import Sender, Transaction, Message


class TestLoggerUnit(InfraTestCase):
    """ """
    CYCLE_LENGTH = 10
    TIME_STEP_MSEC = 1000
    DB_NAME = 'test_logger'

    def setUp(self):
        super(TestLoggerUnit, self).setUp()
        self.core.config.update({
            'DB_NAME': self.DB_NAME,
        })

        # логгер
        self.logger = LoggerUnit(core=self.core)

    def tearDown(self):
        super(TestLoggerUnit, self).tearDown()
        for m in Message.objects:
            m.delete()
        for t in Transaction.objects:
            t.delete()
        for s in Sender.objects:
            s.delete()

    def generateMessages(self, sender_id, start_time, transaction_id=None):

        for msg_nr in range(self.CYCLE_LENGTH):
            message = GenericMessage(
                level='info',
                content='test message %i' % msg_nr,
                # шаг в 1000 микросекунд
                timestamp=start_time + datetime.timedelta(
                    microseconds=self.TIME_STEP_MSEC*msg_nr
                ),
            )
            self.logger.message_log(
                sender_id=sender_id,
                message_container=message,
                transaction_id=transaction_id
            )

    # инициализация
    def _test_init_hostname(self):
        config_dict = {
            'DB_NAME': self.DB_NAME,
            'DB_HOST': 'localhost',
        }

        # для теста мы используем модуль с загрзкой из словаря
        class Config(GenericSystemConfigModule):
            required_keys = ['DB_NAME', 'DB_HOST']
            loader = dict_loader
            source = config_dict

        self.core = AppCore(sys_name='test_core')

        # монтируем модуль и загружаем конфигурацию (обычно это делает ядро)
        Config().mount(self.core)
        self.core.config.load()

        # логгер
        logger = LoggerUnit(core=self.core)

        # в сообщениях есть строчка о хосте
        self.assertDebugMessage(
            'connecting to host: localhost',
            logger.messages.internal
        )

    def _test_init_port(self):
        config_dict = {
            'DB_NAME': self.DB_NAME,
            'DB_HOST': 'localhost',
            'DB_PORT': 27017,
        }

        # для теста мы используем модуль с загрзкой из словаря
        class Config(GenericSystemConfigModule):
            required_keys = ['DB_NAME', 'DB_HOST', 'DB_PORT']
            loader = dict_loader
            source = config_dict

        self.core = AppCore(sys_name='test_core')

        # монтируем модуль и загружаем конфигурацию (обычно это делает ядро)
        Config().mount(self.core)
        self.core.config.load()

        # логгер
        logger = LoggerUnit(core=self.core)

        # в сообщениях есть строчка о хосте
        self.assertDebugMessage(
            'connecting to host: localhost',
            logger.messages.internal
        )

        self.assertDebugMessage(
            'using port: 27017',
            logger.messages.internal
        )

    # операции с отдельными записями
    def _test_message_log(self):
        sender_uuid = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()

        # создаем отправителя
        sender = Sender(id=sender_uuid).save()

        for msg_nr in range(self.CYCLE_LENGTH):
            # записываем сообщение
            message = GenericMessage(
                level='info', content='test message nr %d' % (msg_nr + 1)
            )
            self.logger.message_log(
                sender_id=sender_uuid,
                message_container=message,
                transaction_id=transaction_id
            )

            # верное количество сообщений
            self.assertEqual(len(sender.messages), (msg_nr + 1))

    def _test_message_list_by_sender(self):

        for _ in range(self.CYCLE_LENGTH):
            sender_uuid = uuid.uuid4().__str__()
            start_time = datetime.datetime.now()
            self.generateMessages(sender_uuid, start_time)

            # все сообщения на месте
            msgs = self.logger.message_list(sender_uuid)
            self.assertEqual(len(msgs), self.CYCLE_LENGTH)

    def _test_message_list_by_transaction(self):
        sender_uuid = uuid.uuid4().__str__()

        # генерируем для одного отправителя 10 транзакций по 10 сообщений
        for _ in range(self.CYCLE_LENGTH):
            transaction_uuid = uuid.uuid4().__str__()
            start_time = datetime.datetime.now()
            self.generateMessages(sender_uuid, start_time, transaction_id=transaction_uuid)

            # все сообщения на месте
            msgs = self.logger.message_list(transaction_id=transaction_uuid)
            self.assertEqual(len(msgs), self.CYCLE_LENGTH)

    def _test_message_list_by_time(self):
        start_time = datetime.datetime.now()
        sender_uuid = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()

        self.generateMessages(sender_uuid, start_time, transaction_id=transaction_id)

        def list_nr_by_time(self, nr, start_time, sender_id):
            """ Запрос на фильтрацию n сообщений по времени с заданного
            момента времени. """

            # NR*1000 микросекунд от начала
            end_time = start_time + datetime.timedelta(
                microseconds=self.TIME_STEP_MSEC*(nr-1)
            )
            msgs = self.logger.message_list(
                sender_id, from_date=start_time, to_date=end_time
            )
            # найдено NR сообщений
            self.assertEqual(len(msgs), nr)

            # все даты в нужном диапазоне
            truncated_start_time = self.logger.timestamp_truncate(start_time)
            truncated_end_time = self.logger.timestamp_truncate(end_time)

            for record in msgs:
                # время начала меньше или равно времени сообщений
                self.assertLessEqual(truncated_start_time, record.timestamp)
                # время конца больше или равно времени сообщений
                self.assertGreaterEqual(truncated_end_time, record.timestamp)

        for nr in range(10):
            list_nr_by_time(self, nr+1, start_time, sender_uuid)

    def _test_message_list_by_level(self):
        sender_uuid = uuid.uuid4().__str__()

        # 1 сообщение уровня debug
        message = GenericMessage(level='debug', content='test debug message')
        self.logger.message_log(sender_uuid, message)

        # 2 сообщения уровня info
        message = GenericMessage(level='info', content='test info message')
        self.logger.message_log(sender_uuid, message)
        self.logger.message_log(sender_uuid, message)

        # 3 сообщения уровня warning
        message = GenericMessage(level='warning', content='test warning message')
        self.logger.message_log(sender_uuid, message)
        self.logger.message_log(sender_uuid, message)
        self.logger.message_log(sender_uuid, message)

        # 4 сообщения уровня error
        message = GenericMessage(level='error', content='test error message')
        self.logger.message_log(sender_uuid, message)
        self.logger.message_log(sender_uuid, message)
        self.logger.message_log(sender_uuid, message)
        self.logger.message_log(sender_uuid, message)

        # 4 messages (>=3)
        msgs = self.logger.message_list(sender_uuid, level='error')
        self.assertEqual(len(msgs), 4)

        # 4+3 messages (>=2)
        msgs = self.logger.message_list(sender_uuid, level='warning')
        self.assertEqual(len(msgs), 7)

        # 4+3+2 messages (>=1)
        msgs = self.logger.message_list(sender_uuid, level='info')
        self.assertEqual(len(msgs), 9)

        # 4+3+2+1 messages (>=0)
        msgs = self.logger.message_list(sender_uuid, level='debug')
        self.assertEqual(len(msgs), 10)

    def _test_message_delete(self):
        sender_uuid = uuid.uuid4().__str__()
        start_time = datetime.datetime.now()
        self.generateMessages(sender_uuid, start_time)

        # все сообщения на месте
        sender = Sender.objects.get(id=sender_uuid)
        msgs = Message.objects(sender=sender)
        self.assertEqual(len(msgs), self.CYCLE_LENGTH)

        # очищаем лог
        self.logger.message_delete(sender_id=sender_uuid)

        # лог есть, сообщения пусты
        sender.reload()
        msgs = Message.objects(sender=sender)
        self.assertEqual(len(msgs), 0)

    # операции с отправителями
    def _test_sender_list(self):
        sender_uuid = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()

        # получаем пустой список (логов еще нет)
        sender_list = self.logger.sender_list()
        self.assertEqual(len(sender_list), 0)

        # создает лог
        Sender(id=sender_uuid, max_messages=2).save()
        sender_list = self.logger.sender_list()
        self.assertEqual(len(sender_list), 1)

    def _test_sender_read(self):
        sender_uuid = uuid.uuid4().__str__()

        # создаем отправителя
        Sender(id=sender_uuid, name='test unit', max_messages=2).save()

        # получаем мета-информацию лога
        db_sender = self.logger.sender_read(sender_uuid)

        # мета-информация вылядит правильно
        self.assertEqual(db_sender.id, sender_uuid)
        self.assertEqual(db_sender.name, 'test unit')
        self.assertEqual(db_sender.max_messages, 2)

    def _test_sender_write(self):

        # СОЗДАНИЕ ТОЛЬКО ID
        sender_uuid = uuid.uuid4().__str__()

        self.logger.sender_write(sender_uuid)

        # сообщение о создании объекта
        self.assertDebugMessage(
            'sender not present, creating',
            self.core.mbus
        )
        # ошибок и предупреждений нет
        self.assertFalse(self.core.mbus.errors)
        self.assertFalse(self.core.mbus.warnings)

        # ОБНОВЛЕНИЕ
        sender = self.logger.sender_write(sender_uuid, name='new name')

        # имя правильное
        self.assertEqual(sender.name, 'new name')

        # ошибок и предупреждений нет
        self.assertFalse(self.core.mbus.errors)
        self.assertFalse(self.core.mbus.warnings)

    def _test_sender_delete(self):
        sender_uuid = uuid.uuid4().__str__()

        def generate_transaction(self, sender_id, start_time, transaction_id):
            self.generateMessages(sender_uuid, start_time, transaction_id=transaction_uuid)

            # устанавлиаем отправителя и для транзакции
            sender =Sender.objects.get(id=sender_uuid)
            transaction = Transaction.objects.get(id=transaction_uuid)
            transaction.modify(set__sender=sender)

            # все сообщения на месте
            msgs = self.logger.message_list(transaction_id=transaction_uuid)
            self.assertEqual(len(msgs), self.CYCLE_LENGTH)

        # ПРОСТО УДАЛЕНИЕ
        for _ in range(self.CYCLE_LENGTH):
            transaction_uuid = uuid.uuid4().__str__()
            start_time = datetime.datetime.now()
            generate_transaction(self, sender_uuid, start_time, transaction_uuid)

        sender = self.logger.sender_read(sender_uuid)
        self.logger.sender_delete(sender_uuid)

        # транзакции и сообщения никуда не делись
        self.assertTrue(len(sender.messages))
        self.assertTrue(len(sender.transactions))

        list(map(lambda m: m.delete(), Message.objects))
        list(map(lambda t: t.delete(), Transaction.objects))

        # УДАЛЕНИЕ С КАСКАДОМ ПО СООБЩЕНИЯМ
        for _ in range(self.CYCLE_LENGTH):
            transaction_uuid = uuid.uuid4().__str__()
            start_time = datetime.datetime.now()
            generate_transaction(self, sender_uuid, start_time, transaction_uuid)

        sender = self.logger.sender_read(sender_uuid)
        self.logger.sender_delete(sender_uuid, cascade_messages=True)

        # сообщения удалены, транзакции никуда не делись
        self.assertFalse(len(sender.messages))
        self.assertTrue(len(sender.transactions))

        list(map(lambda t: t.delete(), Transaction.objects))

        # УДАЛЕНИЕ С КАСКАДОМ ПО ТРАНЗАКЦИЯМ
        for _ in range(self.CYCLE_LENGTH):
            transaction_uuid = uuid.uuid4().__str__()
            start_time = datetime.datetime.now()
            generate_transaction(self, sender_uuid, start_time, transaction_uuid)

        sender = self.logger.sender_read(sender_uuid)
        self.logger.sender_delete(sender_uuid, cascade_transactions=True)

        # транзакции и сообщения удалены, поскольку в данном случае все
        # сообщения в транзакциях
        self.assertFalse(len(sender.messages))
        self.assertFalse(len(sender.transactions))
