# -*- coding: utf-8 -*-
import os
import sys

from symphony.apps.configs import CLIClientConfig, ThreadPoolConfig
from symphony.apps.runners import ThreadPool
from symphony.apps.transport.client import EndpointClient
from symphony.configs.loaders import dict_loader
from symphony.nodes.logger.ci import ALL_HANDLERS as LOGGER_HANDLERS
from symphony.nodes.rest.apps import RESTServer, RESTServerConfig
from symphony.nodes.transactions.ci import ALL_HANDLERS as TRANSACTIONS_HANDLERS
from symphony.tests.base import SymphonyBaseTestCase


class RESTServerTest(SymphonyBaseTestCase):

    def setUp(self):
        super(RESTServerTest, self).setUp()
        # PyCharm добавляет свои параметры
        sys.argv = [str(os.path.abspath(__file__))]

        self.messages.info('initializing logger endpoint')

        class LoggerEPConfig(CLIClientConfig):
            loader = dict_loader
            source = {
                'CI_FE_CONF': './sockets/logger.json',
            }

        class LoggerEP(EndpointClient):
            name = 'logger'
            endpoint_handlers = LOGGER_HANDLERS
            config = LoggerEPConfig

        self.messages.info('initializing transactions endpoint')

        class TransactionsEPConfig(CLIClientConfig):
            loader = dict_loader
            source = {
                'CI_FE_CONF': './sockets/transactions.json',
            }

        class TransactionsEP(EndpointClient):
            name = 'transactions'
            endpoint_handlers = TRANSACTIONS_HANDLERS
            config = TransactionsEPConfig

        self.TransactionsEP = TransactionsEP

        self.messages.info('initializing REST node')

        class TestRESTServerConfig(RESTServerConfig):
            loader = dict_loader
            source = {
                'BIND_PORT': 8090
            }

        class TestRESTServer(RESTServer):
            config = TestRESTServerConfig

        self.messages.info('initializing thread pool')

        class PoolConfig(ThreadPoolConfig):
            loader = dict_loader
            source = None

        class Pool(ThreadPool):
            config = PoolConfig
            threads = [
                {
                    'class': LoggerEP,
                    'kwargs': {'sys_id': 'rest-logger-ep-test'},
                },

                {
                    'class': TransactionsEP,
                    'kwargs': {'sys_id': 'rest-transactions-ep-test'},
                },
                {
                    'class': TestRESTServer,
                    'kwargs': {
                        'sys_id': 'rest-server-test', 'name': 'rest-server',
                        'endpoints': [LoggerEP, TransactionsEP], 'pool': '{{ self }}'
                    },
                },

            ]
        self.Pool = Pool

    def tearDown(self):
        super(RESTServerTest, self).tearDown()

    def test_message_log(self):
        pool = self.Pool()
        pool.run()
