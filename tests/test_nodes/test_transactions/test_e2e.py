# -*- coding: utf-8 -*-
import datetime
import uuid
from symphony.messages.containers import GenericMessage
from symphony.protocols.smp.containers import SMPSignal

from symphony.tests.e2e.client import SymphonyE2EClientTestCase


class TransactionsE2ETest(SymphonyE2EClientTestCase):
    UUID = 'c9953459-a3e1-4ed4-998d-d84f0615dc6a'

    def setUp(self):
        super(TransactionsE2ETest, self).setUp()

    def tearDown(self):
        super(TransactionsE2ETest, self).tearDown()

    def _test_message_log(self):
        # сообщения всех отправителей будут в рамках одной транзакции
        signal = SMPSignal(
            group='message',
            name='log',
            args=[self.UUID, GenericMessage(level='info', content='test info message').repr.transport],
        )

        self.executeRequest(signal=signal)

    def test_cleaner_thread(self):
        """ В течение 1 минуты 10 секунд пишем сообщения и ждем, пока клинер-трэд очистит лишние
        """
        dt_now = datetime.datetime.now()
        delta = datetime.timedelta(seconds=70)

        while datetime.datetime.now() < dt_now + delta:

            signal = SMPSignal(
                group='transaction',
                name='write',
                args=[uuid.uuid1().__str__()],
                kwargs={
                    'sender_id': self.UUID,
                    'name': 'test',
                }
            )
            self.client.control_interface.queue.put(signal)
            self.client.ci_queue_handle()
