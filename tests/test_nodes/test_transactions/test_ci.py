# -*- coding: utf-8 -*-
import uuid
import datetime

import simplejson as json

from symphony.tests.infra import InfraTest
from symphony.tests.ci import CITestMixin
from .test_base import TransactionTestBase


from symphony.protocols.smp.containers import SMPSignal
from symphony.protocols.smp import defaults as smp_defaults

from symphony.nodes.transactions import defaults as unit_defaults
from symphony.nodes.transactions.containers import (
    Sender, Transaction
)
from symphony.nodes.transactions.units import TransactionPoolUnit

from symphony.nodes.transactions.ci.sender import (
    SenderWriteHandler, SenderReadHandler, SenderListHandler,
    SenderDeleteHandler
)

from symphony.nodes.transactions.ci.transaction import (
    TransactionReadHandler, TransactionWriteHandler,
    TransactionListHandler
)


class BaseTransactionPoolCITest(TransactionTestBase, CITestMixin, InfraTest):
    CYCLE_LENGTH = 10
    DB_NAME = 'test_transactions'
    handler_class = None

    def setUp(self):
        super(BaseTransactionPoolCITest, self).setUp()
        assert self.handler_class
        self.core.config.update({
            'DB_NAME': self.DB_NAME,
        })
        self.ci_fe = self.DummyWorkerInterface()

        self.transaction_pool = TransactionPoolUnit(core=self.core)
        self.handler = self.handler_class(core=self.core, mount_point=self)


# SENDER
class TestSenderWriteHandler(BaseTransactionPoolCITest):
    handler_class = SenderWriteHandler

    def test_handle(self):
        # сообщения всех отправителей будут в рамках одной транзакции
        sender_id = uuid.uuid4().__str__()

        signal = SMPSignal(
            group='sender',
            name='write',
            args=[sender_id],
        )

        # ОРИГИНАЛЬНЫЙ ОБЪЕКТ
        self.handler._handle(signal)
        sender = Sender.objects.get(id=sender_id)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertEqual(sender.max_transactions, 0)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_CREATED)

        # ОБНОВЛЯЕМ ИМЯ
        signal.kwargs = {'name': 'new name'}
        self.handler._handle(signal)
        sender.reload()

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertEqual(sender.name, 'new name')
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_ACCEPTED)

        # ОБНОВЛЯЕМ MAX_TRANSACTIONS
        signal.kwargs = {'max_transactions': 10,}
        self.handler._handle(signal)
        sender.reload()

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertEqual(sender.max_transactions, 10)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_ACCEPTED)


class TestSenderReadHandler(BaseTransactionPoolCITest):
    handler_class = SenderReadHandler

    def test_handle(self):
        # сообщения всех отправителей будут в рамках одной транзакции
        sender_id = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()
        sender = Sender(id=sender_id).save(force_insert=True)
        Transaction(id=transaction_id, sender=sender).save(force_insert=True)

        signal = SMPSignal(
            group='sender',
            name='read',
            args=[sender_id],
        )

        # ОРИГИНАЛЬНЫЙ ОБЪЕКТ
        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNotNone(self.ci_fe.final_content)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_OK)

        # содержание овета
        self_response = json.loads(self.ci_fe.final_content)
        self.assertIsInstance(self_response, dict)

        print(self_response)


class TestSenderDeleteHandler(BaseTransactionPoolCITest):
    handler_class = SenderDeleteHandler

    def test_handle(self):
        # сообщения всех отправителей будут в рамках одной транзакции
        sender_id = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()
        sender = Sender(id=sender_id).save(force_insert=True)
        Transaction(id=transaction_id, sender=sender).save(force_insert=True)

        signal = SMPSignal(
            group='sender',
            name='delete',
            args=[sender_id],
            kwargs={'cascade_transactions': True}
        )

        # ОРИГИНАЛЬНЫЙ ОБЪЕКТ
        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNone(self.ci_fe.final_content)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_NO_CONTENT)

        # содержание базы
        self.assertEqual(len(Sender.objects), 0)
        self.assertEqual(len(Transaction.objects), 0)


class TestSenderListHandler(BaseTransactionPoolCITest):
    handler_class = SenderListHandler

    def test_handle(self):
        self.generateSenders()

        signal = SMPSignal(group='sender', name='list')

        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNotNone(self.ci_fe.final_content)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_OK)

        # содержание овета
        self_response = json.loads(self.ci_fe.final_content)
        self.assertIsInstance(self_response, list)
        self.assertEqual(len(self_response), self.CYCLE_LENGTH)

        print(self_response)


# TRANSACTION
class TestTransactionWriteHandler(BaseTransactionPoolCITest):
    handler_class = TransactionWriteHandler

    def test_handle(self):
        # сообщения всех отправителей будут в рамках одной транзакции
        sender_id = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()

        signal = SMPSignal(
            group='transaction',
            name='write',
            args=[transaction_id],
            kwargs={
                'sender_id': sender_id
            }
        )

        # ОРИГИНАЛЬНЫЙ ОБЪЕКТ
        self.handler._handle(signal)
        trx = Transaction.objects.get(id=transaction_id)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_CREATED)

        # содержание базы
        self.assertIsNotNone(trx.sender)
        self.assertEqual(trx.sender.id, sender_id)

        # ОБНОВЛЯЕМ ПАРАМЕТРЫ СИГНАЛА
        signal.kwargs = {
            'group': 'super.system',
            'name': 'create',
            'args': ['a', 1, 2, {'a': 1}],
            'kwargs': {'use_barin': True, 'a': 10, },
        }
        self.handler._handle(signal)
        trx.reload()

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_ACCEPTED)

        # содержание базы
        self.assertEqual(trx.group, 'super.system')
        self.assertEqual(trx.name, 'create')
        self.assertEqual(trx.args, ['a', 1, 2, {'a': 1}])
        self.assertEqual(trx.kwargs, {'use_barin': True, 'a': 10, })

        # ОБНОВЛЯЕМ ДОПОЛНИТЕЛЬНЫЕ ПАРАМЕТРЫ
        dt_now = datetime.datetime.now()
        signal.kwargs = {
            'timestamp': dt_now.strftime(unit_defaults.MONGODB_DATE_FORMATTER),
            'state': unit_defaults.STATE_ASSIGNED,
        }
        self.handler._handle(signal)
        trx.reload()

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_ACCEPTED)

        # содержание базы
        self.assertEqual(
            trx.timestamp,
            self.transaction_pool.timestamp_truncate(dt_now)
        )
        self.assertEqual(trx.state, unit_defaults.STATE_ASSIGNED)


class TestTransactionReadHandler(BaseTransactionPoolCITest):
    handler_class = TransactionReadHandler

    def test_handle(self):
        sender_id = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()
        sender = Sender(id=sender_id).save(force_insert=True)
        Transaction(id=transaction_id, sender=sender).save(force_insert=True)

        signal = SMPSignal(
            group='transaction',
            name='read',
            args=[transaction_id],
        )

        # ОРИГИНАЛЬНЫЙ ОБЪЕКТ
        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNotNone(self.ci_fe.final_content)
        self.assertEqual(self.ci_fe.final_code, smp_defaults.SMP_CODE_OK)

        # содержание овета
        self_response = json.loads(self.ci_fe.final_content)
        self.assertIsInstance(self_response, dict)

        print(self_response)


class TestTransactionListHandler(BaseTransactionPoolCITest):
    handler_class = TransactionListHandler

    def test_handle(self):
        senders = [uuid.uuid4().__str__() for _ in range(self.CYCLE_LENGTH)]
        self.generateSenders(senders=senders)

        signal = SMPSignal(
            group='transaction',
            name='list',
        )

        # ЗАПРОС БЕЗ ПАРАМЕТРОВ
        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNotNone(self.ci_fe.final_content)
        self_response = json.loads(self.ci_fe.final_content)
        self.assertIsInstance(self_response, list)
        self.assertEqual(len(self_response), self.CYCLE_LENGTH*self.CYCLE_LENGTH)

        # print self_response

        # ЗАПРОС C SENDER_ID
        signal.kwargs = {'sender_id': senders[0]}
        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNotNone(self.ci_fe.final_content)
        self_response = json.loads(self.ci_fe.final_content)
        self.assertIsInstance(self_response, list)
        self.assertEqual(len(self_response), self.CYCLE_LENGTH)

        # ЗАПРОС C ARGS (строка)
        signal.kwargs = {'args': 'string'}
        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNotNone(self.ci_fe.final_content)
        self_response = json.loads(self.ci_fe.final_content)
        self.assertIsInstance(self_response, list)
        self.assertEqual(len(self_response), self.CYCLE_LENGTH*self.CYCLE_LENGTH)

        # ЗАПРОС C ARGS (список)
        signal.kwargs = {'args': ['string', 2]}
        self.handler._handle(signal)

        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNotNone(self.ci_fe.final_content)
        self_response = json.loads(self.ci_fe.final_content)
        self.assertIsInstance(self_response, list)
        self.assertEqual(len(self_response), self.CYCLE_LENGTH*self.CYCLE_LENGTH)

        # ЗАПРОС C KWARGS
        signal.kwargs = {'kwargs': {'use_brain': True}}
        self.handler._handle(signal)
        # системная шина
        self.assertIsNone(self.core.mbus.errors)
        self.assertIsNone(self.core.mbus.warnings)

        # ответ обработчика
        self.assertIsNotNone(self.ci_fe.final_content)
        self_response = json.loads(self.ci_fe.final_content)
        self.assertIsInstance(self_response, list)
        self.assertEqual(len(self_response), self.CYCLE_LENGTH*self.CYCLE_LENGTH)
