# -*- coding: utf-8 -*-
import uuid
import datetime
import random

from symphony.nodes.transactions.units import TransactionPoolUnit
from symphony.nodes.transactions.containers import (
    Sender, Transaction
)
from symphony.nodes.transactions.defaults import ALL_STATES

from symphony.tests.infra import InfraTestCase
from .test_base import TransactionTestBase


class TestLoggerUnit(TransactionTestBase, InfraTestCase):
    CYCLE_LENGTH = 10
    DB_NAME = 'test_transactions'

    def setUp(self):
        super(TestLoggerUnit, self).setUp()
        self.core.config.update({
            'DB_NAME': self.DB_NAME,
        })
        # пул
        self.transaction_pool = TransactionPoolUnit(core=self.core)

    def test_transaction_read(self):
        sender_id = uuid.uuid4().__str__()
        transaction_id = uuid.uuid4().__str__()
        sender = Sender(id=sender_id).save(force_insert=True)
        db_tr = Transaction(id=transaction_id, sender=sender).save(
            force_insert=True
        )
        response_tr = self.transaction_pool.transaction_read(transaction_id)

        self.assertEqual(db_tr, response_tr)

    # операции с транзакциями
    def test_transaction_write(self):
        sender_id = uuid.uuid4().__str__()
        sender = Sender(id=sender_id).save(force_insert=True)

        for msg_nr in range(self.CYCLE_LENGTH):
            transaction_id = uuid.uuid4().__str__()

            # создаем транзакцию
            self.transaction_pool.transaction_write(
                id=transaction_id,
                sender_id=sender_id,
                group='test',
                name='transaction_write',
                args=['string', 1, 2, {'a': 1}, [5, 6, 7]],
                kwargs={
                    'use_brain': True
                },
                state=random.choice(ALL_STATES),
                timestamp=datetime.datetime.now()
            )

        self.assertEqual(len(sender.transactions), self.CYCLE_LENGTH)
        print(sender.transactions)

    def test_transaction_list_by_sender(self):

        # генерируем 10 отправителей по 10 транзакций
        for _ in range(self.CYCLE_LENGTH):
            sender_uuid = uuid.uuid4().__str__()
            self.generateTransactions(sender_uuid)

            # все сообщения на месте
            trxs = self.transaction_pool.transaction_list(sender_id=sender_uuid)
            self.assertEqual(len(trxs), self.CYCLE_LENGTH)

    def test_transaction_list_by_group(self):
        sender_uuid = uuid.uuid4().__str__()
        self.generateTransactions(sender_uuid, group='group1')
        self.generateTransactions(sender_uuid, group='group2')

        trx1 = self.transaction_pool.transaction_list(group='group1')
        self.assertEqual(len(trx1), self.CYCLE_LENGTH)

        trx2 = self.transaction_pool.transaction_list(group='group2')
        self.assertEqual(len(trx2), self.CYCLE_LENGTH)

    def test_transaction_list_by_name(self):
        sender_uuid = uuid.uuid4().__str__()
        self.generateTransactions(sender_uuid, name='name1')
        self.generateTransactions(sender_uuid, name='name2')

        trx1 = self.transaction_pool.transaction_list(name='name1')
        self.assertEqual(len(trx1), self.CYCLE_LENGTH)

        trx2 = self.transaction_pool.transaction_list(name='name2')
        self.assertEqual(len(trx2), self.CYCLE_LENGTH)

    def test_transaction_list_by_args(self):
        sender_uuid = uuid.uuid4().__str__()

        self.generateTransactions(sender_uuid, args=['value1', 12])

        # транзакции находятся по одному аргументу
        trx1 = self.transaction_pool.transaction_list(args='value1')
        self.assertEqual(len(trx1), self.CYCLE_LENGTH)

        # транзакции находятся по списку аргументов
        trx2 = self.transaction_pool.transaction_list(args=['value1', 12])
        self.assertEqual(len(trx2), self.CYCLE_LENGTH)

    def test_transaction_list_by_kwargs(self):
        sender_uuid = uuid.uuid4().__str__()

        self.generateTransactions(sender_uuid)

        # по верным аргументам находится все
        trx1 = self.transaction_pool.transaction_list(
            kwargs={'use_brain': True, 'a': 10}
        )
        self.assertEqual(len(trx1), self.CYCLE_LENGTH)

        # не несуществующийм не находится ничего
        trx2 = self.transaction_pool.transaction_list(
            kwargs={'use_brain': False}
        )
        self.assertEqual(len(trx2), 0)

    def test_transaction_list_by_state(self):
        sender_uuid = uuid.uuid4().__str__()

        self.generateTransactions(sender_uuid, state='INIT')
        self.generateTransactions(sender_uuid, state='RETRY')

        trx1 = self.transaction_pool.transaction_list(state='INIT')
        self.assertEqual(len(trx1), self.CYCLE_LENGTH)

        trx2 = self.transaction_pool.transaction_list(state='RETRY')
        self.assertEqual(len(trx2), self.CYCLE_LENGTH)

    def test_transaction_list_by_time(self):
        start_time = datetime.datetime.now()
        sender_uuid = uuid.uuid4().__str__()

        self.generateTransactions(sender_uuid, start_time=start_time)

        def list_nr_by_time(self, nr, sender_id, start_time):
            """ Запрос на фильтрацию n сообщений по времени с заданного
            момента времени. """

            # NR*1000 микросекунд от начала
            end_time = start_time + datetime.timedelta(
                microseconds=self.TIME_STEP_MSEC*(nr-1)
            )
            msgs = self.transaction_pool.transaction_list(
                sender_id=sender_id, from_date=start_time, to_date=end_time
            )
            # найдено NR сообщений
            self.assertEqual(len(msgs), nr)

            # все даты в нужном диапазоне
            truncated_start_time = self.transaction_pool.timestamp_truncate(start_time)
            truncated_end_time = self.transaction_pool.timestamp_truncate(end_time)

            for record in msgs:
                # время начала меньше или равно времени сообщений
                self.assertLessEqual(truncated_start_time, record.timestamp)
                # время конца больше или равно времени сообщений
                self.assertGreaterEqual(truncated_end_time, record.timestamp)

        for nr in range(10):
            list_nr_by_time(self, nr+1, sender_uuid, start_time)

    def test_transaction_delete(self):
        sender_uuid = uuid.uuid4().__str__()
        self.generateTransactions(sender_uuid)

        # все сообщения на месте
        sender = Sender.objects.get(id=sender_uuid)
        trx = Transaction.objects(sender=sender)
        self.assertEqual(len(trx), self.CYCLE_LENGTH)

        # удаляем транзакции
        self.transaction_pool.transaction_delete(sender_id=sender_uuid)

        # лог есть, сообщения пусты
        sender.reload()
        trx = Transaction.objects(sender=sender)
        self.assertEqual(len(trx), 0)

    # операции с отправителями
    def test_sender_list(self):
        sender_uuid = uuid.uuid4().__str__()

        # получаем пустой список (логов еще нет)
        sender_list = self.transaction_pool.sender_list()
        self.assertEqual(len(sender_list), 0)

        # создает лог
        Sender(id=sender_uuid, max_transactions=2).save()
        sender_list = self.transaction_pool.sender_list()
        self.assertEqual(len(sender_list), 1)

    def test_sender_read(self):
        sender_uuid = uuid.uuid4().__str__()

        # создаем отправителя
        Sender(id=sender_uuid, name='test unit', max_transactions=2).save()

        # получаем мета-информацию лога
        db_sender = self.transaction_pool.sender_read(sender_uuid)

        # мета-информация вылядит правильно
        self.assertEqual(db_sender.id, sender_uuid)
        self.assertEqual(db_sender.name, 'test unit')
        self.assertEqual(db_sender.max_transactions, 2)

    def test_sender_write(self):

        # СОЗДАНИЕ ТОЛЬКО ID
        sender_uuid = uuid.uuid4().__str__()

        self.transaction_pool.sender_write(sender_uuid)

        # сообщение о создании объекта
        self.assertDebugMessage(
            'sender not present, creating',
            self.core.mbus
        )
        # ошибок и предупреждений нет
        self.assertFalse(self.core.mbus.errors)
        self.assertFalse(self.core.mbus.warnings)

        # ОБНОВЛЕНИЕ
        sender = self.transaction_pool.sender_write(sender_uuid, name='new name')

        # имя правильное
        self.assertEqual(sender.name, 'new name')

        # ошибок и предупреждений нет
        self.assertFalse(self.core.mbus.errors)
        self.assertFalse(self.core.mbus.warnings)

    def test_sender_delete(self):
        sender_uuid = uuid.uuid4().__str__()

        # ПРОСТО УДАЛЕНИЕ
        self.generateTransactions(sender_uuid)

        sender = self.transaction_pool.sender_read(sender_uuid)
        self.transaction_pool.sender_delete(sender_uuid)

        # транзакции и сообщения никуда не делись
        self.assertTrue(len(sender.transactions))

        list(map(lambda t: t.delete(), Transaction.objects))

        # УДАЛЕНИЕ С КАСКАДОМ ПО СООБЩЕНИЯМ
        self.generateTransactions(sender_uuid)

        sender = self.transaction_pool.sender_read(sender_uuid)
        self.transaction_pool.sender_delete(
            sender_uuid, cascade_transactions=True
        )

        # транзакции удалены
        self.assertFalse(len(sender.transactions))
