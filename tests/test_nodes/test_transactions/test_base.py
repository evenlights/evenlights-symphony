# -*- coding: utf-8 -*-
import datetime
import uuid
import random

from symphony.nodes.transactions import defaults as unit_defaults
from symphony.nodes.transactions.containers import (
    Sender, Transaction
)


class TransactionTestBase(object):
    TIME_STEP_MSEC = 1000

    def tearDown(self):
        super(TransactionTestBase, self).tearDown()
        list(map(lambda t: t.delete(), Transaction.objects))
        list(map(lambda s: s.delete(), Sender.objects))

    def generateTransactions(
            self, sender_id, group='test', name='transaction_write',
            args=None, kwargs=None,
            state=None, start_time=None
    ):
        try:
            sender = Sender.objects.get(id=sender_id)
        except Sender.DoesNotExist:
            sender = Sender(id=sender_id).save(force_insert=True)

        start_time = start_time if start_time else datetime.datetime.now()

        for trx_nr in range(self.CYCLE_LENGTH):
            transaction_id = uuid.uuid4().__str__()
            Transaction(
                id=transaction_id,
                sender=sender,
                group=group,
                name=name,
                args=args if args else ['string', 1, 2, {'a': 1}, [5, 6, 7]],
                kwargs=kwargs if kwargs else {'use_brain': True, 'a': 10},
                state=state if state else random.choice(unit_defaults.ALL_STATES),
                timestamp=start_time + datetime.timedelta(
                    microseconds=self.TIME_STEP_MSEC*trx_nr
                )
            ).save(force_insert=True)

    def generateSenders(self, senders=None, **trx_kwargs):

        _senders = senders if senders else \
            [uuid.uuid4().__str__() for _ in range(self.CYCLE_LENGTH)]

        for sid in _senders:
            Sender(id=sid).save(force_insert=True)
            self.generateTransactions(sid, **trx_kwargs)
