# -*- coding: utf-8 -*-
import uuid
import mongoengine as mongo
import simplejson as json
from symphony.tests.base import SymphonyBaseTestCase
from symphony.nodes.transactions.containers import (
    Sender, Transaction
)


class TestTransactionsContainers(SymphonyBaseTestCase):
    DB_NAME = 'test_transactions'

    def setUp(self):
        super(TestTransactionsContainers, self).setUp()
        mongo.connect(self.DB_NAME)

    def tearDown(self):
        super(TestTransactionsContainers, self).tearDown()
        for t in Transaction.objects:
            t.delete()
        for s in Sender.objects:
            s.delete()

    def test_sender(self):
        sender = Sender(id=uuid.uuid4().__str__()).save(force_insert=True)

        # отправитель в базе
        self.assertIn(sender, Sender.objects)

        print(sender.repr.json)

    def test_transaction(self):
        sender = Sender(id=uuid.uuid4().__str__()).save(force_insert=True)
        transaction = Transaction(
            id=uuid.uuid4().__str__(),
            group='message',
            name='log',
            args=[12, json.dumps({'this_date': '2015-11-03 15:55:12'})],
            kwargs={
                'use_brain': True,
            },
            sender=sender
        ).save(force_insert=True)

        # транзакция в базе
        self.assertIn(transaction, Transaction.objects)

        print(transaction.repr.json)
