# -*- coding: utf-8 -*-
import uuid
import random
import time
import datetime
from symphony.tests.base import SymphonyBaseTestCase
from symphony.core.datatypes import DotDict
from symphony.timers.containers import GenericTimer
from symphony.pools.transactions import GenericTransactionPool, TransactionInfo


class TestGenericTransactionPool(SymphonyBaseTestCase):
    NIN_TRANSACTIONS = 100
    MAX_TRANSACTIONS = 200

    def setUp(self):
        super(TestGenericTransactionPool, self).setUp()

        self.pool = GenericTransactionPool()

        self.messages.info('initializing random seed')
        random.seed()
        nr_transactions = random.randrange(self.NIN_TRANSACTIONS, self.MAX_TRANSACTIONS)

        self.messages.info('generating %i transactions' % nr_transactions)
        self.transactions = [
            DotDict({'id': str(uuid.uuid1()), 'name': 'transaction_%d' % _})
            for _ in range(nr_transactions)
            ]

    def tearDown(self):
        super(TestGenericTransactionPool, self).tearDown()

    def test_in(self):
        self.pool.extend(self.transactions)

        for index, value in enumerate(self.transactions):
            # нормальный __contains__
            self.assertTrue(self.transactions[index] in self.pool)

            # __contains__ по id
            self.assertTrue(self.transactions[index].id in self.pool)

    def test_get(self):
        self.pool.extend(self.transactions)

        for index, value in enumerate(self.transactions):
            # нормальный __getitem__
            self.assertEqual(self.pool[index], self.transactions[index])

            # __getitem__ по id
            self.assertEqual(self.pool[self.transactions[index].id], self.transactions[index])

    def test_index(self):
        self.pool.extend(self.transactions)

        for index, value in enumerate(self.transactions):
            # нормальный index
            self.assertEqual(self.pool.index(self.transactions[index]), index)

            # index по id
            self.assertEqual(self.pool.index(self.transactions[index].id), index)

    def test_register(self):
        self.pool.register(self.transactions[0].id, self.transactions[0].name)

        self.assertIn(self.transactions[0].id, self.pool)
        self.assertIsInstance(self.pool[0], TransactionInfo)

        # верный дефолтный таймаут
        self.assertEqual(self.pool[0]._timeout, 1)

        self.pool.register(self.transactions[1].id, self.transactions[1].name, timeout=15)

        # верный установленный таймаут
        self.assertEqual(self.pool[1]._timeout, 15)

    def test_unregister(self):
        self.pool.register(self.transactions[0].id, self.transactions[0].name)

        self.pool.unregister(self.transactions[0].id)

        # транзакция отсутствует
        self.assertNotIn(self.transactions[0].id, self.pool)

    def test_register_activity(self):
        self.pool.register(self.transactions[0].id, self.transactions[0].name)

        # последняя активность -- почти "сейчас" (500 микросекунд)
        self.assertLessEqual(
            datetime.datetime.now(),
            #
            self.pool[0].last_activity + datetime.timedelta(microseconds=500)
        )

        # ждем секунду
        time.sleep(1)

        # регистрируем активность по операции
        self.pool.register_activity(self.transactions[0].id)

        # последняя активность -- снова почти "сейчас" (500 микросекунд)
        self.assertLessEqual(
            datetime.datetime.now(),
            #
            self.pool[0].last_activity + datetime.timedelta(microseconds=500)
        )

    def test_refresh(self):
        self.pool.register(id=self.transactions[0].id, name=self.transactions[0].name)

        # 5 по умолчанию
        self.assertEqual(self.pool[0].liveness, 5)

        # симулируем превышение таймаута
        wait_timer = GenericTimer(seconds=2)
        while not wait_timer.is_timed_out:
            self.pool[self.transactions[0].id].refresh()

        # остается 3 попытки
        self.assertEqual(self.pool[0].liveness, 3)

    def test_is_timed_out(self):
        self.pool.register(self.transactions[0].id, self.transactions[0].name)

        time.sleep(2)
        self.assertTrue(self.pool.is_timed_out(self.transactions[0].id))

    def test_timed_out(self):
        # операция с таймаутом 1 сек
        self.pool.register(self.transactions[0].id, self.transactions[0].name)

        # операция с таймаутом 4 сек
        self.pool.register(self.transactions[1], 'some_name', timeout=4)

        # ждем 2 секунды, должна истечь первая операция
        time.sleep(2)
        self.assertEqual(len(self.pool.timed_out), 1)

        # ждем еще 2 секунды, должна истечь вторая операция
        time.sleep(2)
        self.assertEqual(len(self.pool.timed_out), 2)
