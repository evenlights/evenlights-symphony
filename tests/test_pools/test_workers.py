# -*- coding: utf-8 -*-
import random
from symphony.core.datatypes import DotDict
from symphony.tests.base import SymphonyBaseTestCase
from symphony.pools.workers import GenericWorkerPool, WorkerInfo


class TestGenericWorkerPool(SymphonyBaseTestCase):
    NIN_WORKERS = 100
    MAX_WORKERS = 200

    def setUp(self):
        super(TestGenericWorkerPool, self).setUp()

        self.pool = GenericWorkerPool()

        self.messages.info('initializing random seed')
        random.seed()
        nr_workers = random.randrange(self.NIN_WORKERS, self.MAX_WORKERS)

        self.messages.info('generating %i workers' % nr_workers)
        self.workers = [
            DotDict({'id': 1000+_, 'name': 'worker_%d' % _}) for _ in range(nr_workers)
            ]

    def tearDown(self):
        super(TestGenericWorkerPool, self).tearDown()

    def test_in(self):
        self.pool.extend(self.workers)

        for index, value in enumerate(self.workers):

            # нормальный __contains__
            self.assertTrue(self.workers[index] in self.pool)

            # __contains__ по id
            self.assertTrue(self.workers[index].id in self.pool)

            # __contains__ по name
            self.assertTrue(self.workers[index].name in self.pool)

    def test_get(self):
        self.pool.extend(self.workers)

        for index, value in enumerate(self.workers):

            # нормальный __getitem__
            self.assertEqual(self.pool[index], self.workers[index])

            # __getitem__ по id
            self.assertEqual(self.pool[self.workers[index].id], self.workers[index])

            # __getitem__ по name
            self.assertEqual(self.pool[self.workers[index].name], self.workers[index])

    def test_index(self):
        self.pool.extend(self.workers)

        for index, value in enumerate(self.workers):
            # нормальный index
            self.assertEqual(self.pool.index(self.workers[index]), index)

            # index по id
            self.assertEqual(self.pool.index(self.workers[index].id), index)

            # index по name
            self.assertEqual(self.pool.index(self.workers[index].name), index)

    def test_register(self):
        # регистрируем первый воркер из списка
        self.pool.register(self.workers[0].id)

        self.assertIn(self.workers[0].id, self.pool)
        self.assertIsInstance(self.pool[0], WorkerInfo)
        self.assertEqual(self.pool[0]._hb_timeout, 1)

        self.pool.register(self.workers[1], timeout=15)
        self.assertEqual(self.pool[1]._hb_timeout, 15)

    def test_unregister(self):
        # регистрируем первый воркер из списка
        self.pool.register(self.workers[0].id)

        # воркер на месте
        self.assertIn(self.workers[0].id, self.pool)

        # удаляем ворке
        self.pool.unregister(self.workers[0].id)

        # воркер отсутствует
        self.assertNotIn(self.workers[0].id, self.pool)

    def test_engage(self):
        # регистрируем все воркеры
        list(map(self.pool.register, [w.id for w in self.workers]))

        for nr, wi in enumerate(self.workers):
            # задействуем воркер
            self.pool.engage(wi.id)

            # все воркеры с начала по текущий задействованы
            engaged = [self.pool[wid].is_engaged for wid in [w.id for w in self.workers[:nr+1]]]
            self.assertTrue(all(engaged))

            # все воркеры с текущего до конца свободны
            released = [self.pool[wid].is_engaged for wid in [w.id for w in self.workers[nr+1:]]]
            if len(released):
                self.assertFalse(all(released))

    def test_engage_lru(self):
        # регистрируем все воркеры
        list(map(self.pool.register, [w.id for w in self.workers]))

        for _ in self.workers:

            # задействуем LRU
            self.pool.engage_lru()

            # убеждаемся, что задействован всегда первый в списке
            self.assertTrue(self.pool[self.workers[0].id].is_engaged)

    def test_release(self):
        list(map(self.pool.register, [w.id for w in self.workers]))

        for nr, wi in enumerate(self.workers):

            # задействуем первый воркер из списка
            self.pool.engage_lru()

            # это должен быть worker_0
            self.assertTrue(self.pool[wi.id].is_engaged)

            # освобождаем
            self.pool.release(wi.id)

            # убеждаемся, что освободили
            self.assertFalse(self.pool[wi.id].is_engaged)

            # воркер в конце списка
            self.assertEqual(self.pool.index(wi.id), len(self.pool)-1)

    def test_available(self):
        list(map(self.pool.register, [w.id for w in self.workers]))

        for nr, wi in enumerate(self.workers):
            # задействуем
            self.pool.engage(wi.id)

            # убеждаемся, что количество скоратилось
            self.assertEqual(len(self.pool) - (nr + 1),
                             self.pool.released_count)
