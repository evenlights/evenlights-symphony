# -*- coding: utf-8 -*-
import unittest


CORE_TESTS = [
    'test_core.test_containers.test_object',
    'test_core.test_containers.test_list',
    'test_core.test_containers.test_fields',
    'test_core.test_generators',
    'test_core.test_infra',
    'test_core.test_resolvers',
    'test_core.test_runtime',
]

BASE_TESTS = [
    'test_configs.test_base',
    'test_configs.test_components',
    # 'test_configs.test_loaders',

    # 'test_messages.test_components',
    'test_messages.test_containers',

    'test_files.test_adapters',

    'test_routers.test_components',
    'test_routers.test_handlers',

    'test_ci.test_handlers',
    'test_ci.test_parsers',
    # 'test_ci.test_api',

    'test_timers.test_containers',

    'test_pools.test_workers',
    'test_pools.test_transactions',

    'test_adapters.test_sockets',

    # # 'test_plugins.test_filters',
    # # 'test_plugins.test_chainers',
    #
    # 'test_osutils.test_bindings',

    'test_protocols.test_smp.test_parsers',
    'test_protocols.test_smp.test_containers',
    'test_protocols.test_smp.test_adapters',
    'test_protocols.test_smp.test_routers',
    'test_protocols.test_smp.test_handlers',
]

APP_TESTS = [
    'test_apps.test_core',
    'test_apps.test_base',
    'test_apps.test_cli',

    # 'test_apps.test_handlers.test_smp',

    'test_apps.test_transport.test_schemas',
    'test_apps.test_transport.test_base',
    'test_apps.test_transport.test_broker',
    'test_apps.test_transport.test_worker',
    'test_apps.test_transport.test_client',
    # 'test_apps.test_runners', # fixme: тест гоняет вечно, исправить
    # 'test_apps.test_proto',
]

NODE_TESTS = [
    # 'test_nodes.test_logger.test_containers',
    # 'test_nodes.test_logger.test_units',
    # 'test_nodes.test_logger.test_ci',
    # # todo: врзумительные e2e-тесты
    # # 'test_nodes.test_logger.test_e2e',
    #
    # 'test_nodes.test_transactions.test_containers',
    # 'test_nodes.test_transactions.test_units',
    # 'test_nodes.test_transactions.test_ci',
    # # todo: врзумительные e2e-тесты
    # # 'test_nodes.test_logger.test_e2e',
]

TESTS = [
    # *CORE_TESTS,
    *BASE_TESTS,
    # *APP_TESTS,
    # *NODE_TESTS,
]

for tm in TESTS:
    print('Importing test:', tm)
    mod = __import__(tm, globals(), locals(), ['suite'])
    for name in dir(mod):
        if 'Test' in name:
            globals()[name] = getattr(mod, name)


if __name__ == '__main__':
    unittest.main()
