# -*- coding: utf-8 -*-
import unittest
from symphony.messages.containers import MessageQueue, Message


class TestMBus(unittest.TestCase):

    def setUp(self):
        print('='*80)
        print(unittest.TestCase.id(self))
        print('-'*80)

        self.mbus = MessageQueue()

    def tearDown(self):
        pass

    def test_by_type(self):
        m1 = Message(level='info')
        self.mbus.append(m1)
        self.assertEqual(self.mbus.by_level('info')[0], m1)

        m2 = Message(level='info')
        self.mbus.append(m2)
        self.assertEqual(len(self.mbus.by_level('info')), 2)

        m3 = Message(level='error')
        self.mbus.append(m3)
        self.assertEqual(len(self.mbus.by_level('error')), 1)

    def test_info_messages(self):
        m1 = Message(level='info')
        m2 = Message(level='info')
        m3 = Message(level='warning')
        m4 = Message(level='error')
        self.mbus.extend([m1, m2, m3, m4])
        self.assertEqual(len(self.mbus.info_messages), 2)

    def test_warning_messages(self):
        m1 = Message(level='info')
        m2 = Message(level='info')
        m3 = Message(level='warning')
        m4 = Message(level='error')
        self.mbus.extend([m1, m2, m3, m4])
        self.assertEqual(len(self.mbus.warnings), 1)

    def test_error_messages(self):
        m1 = Message(level='info')
        m2 = Message(level='info')
        m3 = Message(level='warning')
        m4 = Message(level='error')
        self.mbus.extend([m1, m2, m3, m4])
        self.assertEqual(len(self.mbus.errors), 1)

    def test_flush(self):
        m1 = Message(level='info')
        m2 = Message(level='info')
        m3 = Message(level='warning')
        m4 = Message(level='error')
        self.mbus.extend([m1, m2, m3, m4])
        self.assertEqual(len(self.mbus), 4)

        self.mbus.flush()
        self.assertEqual(len(self.mbus), 0)

    def test_info_message(self):
        self.mbus.info('info message')
        self.assertEqual(len(self.mbus.info_messages), 1)

        self.mbus.info('info message')
        self.assertEqual(len(self.mbus.info_messages), 2)

        self.mbus.warning('warning message')
        self.assertEqual(len(self.mbus.info_messages), 2)

    def test_warning_message(self):
        self.mbus.warning('warning message')
        self.assertEqual(len(self.mbus.warnings), 1)

        self.mbus.warning('warning message')
        self.assertEqual(len(self.mbus.warnings), 2)

        self.mbus.info('info message')
        self.assertEqual(len(self.mbus.warnings), 2)
