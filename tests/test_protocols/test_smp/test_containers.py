# -*- coding: utf-8 -*-
from symphony.core.generators import GenericKeyGenerator
from symphony.ci.containers import GenericSignal
from symphony.protocols.smp import defaults as smp_defaults
from symphony.protocols.smp.containers import SMPClientMessage
from symphony.protocols.smp.containers import SMPLifeCycleMessage
from symphony.protocols.smp.containers import SMPWorkerMessage
from symphony.tests.base import BaseTest


class TestSMPClientMessage(BaseTest):
    """
    Frame 0: "SMPC01" (шесть байт, идентификатор субпротокола SMP/Client v0.1)
    Frame 1: message type (один байт, REQUEST)
    Frame 2: transaction id (строка, идентификатор операции)
    Frame 3: content type (один байт, SIGNAL)
    Frame 4: content (бинарный формат)

    Frame 0: "SMPC01" (шесть байт, идентификатор субпротокола SMP/Client v0.1)
    Frame 1: message type (один байт, PARTIAL/FINAL)
    Frame 2: transaction id (строка, идентификатор операции)
    Frame 3: content type (один байт, NONE, STRING, MESSAGE, SIGNAL, FILE)
    Frame 4: code (два байт,а код ответа)
    Frame 5: content (бинарный формат)
    """

    def setUp(self):
        super(TestSMPClientMessage, self).setUp()

        self.signal = GenericSignal(name='run')

        self.sender = str(GenericKeyGenerator())
        self.client = str(GenericKeyGenerator())
        self.transaction_id = str(GenericKeyGenerator())

    # REQUEST
    def test_req(self):
        msg = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_REQUEST,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
            content=self.signal.repr.json
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 5)
        self.assertEqual(msg.repr.transport[0], smp_defaults.SMP_SUB_CLIENT)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_TYPE_REQUEST)
        self.assertEqual(msg.repr.transport[2], self.transaction_id)
        self.assertEqual(msg.repr.transport[3], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[4], self.signal.repr.json)

    def test_req_sender(self):
        msg = SMPClientMessage(
            address=self.sender,
            message_type=smp_defaults.SMP_TYPE_REQUEST,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
            content=self.signal.repr.json
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 6)
        self.assertEqual(msg.repr.transport[0], self.sender)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_SUB_CLIENT)
        self.assertEqual(msg.repr.transport[2], smp_defaults.SMP_TYPE_REQUEST)
        self.assertEqual(msg.repr.transport[3], self.transaction_id)
        self.assertEqual(msg.repr.transport[4], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[5], self.signal.repr.json)

    def test_req_empty_content(self):
        msg = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_REQUEST,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 5)
        self.assertEqual(msg.repr.transport[0], smp_defaults.SMP_SUB_CLIENT)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_TYPE_REQUEST)
        self.assertEqual(msg.repr.transport[2], self.transaction_id)
        self.assertEqual(msg.repr.transport[3], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[4], '')

    def test_req_sender_empty_content(self):
        msg = SMPClientMessage(
            address=self.sender,
            message_type=smp_defaults.SMP_TYPE_REQUEST,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 6)
        self.assertEqual(msg.repr.transport[0], self.sender)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_SUB_CLIENT)
        self.assertEqual(msg.repr.transport[2], smp_defaults.SMP_TYPE_REQUEST)
        self.assertEqual(msg.repr.transport[3], self.transaction_id)
        self.assertEqual(msg.repr.transport[4], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[5], '')

    # PARTIAL/FINAL
    def test_part(self):
        msg = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
            code=smp_defaults.SMP_CODE_OK,
            content=self.signal.repr.json
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 6)
        self.assertEqual(msg.repr.transport[0], smp_defaults.SMP_SUB_CLIENT)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_TYPE_PARTIAL)
        self.assertEqual(msg.repr.transport[2], self.transaction_id)
        self.assertEqual(msg.repr.transport[3], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[4], smp_defaults.SMP_CODE_OK)
        self.assertEqual(msg.repr.transport[5], self.signal.repr.json)

    def test_part_sender(self):
        msg = SMPClientMessage(
            address=self.sender,
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
            code=smp_defaults.SMP_CODE_OK,
            content=self.signal.repr.json
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 7)
        self.assertEqual(msg.repr.transport[0], self.sender)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_SUB_CLIENT)
        self.assertEqual(msg.repr.transport[2], smp_defaults.SMP_TYPE_PARTIAL)
        self.assertEqual(msg.repr.transport[3], self.transaction_id)
        self.assertEqual(msg.repr.transport[4], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[5], smp_defaults.SMP_CODE_OK)
        self.assertEqual(msg.repr.transport[6], self.signal.repr.json)

    def test_part_empty_code(self):
        msg = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
            content=self.signal.repr.json
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 6)
        self.assertEqual(msg.repr.transport[0], smp_defaults.SMP_SUB_CLIENT)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_TYPE_PARTIAL)
        self.assertEqual(msg.repr.transport[2], self.transaction_id)
        self.assertEqual(msg.repr.transport[3], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[4], '')
        self.assertEqual(msg.repr.transport[5], self.signal.repr.json)


class TestSMPLifeCycleMessage(BaseTest):
    """
    Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
    Frame 1: message type (один байт, INIT)

    Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
    Frame 1: message type (один байт, HEARTBEAT)

    Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
    Frame 1: message type (один байт, SHUTDOWN)
    """

    def setUp(self):
        super(TestSMPLifeCycleMessage, self).setUp()

        self.signal = GenericSignal(name='run')

        self.sender = str(GenericKeyGenerator())
        self.client = str(GenericKeyGenerator())
        self.transaction_id = str(GenericKeyGenerator())

    # REQUEST
    def test_init(self):
        msg = SMPLifeCycleMessage(
            message_type=smp_defaults.SMP_TYPE_LC_INIT,
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 2)
        self.assertEqual(msg.repr.transport[0], smp_defaults.SMP_SUB_LIFECYCLE)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_TYPE_LC_INIT)

    def test_init_sender(self):
        msg = SMPLifeCycleMessage(
            address=self.sender,
            message_type=smp_defaults.SMP_TYPE_LC_INIT,
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 3)
        self.assertEqual(msg.repr.transport[0], self.sender)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_SUB_LIFECYCLE)
        self.assertEqual(msg.repr.transport[2], smp_defaults.SMP_TYPE_LC_INIT)


class TestSMPWorkerMessage(object):
    """
    Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
    Frame 1: message type (один байт, REQUEST)
    Frame 2: transaction id (строка, идентификатор операции)
    Frame 3: content type (один байт, SIGNAL)
    Frame 4: client id (строка, идентификатор клиента)
    Frame 5: content (бинарный формат)

    Frame 0: "SMPW01" (шесть байт, идентификатор субпротокола SMP/Worker v0.1)
    Frame 1: message type (один байт, PARTIAL)
    Frame 2: transaction id (строка, идентификатор операции)
    Frame 3: content type (один байт, NONE, STRING, MESSAGE, SIGNAL, FILE)
    Frame 4: code (два байт,а код ответа)
    Frame 5: client id (строка, идентификатор клиента)
    Frame 6: content (бинарный формат)
    """

    def setUp(self):
        super(TestSMPWorkerMessage, self).setUp()

        self.signal = GenericSignal(name='run')

        self.sender = str(GenericKeyGenerator())
        self.client = str(GenericKeyGenerator())
        self.transaction_id = str(GenericKeyGenerator())

    # REQUEST
    def test_req(self):
        msg = SMPWorkerMessage(
            message_type=smp_defaults.SMP_TYPE_REQUEST,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
            client_id=self.client,
            content=self.signal.repr.json
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 6)
        self.assertEqual(msg.repr.transport[0], smp_defaults.SMP_SUB_WORKER)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_TYPE_REQUEST)
        self.assertEqual(msg.repr.transport[2], self.transaction_id)
        self.assertEqual(msg.repr.transport[3], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[4], self.client)
        self.assertEqual(msg.repr.transport[5], self.signal.repr.json)

    def test_req_sender(self):
        msg = SMPWorkerMessage(
            address=self.sender,
            message_type=smp_defaults.SMP_TYPE_REQUEST,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
            client_id=self.client,
            content=self.signal.repr.json
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 7)
        self.assertEqual(msg.repr.transport[0], self.sender)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_SUB_WORKER)
        self.assertEqual(msg.repr.transport[2], smp_defaults.SMP_TYPE_REQUEST)
        self.assertEqual(msg.repr.transport[3], self.transaction_id)
        self.assertEqual(msg.repr.transport[4], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[5], self.client)
        self.assertEqual(msg.repr.transport[6], self.signal.repr.json)

    # PARTIAL/FINAL
    def test_part(self):
        msg = SMPWorkerMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
            code=smp_defaults.SMP_CODE_OK,
            client_id=self.client,
            content=self.signal.repr.json
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 7)
        self.assertEqual(msg.repr.transport[0], smp_defaults.SMP_SUB_WORKER)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_TYPE_PARTIAL)
        self.assertEqual(msg.repr.transport[2], self.transaction_id)
        self.assertEqual(msg.repr.transport[3], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[4], smp_defaults.SMP_CODE_OK)
        self.assertEqual(msg.repr.transport[5], self.client)
        self.assertEqual(msg.repr.transport[6], self.signal.repr.json)

    def test_part_sender(self):
        msg = SMPWorkerMessage(
            address=self.sender,
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id=self.transaction_id,
            content_type=smp_defaults.SMP_CONTENT_SIGNAL,
            code=smp_defaults.SMP_CODE_OK,
            client_id=self.client,
            content=self.signal.repr.json
        )
        print(msg.repr.transport)
        self.assertEqual(len(msg.repr.transport), 8)
        self.assertEqual(msg.repr.transport[0], self.sender)
        self.assertEqual(msg.repr.transport[1], smp_defaults.SMP_SUB_WORKER)
        self.assertEqual(msg.repr.transport[2], smp_defaults.SMP_TYPE_PARTIAL)
        self.assertEqual(msg.repr.transport[3], self.transaction_id)
        self.assertEqual(msg.repr.transport[4], smp_defaults.SMP_CONTENT_SIGNAL)
        self.assertEqual(msg.repr.transport[5], smp_defaults.SMP_CODE_OK)
        self.assertEqual(msg.repr.transport[6], self.client)
        self.assertEqual(msg.repr.transport[7], self.signal.repr.json)
