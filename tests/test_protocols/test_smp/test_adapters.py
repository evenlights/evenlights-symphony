# -*- coding: utf-8 -*-
import os

import zmq
from unittest import skip

from symphony.ci.containers import GenericSignal
from symphony.protocols.smp import defaults
from symphony.protocols.smp import containers
from symphony.protocols.smp.adapters import SMPSocketAdapter
from symphony.tests.infra import InfraTest


class TestSMPSocketAdapterBase(InfraTest):
    """
    """
    SOCKET_CONF_DIR = os.path.dirname(os.path.abspath(__file__))
    INTERFACE_ADDR = 'inproc://interface'
    CYCLE_LENGTH = 100

    def setUp(self):
        super(TestSMPSocketAdapterBase, self).setUp()
        self.context = zmq.Context()
        self.socket_registry = {}

        self.client = SMPSocketAdapter(name='smp_client').init(
            core=self.core, config={
                'type': 'DEALER',
                'connect': 'inproc://broker_fe',
                'option': {
                    'identity': 'client',
                },
            }, registry={}, mount_point=self,
            context=self.context, socket_registry=self.socket_registry,

        )

        self.broker_fe = SMPSocketAdapter(name='smp_broker_fe').init(
            core=self.core, config={
                'type': 'ROUTER',
                'bind': 'inproc://broker_fe',
                'option': {
                    'identity': 'broker',
                },
            }, registry={}, mount_point=self,
            context=self.context, socket_registry=self.socket_registry,

        )

        self.broker_be = SMPSocketAdapter(name='smp_broker_be').init(
            core=self.core, config={
                'type': 'ROUTER',
                'bind': 'inproc://broker_be',
                'option': {
                    'identity': 'broker',
                },
            }, registry={}, mount_point=self,
            context=self.context, socket_registry=self.socket_registry,

        )

        self.worker = SMPSocketAdapter(name='smp_worker').init(
            core=self.core, config={
                'type': 'DEALER',
                'connect': 'inproc://broker_be',
                'option': {
                    'identity': 'worker',
                },
            }, registry={}, mount_point=self,
            context=self.context, socket_registry=self.socket_registry,

        )

        self.broker_fe.connect()
        self.broker_be.connect()
        self.client.connect()
        self.worker.connect()

        self.transaction_id = 'test-transaction-id'
        self.signal = GenericSignal(name='test')

    def tearDown(self):
        super(TestSMPSocketAdapterBase, self).tearDown()
        self.worker.disconnect()
        self.client.disconnect()
        self.broker_fe.disconnect()
        self.broker_be.disconnect()

        self.context.term()

    def test_client_request(self):

        # отправляем клиентом запрос
        self.client.client_request(
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            content=self.signal.repr.json
        )

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # сообщение, которое должен получить воркер
        source_msg = containers.SMPClientMessage(
            address=self.client.options['identity'],
            message_type=defaults.SMP_TYPE_REQUEST,
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            content=self.signal.repr.json,
        )

        # получаем сообщение протокола при помощи парсера
        target_msg = self.broker_fe.client_recv()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # транспортные представления совпадают
        self.assertEqual(source_msg.repr.transport, target_msg.repr.transport)

    def test_client_partial(self):

        # регистрируемся на роутере
        self.client.send_multipart(['hi'])

        # эмулируем ответ воркера
        self.broker_fe.client_partial(
            address=self.client.options['identity'],
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            content=self.signal.repr.json,
        )

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # сообщение, которое должен получить клиент
        source_msg = containers.SMPClientMessage(
            message_type=defaults.SMP_TYPE_PARTIAL,
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            content=self.signal.repr.json,
            )

        # получаем сообщение протокола при помощи парсера
        target_msg = self.client.client_recv()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # транспортные представления совпадают
        self.assertEqual(source_msg.repr.transport, target_msg.repr.transport)

    def test_client_final(self):

        # регистрируемся на роутере
        self.client.send_multipart(['hi'])

        # эмулируем ответ воркера
        self.broker_fe.client_final(
            address=self.client.options['identity'],
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            content=self.signal.repr.json,
        )

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # сообщение, которое должен получить клиент
        source_msg = containers.SMPClientMessage(
            message_type=defaults.SMP_TYPE_FINAL,
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            content=self.signal.repr.json,
            )

        # получаем сообщение протокола при помощи парсера
        target_msg = self.client.client_recv()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # транспортные представления совпадают
        self.assertEqual(source_msg.repr.transport, target_msg.repr.transport)

    def test_lifecycle_init(self):
        self.worker.lifecycle_init()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        target_msg = self.broker_be.lifecycle_recv()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # сообщение, которое должен получить воркер
        source_msg = containers.SMPLifeCycleMessage(
            address=self.worker.options['identity'],
            message_type=defaults.SMP_TYPE_LC_INIT,
            )

        # транспортные представления совпадают
        self.assertEqual(source_msg.repr.transport, target_msg.repr.transport)

    def test_lifecycle_heartbeat(self):
        self.worker.lifecycle_heartbeat()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        target_msg = self.broker_be.lifecycle_recv()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # сообщение, которое должен получить воркер
        source_msg = containers.SMPLifeCycleMessage(
            address=self.worker.options['identity'],
            message_type=defaults.SMP_TYPE_LC_HEARTBEAT,
            )

        # транспортные представления совпадают
        self.assertEqual(source_msg.repr.transport, target_msg.repr.transport)

    def test_lifecycle_shutdown(self):
        self.worker.lifecycle_shutdown()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        target_msg = self.broker_be.lifecycle_recv()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # сообщение, которое должен получить брокер
        source_msg = containers.SMPLifeCycleMessage(
            address=self.worker.options['identity'],
            message_type=defaults.SMP_TYPE_LC_SHUTDOWN,
            )

        # транспортные представления совпадают
        self.assertEqual(source_msg.repr.transport, target_msg.repr.transport)

    def test_worker_request(self):

        # отправляем брокером запрос
        self.broker_be.worker_request(
            address=self.worker.options['identity'],
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            client_id=self.client.options['identity'],
            content=self.signal.repr.json,
        )

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # сообщение, которое должен получить воркер
        source_msg = containers.SMPWorkerMessage(
            message_type=defaults.SMP_TYPE_REQUEST,
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            client_id=self.client.options['identity'],
            content=self.signal.repr.json,
        )

        # получаем сообщение протокола при помощи парсера
        target_msg = self.worker.worker_recv()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # транспортные представления совпадают
        self.assertEqual(source_msg.repr.transport, target_msg.repr.transport)

    def test_worker_partial(self):

        # отправляем брокером запрос
        self.broker_be.worker_partial(
            address=self.worker.options['identity'],
            transaction_id=self.transaction_id,
            code=defaults.SMP_CODE_OK,
            client_id=self.client.options['identity'],
            content_type=defaults.SMP_CONTENT_SIGNAL,
            content=self.signal.repr.json,
        )

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # сообщение, которое должен получить воркер
        source_msg = containers.SMPWorkerMessage(
            message_type=defaults.SMP_TYPE_PARTIAL,
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            code=defaults.SMP_CODE_OK,
            client_id=self.client.options['identity'],
            content=self.signal.repr.json,
        )

        # получаем сообщение протокола при помощи парсера
        target_msg = self.worker.worker_recv()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # транспортные представления совпадают
        self.assertEqual(source_msg.repr.transport, target_msg.repr.transport)

    def test_worker_final(self):

        # отправляем брокером запрос
        self.broker_be.worker_final(
            address=self.worker.options['identity'],
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            code=defaults.SMP_CODE_OK,
            client_id=self.client.options['identity'],
            content=self.signal.repr.json,
        )

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # сообщение, которое должен получить воркер
        source_msg = containers.SMPWorkerMessage(
            message_type=defaults.SMP_TYPE_FINAL,
            transaction_id=self.transaction_id,
            content_type=defaults.SMP_CONTENT_SIGNAL,
            client_id=self.client.options['identity'],
            code=defaults.SMP_CODE_OK,
            content=self.signal.repr.json,
        )

        # получаем сообщение протокола при помощи парсера
        target_msg = self.worker.worker_recv()

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # транспортные представления совпадают
        self.assertEqual(source_msg.repr.transport, target_msg.repr.transport)
