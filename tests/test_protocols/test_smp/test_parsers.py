# -*- coding: utf-8 -*-
import copy

from symphony.core.generators import GenericKeyGenerator
from symphony.ci.containers import GenericSignal
from symphony.protocols.errors import ProtocolViolationError
from symphony.protocols.smp.parsers import SMPParserBase
from symphony.protocols.smp import defaults as smp_defaults
from symphony.tests.base import BaseTest


class TestSMPParserBase(BaseTest):

    def setUp(self):
        super(TestSMPParserBase, self).setUp()

        self.signal = GenericSignal(name='run')

        self.parser = SMPParserBase()
        self.sender = str(GenericKeyGenerator())
        self.client = str(GenericKeyGenerator())
        self.transaction_id = str(GenericKeyGenerator())

        # Client/REQUEST
        self.msg_cl_req = [
            smp_defaults.SMP_SUB_CLIENT,
            smp_defaults.SMP_TYPE_REQUEST,
            self.transaction_id,
            smp_defaults.SMP_CONTENT_SIGNAL,
            self.signal.repr.json
        ]
        self.msg_cl_req_sender = [self.sender] + self.msg_cl_req

        # Client/PARTIAL
        self.msg_cl_part = [
            smp_defaults.SMP_SUB_CLIENT,
            smp_defaults.SMP_TYPE_PARTIAL,
            self.transaction_id,
            smp_defaults.SMP_CONTENT_SIGNAL,
            smp_defaults.SMP_CODE_OK,
            self.signal.repr.json
        ]
        self.msg_cl_part_sender = [self.sender] + self.msg_cl_part

        # Client/FINAL
        self.msg_cl_final = copy.deepcopy(self.msg_cl_part)
        self.msg_cl_final[1] = smp_defaults.SMP_TYPE_FINAL
        self.msg_cl_final_sender = [self.sender] + self.msg_cl_final

        # Client/*
        self.client_messages = [
            self.msg_cl_req, self.msg_cl_req_sender,
            self.msg_cl_part, self.msg_cl_part_sender,
            self.msg_cl_final, self.msg_cl_final_sender,
            ]

        # Worker/REQUEST
        self.msg_wr_req = [
            smp_defaults.SMP_SUB_WORKER,
            smp_defaults.SMP_TYPE_REQUEST,
            self.transaction_id,
            smp_defaults.SMP_CONTENT_SIGNAL,
            self.client,
            self.signal.repr.json
        ]
        self.msg_wr_req_sender = [self.sender] + self.msg_wr_req

        # Worker/PARTIAL
        self.msg_wr_part = [
            smp_defaults.SMP_SUB_WORKER,
            smp_defaults.SMP_TYPE_PARTIAL,
            self.transaction_id,
            smp_defaults.SMP_CONTENT_SIGNAL,
            smp_defaults.SMP_CODE_OK,
            self.client,
            self.signal.repr.json
        ]
        self.msg_wr_part_sender = [self.sender] + self.msg_wr_part

        # Worker/FINAL
        self.msg_wr_final = copy.deepcopy(self.msg_wr_part)
        self.msg_wr_final[1] = smp_defaults.SMP_TYPE_FINAL
        self.msg_wr_final_sender = [self.sender] + self.msg_wr_final

        # Worker/*
        self.worker_messages = [
            self.msg_wr_req, self.msg_wr_req_sender,
            self.msg_wr_part, self.msg_wr_part_sender,
            self.msg_wr_final, self.msg_wr_final_sender
            ]

        # LifeCycle/INIT
        self.msg_lc_init = [
            smp_defaults.SMP_SUB_LIFECYCLE,
            smp_defaults.SMP_TYPE_LC_INIT,
        ]
        self.msg_lc_init_sender = [self.sender] + self.msg_lc_init

        # LifeCycle/HEARTBEAT
        self.msg_lc_heartbeat = [
            smp_defaults.SMP_SUB_LIFECYCLE,
            smp_defaults.SMP_TYPE_LC_HEARTBEAT,
        ]
        self.msg_lc_heartbeat_sender = [self.sender] + self.msg_lc_heartbeat

        # LifeCycle/SHUTDOWN
        self.msg_lc_shutdown = [
            smp_defaults.SMP_SUB_LIFECYCLE,
            smp_defaults.SMP_TYPE_LC_SHUTDOWN,
        ]
        self.msg_lc_shutdown_sender = [self.sender] + self.msg_lc_shutdown

        # LifeCycle/*
        self.lifecycle_messages = [
            self.msg_lc_init, self.msg_lc_init_sender,
            self.msg_lc_heartbeat, self.msg_lc_heartbeat_sender,
            self.msg_lc_shutdown, self.msg_lc_shutdown_sender,
            ]

        # */*
        self.worker_client_messages = self.client_messages + self.worker_messages
        self.all_messages = self.worker_client_messages + self.lifecycle_messages

    # Test Methods
    def validateClientMessage(self, msg):
        # верный субпротокол
        self.assertEqual(msg.sub, smp_defaults.SMP_SUB_CLIENT)

        # верный тип сообщения
        self.assertIn(msg.message_type, smp_defaults.SMP_TYPES_COMMON)

        # верный тип transaction_id
        self.assertEqual(msg.transaction_id, self.transaction_id)

        # верный content_type
        self.assertEqual(msg.content_type, smp_defaults.SMP_CONTENT_SIGNAL)

        # если тип сообщения PARTIAL или FINAL, проверяем код
        if msg.message_type != smp_defaults.SMP_TYPE_REQUEST:
            self.assertEqual(msg.code, smp_defaults.SMP_CODE_OK)

        # если REQUEST, код должен быть None
        else:
            self.assertIsNone(msg.code)

        # верный контент
        self.assertEqual(msg.content, self.signal.repr.json)

    def validateLifecycleMessage(self, msg):
        # верный субпротокол
        self.assertEqual(msg.sub, smp_defaults.SMP_SUB_LIFECYCLE)

        # верный тип сообщения
        self.assertIn(msg.message_type, smp_defaults.SMP_TYPES_LIFECYCLE)

    def validateWorkerMessage(self, msg):
        # верный субпротокол
        self.assertEqual(msg.sub, smp_defaults.SMP_SUB_WORKER)

        # верный тип сообщения
        self.assertIn(msg.message_type, smp_defaults.SMP_TYPES_COMMON)

        # верный тип transaction_id
        self.assertEqual(msg.transaction_id, self.transaction_id)

        # верный content_type
        self.assertEqual(msg.content_type, smp_defaults.SMP_CONTENT_SIGNAL)

        # если тип сообщения PARTIAL или FINAL, проверяем код
        if msg.message_type in [smp_defaults.SMP_TYPE_PARTIAL,
                                smp_defaults.SMP_TYPE_FINAL]:
            self.assertEqual(msg.code, smp_defaults.SMP_CODE_OK)

        # если REQUEST, код должен быть None
        else:
            self.assertIsNone(msg.code)

        # верный client_id
        self.assertEqual(msg.client_id, self.client)

        # верный контент
        self.assertEqual(msg.content, self.signal.repr.json)

    def test_validate_protocol_message(self):
        for message in self.all_messages:

            address = self.parser.get_address(message)
            sub = self.parser.get_sub(message)
            message_type = self.parser.get_message_type(message)
            start_index = 0 if not address else 1
            sub_hr = smp_defaults.SMP_SUB_HR[sub]
            message_type_hr = smp_defaults.SMP_TYPES_HR[message_type]

            self.messages.info(
                'testing message: %s/%s, address: %s' % (
                    sub_hr, message_type_hr,
                    'yes' if address else 'no',
                )
            )
            self.messages.info(message)

            # address
            if address:
                with self.assertRaisesRegex(
                        ProtocolViolationError,
                        'initial signature: address exceeds allowed length: expected \d+, actual 300'):
                    msg = copy.deepcopy(message)
                    msg[0] = self.randomString(300)
                    self.parser.get_client_message(msg)

            # 0: sub
            with self.assertRaisesRegex(ProtocolViolationError, 'no valid sub-protocol definition'):
                msg = copy.deepcopy(message)
                msg[start_index] = 'WRONG_PROTOCOL'
                self.parser.validate_protocol_message(msg)

            # 1: message type: not an integer
            with self.assertRaisesRegex(
                    ProtocolViolationError,
                    'initial signature: incorrect data type for message_type: a, expected int'):
                msg = copy.deepcopy(message)
                msg[start_index + 1] = 'a'
                self.parser.validate_protocol_message(msg)

            # 1: message type: not a valid type
            with self.assertRaisesRegex(
                    ProtocolViolationError,
                    'initial signature: message_type out of expected range: 10'):
                msg = copy.deepcopy(message)
                msg[start_index + 1] = '10'
                self.parser.validate_protocol_message(msg)

            # Длина
            with self.assertRaisesRegex(
                    ProtocolViolationError,
                    '%s sub-protocol: %s message exceeds allowed length: '
                    'expected \d+, actual \d+' % (sub_hr, message_type_hr)):
                msg = copy.deepcopy(message)
                msg += ['10', 11]
                self.parser.validate_protocol_message(msg)

            # Client/Worker
            if sub != smp_defaults.SMP_SUB_LIFECYCLE:
                # 2: transaction_id: too long
                with self.assertRaisesRegex(
                        ProtocolViolationError,
                        'transaction_id exceeds allowed length: expected \d+, actual 300'):
                    msg = copy.deepcopy(message)
                    msg[start_index + 2] = self.randomString(300)
                    self.parser.validate_protocol_message(msg)

                # 3: content type
                with self.assertRaisesRegex(
                        ProtocolViolationError, 'incorrect data type for content_type: a, expected int'):
                    msg = copy.deepcopy(message)
                    msg[start_index + 3] = 'a'
                    self.parser.validate_protocol_message(msg)

    def test_get_client_message(self):

        for message in self.client_messages:

            address = self.parser.get_address(message)
            sub = self.parser.get_sub(message)
            message_type = self.parser.get_message_type(message)
            start_index = 0 if not address else 1

            self.messages.info(
                'testing message: %s/%s, address: %s' % (
                    smp_defaults.SMP_SUB_HR[sub],
                    smp_defaults.SMP_TYPES_HR[message_type],
                    'yes' if address else 'no',
                )
            )
            self.messages.info(message)

            # нормальное сообщение
            msg = copy.deepcopy(message)
            self.validateClientMessage(self.parser.get_client_message(msg))

            with self.assertRaisesRegex(
                    ProtocolViolationError,
                    'Client sub-protocol: message_type out of expected range: INIT'):
                msg = copy.deepcopy(message)
                msg[start_index + 1] = smp_defaults.SMP_TYPE_LC_INIT
                if message_type == smp_defaults.SMP_TYPE_REQUEST:
                    # SMP_TYPE_LC_INIT сообщение будет определяться как PARTIAL/FINAL, добавляем кадр
                    msg += ['f']
                self.parser.get_client_message(msg)

    def test_get_lifecycle_message(self):

        for message in self.lifecycle_messages:
            address = self.parser.get_address(message)
            sub = self.parser.get_sub(message)
            message_type = self.parser.get_message_type(message)
            start_index = 0 if not address else 1

            self.messages.info(
                'testing message: %s/%s, address: %s' % (
                    smp_defaults.SMP_SUB_HR[sub],
                    smp_defaults.SMP_TYPES_HR[message_type],
                    'yes' if address else 'no',
                )
            )
            self.messages.info(message)

            # сообщение протокола
            msg = self.parser.get_lifecycle_message(message)
            self.validateLifecycleMessage(msg)

            # 0: sub: пробуем обмануть парсер -- подменяем суб-протокол и добавляем лишние фреймы
            with self.assertRaisesRegex(
                    ProtocolViolationError,
                    'LifeCycle sub-protocol: sub mismatch: expected LifeCycle, actual Worker'):
                msg = copy.deepcopy(message)
                msg[start_index] = smp_defaults.SMP_SUB_WORKER
                msg += [1, 2, 3, 4, 5]
                self.parser.get_lifecycle_message(msg)

            # 1: message_type
            with self.assertRaisesRegex(
                    ProtocolViolationError,
                    'LifeCycle sub-protocol: message_type out of expected range: REQUEST'):
                msg = copy.deepcopy(message)
                msg[start_index + 1] = smp_defaults.SMP_TYPE_REQUEST
                self.parser.get_lifecycle_message(msg)

    def test_get_worker_message(self):

        for message in self.worker_messages:

            address = self.parser.get_address(message)
            sub = self.parser.get_sub(message)
            message_type = self.parser.get_message_type(message)
            start_index = 0 if not address else 1

            self.messages.info(
                'testing message: %s/%s, address: %s' % (
                    smp_defaults.SMP_SUB_HR[sub],
                    smp_defaults.SMP_TYPES_HR[message_type],
                    'yes' if address else 'no',
                )
            )
            self.messages.info(message)

            # нормальное сообщение
            msg = copy.deepcopy(message)
            self.validateWorkerMessage(self.parser.get_worker_message(msg))

            # 4/5: client_id
            with self.assertRaisesRegex(
                    ProtocolViolationError,
                    'Worker sub-protocol: client_id exceeds allowed length: expected \d+, actual 300'):
                msg = copy.deepcopy(message)
                if message_type == smp_defaults.SMP_TYPE_REQUEST:
                    msg[start_index + 4] = self.randomString(300)
                else:
                    msg[start_index + 5] = self.randomString(300)
                self.parser.get_worker_message(msg)

    def test_get_message(self):

        for message in self.all_messages:
            address = self.parser.get_address(message)
            sub = self.parser.get_sub(message)
            message_type = self.parser.get_message_type(message)
            start_index = 0 if not address else 1

            self.messages.info(
                'testing message: %s/%s, address: %s' % (
                    smp_defaults.SMP_SUB_HR[sub],
                    smp_defaults.SMP_TYPES_HR[message_type],
                    'yes' if address else 'no',
                )
            )
            self.messages.info(message)

            msg = self.parser.get_message(message)

            if msg.sub == smp_defaults.SMP_SUB_CLIENT:
                self.validateClientMessage(msg)
            elif msg.sub == smp_defaults.SMP_SUB_LIFECYCLE:
                self.validateLifecycleMessage(msg)
            elif msg.sub == smp_defaults.SMP_SUB_WORKER:
                self.validateWorkerMessage(msg)
