# -*- coding: utf-8 -*-
from unittest import skip

from symphony.core.datatypes import DotDict
from symphony.routers.components import Router
from symphony.protocols.smp.handlers import (
    SMPMessageHandler, SMPSignalHandler, SMPWorkerHandler, SMPBrokerHandler
)
from symphony.tests.infra import InfraTest
from symphony.tests.transport import ZMQTransportTest

from symphony.protocols.smp.defaults import SMP_CONTENT_NONE


class TestSMPMessageHandler(InfraTest):

    def setUp(self):
        super(TestSMPMessageHandler, self).setUp()

        class Handler(SMPMessageHandler):

            def prepare(self):
                super(Handler, self).prepare()

                if self.runtime['kwargs']['prepare'] == 'error':
                    self.messages.error('error in prepare')

                elif self.runtime['kwargs']['prepare'] == 'warning':
                    self.messages.warning('warning in prepare')
                else:
                    self.messages.info('prepare ok')

            def handle(self):
                super(Handler, self).handle()

                if self.runtime['kwargs']['handle'] == 'error':
                    self.messages.error('error in handle')

                elif self.runtime['kwargs']['handle'] == 'warning':
                    self.messages.warning('warning in handle')
                else:
                   self.ok_response('handle ok')

        self.infra.join(
            Router(
                name='smp_router',
                handlers=[
                    Handler(name='default_handler', content_type='default'),
                    Handler(name='none_handler', content_type=SMP_CONTENT_NONE),
                ]
            )
        )

    def getSignal(self, prepare=None, handle=None):
        return DotDict({
            'name': 'test',
            'args': [],
            'kwargs': {
                'prepare': prepare,
                'handle': handle,
            },
        })

    def test_init(self):
        # все обработчики зарегистрировали себя по типу контента
        self.assertIn('default', self.smp_router.mapping)
        self.assertIn(SMP_CONTENT_NONE, self.smp_router.mapping)


class TestSMPSignalHandler(InfraTest):

    def setUp(self):
        super(TestSMPSignalHandler, self).setUp()

        class Handler(SMPSignalHandler):
            name = 'test'
            kwargs = {
                'prepare': None,
                'handle': None,
            }

            def prepare(self):
                super(Handler, self).prepare()

                if self.runtime['kwargs']['prepare'] == 'error':
                    self.messages.error('error in prepare')

                elif self.runtime['kwargs']['prepare'] == 'warning':
                    self.messages.warning('warning in prepare')
                else:
                    self.messages.info('prepare ok')

            def handle(self):
                super(Handler, self).handle()

                if self.runtime['kwargs']['handle'] == 'error':
                    self.messages.error('error in handle')

                elif self.runtime['kwargs']['handle'] == 'warning':
                    self.messages.warning('warning in handle')
                else:
                   self.ok_response('handle ok')

            def partial_response(self, content=None, code=None, content_type=SMP_CONTENT_NONE):
                self.messages.info('PARTIAL on wire: [%s, %s, %s]' % (content, code, content_type))

            def final_response(self, content=None, code=None, content_type=SMP_CONTENT_NONE):
                self.messages.info('FINAL on wire: [%s, %s, %s]' % (content, code, content_type))

        self.infra.join(Router(name='ci', handlers=[Handler]))

    def getSignal(self, prepare=None, handle=None):
        return DotDict({
            'name': 'test',
            'args': [],
            'kwargs': {
                'prepare': prepare,
                'handle': handle,
            },
        })

    def test_normal(self):
        self.ci.handle(self.getSignal())

        # ошибок нет
        self.assertIsNone(self.core.mbus.errors)

        # сообщение метода prepare
        self.assertInfoMessage('prepare ok')

        # контроль передан методу handle
        self.assertDebugMessage('control obtained')

        # ответ метода handle
        self.assertInfoMessage('[200] OK: handle ok')

        # сообщние на проводе
        self.assertInfoMessage('FINAL on wire: [handle ok, 200, 2]')

    def test_prepare_error(self):
        self.ci.handle(self.getSignal(prepare='error'))

        # сообщение метода prepare
        self.assertErrorMessage('error in prepare')

        # контроль методу handle не передавался
        self.assertNoDebugMessage('control obtained')

        # ответ обертки
        self.assertErrorMessage('[400] BAD REQUEST: error in prepare')

        # сообщние на проводе
        self.assertInfoMessage('FINAL on wire: [error in prepare, 400, 2]')

    def test_handle_error(self):
        self.ci.handle(self.getSignal(handle='error'))

        # сообщение метода prepare
        self.assertInfoMessage('prepare ok')

        # контроль передан методу handle
        self.assertDebugMessage('control obtained')

        # сообщение метода handle
        self.assertErrorMessage('error in handle')

        # ответ обертки
        self.assertErrorMessage('[500] ERROR: error in handle')

        # сообщние на проводе
        self.assertInfoMessage('FINAL on wire: [error in handle, 500, 2]')


class TestSMPWorkerHandler(ZMQTransportTest):
    sockets = {
        'ci_socket': {
            'type': 'DEALER',
            'connect': 'inproc://broker',
            'option': {
                'identity': 'worker'
            },
        },
    }

    def setUp(self):
        super(TestSMPWorkerHandler, self).setUp()

        self.infra.join(Router(name='ci', handlers=[SMPWorkerHandler(name='test')]))

    def test_get_ci_socket(self):
        handler = self.ci.test
        ci_socket = self.ci_socket

        # сокет определился правильно
        self.assertEqual(ci_socket, handler.get_ci_socket())


class TestSMPBrokerHandler(ZMQTransportTest):
    sockets = {
        'ci_fe': {
            'type': 'ROUTER',
            'bind': 'inproc://broker',
            'option': {
                'identity': 'broker'
            },
        },

    }

    def setUp(self):
        super(TestSMPBrokerHandler, self).setUp()
        self.infra.join(Router(name='ci', handlers=[SMPBrokerHandler(name='test')]))

    def test_get_ci_socket(self):
        handler = self.ci.test
        ci_socket = self.ci_fe

        # сокет определился правильно
        self.assertEqual(ci_socket, handler.get_ci_socket())
