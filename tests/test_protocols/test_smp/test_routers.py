# -*- coding: utf-8 -*-
from symphony.routers.handlers import BaseHandler
from symphony.protocols.smp.routers import SMPMessageRouter
from symphony.protocols.smp import defaults as smp_defaults
from symphony.protocols.smp.containers import SMPClientMessage
from symphony.tests.infra import InfraTest


class TestSMPContentRouter(InfraTest):
    """ """

    def setUp(self):
        super(TestSMPContentRouter, self).setUp()

        class DefaultHandler(BaseHandler):
            name = 'smp_handler_default'
            aliases = ['default']

            def handle(self):
                self.messages.info('handling')

        class RawHandler(BaseHandler):
            name = 'smp_handler_raw'
            aliases = [smp_defaults.SMP_CONTENT_RAW]

            def handle(self):
                self.messages.info('handling')

        self.router = SMPMessageRouter(handlers=[DefaultHandler, RawHandler]).init(
            core=self.core, config=None, registry={}, mount_point=self
        )

        self.message_raw = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id='uuid001',
            content_type=smp_defaults.SMP_CONTENT_RAW,
            content='raw'
        )
        self.message_string = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id='uuid002',
            content_type=smp_defaults.SMP_CONTENT_STRING,
            content='string'
        )

        self.message_stream = SMPClientMessage(
            message_type=smp_defaults.SMP_TYPE_PARTIAL,
            transaction_id='uuid002',
            content_type=smp_defaults.SMP_CONTENT_STREAM,
            content='stream'
        )

    def test_handle(self):
        # прямое соответствие
        self.router.handle(self.message_raw),
        self.assertIsNone(self.router.messages.errors)

        self.assertInfoMessage('smp_handler_raw: handling')

        self.core.mbus.flush()
        self.router.flush()

        # обработчик по умолчанию
        self.router.handle(self.message_string),
        self.assertIsNone(self.router.messages.errors)

        self.assertInfoMessage('smp_handler_default: handling')

        # обработчик по умолчанию не найден
        self.core.mbus.flush()
        self.router.flush()
        del self.router.mapping['default']

        self.router.handle(self.message_string),
        self.assertErrorMessage('handler not found: 2')
