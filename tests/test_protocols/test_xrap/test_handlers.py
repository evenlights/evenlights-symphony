# -*- coding: utf-8 -*-
from unittest import skip

from zebra import ZebClient

from symphony.tests.transport import ZMQTransportTest


class TestWorkerBase(ZMQTransportTest):
    """ https://travis-ci.org/zeromq/zebra
    При компиляции висит тест брокера, make check не работает


    Проблема: сейчас у zeb_handler дохдый метод add_offer::
        https://github.com/zeromq/zebra/commit/a887fc010cb0ea340f4844aeddfe648d6e71fdf4

    """
    sockets = {
    }

    def test_main(self):

        client = ZebClient()
        client.test(True)
