# -*- coding: utf-8 -*-
from unittest import skip

import zmq

from symphony.ci.containers import GenericSignal
from symphony.adapters.sockets import ZMQSocketAdapter
from symphony.tests.transport import ZMQTransportTest


class TestSocketAdapterBase(ZMQTransportTest):
    INTERFACE_ADDR = 'inproc://interface'
    CYCLE_LENGTH = 100
    sockets = {
        'client': {
            'type': 'DEALER',
            'connect': INTERFACE_ADDR,
        }
    }

    def setUp(self):
        super(TestSocketAdapterBase, self).setUp()
        self.default_config = {
            'type': 'ROUTER',
            'bind': self.INTERFACE_ADDR,
            'option': {
                'identity': 'test-socket',
            }
        }
        self.client_signal = GenericSignal(name='a')

    # @skip('')
    def test_config_load(self):
        socket = ZMQSocketAdapter(name='socket')

        # нормальный конфиг
        socket.config_load(self.default_config)
        self.assertEqual(self.INTERFACE_ADDR, socket.runtime['config']['target'])
        self.assertEqual(zmq.ROUTER, socket.runtime['config']['type'])
        self.assertEqual('bind', socket.runtime['config']['method'])
        socket.runtime.flush()

        # неверный тип
        self.assertRaises(ValueError, socket.config_load, {'type': 'ROUTER!!!'})

        # bind + connect
        self.assertRaises(ValueError, socket.config_load, {'bind': 'a', 'connect': 'b'})

        # неверная опция
        self.assertRaises(ValueError, socket.config_load, {'option': {'iddentity1': 'no_id'}})

    # @skip('')
    def test_config_resolve(self):
        # базовые настройки из конфига + дефолтные значения
        socket = ZMQSocketAdapter(name='socket')
        socket.config_load({
            'type': 'ROUTER',
            'bind': self.INTERFACE_ADDR,
        })
        socket.config_resolve()

        self.assertEqual(self.INTERFACE_ADDR, socket.target)
        self.assertEqual(zmq.ROUTER, socket.type)
        self.assertEqual('bind', socket.method)
        self.assertEqual(0.2, socket.options['reconnect_delay'])

        # оверрайд дефолтного значения конфигом
        socket.runtime.flush()
        socket.config_load({
            'type': 'ROUTER',
            'bind': self.INTERFACE_ADDR,
            'option': {
                'reconnect_delay': 1
            }
        })
        socket.config_resolve()
        self.assertEqual(1, socket.options['reconnect_delay'])

        # оверрайд дефолтного значения параметром инициализации
        socket = ZMQSocketAdapter(name='socket', reconnect_delay=2)
        socket.config_load({
            'type': 'ROUTER',
            'bind': self.INTERFACE_ADDR,
            'option': {
                'reconnect_delay': 1
            }
        })
        socket.config_resolve()
        self.assertEqual(2, socket.options['reconnect_delay'])

        # оверрайд базового значения параметром инициализации
        socket = ZMQSocketAdapter(name='socket', target='aaa')
        socket.config_load({
            'type': 'ROUTER',
            'bind': self.INTERFACE_ADDR,
            'option': {
                'reconnect_delay': 1
            }
        })
        socket.config_resolve()
        self.assertEqual('aaa', socket.target)

    # @skip('')
    def test_apply_options(self):

        socket = ZMQSocketAdapter(name='socket')
        socket.config_load({
            'type': 'ROUTER',
            'bind': self.INTERFACE_ADDR,
            'option': {
                'identity': 'test-socket',
                'sndhwm': 10,
            }
        })
        socket.config_resolve()
        socket.context = self.context
        socket.socket = self.context.socket(zmq.ROUTER)
        socket.apply_options()
        self.assertEqual('test-socket', socket.identity)
        self.assertEqual(10, socket.socket.sndhwm)
        socket.socket.close()

    # Component
    # @skip('')
    def test_init(self):
        infra_registry = {}
        socket_registry = {}
        socket = ZMQSocketAdapter(name='socket').init(
            core=self.core, config=self.default_config, registry=infra_registry,
            mount_point=self,
            context=self.context, poller=self.poller,
            socket_registry=socket_registry
        )

        self.assertEqual(socket.context, self.context)
        self.assertEqual(socket.poller, self.poller)
        self.assertIn(socket.name, socket_registry)
        self.assertIn(socket.name, infra_registry)

    # @skip('')
    def test_activate(self):
        self.socketsConnect()

        # обычное подключение
        socket = ZMQSocketAdapter(name='socket').init(
            core=self.core, config=self.default_config, registry={},
            mount_point=self,
            context=self.context, poller=self.poller
        )
        socket.connect()

        self.client.socket.send_multipart([b'hi there!'])
        msg = socket.socket.recv_multipart()
        self.assertEqual(msg[1], b'hi there!')
        self.assertIn(socket.socket, self.poller)

        socket.disconnect()
        socket.is_connected = False

        # список в target
        socket.target = ['inproc://a', 'inproc://b']
        socket.connect()
        self.assertDebugMessage("attaching to target: ['inproc://a', 'inproc://b']")

        socket.disconnect()
        socket.is_connected = False

        # список в subscribe
        socket.target = self.INTERFACE_ADDR
        socket.method = 'connect'
        socket.type = zmq.SUB
        socket.options['subscribe'] = ['aaa', 'bbb']
        socket.connect()
        self.assertDebugMessage("applying option: subscribe=['aaa', 'bbb']")

        socket.disconnect()

    # @skip('')
    def test_deactivate(self):
        socket = ZMQSocketAdapter(name='socket').init(
            core=self.core, config=self.default_config, registry={},
            mount_point=self, context=self.context
        )
        socket.connect()
        socket.disconnect()

        self.assertEqual([], self.poller.sockets)
        self.assertIsNone(socket.socket)
        self.assertFalse(socket.is_connected)

    # @skip('')
    def test_reactivate(self):
        socket = ZMQSocketAdapter(name='socket').init(
            core=self.core, config=self.default_config, registry={},
            mount_point=self, context=self.context
        )
        socket.connect()
        self.core.mbus.flush()
        socket.messages.flush()
        socket.reconnect()

        self.assertInfoMessage('reconnecting in 0.20 seconds')
        self.assertDebugMessage("attaching to target: 'inproc://interface'")
        self.assertDebugMessage('applying option: reconnect_delay=0.2')
        self.assertDebugMessage('applying option: identity=test-socket')

        socket.socket.close()

    # Message
    # @skip('')
    def test_encode_multipart(self):
        socket = ZMQSocketAdapter(name='socket').init(
            core=self.core, config=self.default_config, registry={},
            mount_point=self, context=self.context, poller=self.poller
        )

        # все значения готовы к передаче по проводам
        for f in socket.encode_multipart([1, 'привет']):
            self.assertIsInstance(f, bytes)

    # @skip('')
    def test_decode_multipart(self):
        self.socketsConnect()

        socket = ZMQSocketAdapter(name='socket').init(
            core=self.core, config=self.default_config, registry={},
            mount_point=self, context=self.context, poller=self.poller
        )
        socket.connect()
        # zmq кодирует identity клиента по умолчанию в ISO-8859-2
        self.client.send_multipart(['HI'])
        for f in socket.decode_multipart(socket.socket.recv_multipart()):
            self.assertIsInstance(f, str)

        socket.disconnect()

    # Adapter
    # @skip('')
    def test_write(self):
        self.socketsConnect()

        socket = ZMQSocketAdapter(name='socket').init(
            core=self.core, config=self.default_config, registry={},
            mount_point=self, context=self.context, poller=self.poller
        )
        socket.connect()

        socket.write([self.client.identity, 'привет', 1])

        # клиент получил сообщение
        self.assertEqual(1, self.client.socket.poll(1000))

        socket.disconnect()

    # @skip('')
    def test_read(self):
        self.socketsConnect()
        socket = ZMQSocketAdapter(name='socket').init(
            core=self.core, config=self.default_config, registry={},
            mount_point=self, context=self.context, poller=self.poller
        )
        socket.connect()

        self.client.send_multipart(['HI'])

        msg = socket.read()
        self.assertEqual('HI', msg[1])

        socket.disconnect()
