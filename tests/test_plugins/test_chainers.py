# -*- coding: utf-8 -*-
from symphony.tests.infra import InfraTestCase
from symphony.core.units import GenericInfraUnit
from symphony.plugins.chainers import PluginChain, PluginChainElement
from symphony.plugins.chainers import BasePluginChainer


class TestPluginChain(InfraTestCase):

    def setUp(self):
        super(TestPluginChain, self).setUp()

        self.element = PluginChainElement(name='test_plugin', presets=['default'])
        self.chain = PluginChain([self.element])

    def tearDown(self):
        super(TestPluginChain, self).tearDown()

    def test_init(self):
        self.assertIn('plugin_chain', self.chain.repr.transport)

        self.assertIn('presets', self.chain[0].repr.transport)
        self.assertTrue(hasattr(self.chain[0], 'presets'))
        self.assertTrue(hasattr(self.chain[0], 'name'))
        self.assertTrue(hasattr(self.chain[0], 'custom'))

        self.assertEqual('test_plugin', self.chain[0].name)
        self.assertEqual('default', self.chain[0].presets[0])


class TestBasePluginChainer(InfraTestCase):
    """ Для теста чейнера необходимо:
     * общее ядро, поскольку он производит валидацию JSON-строки при загрузке цепочки
     * логгер для визуального контроля
     * юнит, чтобы модуль мог отправлять сообщения
     * тестовая цепочка с настройками
    """
    def setUp(self):
        super(TestBasePluginChainer, self).setUp()

        self.element = PluginChainElement(name='test_plugin', presets=['default'])
        self.chain = PluginChain([self.element])
        self.unit = GenericInfraUnit(core=self.core, sys_name='test_unit')
        self.chainer = BasePluginChainer()
        self.chainer.mount(self.unit)

    def tearDown(self):
        super(TestBasePluginChainer, self).tearDown()

    def test_init(self):
        self.assertTrue(hasattr(self.unit, 'plugin_chainer'))

    def test_load_chain(self):
        self.unit.plugin_chainer.load_chain(self.chain.repr.json)
