# -*- coding: utf-8 -*-
from symphony.tests.infra import InfraTestCase
from evenlights_sonar.plugins.filters import RangePassFilter, RangeStopFilter, RegexStopFilter
from evenlights_sonar.plugins.filters import HTMLVisibleFilter


class TestRangePassFilter(InfraTestCase):

    def setUp(self):
        super(TestRangePassFilter, self).setUp()

    def tearDown(self):
        super(TestRangePassFilter, self).tearDown()
        self.filter.flush()

    def test_init(self):
        # категория и имя зарегистрирована
        self.assertTrue(hasattr(self.core.shared.plugins, self.filter.category))
        # имя в нужной категории
        self.assertTrue(hasattr(getattr(self.core.shared.plugins, self.filter.category),
                                self.filter.sys_name))
        # ресурс-менеджер видит плагин по имени в категории
        self.assertIsNotNone(self.core.shared.plugins.by_name(self.filter.sys_name,
                                                              category=self.filter.category))
        # и просто по имени
        self.assertIsNotNone(self.core.shared.plugins.by_name(self.filter.sys_name))

    def test_default_preset(self):
        print(self.filter.run())
        self.assertEqual(self.filter.target, 'русский текст и english text 123 W')

    def test_default_explicit(self):
        self.filter.runtime.presets = ['default']
        print(self.filter.process())
        print(self.filter.messages.internal)
        self.assertEqual(self.filter.target, 'русский текст и english text 123 W')

    def test_single_preset(self):
        self.filter.runtime.presets = ['digits']
        print(self.filter.process())
        self.assertEqual(self.filter.target, '123')

    def test_multiple_presets(self):
        self.filter.runtime.presets = ['space', 'digits', 'cyrillic', 'latin']
        print(self.filter.process())
        self.assertEqual(self.filter.target, 'русский текст и english text 123 W')

    def test_custom_preset(self):
        # фильтр - только латиница
        self.filter.runtime.presets = ['custom_filter1']
        self.filter.runtime.custom = '{"presets":[{"name": "custom_filter1", "data": [[65, 90], [97, 122]]}]}'
        print(self.filter.process())
        self.assertEqual(self.filter.target, 'englishtextW')

    def test_default_plus_custom(self):
        # кастомный фильтр разрешает пунктуацию
        self.filter.runtime.custom = '{"presets":[{"name": "custom_filter1", "data": [[33, 47], [58, 63], [123, 126]]}]}'
        print(self.filter.process())

        # кастомный фильтр используется без явного указания в дополнение к существующим:
        self.assertEqual(self.filter.target, 'русский текст и english text 123 .,;*&*(&W#(*#$')

    def test_default_plus_custom_explicit(self):
        # кастомный фильтр разрешает пунктуацию
        self.filter.runtime.presets = ['default', 'custom_filter1']
        self.filter.runtime.custom = '{"presets":[{"name": "custom_filter1", "data": [[33, 47], [58, 63], [123, 126]]}]}'
        print(self.filter.process())
        self.assertEqual(self.filter.target, 'русский текст и english text 123 .,;*&*(&W#(*#$')

    # validation
    def test_validate_preset_name(self):
        self.filter.runtime.presets = ['no_space', ]

        self.filter.process()
        self.assertEqual(len(self.filter.messages.internal.errors), 1)

    def test_validate_wrong_custom_name(self):
        self.filter.runtime.presets = ['no_space', ]
        self.filter.runtime.custom = '{"presets":[{"name": "custom_filter1", "data": [[65, 90], [97, 122]]}]}'

        self.filter.process()
        self.assertEqual(len(self.filter.messages.internal.errors), 1)


class TestRangeStopFilter(unittest.TestCase):

    def setUp(self):
        self.core = BaseProcessCore(sys_name='test_core')
        self.filter = RangeStopFilter(core=self.core)
        self.sample_text = 'русский текст и english text 123 .,;*&^*(&^@W#(*^@#$'
        self.filter.source = self.sample_text

    def tearDown(self):
        self.filter.flush()

    def test_init(self):
        # категория и имя зарегистрирована
        self.assertTrue(hasattr(self.core.shared.plugins, self.filter.category))
        # имя в нужной категории
        self.assertTrue(hasattr(getattr(self.core.shared.plugins, self.filter.category),
                                self.filter.sys_name))
        # ресурс-менеджер видит плагин по имени в категории
        self.assertIsNotNone(self.core.shared.plugins.by_name(self.filter.sys_name,
                                                              category=self.filter.category))
        # и просто по имени
        self.assertIsNotNone(self.core.shared.plugins.by_name(self.filter.sys_name))

    def test_default_preset(self):
        print(self.filter.process())
        # вырезана пунктуация и цифры
        self.assertEqual(self.filter.target, 'русский текст и english text  @W@')


class TestRegexStopFilter(unittest.TestCase):

    def setUp(self):
        self.core = BaseProcessCore(sys_name='test_core')
        self.filter = RegexStopFilter(core=self.core)
        self.sample_text = 'русский текст и english text 123 <some>nasty</tags>'
        self.filter.source = self.sample_text

    def tearDown(self):
        self.filter.flush()

    def test_init(self):
        # категория и имя зарегистрирована
        self.assertTrue(hasattr(self.core.shared.plugins, self.filter.category))
        # имя в нужной категории
        self.assertTrue(hasattr(getattr(self.core.shared.plugins, self.filter.category),
                                self.filter.sys_name))
        # ресурс-менеджер видит плагин по имени в категории
        self.assertIsNotNone(self.core.shared.plugins.by_name(self.filter.sys_name,
                                                              category=self.filter.category))
        # и просто по имени
        self.assertIsNotNone(self.core.shared.plugins.by_name(self.filter.sys_name))

    def test_strip_tags(self):
        print(self.filter.process())
        # фильтр по умолчанию
        # содержание тэга сохранено
        self.assertEqual(self.filter.target, 'русский текст и english text 123 nasty')

    def test_remove_tags(self):
        self.filter.runtime.presets = ['remove_tags', ]
        print(self.filter.process())
        # тэг удален полностью (WARNING: пробел в конце останется!)
        self.assertEqual(self.filter.target, 'русский текст и english text 123 ')

    def test_custom_filter(self):
        self.filter.runtime.presets = ['custom_regex', ]
        # фильтр вырезает все слова 'english'
        self.filter.runtime.custom = '{"presets":[{"name": "custom_regex", "data": "english"}]}'
        print(self.filter.process())
        self.assertEqual(self.filter.target, 'русский текст и  text 123 <some>nasty</tags>')


class TestHTMLVisibleFilter(unittest.TestCase):

    def setUp(self):
        self.core = BaseProcessCore(sys_name='test_core')
        self.filter = HTMLVisibleFilter(core=self.core)
        self.sample_text = """
            <HTML>
            <head>
            <title>A Small Sample of HTML</title>
            </head>
            <body>
            <h1>The University of Illinois at Chicago </h1>
            <H2> Brought to you by <IMG SRC="http://www.uic.edu/depts/adn/icons/skyline_adn.gif" ALT="The ADN" ALIGN="middle" >lt;/
            <p>Follow an item by clicking on the underlined word. Each main category has a few key subcategories listed here. In ad
            <p> <bold>The World </bold> (<A HREF="gopher://gopher.uic.edu/11/world">complete<A> list)
            <UL>
            <LI>To see a much better HTML guide from NCSA, click
            <A HREF="http://www.ncsa.uiuc.edu/demoweb/html-primer.html">here<\A>
            <LI>lt;A HREF="http://www.uic.edu/world.html">Visit the Internet</A>
            </ul>
            </body>
            </html>
            """

        self.filter.source = self.sample_text

    def tearDown(self):
        self.filter.flush()

    def test_init(self):
        # категория и имя зарегистрирована
        self.assertTrue(hasattr(self.core.shared.plugins, self.filter.category))
        # имя в нужной категории
        self.assertTrue(hasattr(getattr(self.core.shared.plugins, self.filter.category),
                                self.filter.sys_name))
        # ресурс-менеджер видит плагин по имени в категории
        self.assertIsNotNone(self.core.shared.plugins.by_name(self.filter.sys_name,
                                                              category=self.filter.category))
        # и просто по имени
        self.assertIsNotNone(self.core.shared.plugins.by_name(self.filter.sys_name))

    def test_process(self):
        print(self.filter.process())
